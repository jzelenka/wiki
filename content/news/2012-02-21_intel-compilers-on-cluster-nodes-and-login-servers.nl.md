---
author: polman
date: 2012-02-21 12:48:00
tags:
- medewerkers
title: Intel Compilers op clusternodes en loginservers
---
De [Intel Cluster Studio voor
Linux](http://software.intel.com/en-us/articles/intel-cluster-studio-xe/)
is beschikbaar op de [clusternodes en
loginservers](/nl/howto/hardware-servers/) in `/opt/intel`. C&CZ heeft
twee licenties voor gelijktijdig gebruik hiervan aangeschaft.
