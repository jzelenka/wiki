---
author: wim
date: 2021-09-15 16:42:00
tags:
- medewerkers
- docenten
- studenten
title: Groot RU-netwerkonderhoud zaterdag 9 oktober 08:30-15:00
---
ISC netwerkbeheer [aankondiging.](https://www.ru.nl/systeem-meldingen/)

Op zaterdag 9 oktober a.s. wordt er groot RU-netwerkonderhoud
uitgevoerd. Daarbij zal een aantal onderbrekingen van maximaal 15
minuten optreden. Voor de FNWI-gebouwen zullen de onderbrekingen om
15:00 stoppen, voor de rest van de RU kan het tot 19:00 uur duren.
