---
author: polman
date: 2015-10-14 15:45:00
tags:
- medewerkers
- studenten
title: Science compute cluster will move to SLURM
---
The C&CZ managed [compute clusters](/en/howto/hardware-servers/) will be
migrated to use [SLURM](/en/howto/slurm/) as cluster software. The older
cluster software
[Torque](http://www.adaptivecomputing.com/products/open-source/torque/)
/ [MAUI](http://www.adaptivecomputing.com/products/open-source/maui/)
and [Sun/Oracle
GridEngine](http://en.wikipedia.org/wiki/Oracle_Grid_Engine) will be
phased out. Many thanks to Pim Schellart of Astrophysics for creating
[the documentation](/en/howto/slurm/).
