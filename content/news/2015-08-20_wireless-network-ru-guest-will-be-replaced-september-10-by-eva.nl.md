---
author: mkup
date: 2015-08-20 16:32:00
tags:
- medewerkers
- docenten
- studenten
title: Wireless netwerk ru-guest wordt 10 september vervangen door eVA
---
Per 10 september a.s. zal het wireless netwerk ‘ru-guest’ niet meer
beschikbaar zijn. Gasten van de Radboud Universiteit kunnen voortaan
gebruik maken van het draadloze netwerk ‘eduroam’ via ‘eVA’ (eduroam
Visitor Access). Alle medewerkers en studenten van FNWI kunnen een of
meerdere tijdelijk eVA gastaccounts aanvragen bij de C&CZ helpdesk
(helpdesk\@science.ru.nl). Bij het aanvragen van eVA accounts dient u uw
e-mailadres en uw mobiele nummer door te geven. Nadere informatie over
eVA is te vinden op [de SURF
website](https://www.surf.nl/kennis-en-innovatie/innovatieprojecten/startdatum-2012/draadvrij/wifi-op-de-campus-met-eduroam/eduroam-visitor-access/index.html).
