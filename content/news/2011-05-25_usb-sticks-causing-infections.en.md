---
author: polman
date: 2011-05-25 15:57:00
tags:
- studenten
- medewerkers
- docenten
title: USB-sticks causing infections
---
Many PC infections occur because the malware on a USB-stick is
automatically started when you connect it to the PC, resulting in an
infected PC. You can prevent this from happening by [disabling the
‘autorun’-option](http://www.ru.nl/ict-beveiliging/bedreigingen_en/uitzetten-autorun/)
on the PC.
