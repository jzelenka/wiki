---
author: bbellink
date: 2021-10-07 18:15:00
tags:
- studenten
- medewerkers
- docenten
title: FNWI network cable administration by ISC networks
---
As of October 11, 2021, C&CZ will transfer the management of the fixed
data connection points (network outlets, switch ports) in the Faculty of
Science buildings (Huygens, Mercator, HFML/FELIX, Goudsmit, Proeftuin)
to [ILS networks](/en/howto/contact/#ils-helpdesk). This concerns both
activating/deactivating an outlet (patching) and adjusting settings or
solving malfunctions. Taking care of the IP addresses (DNS/DHCP) and the
shielding/security by means of Access Control Lists will remain with
[C&CZ](/en/howto/contact/). All requests and malfunctions relating to
data connection points can therefore be addressed to the [Helpdesk of the
ISC](/en/howto/contact/#ils-helpdesk) as of October 11.
