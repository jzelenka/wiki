---
author: petervc
date: 2011-09-14 11:11:00
tags:
- studenten
- medewerkers
title: Adobe Creative Suite 5.5 Web/Design Premium
---
The most recent version of the [Adobe Creative Suite Design/Web Premium
Bundle](http://www.adobe.com/products/creativesuite), 5.5, is available
for MS-Windows and Mac OS X. It can be found on the
[Install](/en/howto/install-share/) network share and can also be
[borrowed](/en/howto/microsoft-windows/). The license permits use on
university computers and also home use by employees. One can also order
the DVD’s on [Surfspot](http://www.surfspot.nl).
