---
author: mkup
date: 2012-02-28 14:44:00
tags:
- medewerkers
title: Netwerk miniswitch alleen gebruiken na overleg
---
De nieuwe netwerkswitches staan standaard maar twee verschillende
machines (MAC-adressen) aan elke netwerk-outlet toe, bij een derde wordt
de outlet een kwartier lang geblokkeerd. Indien het nodig is om meer
computers aan een netwerk-outlet te hangen, overleg dan vooraf eerst met
[C&CZ netwerkbeheer](/nl/howto/netwerkbeheer/).
