---
author: caspar
date: 2010-01-07 17:08:00
title: BASS op Mac en Linux
---
Het gebruik van de BASS (Oracle) concern applicaties voor HRM en
financiele transacties wordt alleen ondersteund voor een beperkt aantal
Windows-omgevingen. Maar omdat BASS in essentie een Java-toepassing is
die vanuit een browser wordt gestart, moet het in principe ook kunnen
werken op andere platformen zoals Macs en Linux werkstations. Het blijkt
inderdaad mogelijk te zijn om b.v. een bestelling te doen via BASS vanaf
een Linux (Fedora 11) systeem en we hebben gehoord dat het ook kan
werken vanaf een Mac. Op onze [BASS](/nl/howto/bass/) pagina hebben we
enkele hints verzameld die kunnen helpen om e.e.a. aan de gang te
krijgen voor diegenen die enige experimenteerwerk niet uit de weg gaan.
