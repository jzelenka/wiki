---
author: polman
date: 2009-02-20 16:54:00
title: Nieuwe versie squirrel (webmail)
---
[squirrel.science.ru.nl](http://squirrel.science.ru.nl/) is vervangen
door de laatste nieuwe versie en is verhuisd naar een nieuwe webserver.
