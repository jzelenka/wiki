---
author: petervc
date: 2013-11-21 16:25:00
tags:
- medewerkers
- studenten
title: F-Secure security software update available / home use not free anymore
---
F-Secure security software is no longer free for home use. It still can
be bought at relative low cost by students and employees through
[Surfspot](http://www.surfspot.nl). Surfspot still has to lower the
price to ca. € 11.50, please wait for this. For PC systems owned by
Radboud University, F-Secure security software can be used freely. The
software, also the newest version that supports Windows 8, can be found
on the [Install](/en/howto/install/) network share. It can also be
downloaded from [the website of
F-Secure](http://www.f-secure.com/en/web/business_global/support/downloads).
This information will also be available on the F-Secure page on
[Radboudnet](http://www.radboudnet.nl/fsecure) shortly.
