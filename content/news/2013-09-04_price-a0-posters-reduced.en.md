---
author: stefan
date: 2013-09-04 10:17:00
tags:
- medewerkers
- studenten
title: Price A0 posters reduced
---
The price of an A0 poster print on the [A0 photo
printer](/en/howto/kamerbreed/) has been reduced from 32 to 24 euro,
because the costs are lower than expected.
