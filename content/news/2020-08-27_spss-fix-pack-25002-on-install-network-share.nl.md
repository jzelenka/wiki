---
author: petervc
date: 2020-08-27 15:04:00
tags:
- medewerkers
- docenten
title: SPSS Fix Pack 25.0.0.2 op Install schijf
---
Een [Fix Pack voor SPSS
25](https://www.ibm.com/support/pages/spss-statistics-250-fix-pack-2),
25.0.0.2, is op de [Install](/nl/howto/install-share/)-netwerkschijf te
vinden.
