---
author: stefan
date: 2018-01-05 17:40:00
tags:
- medewerkers
- docenten
title: HP laptop accu terugroepactie
---
HP is een terugroepactie begonnen voor brandgevaarlijke laptopaccu’s die
tussen december 2015 en december 2017 zijn verkocht. Mocht u in die tijd
een HP laptop of accu gekocht hebben, dan is het verstandig te
[controleren of de accu vervangen moet
worden](https://batteryprogram687.ext.hp.com/).
