---
author: petervc
date: 2016-08-17 16:31:00
tags:
- medewerkers
title: 'Departments: printbudget transferred back to charge account (kostenplaats)'
---
All FNWI Konica Minolta MFPs with the C&CZ-printbudget system have been
switched to Peage. The C&CZ printbudget system is only used for the HP
printer escher (Library of Science) and the posterprinter kamerbreed.
C&CZ has therefore emptied all print budgets that are associated with a
charge account (kostenplaats) on August 23. We will instruct
Finance&Control to transfer the last budget back to the corresponding
charge account (kostenplaats). Owners of sufficiently positive group
budgets without charge account will be asked what to do with the
remaining budget. Budget for escher/kamerbreed can as of now only be
topped up or settled at C&CZ (postmaster\@science.ru.nl) indicating cost
charge account or if necessary with cash.
