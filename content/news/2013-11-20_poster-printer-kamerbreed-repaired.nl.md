---
author: stefan
date: 2013-11-20 14:07:00
tags:
- medewerkers
- studenten
title: Posterprinter "kamerbreed" gerepareerd
---
De posterprinter “[Kamerbreed](/nl/howto/kamerbreed/)” is weer
beschikbaar. Omdat de onderdelen slecht leverbaar waren heeft de
reparatie helaas lang geduurd.
