---
author: wim
date: 2015-03-24 15:45:00
tags:
- medewerkers
- studenten
title: Lowering printing prices
---
Printing prices, for black&white als well as color, have been lowered,
in accordance with the campus-wide printing prices. Reason for this
reduction in cost is the fact that the total number of pages printed at
Radboud University exceeded expectations and also that the fixed costs
for the printing infrastructure were less than estimated beforehand. See
also [our printing information page](/en/howto/printers-en-printen/)
