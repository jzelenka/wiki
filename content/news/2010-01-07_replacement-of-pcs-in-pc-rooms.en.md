---
author: theon
date: 2010-01-07 16:07:00
title: Replacement of pc's in pc rooms
---
In the third week of January, in total 122 pc’s in [pc
rooms](/en/howto/terminalkamers/) TK149/TK153, Biology Lab TK329 and the
study landscape will be replaced by new ones. The inconvenience for
students will be minimal in this week. Departments can contact C&CZ
(Theo Neuij, 56666) if they are interested in acquiring a number of
these more than 5 year old pc’s.
