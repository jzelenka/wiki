---
author: petervc
date: 2015-12-14 17:55:00
tags:
- medewerkers
- studenten
title: Data breaches must be reported and will be fined
---
As of January 1, 2016, organizations are [obliged by law to report data
leaks](https://www.cbpweb.nl/nl/melden/meldplicht-datalekken). If you
notice that you have access to personal data that you shouldn’t have,
you have to report that, e.g. to [C&CZ](/en/howto/contact/) and/or
[CERT-RU](http://www.ru.nl/informatiebeveiliging/ru-professionals/cert-ru/).
If you yourself are working with personal data, you need to do that
securely. Unsecure handling of personal data could lead to a huge fine.
In the near future, extra attention will be given to informing and
involving the RU community w.r.t. this issue.
