---
author: petervc
date: 2012-01-27 13:11:00
tags:
- studenten
- medewerkers
- docenten
title: Maple 15
---
Er is een nieuwe versie (15) van [Maple](/nl/howto/maple/) geïnstalleerd
op alle 64-bit Ubuntu Linux machines. Binnenkort zal het ook op de
[Windows-XP Beheerde Werkplek PCs](/nl/howto/windows-beheerde-werkplek/)
beschikbaar zijn. Het kan voor Windows (64-bit of 32-bit), Mac of Linux
(64-bit of 32-bit) geïnstalleerd worden vanaf de
[Install](/nl/howto/install-share/) netwerk schijf. De licentiecodes zijn
via postmaster te verkrijgen. Maple mag door medewerkers en studenten
ook thuis gebruikt worden.
