---
author: petervc
date: 2014-05-13 13:03:00
tags:
- medewerkers
- studenten
title: 'C&CZ day out: Thursday June 12'
---
The C&CZ yearly day out is scheduled for Thursday June 12. C&CZ can be
reached in case of serious disruptions of services.
