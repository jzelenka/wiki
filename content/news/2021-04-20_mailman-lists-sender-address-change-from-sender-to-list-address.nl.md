---
author: petervc
date: 2021-04-20 11:22:00
tags:
- medewerkers
- docenten
- studenten
title: 'Afzender mailmanlijsten aangepast: van afzender- naar lijstadres'
---
Steeds meer wordt [DMARC](https://nl.wikipedia.org/wiki/DMARC) gebruikt
als spamfilter, om bij binnenkomende mail te controleren of de afzender
wel via deze weg mail mag sturen. Om hieraan te voldoen voor via
[Mailman](https://wiki.cncz.science.ru.nl/index.php?title=Categorie%3AEmail&setlang=nl#.5Bnl.5DMailing_lijsten.5B.2Fnl.5D.5Ben.5DMailing_lists.5B.2Fen.5D)
verstuurde mails zal voor alle Mailman maillijsten in science.ru.nl
binnenkort het From: afzender-adres gewijzigd worden in het lijstadres.
Nu is het nog:

`From: Some User <some.user@somewhere.domain>`
`Sender: "listname" <listname-bounces@science.ru.nl>`

Dit wordt aangepast naar

`From: Some User via listname <listname@science.ru.nl>`
`Reply-To: Some User <some.user@somewhere.domain>`
`Sender: "listname" <listname-bounces@science.ru.nl>`

Wie mail automatisch filtert of doorstuurt, moet misschien een filter
wijzigen.
