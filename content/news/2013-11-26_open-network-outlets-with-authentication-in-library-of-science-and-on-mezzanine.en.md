---
author: mkup
date: 2013-11-26 09:02:00
tags:
- studenten
- medewerkers
- docenten
title: Open network outlets with authentication in Library of Science and on mezzanine
---
For some time, the reading room of the Library of Science and the
mezzanine above the restaurant have 32 and 64 authenticated network
outlets respectively, with network speeds of 100 Mbps. Connect your
laptop with a (your own) UTP cable and start a browser. You will be
redirected to a website where you can register with your personnel
number (`U123456`) or student number (`s123456`) or
`scienceloginname@science.ru.nl`. After that, you will have Internet
access. We intend to implement authentication for all publicly
accessible active network outlets in the future. The technique behind
this is Qmanage by [Quarantainenet](http://www.quarantainenet.com/).
