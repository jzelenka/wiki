---
author: petervc
date: 2013-09-22 13:51:00
tags:
- medewerkers
- studenten
- docenten
title: 'BASS: alleen directe toegang, Port Forwarder stopt'
---
Vanaf 25 september is directe toegang tot BASS voor vrijwel alle
FNWI-werkplekken mogelijk zijn. Wie niet direct naar BASS kan, kan
werken via [VPN](/nl/howto/vpn/) of de [Windows Terminal
Server](/nl/howto/windows-terminal-server/). De indirecte toegang tot
BASS via de Port Forwarder wordt beëindigd. Gebruikers die hun eigen
werkplek beheren en nu via de Port Forwarder werken, dienen zelf de
port-forwarder regels uit uw *hosts* bestand te verwijderen. Zie voor
meer informatie de [BASS](/nl/howto/bass/)-pagina.
