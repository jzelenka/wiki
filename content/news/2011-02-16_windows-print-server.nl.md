---
author: wim
date: 2011-02-16 10:53:00
tags:
- medewerkers
title: Windows printer server
---
Om afdelings printers beter te bedienen is er een Windows 2008 printer
server ingericht. Het betreft de
**drukker.nwi.ru.nl** – edit 2014-11-18 is
inmiddels gewijzigd in **drukker.b-fac.ru.nl**.
Deze machine kan zowel Windows XP, Windows Vista (32 en 64 bit) en
Windows 7 (32 en 64 bit) bedienen. Ook worden drivers dmv aankoppelen
van de printer server en het selecteren van de print queue automatisch
geinstalleerd.

Voordelen:

-   Tot op heden weinig klachten

Nadelen:

-   **Uitsluitend** bedoeld voor standaard, veel
    voorkomende printers (HP LaserJet, Xerox, Ricoh)
-   **Beperkte** beveiliging tegen misbruik
-   Sluit niet aan op de bestaande CnCZ printbudget software, dus
    **geen** accounting of printbuget faciliteiten.

Daarom komen alleen printers in aanmerking die niet publiek toegankelijk
zijn. Neem contact op met CnCZ om te overleggen of een printer in
aanmerking komt.

Zie [Printers\_en\_printen](/nl/howto/printers-en-printen/) voor uitleg
hoe een printer aan te koppelen.
