---
author: petervc
date: 2010-11-20 19:51:00
title: MS Office, nieuwste versies voor campusgebruik voor Windows en Macintosh
---
De DVD van [MS Office Professional Plus
2010](http://office.microsoft.com/en-gb/professional-plus/) voor Windows
XP/Vista/7 NL/UK 32/64 en de UK en NL versie van [MS Office for Mac
2011](http://store.microsoft.com/microsoft/Office-for-Mac-Home-and-Student-2011/product/C565DB0E)
zijn beschikbaar voor computers die eigendom van de faculteit zijn. De
[DVD voor Windows](/nl/howto/microsoft-windows/) kan geleend worden,
tevens staat deze op de [install](/nl/tags/software) schijf. De DVD’s
voor [Macintosh](/nl/howto/macintosh/) kunnen geleend worden.
