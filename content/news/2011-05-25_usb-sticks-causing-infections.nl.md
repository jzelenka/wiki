---
author: polman
date: 2011-05-25 15:57:00
tags:
- studenten
- medewerkers
- docenten
title: USB-sticks als bron van besmetting
---
Veel besmettingen van pc’s vinden plaats doordat malware op een
USB-stick automatisch opstart zodra u de stick in de pc steekt. U kunt
dat voorkomen door de [‘autorun’- functie voor
USB-sticks](http://www.ru.nl/ict-beveiliging/bedreigingen_en/uitzetten-autorun/)
op uw pc uit te zetten.
