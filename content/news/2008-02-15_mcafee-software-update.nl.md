---
author: petervc
date: 2008-02-15 13:37:00
title: McAfee software update
---
Een nieuwe versie van de [McAfee](http://www.mfafee.com) software, de
Internet Security Suite 2008 NL, is beschikbaar op de
[install](http://www.cncz.science.ru.nl/software/installscience)-schijf
en in de C&CZ [uitleen](/nl/howto/microsoft-windows/).
