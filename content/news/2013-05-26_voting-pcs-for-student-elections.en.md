---
author: petervc
date: 2013-05-26 12:13:00
tags:
- studenten
title: Voting PC's for student elections
---
This year too, C&CZ provided a few voting pc’s in the Huygens building
for the [student elections](http://www.ru.nl/verkiezingen/) for the
[USR](http://www.ru.nl/usr/), [FSR](http://www.ru.nl/fnwi/fsr/) and
[OLC](http://www.ru.nl/fnwi/fsr/medezeggenschap/olc/). These are Ubuntu
12.04 Linux pc’s, with the browser [Opera](http://www.opera.com/nl/) in
[kiosk mode](http://choice.opera.com/support/mastering/kiosk/), which
lets a user only visit the elections website, without having to log in.
