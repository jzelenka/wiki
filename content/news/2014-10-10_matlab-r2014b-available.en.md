---
author: petervc
date: 2014-10-10 18:15:00
tags:
- studenten
- medewerkers
- docenten
title: Matlab R2014b available
---
The latest version of [Matlab](/en/howto/matlab/), R2014b, is available
for departments that have licenses. The software and license codes can
be obtained through a mail to postmaster for those entitled to it. The
software can also be found on the [install](/en/tags/software)-disc. The
C&CZ-managed 64-bit Linux machines will soon have this version
installed, an older version (/opt/matlab-R2014a/bin/matlab) is still
available temporarily. The C&CZ-managed Windows machines will not
receive a new version during the semester to prevent problems with
version dependencies in current lectures.
