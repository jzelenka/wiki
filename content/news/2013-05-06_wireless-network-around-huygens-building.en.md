---
author: mkup
date: 2013-05-06 11:28:00
tags:
- studenten
- docenten
- medewerkers
title: Wireless network around Huygens building
---
For some time one can use the wireless networks eduroam, ru-wlan and
ru-guest (Wireless\@RU) on the terrace behind the Huygens building. From
now on these wireless networks are also available on every green around
the Huygens building, the square at the main entrance and the square
between the Huygens and Linnaeus buildings. Further info and user
manuals of Wireless\@RU can be found at
<http://www.ru.nl/uci/english/wireless_ru/wireless>.
