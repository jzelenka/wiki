---
author: caspar
date: 2011-10-06 12:43:00
tags:
- studenten
- medewerkers
- docenten
title: Nieuws over de overdracht van PC's naar GDI
---
Bij P&O is de [overname van de PC’s door GDI](/nl/howto/gdi/) vrijwel
afgerond en er is een werkwijze afgesproken voor de overname van de
onderwijs-PC’s/studentwerkplekken in alle onderwijsruimtes, de
bibliotheek en het studielandschap.
