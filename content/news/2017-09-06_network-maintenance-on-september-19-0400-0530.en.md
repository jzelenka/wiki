---
author: mkup
date: 2017-09-06 14:30:00
tags:
- studenten
- medewerkers
- docenten
title: Network maintenance on September 19, 04:00 - 05:30
---
Tuesday morning September 19, there will be maintenance on RU network
equipment. Between ca. 04:00-05:30 no network traffic will be possible
at the Faculty of Science on the wired and wireless networks. The
network connected IP phones will also be out of order. Please think
which processes depend on network connectivity and prepare for this
outage if necessary.
