---
author: polman
date: 2008-01-10 10:23:00
title: X-Win32 9.0 installed on managed Windows computers
---
`The latest version of`[`X-Win32`](/en/howto/xwin32/)`is installed on all C&CZ-managed Windows PCs.`
