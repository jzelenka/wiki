---
author: petervc
date: 2021-10-29 14:58:00
tags:
- medewerkers
- docenten
- studenten
title: 'Peage: workaround voor PrintNightmare'
---
C&CZ heeft een workaround die men kan proberen als men op een
zelfbeheerde/BYOD Windows pc last heeft van de [RU Peage PrintNightmare
storing](https://meldingen.ru.nl/detail.php?id=1167&lang=nl&tg=0&f=0ⅈ).
Deze workaround is beschreven op [de C&CZ
Peage-pagina](https://wiki.cncz.science.ru.nl/index.php?title=Peage&setlang=nl#.5BPrintNightmare_probleem_workaround.5D.5BPrintNightmare_problem_workaround.5D).
