---
author: petervc
date: 2017-12-08 17:23:00
tags:
- medewerkers
- studenten
title: Nieuwe versie van OriginPro (2018)
---
Er is een nieuwe versie (2018) van [OriginLab](/nl/howto/originlab/),
software voor wetenschappelijke grafieken en data-analyse,beschikbaar op
de [Install](/nl/howto/install-share/)-schijf. De licentieserver
ondersteunt deze versie. Afdelingen die meebetalen aan de licentie
kunnen bij C&CZ de licentie- en installatie-informatie krijgen, ook voor
standalone gebruik. Installatie op door C&CZ beheerde PC’s wordt
gepland. Afdelingen die OriginLab willen gaan gebruiken, kunnen zich
melden bij [C&CZ](/nl/howto/contact/).
