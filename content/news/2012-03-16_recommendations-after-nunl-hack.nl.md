---
author: petervc
date: 2012-03-16 16:20:00
tags:
- studenten
- medewerkers
- docenten
title: Adviezen n.a.v. gekraakte nu.nl website
---
De website
[NU.nl](http://www.nu.nl/internet/2763447/korte-tijd-malware-verspreid-via-nunl.html)
is gehackt, bezoekers kregen op 14 maart tussen 11:30 en 12:30 een
kwaadaardige worm aangeboden. PC’s met achterstallige versies van Adobe
Reader and Java kunnen besmet geraakt zijn met de Sinowal worm, die tot
doel heeft wachtwoorden, online bankgegevens en andere gevoelige data te
stelen. Wij hoorden dat een up-to-date installatie van [F-Secure Client
Security](http://www.radboudnet.nl/fsecure) deze malware blokkeerde.
Volgens
[ComputerIdee](http://www.computeridee.nl/nieuws/nunl-malware-gratis-te-verwijderen)
herkenden maar weinig beveiligingssuites deze malware. De daar genoemde
versie van HitmanPro is naar de [Install](/nl/howto/install-share/)
netwerkschijf gekopieerd, voor medewerkers en studenten die hun PC
willen scannen.

Algemene adviezen om de risico’s te beperken:

-   Houd alle software altijd up-to-date, speciaal browsers,
    mailprogramma’s en programma’s die via het Internet verkregen inhoud
    laten zien: PDF-readers (Adobe, …), Office, Flash, …
-   Vertrouw een besmette PC nooit meer: Maak de Master Boot Record
    schoon, b.v. met [TDSSkiller van
    Kaspersky](http://support.kaspersky.com/faq/?qid=208283363) en doe
    een verse installatie van het OS met alle beschikbare updates.
