---
author: polman
date: 2018-06-11 15:53:00
tags:
- studenten
- medewerkers
- docenten
title: Meer privacy in Mailman lijsten
---
`In het kader van de nieuwe privacywetgeving zijn de standaardinstellingen van alle mailmanlijsten aangepast.`

-   de lijsten zijn niet meer publiek opvraagbaar.
-   de leden van de lijst zijn niet meer opvraagbaar voor de leden van
    de lijst.
-   aanmelden op alle lijsten is nu beperkt tot adressen in ru.nl,
    radboudumc.nl en mpi.nl.

De laatste instelling is mogelijk te strikt voor sommige lijsten,
lijstbeheerders kunnen deze instelling ongedaan maken in de privacy
instellingen van mailman (zwarte lijst/ban list).
