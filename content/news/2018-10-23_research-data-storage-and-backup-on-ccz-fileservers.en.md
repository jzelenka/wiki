---
author: petervc
date: 2018-10-23 13:40:00
tags:
- medewerkers
title: 'Research data: storage and backup on C&CZ fileservers'
---
With the [lower prices for network discs and
backup](https://wiki.cncz.science.ru.nl/index.php?title=Nieuws&setlang=en#.5BLagere_prijzen_voor_netwerkschijven_en_backup.5D.5BLower_prices_for_network_discs_and_backup.5D),
several departments have started the move from locally managed discs to
backupped fileservers of C&CZ. In general this has [big
advantages](https://wiki.cncz.science.ru.nl/index.php?title=Diskruimte&setlang=en#.5BFunctionaliteit_en_prijs_van_netwerkschijven.5D.5BFunctionality_and_costs_of_network_shares.5D)
for reliability, backup and access rights that can be mananaged by the
department itself. Biggest disadvantage is that network discs that have
to be backed up, have to be divided in projects or other chunks of max.
ca. 400 GB. Departments interested in moving their data to C&CZ
fileservers should [contact postmaster](/en/howto/contact/). The current
setup is based on individual
[NAS-fileservers](https://en.wikipedia.org/wiki/Network-attached_storage),
C&CZ is investigating a new setup based on
[Ceph](https://en.wikipedia.org/wiki/Ceph_(software)).
