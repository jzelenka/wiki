---
author: petervc
date: 2016-11-23 13:02:00
tags:
- medewerkers
- studenten
title: RU netwerk down maandagavond 12 dec 22:00-03:00
---
Het [ISC](http://www.ru.nl/isc) heeft meegedeeld dat in de nacht van
maandagavond 12 december 22:00 tot dinsdagochtend 13 december 03:00 het
centrale RU-netwerk down zal zijn omdat dan de RU-firewall vervangen
wordt. Vanaf 22:00 uur zullen RU-centrale diensten als Exchange (e-mail
en agenda), BASS, Osiris en Blackboard niet bereikbaar zijn. Vanaf 23:00
uur zal geen verkeer via de RU-firewall mogelijk zijn, de RU is dan
afgesloten van het Internet. Intern bedraad FNWI-verkeer zal gewoon
blijven werkn. Ook telefonie (intern en extern RU) blijft mogelijk. Als
er zwaarwegende bezwaren tegen dit onderhoud zijn vanwege lopend
onderzoek, dan kan men dat [ons melden](/nl/howto/contact/).
