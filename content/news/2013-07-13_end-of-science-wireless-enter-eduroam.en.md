---
author: mkup
date: 2013-07-13 12:10:00
title: End of Science wireless, enter Eduroam
---
In the near future the preparations will start for the Eduroam wireless
network in the Faculty of Science buildings and the increase of capacity
of the wireless network. This also marks the end of the Science wireless
network, as was [announced earlier](/en/news/).

The work will probably start on August 5. Over the course of a few weeks
the Science network will gradually disappear while the Eduroam network
appears. Because of this transition period we strongly advise to [switch
all wireless devices to
ru-wlan](http://www.ru.nl/gdi/voorzieningen/campusbrede-systemen/wireless/wireless-ru-1/)
before that time.
