---
author: caspar
date: 2011-09-01 17:14:00
tags:
- studenten
title: Intro ICT faciliteiten voor nieuwe studenten
---
In the eerste (introductie) week van het nieuwe studiejaar worden
presentaties verzorgd over de facultaire ICT diensten, Blackboard en de
studentenportal. Deze presentaties staan nu ook
[online](/nl/tags/studenten).
