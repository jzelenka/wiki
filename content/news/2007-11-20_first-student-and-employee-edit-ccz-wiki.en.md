---
author: petervc
date: 2007-11-20 12:55:00
title: First student and employee edit C&CZ wiki
---
The explanation how to [install the wireless network for
Linux](/en/howto/netwerk-draadloos-handleidinglinux/) has been edited by
student Leon Swinkels with details for Ubuntu Gutsy Gibbon and by
employeee Gerrit Groenenboom for Suse 10.1.
