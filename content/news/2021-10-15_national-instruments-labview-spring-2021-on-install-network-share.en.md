---
author: petervc
date: 2021-10-15 11:59:00
tags:
- software
cover:
  image: img/2021/labview.png
title: National Instruments LabVIEW Spring 2021 on Install network share
---
To make it easier to install [NI
LabVIEW](https://www.ni.com/nl-nl/shop/labview.html) for departments
that participate in the license, the newly arrived “Spring 2021” version
has been copied to the [Install](/en/howto/install-share/) network share.
Install media can also be borrowed. License codes can be obtained from
C&CZ helpdesk or postmaster. For [macOS/Linux this version is
necessary](https://www.ni.com/nl-nl/innovations/white-papers/15/install-ni-academic-software-for-mac-os-x-and-linux.html),
for MS Windows one can also [install LabVIEW from the NI
website](https://knowledge.ni.com/KnowledgeArticleDetails?id=kA00Z0000019Ke3SAE&l=nl-NL).
