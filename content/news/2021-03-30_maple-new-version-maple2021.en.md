---
author: petervc
date: 2021-03-30 18:01:00
tags:
- studenten
- medewerkers
- docenten
title: 'Maple new version: Maple2021'
---
The latest version of [Maple](/en/howto/maple/), Maple2021, can be found
on the [Install](/en/howto/install-share/) network share and has been
installed on C&CZ managed Linux computers. License codes can be
requested from C&CZ helpdesk or postmaster by departments that take part
in the license.
