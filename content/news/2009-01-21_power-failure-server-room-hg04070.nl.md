---
author: josal
date: 2009-01-21 13:49:00
title: Stroomstoring serverruimte HG04.070
---
Om nog onbekende reden is op 20 januari rond 10:45 de brandbeveiliging
op de stroomvoorziening van de computerruimte HG04.070 geactiveerd,
waardoor de stroomvoorziening volledig afgeschakeld werd.

Nadat de de brandbeveiliging buiten bedrijf gesteld was, werd de
stroomvoorziening in fasen weer ingeschakeld, wat rond 12:00 uur
afgerond was.

Hard- en software-problemen konden daarna opgelost worden. Dit heeft
voor sommige systemen (uren-)lang geduurd. Zo is de disk-service voor
studenten de volgende ochtend pas weer hervat.

Rond 18:00 uur is de brandbeveiliging weer ingeschakeld, met een grotere
marge t.o.v. de ruimte temperatuur.
