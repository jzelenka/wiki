---
author: polman
date: 2008-02-14 22:59:00
title: Mediawiki upgrade to version 1.11 completed
---
The mediawiki code on which all wikis of the Science Faculty are based
has been replaced by the latest stable version
