---
author: petervc
date: 2007-11-20 14:14:00
title: Acrobat Reader, new version with problems
---
On all [Managed PCs](/en/howto/windows-beheerde-werkplek/) version 7 of
[Adobe](http://www.adobe.com) Reader has been replaced by version 8.1.1,
because of a [security
hole](http://www.adobe.com/support/security/bulletins/apsb07-18.html).
The upgrade lead to several problems:

1.  Printing from Adobe Reader to a HP4250/HP4350 printer doesn’t work.
    According to Adobe this is a fault of the software in these HP
    printers. As an alternative one can use the dali printer in HG00.201
    or one of the colour printers (HP or Tektronix). The Ricoh
    multifunctional copier/printer/scanners that will be placed soon in
    several locations to replace the Oce copiers do not have this
    problem.
2.  PCs with a license for Adobe Professional do not (yet?) have a
    license to upgrade, but having Adobe Professional version 7 and
    Reader version 8 at the same time doesn’t work.
