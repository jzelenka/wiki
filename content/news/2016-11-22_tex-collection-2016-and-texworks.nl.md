---
author: polman
date: 2016-11-22 15:31:00
tags:
- studenten
- medewerkers
- docenten
title: TeX Collection 2016 en TexWorks
---
De nieuwste versie van de [TeX
Collection](http://www.tug.org/texcollection), 2016, geschikt voor
MS-Windows, Mac OS X en Linux, is beschikbaar. De DVD is te vinden op de
[Install](/nl/howto/install-share/)-netwerkschijf. Voor Windows PC’s is
deze op de S:-schijf geïnstalleerd in de map texlive/2016. De grafische
front-end [TeXworks](https://www.tug.org/texworks/) is te vinden in
texlive/2016/bin/win32/texworks.exe.
