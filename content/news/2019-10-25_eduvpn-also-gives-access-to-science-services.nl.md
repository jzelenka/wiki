---
author: petervc
date: 2019-10-25 15:57:00
tags:
- studenten
- medewerkers
- docenten
title: EduVPN geeft ook toegang tot Science services
---
Al geruime tijd is [SURF
EduVPN](https://www.surf.nl/eduvpn-maak-onveilige-verbindingen-veilig)
de standaard [VPN voor centrale
RU-diensten](https://www.ru.nl/ict/medewerkers/off-campus-werken/vpn-virtual-private-network/).
Nu zijn ook alle afgeschermde Science-diensten, die al toegankelijk
waren vanuit de [Science VPN-diensten](/nl/howto/vpn/) en/of vanuit [RU
Eduroam](https://www.ru.nl/ict/studenten/eduroam/wifi-eduroam-instellen/),
toegankelijk vanaf SURF RU EduVPN. Dit omvat netwerkschijven,
licentieservers, websites, afgeschermde labs, toegang tot werkplekken en
speciale services als
[telgids](https://wiki.cncz.science.ru.nl/FNWI_Telefoon_en_E-mail_gids)/ldap/kerberos.
