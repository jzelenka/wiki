---
author: petervc
date: 2008-09-02 17:24:00
title: Openingstijden C&CZ verruimd
---
C&CZ heeft de [openingstijden](/nl/howto/helpdesk/) verruimd, onze nieuwe
openingstijden zijn van 08:00 uur tot 18:00 uur.
