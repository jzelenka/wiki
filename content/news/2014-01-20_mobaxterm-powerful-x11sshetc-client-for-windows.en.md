---
author: petervc
date: 2014-01-20 16:56:00
tags:
- medewerkers
- docenten
- studenten
title: 'MobaXterm: powerful X11/SSH/etc. client for Windows'
---
[MobaXterm](http://mobaxterm.mobatek.net/) is now available on the
[S-disk](/en/howto/s-schijf/). It is a powerful and easily usable
alternative for X11/SSH software like Xming, Xwin32, WinSCP and Putty.
From the MobaXterm website: “MobaXterm is an enhanced terminal for
Windows with an X11 server, a tabbed SSH client and several other
network tools for remote computing (VNC, RDP, telnet, rlogin). MobaXterm
brings all the essential Unix commands to Windows desktop, in a single
portable exe file which works out of the box.” The support of OpenGL
could also be a reason to start using MobaXterm. If you use it
professionally, you should consider subscribing to [MobaXterm
Professional Edition](http://mobaxterm.mobatek.net/download.html). We
already reported that [the support of X-win32 ends](/en/news/)
January 31, 2014. If you still need to switch, MobaXterm is a good
choice.
