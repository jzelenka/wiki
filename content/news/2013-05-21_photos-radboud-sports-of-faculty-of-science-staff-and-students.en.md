---
author: wim
date: 2013-05-21 18:01:00
tags:
- medewerkers
- studenten
title: Photos Radboud Sports of Faculty of Science staff and students
---
There are
[pictures](http://www.radboudnet.nl/fnwi/actueel/fotos-lustumsportdag/)
of our heros who defended the honour of our faculty on the Radboud
Sports day, in celebration of the 90th anniversary of our university, by
winning the classifying first round. Additional photos are more than
welcome.
