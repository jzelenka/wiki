---
author: sommel
date: 2008-11-18 13:40:00
title: General remarks/warnings about printing
---
On the page [Printers and printing](/en/howto/printers-en-printen/) the
following remarks/warnings have been added:

-   *Do not make lots of copies* without testing in advance.
-   *Do not use Adobe Acrobat 8* to print to an HP4250/4350 printer.
-   *Do not use a color printer* for black&white prints of a color file.
