---
author: caspar
date: 2013-05-29 15:50:00
tags:
- medewerkers
title: Wachtwoord BASS gelijk aan RU-wachtwoord
---
Vanaf zaterdag 1 juni wordt voor alle medewerkers het wachtwoord voor
BASS (Oracle) hetzelfde als voor de andere concernsystemen: men gebruikt
dan bij het inloggen in BASS het U-nummer en RU-wachtwoord. Het aparte
wachtwoord voor BASS komt dus te vervallen.

Tevens wijzigt het web-adres (URL) van BASS. Dit wordt:
<https://bass.ru.nl/> Gebruikers die rechtstreeks in BASS inloggen en
een bookmark (favoriet) of snelkoppeling naar BASS hebben gemaakt,
moeten deze aanpassen. Gebruikers die via de port forwarder werken
moeten in de meeste gevallen ook bepaalde instellingen aanpassen. Op de
pagina over [BASS](/nl/howto/bass/) wordt e.e.a. verder toegelicht.
