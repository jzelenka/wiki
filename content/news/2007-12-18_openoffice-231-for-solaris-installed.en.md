---
author: petervc
date: 2007-12-18 15:59:00
title: OpenOffice 2.3.1 for Solaris installed
---
The newest version of the office software
[OpenOffice](http://www.openoffice.org) (2.3.1) has been installed for
Solaris. Type `ooffice` to use it.
