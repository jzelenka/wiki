---
author: petervc
date: 2009-06-16 17:11:00
title: Fast info pc's, also for non-Science users
---
A few days ago a column with four pc’s has been placed in the central
street of the Huygens building, next to the [Library of
Science](http://www.ru.nl/ubn/op_bezoek/library_of_science/). Students
asked for pc’s to look up information fast,
[IHZ](http://www.ru.nl/fnwi/ihz/) ordered the design of the piece of
furniture, C&CZ placed the pc’s. New is that also non-Science users can
login with credentials from a different domain than Science, e.g. in the
RU-domain with s-number and KISS-password for a student. U-number and
RU-password for an employee doesn’t work (yet). In order to login fast,
no roaming profiles are loaded for a user. Specific Science software and
printing is not possible (yet).
