---
author: petervc
date: 2017-12-13 11:19:00
tags:
- studenten
- medewerkers
title: New versions of SPSS and Amos on Install network share
---
More recent versions of [IBM SPSS (23 to 25) and Amos (23 and
24)](http://www-01.ibm.com/software/analytics/spss/products/statistics/)
are available on the [Install](/en/howto/install-share/) network share.
SPSS for MS-Windows and Mac OS X, Amos only for Windows. The license
permits use on university computers and also home use by RU employees
and students. License codes can be requested from C&CZ helpdesk or
postmaster. One can also order the DVD’s on
[Surfspot](http://www.surfspot.nl).
