---
author: petervc
date: 2017-10-13 12:33:00
tags:
- medewerkers
- docenten
title: Oud kettingpapier gratis af te halen
---
Voor [de ingang van C&CZ](/nl/howto/contact/) hebben we een stapeltje
dozen met [kettingpapier](https://nl.wikipedia.org/wiki/Kettingpapier)
neergezet, die men gratis af mag halen. Dat kettingpapier werd jaren
geleden gebruikt in de door C&CZ beheerde matrixprinters, waarvan het
gebruik overigens ook altijd gratis was.
