---
author: caspar
date: 2009-12-23 16:48:00
title: PC's in de Library of Science toegankelijk voor alle RU studenten
---
Alle studenten van de Radboud Universiteit kunnen sinds 23 december 2009
met hun RU account (s-nummer) gebruik maken van de [PC’s in de Library
of Science](/nl/howto/terminalkamers/) in het Huygensgebouw. Kies bij het
inloggen voor het RU logon domein.

Let op:

-   Persoonlijke instellingen van de software blijven niet bewaard op
    deze PC’s.
-   De zwart-wit-printers in de nabije omgeving van de bibliotheek zijn
    standaard gekoppeld. Andere printers moeten bij iedere sessie
    opnieuw ingesteld worden. Je kunt een instructie krijgen bij de
    balie.

FNWI-studenten en -medewerkers kunnen ook inloggen met hun
science-account (domein B-fac), echter zonder hun persoonlijk profiel.
De U-netwerkschijf met hun persoonlijke data is dan wel gekoppeld. Op de
PC’s in de terminalkamers is het persoonlijke profiel nog steeds
beschikbaar.

In januari 2010 worden de PC’s in het studielandschap vervangen door
gloednieuwe exemplaren. Ook deze PC’s zullen dan toegankelijk worden
voor alle studenten van de RU.
