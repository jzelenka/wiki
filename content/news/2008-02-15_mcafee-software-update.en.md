---
author: petervc
date: 2008-02-15 13:37:00
title: McAfee software update
---
A new version of the [McAfee](http://www.mfafee.com) software, the
Internet Security Suite 2008 NL, is available on the
[install](http://www.cncz.science.ru.nl/software/installscience)-disk
and can be [borrowed](/en/howto/microsoft-windows/) from C&CZ.
