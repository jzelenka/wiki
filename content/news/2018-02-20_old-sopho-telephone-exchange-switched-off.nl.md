---
author: mkup
date: 2018-02-20 16:58:00
tags:
- medewerkers
- docenten
- studenten
title: Oude Sopho telefooncentrale uitgezet
---
Na ruim 30 jaar trouwe dienst is de oude Philips Sopho S2500
telefooncentrale van de SKU (RU en Radboudumc) uitgezet en afgevoerd.
Deze ‘eerste digitale telefooncentrale’ is in augustus 1987 bij FNWI in
gebruik genomen, als pilot project van PTT, Philips en de RU (toen KUN).
Aad Hulsbosch van C&CZ was een van de drijvende krachten tijdens de
ingebruikname. In 1991 is de Sopho centrale uitgebreid t.b.v. de hele
SKU, tot ca 15.000 aansluitingen. De [telefoonfunctionaliteit op de
SKU](http://www.ru.nl/ict/medewerkers/telefonie/vast-mobiel/) is onlangs
volledig overgenomen door een IP-telefoniecentrale van
[Mitel](https://nl.wikipedia.org/wiki/Mitel) en de
[Vodafone](http://www.vodafone.nl) Wireless Office draadloze telefonie.
