---
author: petervc
date: 2018-01-16 10:33:00
tags:
- studenten
title: F-Secure security software now free for RU students
---
From now on students of Radboud University may download F-Secure SAFE
free of charge throughout the year, for their
pc/laptop/smartphone/tablet/iMac/Macbook. For more info, visit the
[SURFspot download
page](https://www.surfspot.nl/f-secure-safe-1-apparaat-radboud-universiteit-nijmegen-en-fontys.html?___store=engels&___from_store=default).
