---
author: wim
date: 2015-03-24 15:45:00
tags:
- medewerkers
- studenten
title: Verlaging printprijzen
---
De printprijzen zijn, zowel voor zwart/wit als kleur, verlaagd, net als
de campusbrede printprijzen. Reden voor deze prijsreductie is het feit
dat het totaal aantal op de RU afgedrukte pagina’s beduidend meer was
dan begroot en tevens de vaste kosten lager bleken dan begroot. Zie ook
[ons printer overzicht pagina](/nl/howto/printers-en-printen/).
