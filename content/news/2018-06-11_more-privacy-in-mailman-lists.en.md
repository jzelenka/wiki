---
author: polman
date: 2018-06-11 15:53:00
tags:
- studenten
- medewerkers
- docenten
title: More privacy in Mailman lists
---
`In the context of the new privacy legislation the default properties of all mailman lists have been modified.`

-   the mailman lists are no longer publicly announced on the mailman
    server.
-   the members of a list can only be viewed by the list administrators.
-   subscription to a list is restricted to addresses in ru.nl,
    radboudumc.nl and mpi.nl.

The third property is probably too restrictive for some of the lists,
list administrators can revert this change in the privacy options (Ban
list).
