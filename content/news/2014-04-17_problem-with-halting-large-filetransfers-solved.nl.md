---
author: mkup
date: 2014-04-17 17:15:00
tags:
- studenten
- medewerkers
title: Probleem met stokkende grote filetransfers opgelost
---
A.s. donderdagochtend 24 april zal [ISC
Netwerkbeheer](http://www.ru.nl/isc) n.a.v. enkele gemelde problemen met
stokkende transfers van grote databestanden een instelling in de
RU-firewall aanpassen. In de firewall wordt i.v.m. beveiliging [TCP
Sequence Number
Randomization](http://www.cisco.com/c/en/us/td/docs/security/fwsm/fwsm31/configuration/guide/fwsm_cfg/protct_f.html)
toegepast. Daarom zal [TCP
SACK](http://en.wikipedia.org/wiki/Transmission_Control_Protocol#Selective_acknowledgments)
in de firewall uitgezet worden, waardoor moderne hosts die SACK aan
hebben staan, toch geen overlast meer ondervinden van stokkende
verbindingen door de firewall heen.
