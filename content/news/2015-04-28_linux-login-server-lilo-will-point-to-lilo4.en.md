---
author: wim
date: 2015-04-28 16:01:00
tags:
- medewerkers
- docenten
- studenten
title: Linux login server lilo will point to lilo4
---
The name `lilo`, that always points to the advized login server, will be
moved on Wednesday May 6 from the more than three year old `lilo3` to
the newer [lilo4](/en/news/). This is a small step in the upgrade
to Ubuntu 14.04. During the summer break the [computer
labs](/en/howto/terminalkamers/) will be (re)installed with Ubuntu 14.04
LTS. Please remove the old entry of lilo(.science.ru.nl) from your
\~/.ssh/known\_hosts file. For cautious people wanting to check the
fingerprint of the public RSA key before supplying their
Science-password to the new server:
`aa:ad:c0:2e:60:9d:d3:cd:ca:a4:59:7d:d0:d8:4c:68`. The name `stitch` for
the second loginserver, that can be used if there is a problem with
`lilo`, will be moved on May 6 to the older `lilo3`.
