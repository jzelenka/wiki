---
author: mkup
date: 2007-11-30 13:00:00
title: Wireless network Huygens building full coverage
---
In the [Huygens
building](http://www.ru.nl/fnwi/over_de_faculteit/huygensgebouw/) a
[wireless network](/en/howto/netwerk-draadloos/) with full coverage was
realized shortly after the building was finished. Measurements over a
long period have determined that about 25 of the 90 access points can be
removed, so they can be used in different locations within the RU. Full
coverage is still guaranteed.
