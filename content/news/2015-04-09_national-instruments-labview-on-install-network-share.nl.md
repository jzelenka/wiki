---
author: petervc
date: 2015-04-09 17:10:00
tags:
- software
cover:
  image: img/2015/labview.png
title: National Instruments LabVIEW op Install-schijf
---
Om het voor de afdelingen die meebetalen aan de licentie van [NI
LabVIEW](http://netherlands.ni.com/labview) makkelijker te maken om
LabVIEW te installeren, zijn alle DVD’s van de versie “Spring 2015” op
de [Install](/nl/howto/install-share/)-netwerkschijf gezet. Licentiecodes
zijn bij C&CZ helpdesk of postmaster te verkrijgen.
