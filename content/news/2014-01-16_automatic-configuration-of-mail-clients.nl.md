---
author: polman
date: 2014-01-16 17:58:00
tags:
- medewerkers
- docenten
- studenten
title: Automatisch configuratie van mailprogramma's
---
Bij de installatie van allerlei mailprogramma’s kunnen vanaf nu alle
instellingen (loginnaam, IMAP- en SMTP-server en -parameters) voor de
door C&CZ beheerde maildomeinen automatisch opgehaald worden. Men hoeft
hiervoor zelf niet veel meer dan het eigen hoofd-mail-adres in te
vullen. Zie voor details [de mail service pagina](/nl/tags/email). Het
gebruik van een webmail dienst als
[Roundcube](http://roundcube.science.ru.nl/) is nog eenvoudiger, daar
hoeft men helemaal niets aan in te stellen.
