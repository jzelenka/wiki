---
author: bram
date: 2014-02-18 10:44:00
tags:
- medewerkers
- studenten
title: Julia programmeertaal
---
Op de [Linux loginservers](/nl/howto/hardware-servers/) is vanaf vandaag
de [programmeertaal
Julia](http://en.wikipedia.org/wiki/Julia_%28programming_language%29)
beschikbaar. Julia is een high-level dynamische programmeertaal, die
ontworpen is om te voldoen aan de eisen van high-performance numerieke
en wetenschappelijke berekeningen. Het opvallendste aan Julia is de
snelheid, die vaak minder dan een factor twee komt van volledig
geoptimaliseerde C-code. Wie Julia ook op andere computers beschikbaar
wil hebben, kan [contact opnemen met C&CZ](/nl/howto/contact/).
