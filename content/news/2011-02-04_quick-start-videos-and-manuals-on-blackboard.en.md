---
author: pberens
date: 2011-02-04 13:44:00
tags:
- docenten
title: Quick start videos and manuals on Blackboard
---
To become handy in Blackboard 9.1., there’re [plenty of learning
materials available](/en/howto/blackboard/). There’s even a Blackboard
organization run by Manfred te Grotenhuis, an FSW teacher, on
assessments in Blackboard 9.1. Please let us know before the 18th of
February 2011 in case you want to be enrolled in this organization.
Teachers can send their u-number to bb-nwi\@science.ru.nl
