---
author: bram
date: 2013-12-19 14:10:00
tags:
- docenten
- studenten
title: Major improvements in Web lectures user interface
---
The user interface of “Onderwijs in Beeld” has gone through some major
changes. In Blackboard, you can find a link to the video’s for each
academic year, if available. In the playlist, each video also offers the
following options:

-   external player\*
-   download slides as a pdf document\*
-   permalink\*\*
-   download video

\* only for new recordings with slides \*\* only for new recordings
