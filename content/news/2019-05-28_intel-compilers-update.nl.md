---
author: petervc
date: 2019-05-28 16:42:00
tags:
- medewerkers
- studenten
title: Intel compilers update
---
Update 4 van de [Intel Parallel Studio XE Cluster Edition voor Linux
2019](https://software.intel.com/en-us/parallel-studio-xe) is
geïnstalleerd in `/vol/opt/intelcompilers` en beschikbaar op o.a.
[clusternodes](/nl/howto/hardware-servers/) en
[loginservers](/nl/howto/hardware-servers/). Ook de oudere (2019n en
2014) versies zijn daar te vinden. Om de omgevingsvariabelen goed te
zetten, moeten BASH-gebruikers vooraf uitvoeren:

source
/vol/opt/intelcompilers/intel-2019u4/composerxe/bin/compilervars.sh
intel64

Daarna levert `icc -V` het versienummer. Voor meer info zie [de pagina
over de
Intel-compilers](https://wiki.cncz.science.ru.nl/index.php?title=Intel_compilers&setlang=nl).
