---
author: petervc
date: 2016-06-06 17:33:00
tags:
- medewerkers
- docenten
title: Filemaker 14/15 Pro and Server Advanced on Install network share
---
New versions of [Filemaker
Pro](http://www.filemaker.com/products/filemaker-pro/) and of [Filemaker
Server Advanced](http://www.filemaker.com/products/filemaker-server/),
14 and 15, for Windows (32- and 64-bit) and Mac are available on the
[Install](/en/howto/install-share/) network share. The license permits
the use on university computers. License codes can be obtained from C&CZ
helpdesk or postmaster. For home PCs, the software is available through
[Surfspot](http://www.surfspot.nl).
