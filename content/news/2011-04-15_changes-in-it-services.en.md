---
author: caspar
date: 2011-04-15 18:07:00
tags:
- studenten
- medewerkers
- docenten
title: Changes in IT services
---
On September 1, 2009 [GDI](http://www.ru.nl/gdi) (*Gebruikersdienst
ICT*) was formed by combining the former Computer Support Groups (COG’s)
of the various faculties and clusters of our university. Because of the
special role of IT for FNWI, C&CZ continued to exist as the IT service
group for FNWI.

Based on a formal decision of the Executive Board all standard Windows
workstations and student PC’s campus wide will be transferred to GDI.
C&CZ will remain responsible for all other IT services in this faculty.
At the moment GDI is preparing the transfer of the Windows workstations
of the service departments as well as all course PC’s. Users will be
informed in time about the consequences of these changes. **Until the
takeover, which will take place in steps, C&CZ will remain responsible
for all IT services for FNWI and will remain [reachable through the well
known channels](/en/howto/contact/).**
