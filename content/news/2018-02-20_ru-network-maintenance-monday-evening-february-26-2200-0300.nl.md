---
author: mkup
date: 2018-02-20 12:53:00
tags:
- medewerkers
- docenten
- studenten
title: 'Netwerkonderhoud RU: maandagavond 26 februari 22:00-03:00'
---
Op maandagavond 26 februari zal onderhoud plaatsvinden aan centrale
netwerkonderdelen van de RU. Tussen 22:00-03:00 uur zullen centrale
RU-applicaties als Eduroam, Blackboard, Osiris, BASS, RU VPN en het
RU-bestandsysteem meestal niet beschikbaar zijn. Voor telefoons en de
C&CZ-services wordt geen verstoring verwacht. Denk s.v.p. na of deze
verstoring voor u gevolgen heeft en bereid u indien nodig voor.
Toekomstige onderhoudsvensters zijn op de [website van het
ISC](http://www.ru.nl/systeem-meldingen/) te zien.
