---
author: polman
date: 2019-11-26 12:13:00
tags:
- medewerkers
- studenten
title: New license period for Mathematica coming up
---
The current license period for [Mathematica](http://www.wolfram.com)
will end soon, departments interested in using
[Mathematica](/en/howto/mathematica/) in the next contract period of 3
years can [contact](/en/howto/contact/) C&CZ.
