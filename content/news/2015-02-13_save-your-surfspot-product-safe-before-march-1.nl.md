---
author: petervc
date: 2015-02-13 14:02:00
tags:
- studenten
- medewerkers
title: Red je SURFspot productkluis voor 1 maart\!
---
SURFspot heeft [laten weten](https://www.surfspot.nl/productkluis) dat
de productkluis verdwijnt bij de overgang naar een nieuw
webwinkel-platform. In de productkluis staan de installatiecodes en
downloadlinks van jouw download-bestelling(en). Deze gegevens heb je
nodig als je bijvoorbeeld: - Nog installaties tegoed hebt (je hebt
bijvoorbeeld een antivirus voor 5 pc’s aangeschaft en nog maar op 2 pc’s
geïnstalleerd). - Je computer crasht, dan kun je met de gegevens uit je
productkluis de software opnieuw installeren.

Heb je de afgelopen jaren een download bij
[Surfspot](http://www.surfspot.nl) gekocht waarvan de licentie nog niet
verlopen is? Volg dan [de aangeraden
stappen](https://www.surfspot.nl/productkluis).
