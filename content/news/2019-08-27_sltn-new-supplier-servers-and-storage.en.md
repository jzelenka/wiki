---
author: petervc
date: 2019-08-27 13:08:00
tags:
- medewerkers
title: SLTN new supplier servers and storage
---
The [European tender for storage and
servers](https://www.ru.nl/purchasing/purchasing/current-tenders/) has
been won by supplier [SLTN](https://www.sltn.nl/), the contract starts
Sep. 1, 2019. The last years servers were purchased through
[Centralpoint (Scholten Awater)](https://www.centralpoint.nl/).
