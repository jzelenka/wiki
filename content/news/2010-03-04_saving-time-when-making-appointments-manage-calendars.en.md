---
author: petervc
date: 2010-03-04 17:05:00
title: Saving time when making appointments (manage calendars)
---
If you spend a lot of time planning appointments, especially with stable
and/or big lists of employees, please read on.

The last few weeks, the Faculty Office succesfully tested the [Mozilla
SunBird](http://www.mozilla.org/projects/calendar/sunbird/) calendar
software. At this moment, with this software one can view the free/busy
information of a few dozen employees that are using Outlook, Google
Calendar or a Science Faculty E-Groupware calendar.

If you are interested in using this calendar software, we would like to
hear from you, with a list of persons with whom you most often make
(group) appointments. These persons can [change their calender
settings](/en/howto/bezet-info-publiceren/) themselves.

Radboud University intends to migrate in 2010 to a modern mail/agenda
system for all employees and students. This system can be used with a
nice webclient, but also with Outlook, Thunderbird etc. This should make
planning appointments with RU employees really simple and efficient. If
you can wait for this new system, you do not have to react of course.
