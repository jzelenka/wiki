---
author: josal
date: 2008-04-10 12:48:00
title: Removing old mail addresses helps prevent spam
---
At this time (May 2008) we often see that suddenly a lot of spam is sent
from different places on the Internet, with a (fake) sender address of
the old “@sci.kun.nl” form. For the owner of such an address this gives
some trouble, since the spam that cannot be delivered wiil come “back”
to the sender address that was used. To help prevent this, it is a good
idea to look in the [DIY website](http://diy.science.ru.nl) which old
“kun.nl” addresses can be removed and mail that to postmaster.
