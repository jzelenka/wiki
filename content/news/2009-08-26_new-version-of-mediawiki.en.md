---
author: polman
date: 2009-08-26 11:38:00
title: New version of Mediawiki
---
`The latest stable version of`[`Mediawiki`](/en/tags/wiki)`(1.15.1) has been installed, all wikis of the Science Faculty`

are running on this version now. At the same time the [installed
extensions](/en/howto/geïnstalleerde-mediawiki-extensies/) have been
upgraded.
