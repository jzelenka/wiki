---
author: stefan
date: 2013-09-03 17:40:00
tags:
- studenten
title: Old pc clearance
---
Update, September 6: *There are already more requests than available
PC’s.* Next year there will probably be a PC clearance again.

This year too, C&CZ will give away old PC’s from the [PC
labs](/en/howto/terminalkamers/). The PC’s are Dell Optiplex USFF 760
“little backpacks” including monitor, keyboard and mouse. The hard disc
has been wiped, the PC’s given are without operating system or any form
of warranty, help or support. PC’s are ‘only given to students of the
Faculty of Science’. There is a maximum of 1 PC per person. Send a mail
to `helpdesk@science.ru.nl`, the [C&CZ helpdesk](/en/howto/helpdesk/)
specifying your Science mail address, i.e. ending in
`@student.science.ru.nl`. This offer is valid until October 1, 2013 or
until the supply runs out. You will get a mail with a date/time to
collect your PC.
