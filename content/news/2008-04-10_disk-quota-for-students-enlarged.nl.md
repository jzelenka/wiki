---
author: petervc
date: 2008-04-10 11:31:00
title: Diskquota studenten verhoogd
---
De [standaard diskruimte van studentenlogins](/nl/howto/studenten/) is
verhoogd tot 400 MB. Daardoor kunnen studenten meer bestanden opslaan op
hun homedirectory (H:-schijf), waardoor men hopelijk minder de neiging
heeft om bestanden op de Desktop (of ergens anders in het
netwerk-profiel) op te slaan. Een groot profile maakt o.a. het in- en
uitloggen traag.
