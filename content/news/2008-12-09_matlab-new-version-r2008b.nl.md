---
author: polman
date: 2008-12-09 08:29:00
title: Matlab nieuwe versie (R2008b)
---
Met ingang van december 2008 is er een nieuwe versie (R2008b) van
[Matlab](/nl/howto/matlab/). De licentie server heeft al een update
gekregen. Binnenkort is deze beschikbaar op alle door C&CZ beheerde PCs
met Linux of Windows. Voor meer info over deze release, zie [de
Mathworks
website](http://www.mathworks.com/products/new_products/latest_features.html)
