---
author: polman
date: 2017-01-18 17:37:00
tags:
- medewerkers
- studenten
title: New servers for home directories
---
The home directory/U:-disc servers `pile` and `bundle`, that are more
than four years old, are being replaced by new servers with the name
`home1` and `home2`. Moving the home directories (U:-discs) of users is
scheduled outside of working hours. Special arrangements will be made
for groups of users who would be troubled by this, e.g. having Outlook
.pst files on the network or using the compute cluster. During the move,
the minimal [quota](/en/howto/quota-bekijken/) for the Science [home
directory / U:-disc](/en/howto/diskruimte/) is enlarged from 2 GB to 5
GB. Workstations/PCs might need a reboot if successful login to Ubuntu
proves to be impossible.
