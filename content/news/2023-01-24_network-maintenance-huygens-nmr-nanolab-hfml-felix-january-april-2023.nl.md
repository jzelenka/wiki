---
title: Netwerkonderhoud Huygens, NMR, Nanolab, HFML-FELIX januari-april 2023
author: petervc
date: 2023-01-24
tags:
- medewerkers
- studenten
cover:
  image: img/2023/network-maintenance-huygens-nmr-nanolab-hfml-felix-january-april-2023.png
---
RU ILS Connectivity vervangt netwerk switches in FNWI. Dit
veroorzaakt korte storingen van de netwerkconnectiviteit voor bedrade en
draadloze systemen op enkele avonden tussen 19:00 en 23:00 uur
voor getroffen vleugels/locaties, d.w.z. verbonden met een specifieke SER
(Satellite Equipment Room). Voor bedrade netwerkuitgangen kan
gecontroleerd worden of ze getroffen worden door naar het outletnummer op de muur te kijken
(beginnend met het SER-nummer voor getroffen systemen) of ga naar [FNWI
ethergids](https://cncz.science.ru.nl/nl/howto/netwerk-ethergids/).

Globaal schema:
- Maandag 30 jan: SER 105 (in en rond vleugel 1 Huygens, alle verdiepingen).
- Woensdag 1 feb: SER 106 (in en rond vleugel 2 Huygens, alle verdiepingen).
- Half februari: SER 111.
- Eind februari: korte avondonderbreking voor netwerken in Huygens, NMR, Nanolab, HFML-FELIX.
- Maart: SER 109, 73, 72, 116, 101 en 135.
- Begin april: SER 107, 110 en 113.
