---
author: pberens
date: 2011-01-26 16:25:00
tags:
- docenten
title: 'Haal meer uit Blackboard: Evaluation tools'
---
Kent u de tools al waarmee u het gedrag van uw studenten in uw
Blackboard-cursus kunt volgen? Probeer ’ns Control Panel, Evaluation

-   Performance Dashboard: wanneer bezocht een student voor het laatst
    uw Blackboard-cursus, hoeveel bijdragen aan de discussie leverde een
    student e.d.
-   Course Statistics: laat alle activiteit van een student in uw
    Blackboard-cursus zien
-   Early Warning System: geeft een melding als een student een
    voorwaarde niet haalt, zoals inleveren van een opdracht of laag
    cijfer. Handig om op tijd actie te ondernemen! Gewoon een kwestie
    van uitproberen.

Hulp nodig? Mail de FNWI Blackboard Helpdesk
