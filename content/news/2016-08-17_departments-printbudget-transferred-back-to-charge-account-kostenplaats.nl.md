---
author: petervc
date: 2016-08-17 16:31:00
tags:
- medewerkers
title: 'Afdelingen: printbudget teruggeboekt naar kostenplaats'
---
Alle FNWI Konica Minolta MFP’s die doorbelast werden via het
C&CZ-printbudgetsysteem, zijn overgezet naar Peage. Het
C&CZ-printbudgetsysteem wordt alleen nog gebruikt voor de HP-printer
escher (Library of Science) en de posterprinter kamerbreed. Op 23
augustus heeft C&CZ daarom alle printbudgetten die gekoppeld zijn aan
een kostenplaats, leeg gemaakt. We zullen Finance&Control opdracht geven
het laatste budget terug te boeken naar de bijbehorende kostenplaats.
Eigenaren van voldoende positieve groepsbudgetten zonder kostenplaats
zal gevraagd worden wat er met het resterende budget moet gebeuren.
Budget voor escher/kamerbreed kan alleen nog bijgeboekt worden bij C&CZ
(postmaster\@science.ru.nl) met vermelding van kostenplaats of evt. met
cash.
