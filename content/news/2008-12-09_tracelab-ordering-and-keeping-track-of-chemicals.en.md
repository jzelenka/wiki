---
author: keesk
date: 2008-12-09 16:38:00
title: 'Tracelab: ordering and keeping track of chemicals'
---
Due to ever more stringent rules for ordering and keeping track of
chemicals, the intention is to only order chemicals through
[Tracelab](/en/howto/tracelab/) from January 1, 2009 and therefore reject
orders for chemicals that are directly entered into [BASS
Finlog](/en/howto/bass/). This holds for all units of the Faculty of
Science (in the Research Tower as well) and for all users who are
located in the buildings of the Faculty of Science (hence also the
spin-offs).
