---
author: wim
date: 2013-05-21 18:01:00
tags:
- medewerkers
- studenten
title: Foto's Radboud Sports van FNWI medewerkers en studenten in actie
---
Er zijn
[foto’s](http://www.radboudnet.nl/fnwi/actueel/fotos-lustumsportdag/)
van onze helden die op de Radboud Sportdag ter gelegenheid van het
90-jarig jubileum van onze universiteit de eer van onze faculteit hoog
hebben gehouden door de voorrondes te winnen. Aanvullende foto’s zijn
van harte welkom.
