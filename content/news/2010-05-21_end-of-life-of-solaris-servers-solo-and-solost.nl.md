---
author: wim
date: 2010-05-21 09:52:00
title: 'Uitfaseren Solaris servers: Solo en Solost'
---
De Solaris loginservers **solo** en
**solost** zullen op 1 juli 2010 definitief
uitgefaseerd worden. Stap svp over naar de vervangende Linux Fedora
loginservers **lilo** en
**stitch**. Bij problemen hiermee kan Postmaster
hulp bieden. Door het uitfaseren van alle Solaris servers stopt de
beheerinspanning van C&CZ voor dit operating system. Nu er plannen zijn
om op een andere Linux distributie over te gaan voor servers en/of
werkplekken, zal hiermee het aantal ondersteunde operating systemen
voorlopig gelijk blijven.
