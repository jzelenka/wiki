---
author: wim
date: 2018-02-28 18:55:00
tags:
- medewerkers
- docenten
title: Download uw oude "Onderwijs in Beeld" opnames van colleges
---
De service ‘Onderwijs in Beeld’ wordt per 1 augustus 2018 opgeheven, de
daarin opgenomen video’s zijn dan niet meer toegankelijk.

Deze voorloper van de [RU weblectures](http://www.ru.nl/weblectures/)
heeft in de collegejaren 2007-2015 opnames van colleges op FNWI
verzorgd, die via [BlackBoard](https://blackboard.ru.nl/) aangeboden
werden.

Als u oude opnames van uw colleges wilt downloaden, kunt u dat doen op
basis van de cursuscode via [de Podcast
service](https://podcast.science.ru.nl/search/).
