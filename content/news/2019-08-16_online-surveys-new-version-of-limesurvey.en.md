---
author: remcoa
date: 2019-08-16 13:49:00
tags:
- medewerkers
- studenten
title: 'Online surveys: New version of LimeSurvey'
---
A new version of [LimeSurvey](/en/howto/limesurvey/) has been installed,
along with some new and modern templates. More information about
[LimeSurvey](/en/howto/limesurvey/)…
