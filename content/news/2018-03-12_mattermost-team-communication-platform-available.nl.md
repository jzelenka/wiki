---
author: bram
date: 2018-03-12 14:44:00
tags:
- medewerkers
- docenten
- studenten
title: Mattermost team communicatieplatform beschikbaar
---
Vanaf begin maart is het team communicatieplatform Mattermost
beschikbaar voor iedereen met een Science login. Mattermost is een open
source Slack alternatief. Binnen een Mattermost team is het mogelijk om
een integratie te maken met een GitLab project of andere online
diensten. Mattermost is te vinden op <https://mattermost.science.ru.nl>.
