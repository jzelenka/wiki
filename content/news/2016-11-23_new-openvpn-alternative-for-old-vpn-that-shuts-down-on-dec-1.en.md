---
author: visser
date: 2016-11-23 13:53:00
tags:
- medewerkers
- studenten
title: New OpenVPN alternative for old VPN that shuts down on Dec. 1
---
Because some Linux and MacOS users had problems with the new IPsec-based
[VPN](/en/howto/vpn/) and also with the existing alternatives, we setup a
new [OpenVPN](/en/howto/vpn/) service. The hope and expectation is that
with this new service, we can shut down the old PPTP-based
VPN.science.ru.nl, which is no longer considered to be secure, on
December 1 as earlier announced.
