---
author: petervc
date: 2012-09-24 11:00:00
tags:
- studenten
- medewerkers
- docenten
title: TeX Collection 2012
---
De nieuwste versie van de [TeX
Collection](http://www.tug.org/texcollection), juni 2012, geschikt voor
MS-Windows, Mac OS X en Linux/Unix, is beschikbaar. De DVD is te vinden
op de [Install](/nl/howto/install-share/)-netwerkschijf, maar is ook [te
leen](/nl/howto/microsoft-windows/).
