---
author: sommel
date: 2009-11-19 14:53:00
title: Linux print problem and change
---
The [FSR](http://www.ru.nl/fnwi/fsr/) reported a security problem with
printing on Fedora Linux, that made it possible to print using the
budget of a different user. As a temporary emergency solution, one has
to supply a password when printing login server, like `lilo` and
`stitch`, to a [budgeted printer](/en/howto/printers-en-printen/). See
the [print information](/en/howto/printers-en-printen/) to print from
within an application that doesn’t ask for a password. A final solution,
the deployment of a central [CUPS](http://www.cups.org/)-server,
couldn’t be realized in a very short timeframe. Thanks to the FSR for
reporting this problem.
