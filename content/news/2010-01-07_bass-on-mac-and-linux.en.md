---
author: caspar
date: 2010-01-07 17:08:00
title: BASS on Mac and Linux
---
The use of the BASS (Oracle) concern applications for HRM and financial
transactions is supported only on some Windows platforms. However
because BASS is essentially a Java application running from a browser it
should function just as well from other platforms such as Macs and Linux
systems. Indeed we were able to place an order through BASS from a Linux
(Fedora 11) system and we’ve heard that for some people BASS also works
from a Mac. For those who are not afraid to experiment a little we have
collected a few hints on our [BASS](/en/howto/bass/) page which may help
you to get things going.
