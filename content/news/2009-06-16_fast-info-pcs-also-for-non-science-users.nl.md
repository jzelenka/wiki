---
author: petervc
date: 2009-06-16 17:11:00
title: Snelle info pc's, ook voor niet-FNWI gebruikers
---
Sinds enkele dagen staat er een zuil met vier pc’s in de centrale straat
van het Huygens gebouw bij de [Library of
Science](http://www.ru.nl/ubn/op_bezoek/library_of_science/). Studenten
hadden behoefte aan pc’s voor het snel raadplegen van informatie,
[IHZ](http://www.ru.nl/fnwi/ihz/) heeft het meubel laten ontwerpen, C&CZ
heeft de pc’s geplaatst. Nieuw is dat ook niet-FNWI gebruikers in kunnen
loggen met username/password van een ander domein dan Science,
bijvoorbeeld in het domein RU met s-nummer en KISS-wachtwoord voor een
student. U-nummer en RU-wachtwoord voor een medewerker werkt (nog) niet.
Vanwege de snelheid van inloggen worden geen netwerkprofielen van een
gebruiker geladen. Specifieke FNWI-programmatuur en printen is
(voorlopig) niet mogelijk.
