---
author: mkup
date: 2018-02-20 12:53:00
tags:
- medewerkers
- docenten
- studenten
title: 'RU Network maintenance: Monday evening February 26 22:00-03:00'
---
Monday evening February 26, there will be maintenance on central RU
network equipment. Between 22:00-03:00 central RU applications like new
Eduroam, Blackboard, Osiris, BASS, RU VPN and the RU filesystem will not
be available most of the time. For telephones and the C&CZ services we
expect no disruption. Please think which processes depend on
connectivity to RU central services and prepare for this outage if
necessary. Future maintenance windows can be seen on the [ISC
website](http://www.ru.nl/systeem-meldingen/).
