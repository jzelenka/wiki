---
title: Nieuwe versie van Mathematica (13.2.0)
author: petervc
date: 2022-12-23
tags:
- medewerkers
- studenten
cover:
  image: img/2022/mathematica-13.png
---
Er is een nieuwe versie (13.2.0) van
[Mathematica](/nl/howto/mathematica/) geïnstalleerd op alle door C&CZ
beheerde Linux systemen, oudere versies zijn nog in `/vol/mathematica`
te vinden. De installatiebestanden voor Windows, Linux en macOS zijn op
de [Install](/nl/howto/install-share/)-schijf te vinden, ook voor oudere
versies. Licentie- en installatiegegevens zijn bij
[C&CZ](/nl/howto/contact/) te verkrijgen.

Volgens de [Mathematica Quick Revision History](https://www.wolfram.com/mathematica/quick-revision-history.html):
"Version 13.2 introduces new functionality in astronomy and compilation,
as well as substantially enhancing functions for machine learning, trees,
mathematical computations, video and more. This release also includes
over a thousand bug fixes, documentation enhancements and significant
performance improvements."
