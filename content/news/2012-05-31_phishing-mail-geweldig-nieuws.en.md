---
author: petervc
date: 2012-05-31 13:31:00
tags:
- studenten
- medewerkers
- docenten
title: 'Phishing mail: Geweldig nieuws\!'
---
From May 31, RU-personnel receives phishing mail with subject “Geweldig
nieuws!”. A link in the mail points to a shortened URL of
<http://goo.gl/>. If one falls for this and clicks the link, then one is
redirected to a [Google Docs](https://docs.google.com/) site, that looks
like it could be a RU site. If one is stupid enough to enter the
U-number and RU-password, then these are at that moment given to
internet criminals. They can use this to e.g. try to send spam.
