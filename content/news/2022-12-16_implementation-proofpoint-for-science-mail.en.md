---
title: Implementation Proofpoint for Science mail
author: bbellink
date: 2022-12-16
tags:
- email
cover:
  image: img/2022/proofpoint.png
---
Unfortunately, we have recently seen a strong increase in successful [phishing](/en/howto/know-about-phishing) attempts in the Science domain, a mailing about this issue was already sent last week.

At the same time, we worked hard in collaboration with ILS to also deploy the centrally used anti-phishing tooling from Proofpoint for FNWI. The central RU mail environment has been supplemented with this highly effective anti-phishing measure for some time now.

As soon as the Science mail domains are also placed 'behind' this anti-phishing tool, things will change for users who have not yet had to deal with this. More information can be found on <https://www.ru.nl/ict/algemeen/proofpoint/>.

An important difference for users who do not use a device managed by ILS, is that there is no "report phishing" button in the mail client to report phishing yourself. If you receive a phishing email that is not already blocked by Proofpoint, we would like to ask you to report this to [C&CZ](/en/howto/contact).

Furthermore, any links (URLs) present in the received mail will be rewritten and there is a user portal in which mail blocked by Proofpoint can be seen that has been placed in quarantine. Many options are available in this portal, such as releasing mail and allowing or blocking senders. The C&CZ helpdesk can also support you with this if you have any questions about this.

At the moment, there's no way for individual users to opt-out for
Proofpoint for their RU or Science mail address.

We assume that this measure will lead to us becoming less vulnerable to phishing Science logins and thus improving information security. If this leads to questions or problems in the handling of mail, we would of course like to hear this.

Thank you in advance for your understanding.
