---
title: Science Jitsi Meet videoconferencing achter wachtwoord
author: sioo
date: 2023-01-19
tags:
- medewerkers
- studenten
cover:
  image: img/2023/jitsimeet.png
---
# Science Jitsi beveiliging

Sinds 18 januari 2023 is het starten van een
[jitsi](https://jitsi.science.ru.nl/) meeting room beveiligd. Om een nieuwe
room te openen moet je nu je [science loginnaam](/nl/howto/login) en
wachtwoord invoeren om verder te komen. Gasten hebben geen wachtwoord
nodig als de sessie al is opgestart.
[Jitsi](https://nl.wikipedia.org/wiki/Jitsi) is gratis en open-source, en werkt zowel op het web, als op Windows,
Linux, macOS, iOS en Android. Het meest bekende deelproject is Jitsi
Meet voor videoconferencing.
