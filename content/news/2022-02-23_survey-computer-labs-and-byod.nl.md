---
author: bbellink
date: 2022-02-23 16:49:00
tags:
- medewerkers
- studenten
title: "Enquête pc onderwijsruimtes en BYOD"
---
De FNWI vraagt aan docenten en studenten om een [enquête over de pc
onderwijsruimtes](https://u1.survey.science.ru.nl/index.php/735564?lang=nl-informal)
in te vullen, dit kost maar 2 (studenten) of 5 (docenten) minuten. De
FNWI is namelijk van plan om haar onderwijszalen multifunctioneel in te
richten, waarbij computerpractica middels [‘Bring Your Own Device’
(BYOD)](https://en.wikipedia.org/wiki/Bring_your_own_device) gegeven
gaan worden. Dit betekent dat de bestaande pc-onderwijszalen
(terminalkamers, TK’s) in het Huygensgebouw de komende jaren zullen
verdwijnen. Om verdere invulling te geven aan de implementatie willen we
graag [uw input over het huidige gebruik van de pc onderwijsruimtes
middels een enquête
opvragen](https://u1.survey.science.ru.nl/index.php/735564?lang=nl-informal),
om hiermee het tempo te kunnen bepalen en welke alternatieven mogelijk
gerealiseerd moeten worden ter vervanging van de verschillende
gebruiksvormen van de huidige TK’s. De projectgroep brengt op basis van
deze enquête en data van het gebruik van de ruimtes en apparatuur een
advies uit aan het faculteitsbestuur.
