---
author: petervc
date: 2011-12-16 17:26:00
tags:
- studenten
- medewerkers
- docenten
title: TeX Collection 2011
---
The most recent version of the [TeX
Collection](http://www.tug.org/texcollection), 2011, is available for
MS-Windows, Mac OS X and Unix/Linux. It can be found on the
[Install](/en/howto/install-share/) network share and can also be
[borrowed](/en/howto/microsoft-windows/).
