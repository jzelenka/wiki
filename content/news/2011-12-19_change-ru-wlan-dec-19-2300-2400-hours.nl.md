---
author: mkup
date: 2011-12-19 16:31:00
tags:
- studenten
- medewerkers
- docenten
title: Aanpassing ru-wlan, 19 dec 2011 23:00-24:00 uur
---
Het [UCI](http://www.uci.ru.nl) deelde mee dat op 19 december 2011
tussen 23:00 en 24:00 uur onderhoud wordt gepleegd aan het [ru-wlan
draadloze
netwerk](http://www.ru.nl/uci/diensten_voor/medewerkers/wireless_ru/).
Hierbij wordt het aantal IP-adressen verhoogd van ca 2000 naar ca 4000
en de authenticatie wordt versneld en voor iPhones verbeterd. Hierna is
het niet meer nodig om profiles te gebruiken voor iPads en iPhones.
Gedurende deze tijd zal gebruik van ru-wlan niet mogelijk zijn.
