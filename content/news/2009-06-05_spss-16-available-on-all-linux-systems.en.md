---
author: polman
date: 2009-06-05 16:08:00
title: SPSS 16 available on all linux systems
---
The statistical software package [SPSS](http://www.spss.com/) is now
available on all linux systems.
