---
author: petervc
date: 2010-04-29 15:12:00
title: Zaalwacht studielandschappen voor problemen in pc-zalen
---
C&CZ en de Library of Science gaan vanaf 3 mei testen met het inzetten
van de zaalwachten van het studielandschap voor de pc-zalen op de begane
grond van het Huygensgebouw. [Problemen](/nl/tags/storingen) met pc’s in
die zalen kunnen tijdens deze test bij voorkeur bij de zaalwacht in het
studielandschap gemeld worden. Ook kijkt de zaalwacht regelmatig in de
pc-zalen of de gedragsregels (niet eten, drinken of anderszins overlast
veroorzaken) gehandhaafd worden.
