---
author: mkup
date: 2018-09-25 10:23:00
tags:
- medewerkers
- docenten
- studenten
title: 'Netwerkonderhoud RU: maandagavond/nacht 1 op 2 oktober 22:00-03:00 uur'
---
Op maandagavond 1 oktober zal onderhoud plaatsvinden aan centrale
netwerkonderdelen van de RU: alle routers krijgen een software-upgrade.
Vanaf 22:00 uur zullen delen van het RU-netwerk niet beschikbaar zijn.
Het netwerk in Huygens en andere FNWI-gebouwen zal tusen 01:00 en 02:00
uur niet beschikbaar zijn, waaronder het draadloze netwerk en
Internettoegang. Voor de C&CZ-services zal dus ook verstoring optreden.
Denk s.v.p. na of deze mogelijke verstoring voor u gevolgen heeft en
bereid u indien nodig voor. Toekomstige onderhoudsvensters zijn op de
[website van het ISC](http://www.ru.nl/systeem-meldingen/) te zien.
