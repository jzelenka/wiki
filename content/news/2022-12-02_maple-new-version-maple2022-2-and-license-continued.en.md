---
title: 'Maple new version: Maple2022.2 and license continued'
author: petervc
date: 2022-12-02
tags:
- medewerkers
- studenten
cover:
  image: img/2022/maple.png
---
The C&CZ administered “4 concurrent user” license for Maple has been
continued until December 31, 2023. The latest version of
[Maple](/en/howto/maple/), Maple2022.2, for Windows/macOS/Linux can be
found on the [Install](/en/howto/install-share/) network share and has
been installed on C&CZ managed Linux computers. License codes can be
requested from C&CZ helpdesk or postmaster.
