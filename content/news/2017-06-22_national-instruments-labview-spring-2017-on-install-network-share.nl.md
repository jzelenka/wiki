---
author: polman
date: 2017-06-22 16:59:00
tags:
- software
cover:
  image: img/2017/labview.png
title: National Instruments LabVIEW Spring 2017 op Install-schijf
---
Om het voor de afdelingen die meebetalen aan de licentie van [NI
LabVIEW](http://netherlands.ni.com/labview) makkelijker te maken om
LabVIEW te installeren, is de zojuist binnengekomen versie “Spring 2017”
op de [Install](/nl/howto/install-share/)-netwerkschijf gezet.
Installatiemedia zijn ook te leen. Licentiecodes zijn bij C&CZ helpdesk
of postmaster te verkrijgen.
