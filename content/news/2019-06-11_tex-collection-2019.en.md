---
author: petervc
date: 2019-06-11 11:03:00
tags:
- studenten
- medewerkers
- docenten
title: TeX Collection 2019
---
The most recent version of the [TeX
Collection](http://www.tug.org/texcollection), 2019, is available for
MS-Windows, Mac OS X and Linux. It can be found on the
[Install](/en/howto/install-share/) network share.
