---
author: petervc
date: 2013-05-29 14:18:00
tags:
- medewerkers
- docenten
- studenten
title: ATLAS.ti on Install network share
---
The most recent version of [ATLAS.ti](http://www.atlasti.com), 7, is
available on the [Install](/en/howto/install-share/) network share. With
ATLAS.ti you can do qualitative data analyses of unstructured data
(text, multimedia, geospatial). The license permits use on university
computers. License codes can be requested from C&CZ helpdesk or
postmaster.
