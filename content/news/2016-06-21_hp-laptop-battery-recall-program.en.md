---
author: stefan
date: 2016-06-21 15:40:00
tags:
- medewerkers
- docenten
title: HP laptop battery recall program
---
HP announced a recall program for laptop batteries that may overheat and
pose a fire hazard. The batteries were sold in the period between March
2013 and August 2015. If you bought an HP laptop or battery in that
period, it is wise to [[check whether the battery should be
replaced](https://h30686.www3.hp.com/).
