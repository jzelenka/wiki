---
author: visser
date: 2014-06-19 13:50:00
tags:
- medewerkers
- studenten
title: Ubuntu 14.04 on workstations
---
Employees who want Ubuntu 14.04 on their workstation, can [contact
C&CZ](/en/howto/contact/). Testing Ubuntu 14.04 is possible on the new
login server `lilo4.science.ru.nl`. The PCs in the [computer labs and
study area](/en/howto/terminalkamers/) will keep on running Ubuntu 12.04
in 2014.
