---
author: mkup
date: 2011-05-26 23:21:00
tags:
- studenten
- medewerkers
- docenten
title: Spamming PC's noticed faster
---
[CERT-RU](http://www.ru.nl/cert), that coordinates RU IT-security
incidents, regularly receives reports about PC’s that are distributing
spam mail. Often these have been noticed by
[Senderbase](http://www.senderbase.org/senderbase_queries/detailip?search_string=131.174.0.0%2F16;amp;max_rows=50;amp;dnext_set=0;amp;tddorder=lastday+desc;amp;which_others=%2F16#page),
because the daily mail volume has been much larger than the monthly
average. The routers at the Science Faculty log the initialization of an
SMTP-connection outside Radboud University. Therefore spamming PC’s
often already have been cleaned when CERT-RU gets noticed by an outside
party.
