---
author: visser
date: 2014-06-19 13:50:00
tags:
- medewerkers
- studenten
title: Ubuntu 14.04 op werkstations
---
Medewerkers die Ubuntu 14.04 op hun werkstation willen, kunnen dat
[melden bij C&CZ](/nl/howto/contact/). Testen van Ubuntu 14.04 kan op de
nieuwe loginserver `lilo4.science.ru.nl`. De PC’s in de [terminalkamers
en studielandschap](/nl/howto/terminalkamers/) blijven in 2014 nog Ubuntu
12.04 draaien.
