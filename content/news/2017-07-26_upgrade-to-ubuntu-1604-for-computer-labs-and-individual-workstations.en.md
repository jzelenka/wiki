---
author: wim
date: 2017-07-26 14:11:00
tags:
- medewerkers
- docenten
- studenten
title: Upgrade to Ubuntu 16.04 for Computer labs and individual workstations
---
During the summer break all [computer labs](/en/howto/terminalkamers/)
will be (re)installed with Ubuntu 16.04 LTS. Since months ago, one can
test course software using the Ubuntu 16.04
[loginserver](/en/howto/hardware-servers/) lilo5. All PC’s in HG00.029,
HG00.206, HG02.253, library, study landscape, project rooms and college
rooms will be replaced by new ones, (All-in-One, dual boot with Windows7
and Ubuntu 16.04 Linux).

If you are using Linux PC’s in [computer labs](/en/howto/terminalkamers/)
as well as a managed Linux PC in your office, we advise you to consider
upgrading your office PC to Ubuntu 16.04 as well. Running Ubuntu 12.04
or 14.04 in your office on a managed PC and using an Ubuntu 16.04 PC in
the [computer labs](/en/howto/terminalkamers/) will result in problems.
