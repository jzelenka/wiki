---
author: petervc
date: 2015-01-05 16:57:00
tags:
- studenten
- medewerkers
title: Euroglot 8 on Install network share, new license without home use
---
The most recent version (8) of the [Euroglot](http://www.euroglot.nl)
translation software, is available on the
[Install](/en/howto/install-share/) network share. License codes, valid
until January 1 2016, can be requested from [the
helpdesk](/en/howto/contact/). Home use has been removed from this
license. For home use you can buy a private license relatively cheap at
[Surfspot](http://www.surfspot.nl).
