---
author: petervc
date: 2014-01-09 15:39:00
tags:
- medewerkers
- studenten
title: Backup service completely renewed
---
C&CZ makes [backups](/en/howto/backup/) of all important [network
disks](/en/howto/diskruimte/). In the last months the [backup
service](/en/howto/backup/) has been completely renewed. It’s now faster
and has a larger capacity to cope with the growing disk space demands.
If you lose files on one of the network disks, please [contact
C&CZ](/en/howto/contact/) a.s.a.p. and provide the name of the network
disk and files, and the time period in which the files were still
present.
