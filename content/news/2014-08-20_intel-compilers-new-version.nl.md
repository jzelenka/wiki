---
author: polman
date: 2014-08-20 17:42:00
tags:
- medewerkers
- studenten
title: Intel compilers nieuwe versie
---
C&CZ heeft twee licenties voor gelijktijdig gebruik van de nieuwste
versie van de [Intel Cluster Studio voor
Linux](http://software.intel.com/en-us/articles/intel-cluster-studio-xe/)
aangeschaft. Deze is geïnstalleerd in `/vol/opt/intelcompilers` en
beschikbaar op o.a. [clusternodes](/nl/howto/hardware-servers/) en
[loginservers](/nl/howto/hardware-servers/). Ook de oude (2011) versie
wordt verhuisd naar `/vol/opt/intelcompilers`. Om de omgevingsvariabelen
goed te zetten, moet men vooraf uitvoeren:

source /vol/opt/intelcompilers/intel-2014/composerxe/bin/compilervars.sh
intel64

Daarna levert `icc -V` het versienummer.
