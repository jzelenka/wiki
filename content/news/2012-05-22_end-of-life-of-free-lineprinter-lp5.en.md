---
author: petervc
date: 2012-05-22 15:42:00
tags:
- studenten
- medewerkers
- docenten
title: End of life of free lineprinter lp5
---
Since around 1995 is was possible to print without costs on continuous
paper on the [printer](/en/howto/printers-en-printen/) lp5. Last week the
printer showed a major hardware problem. The incidental usage by about
ten students doesn’t give C&CZ a reason to order the costly repair.
