---
author: john
date: 2014-09-19 13:10:00
tags:
- medewerkers
- studenten
title: Tijdelijke terminalkamer in HG00.625/629
---
Vanwege de gestegen studentenaantallen wordt een nieuwe
[terminalkamer](/nl/howto/terminalkamers/) gerealiseerd in HG00.625/629.
Deze ruimte komt vrij door de verhuizing van
[FEZ](http://www.ru.nl/fnwi/fez/) naar Mercator 1, vierde verdieping.
Omdat nog niet besloten is over de definitieve indeling van vleugel 6,
zal een tijdelijke voorziening worden gerealiseerd. Er komen 42 student-
en 1 docent-pc, van het type [HP EliteOne 800
G1](http://store.hp.com/NetherlandsStore/Merch/Product.aspx?id=H5U26ET&opt=ABH&sel=PBDT)
(All-in-One, Core i5-4570S, 2.9GHz, 8GB, 23-inch widescreen LCD monitor,
sound), dual boot met Windows and Linux (Ubuntu). Deze terminalkamer
moet begin november bruikbaar zijn.
