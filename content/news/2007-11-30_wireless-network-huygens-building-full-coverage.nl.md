---
author: mkup
date: 2007-11-30 13:00:00
title: Draadloos netwerk Huygensgebouw blijft dekkend
---
In het
[Huygensgebouw](http://www.ru.nl/fnwi/over_de_faculteit/huygensgebouw/)
is kort na de oplevering een volledig dekkend [wireless
netwerk](/nl/howto/netwerk-draadloos/) gerealiseerd. Uit metingen over
langere tijd is gebleken dat de dekking daarvan dusdanig is, dat zo’n 25
van de 90 access points als overtollig beschouwd kunnen worden. Eind
november - begin december 2007 worden deze access points dan ook
verwijderd, zodat ze op andere plekken binnen de RU opnieuw ingezet
kunnen worden. De volledige dekking in het Huygensgebouw blijft echter
gewaarborgd.
