---
author: caspar
date: 2011-09-01 17:14:00
tags:
- studenten
title: Intro ICT services for new students
---
In the first (introductory) week of the new college year a presentation
is given about the facultary ICT services, Blackboard, and the student
portal. These presentations are now [online](/en/tags/studenten).
