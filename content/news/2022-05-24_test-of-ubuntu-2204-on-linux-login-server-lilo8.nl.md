---
author: petervc
date: 2022-05-24 16:10:00
tags:
- medewerkers
- studenten
title: Test van Ubuntu 22.04 met Linux loginserver lilo8
cover:
  image: img/2022/ubuntu2204.png
---
Als test voor de nieuwe versie van Ubuntu, 22.04 LTS, hebben we een
nieuwe loginserver beschikbaar, onder de naam `lilo8.science.ru.nl`.
Over enige tijd zal deze de vier jaar oude loginserver `lilo6`
vervangen. De naam `lilo`, die altijd naar de nieuwste/snelste
loginserver wijst, zal pas dan omgezet worden van de twee jaar oude
`lilo7` naar de nieuwe `lilo8`. De nieuwe `lilo8` is een Dell PowerEdge
R6515 met 1 AMD 7502P 2.5GHz 32-core processor en 64GB RAM. Met
hyperthreading aan lijkt dit dus een 64-processor machine. Voor wie de
identiteit van deze server wil controleren alvorens het Science
wachtwoord aan de nieuwe server te geven, zie [de sectie over onze
loginservers](/nl/howto/hardware-servers/). Alle problemen met deze
nieuwe versie van Ubuntu Linux [graag melden](/nl/howto/contact/).
