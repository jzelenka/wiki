---
author: petervc
date: 2019-12-18 15:02:00
tags:
- medewerkers
- docenten
title: 'MS Windows 10: nieuwere versies op Install-schijf'
---
Nieuwere versies van [Microsoft
Windows](https://www.microsoft.com/en-us/windows), 10, zijn op de
[Install](/nl/howto/install-share/)-netwerkschijf te vinden. De
licentievoorwaarden staan upgrade naar deze versies op RU-computers toe.
Licentiecodes zijn bij C&CZ helpdesk of postmaster te verkrijgen. Voor
eigen PC’s kan de software goedkoop op
[Surfspot](http://www.surfspot.nl) besteld worden. De toegevoegde
versies zijn: 1809 met Multilanguage Pack en 1903.
