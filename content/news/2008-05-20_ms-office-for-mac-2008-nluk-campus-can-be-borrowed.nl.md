---
author: wim
date: 2008-05-20 17:02:00
title: MS Office for Mac 2008 NL/UK \*campus\* te leen
---
Een CD van [MS Office 2008 voor Mac](/nl/howto/macintosh/) is te leen
voor gebruik op de campus en Macs van afdelingen, zowel de NL- als de
UK-versie. Voor thuisgebruik kan men bij
[Surfspot](http://www.surfspot.nl) relatief goedkoop de software
aanschaffen, al is het altijd duurder dan het vrij verkrijgbare
[OpenOffice](http://www.openoffice.org).
