---
author: wim
date: 2008-09-03 00:12:00
title: New computer lab TK023
---
A new [computer lab](/en/howto/terminalkamers/) TK023 (HG00.023) is
available, with 41 modern PC’s with big 24" screens with high
resolution. For C&CZ this is a new type: the user can boot Windows-XP as
well as Fedora Linux. Such a dual- or multi-boot PC is the standard for
the future for the computer labs, with in the future almost certainly
also Windows-Vista next to Windows-XP.
