---
author: wim
date: 2012-02-28 15:53:00
tags:
- docenten
title: 'Teachers: test your course software in the summer\!'
---
C&CZ plans to reinstall all pc’s in the [computer
labs](/en/howto/terminalkamers/) with 64-bit Windows 7 and Ubuntu 12.04
during the summer holidays. Because it is not guaranteed that all
software just runs with these versions of the operating systems, you are
advised to reserve time for testing purposes. Test systems will be
available at C&CZ in time.
