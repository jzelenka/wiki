---
author: bram
date: 2014-05-23 17:25:00
tags:
- medewerkers
- studenten
title: Printerwachtrijen te zien op Doe Het Zelf pagina
---
Het bekijken van de gecombineerde wachtrij van alle printers is nu
mogelijk op de [DHZ pagina](/nl/howto/dhz/) van C&CZ. Tot nu toe was dit
alleen mogelijk via het commando
[lpq](http://manned.org/lpq/c2ebe2ea?v=a) op de door C&CZ beheerde
Ubuntu Linux loginservers en werkplekken. Nu kan dit dus vanaf alle
apparaten met een webbrowser.
