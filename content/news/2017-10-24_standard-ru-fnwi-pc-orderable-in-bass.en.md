---
author: john
date: 2017-10-24 16:08:00
tags:
- medewerkers
title: Standard RU-FNWI PC orderable in BASS
---
In the webshop of [Scholten Awater](http://www.sa.nl) in
[BASS](http://bass.ru.nl) a “Standaard PC RU-FNWI” can be chosen. C&CZ
tested this configuration with the network installation of Windows 7 and
Ubuntu 16.04. It is a Dell OptiPlex 3050 SFF with an Intel Core i5 6500
(3.2GHz,QC) processor, 1X8GB memory, a 256GB SSD hard disc and a DVD
Burner, with 3 years of ProSupport and Next Business Day warranty. This
system, with an Intel 6th generation processor, has support for Windows
7.
