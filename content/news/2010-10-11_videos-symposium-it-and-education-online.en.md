---
author: pberens
date: 2010-10-11 14:35:00
tags:
- docenten
title: Videos Symposium IT and Education online
---
The [videos (in
Dutch)](http://podcast.science.ru.nl/Podcasts/flashplaylist.php?courseID=oib)
of all presentations of the IT & Education Symposium which was held on
October 1, 2010 are recommended to all lecturers interested in
educational reform. Contributors: Drs. H.P.A.M. Geurts: Symposium
Education-ICT Opening Prof. dr. F.P.J.T. Rutjes: Chemical education in
silico Prof. dr. S.E. Speller: An STM for everybody (STM=Scanning
Tunneling Microscope) Dr. E.S. Pierson: Virtual Classroom Biology: the
springboard impact Dr. T.F. Oostendorp: ECGSIM (ECGSIM = Electro
CardioGram SIMulation) Prof. dr. E. Barendsen: Wiki-technology and
cooperative learning
