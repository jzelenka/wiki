---
author: wim
date: 2009-02-02 21:09:00
title: Computer lab usage available again
---
The [computer lab](/en/howto/terminalkamers/) usage is being
[monitored](http://graphs.science.ru.nl/graph_view.php?action=tree&tree_id=7)
again. [Cacti](http://www.cacti.net/) is used as graphing tool for the
data that are gathered with SNMP.
