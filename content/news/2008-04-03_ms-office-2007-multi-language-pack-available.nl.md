---
author: petervc
date: 2008-04-03 18:57:00
title: MS Office 2007 Multi-Language Pack beschikbaar
---
De CD en 2 dual-layer DVDs (13 GB totaal) van het MS Office 2007
Multi-Language Pack zijn nu [te leen](/nl/howto/microsoft-windows/) voor
gebruik op de campus.
