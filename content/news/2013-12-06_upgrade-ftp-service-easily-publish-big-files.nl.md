---
author: polman
date: 2013-12-06 18:10:00
tags:
- medewerkers
- docenten
title: 'Upgrade FTP service: simpel grote bestanden aanbieden'
---
De Science FTP service is vernieuwd. De service is zowel toegankelijk
via [het originele FTP-protocol](ftp://ftp.science.ru.nl/pub) als via
[het HTTP-protocol van het web](http://ftp.science.ru.nl). Dit kan de
simpelste manier zijn om grote bestanden ter beschikking te stellen van
anderen. Op de [serverpagina](/nl/howto/hardware-servers/) is te lezen
hoe men deze service kan gebruiken.
