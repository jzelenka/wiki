---
author: caspar
date: 2009-01-16 14:39:00
title: Department contacts instead of first line contact (person)
---
The title “first line contact (person)” for contacts for C&CZ in the
departments of the Faculty of Science was often misinterpreted by people
elsewhere because the term “first line support” in general is associated
with the ICT support groups. Therefore on the C&CZ wiki the term
“[department contact](/en/howto/:category:contactpersonen/)” is now used
instead.
