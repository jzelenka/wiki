---
author: petervc
date: 2010-08-31 14:29:00
title: Symposium Onderwijs-ICT bij afscheid Kees Keijzers
---
Bij gelegenheid van het afscheid van Kees Keijzers van de faculteit NWI
organiseren het Onderwijscentrum (OWC) en de afdeling Computer- en
Communicatiezaken (C&CZ) op vrijdag 1 oktober vanaf 13:15 uur een
symposium in het Linnaeusgebouw: “Simuleren en afkijken in de
collegezaal: onderwijsinnovatie met ICT”. Het symposium laat producten
zien voor het onderwijs, die het resultaat zijn van de nauwe
samenwerking tussen docenten en ICT’ers die op zeer korte afstand van de
docent/onderzoeker werken en die affiniteit hebben met het betreffende
vakgebied.

Voor het programma en gedetailleerde informatie, zie [de
uitnodiging](/download/old/symposium_onderwijs_-_ict%2c_afscheid_kees_keijzers.pdf).
