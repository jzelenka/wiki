---
author: petervc
date: 2013-09-03 15:45:00
tags:
- studenten
- medewerkers
- docenten
title: 'Laptop pool: Eduroam and guest login'
---
During the summer break we have changed the [laptop
pool](/en/howto/laptop-pool/). The laptops now connect to the [wireless
network Eduroam](/en/howto/netwerk-draadloos/). Also every laptop now has
a guest login, in order to be able to use the laptops without
authentication and network.
