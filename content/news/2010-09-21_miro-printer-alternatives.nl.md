---
author: mbouwens
date: 2010-09-21 18:29:00
title: Miro printer alternatieven
---
De oude [printer
miro](/nl/howto/printers-en-printen#.5bpostscript-kleurenprinters.5d.5bpostscript-color-printers.5d/)
is enkele weken defect geweest, maar nu weer gerepareerd. Er is geen
onderhoudscontract meer, we proberen miro aan de praat te houden, al was
het maar om de tonercartridges op te maken. Indien men de output van
deze Xerox Phaser 7300DX veel beter vindt dan die van de [andere (Ricoh,
HP)
kleurenprinters](/nl/howto/printers-en-printen#.5bpostscript-kleurenprinters.5d.5bpostscript-color-printers.5d/)
en ook die van de [RU copyshop](http://www.ru.nl/copyshop/), kan men
zich melden bij C&CZ, dan kan C&CZ de aanschaf van een nieuw exemplaar
overwegen.
