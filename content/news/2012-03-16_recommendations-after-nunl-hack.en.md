---
author: petervc
date: 2012-03-16 16:20:00
tags:
- studenten
- medewerkers
- docenten
title: Recommendations after nu.nl hack
---
The popular Dutch news website
[NU.nl](http://www.nu.nl/internet/2763447/korte-tijd-malware-verspreid-via-nunl.html)
has been compromised and altered to serve a malicious worm to those who
visited it on March 14, 2012 between 11:30 and 12:30. PC’s with older
versions of Adobe Reader and Java may have been infected with the
Sinowal worm, that is designed to steal passwords, online banking
information and other sensitive data. We heard reports that up-to-date
installations of [F-Secure Client
Security](http://www.radboudnet.nl/fsecure) blocked this malware.
According to
[ComputerIdee](http://www.computeridee.nl/nieuws/nunl-malware-gratis-te-verwijderen)
only a few security software suites recognized this malware. The version
of Hitmanpro mentioned there has been copied to the
[Install](/en/howto/install-share/) network share, for employees and
students who want to scan their PC.

General recommendations to limit risks:

-   Always update all software, especially browsers, mail clients and
    software used to show content from the Internet: Java, PDF-readers
    (Adobe, …), Office suites, Flash, …
-   Never trust a compromised system again: Clean the Master Boot
    Record, e.g. with [TDSSkiller by
    Kaspersky](http://support.kaspersky.com/faq/?qid=208283363) and do a
    fresh install of the OS, with all updates available.
