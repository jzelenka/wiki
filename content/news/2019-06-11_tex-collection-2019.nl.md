---
author: petervc
date: 2019-06-11 11:03:00
tags:
- studenten
- medewerkers
- docenten
title: TeX Collection 2019
---
De nieuwste versie van de [TeX
Collection](http://www.tug.org/texcollection), 2019, voor MS-Windows,
Mac OS X en Linux, is beschikbaar. De DVD is te vinden op de
[Install](/nl/howto/install-share/)-netwerkschijf.
