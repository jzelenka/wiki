---
author: polman
date: 2016-05-10 13:54:00
tags:
- studenten
- medewerkers
title: New version ChemOffice, license extended
---
The license of [ChemBioOffice](/en/howto/chembiooffice/) has been
extended till May 2019, there is also a new verson available ChemOffice
2015. This version will replace the old version from 2013 on all C&CZ
managed pc’s.
