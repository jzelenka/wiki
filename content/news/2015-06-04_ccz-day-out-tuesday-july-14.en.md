---
author: petervc
date: 2015-06-04 15:47:00
tags:
- medewerkers
- studenten
title: 'C&CZ day out: Tuesday July 14'
---
The C&CZ yearly day out is scheduled for Tuesday July 14. C&CZ can be
reached in case of serious disruptions of services.
