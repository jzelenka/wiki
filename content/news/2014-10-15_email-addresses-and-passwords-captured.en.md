---
author: petervc
date: 2014-10-15 17:11:00
tags:
- studenten
- medewerkers
- docenten
title: Email addresses and passwords captured
---
The
[CERT-RU](http://www.ru.nl/informatiebeveiliging/ru-professionals/cert-ru-0/)
has sent a [warning
mail](http://www.ru.nl/systeem-meldingen/?id=69&lang=nl&tg=0&f=0) to the
600 ru.nl mail addresses that were in the [list of 1.3 million captured
Dutch
accounts](http://tweakers.net/nieuws/99018/ruim-miljoen-nederlandse-e-mailadressen-en-wachtwoorden-buitgemaakt.html).
That ru.nl mail address has apparently been used as a registration
address in some website, that has been hacked. You must assume that
passwords that have been used in such a hacked site are now known to
Internet criminals. Therefore, you should never use that password
anywhere again. Furthermore, everyone is advised to use different
passwords for different websites/applications. Often you do not even
have to remember these passwords, as they can be easily reset. In other
cases, you can use a password vault such as
[KeePass](http://keepass.info/download.html), which is available for
Windows, Apple, Android and Linux.
