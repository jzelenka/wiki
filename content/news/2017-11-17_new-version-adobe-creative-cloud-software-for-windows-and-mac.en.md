---
author: petervc
date: 2017-11-17 15:56:00
tags:
- medewerkers
- docenten
title: New version Adobe Creative Cloud software for Windows and Mac
---
A new version of the [Adobe Creative Cloud
software](https://www.adobe.com/nl/creativecloud/catalog/desktop.html)
for MS-Windows and Apple Mac is available on the
[Install](/en/howto/install-share/) network share. License codes for the
period 2017-2020 have been built-in, therefore the software is available
in password-protected zip-files. The passwords are available from C&CZ
helpdesk or postmaster. According to the site license, this Adobe
Creative Cloud for Enterprise (Specified Apps Only Non-Video) may be
installed on all computers owned by RU without additional costs. For
home use of this software, staff and students can visit
[SurfSpot](http://www.surfspot.nl). The software consists of the
following components: Acrobat DC 2017-2020-ML, Audition CC 2017 (Windows
only), Bridge CC 2017, Dreamweaver CC 2017, Fireworks CS6 (Windows
only), Flash Builder Premium (Windows only), Illustrator CC 2017, InCopy
CC 2017, InDesign CC 2017, Media Encoder CC 2017, Muse CC 2017 (Windows
only) and Photoshop CC 2017.
