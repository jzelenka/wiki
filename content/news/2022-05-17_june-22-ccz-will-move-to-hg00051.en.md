---
author: petervc
date: 2022-05-17 15:05:00
tags:
- studenten
- medewerkers
- docenten
title: June 22, CCZ will move to HG00.051
---
To make room for the expansion of IMM departments, C&CZ will move June
22 to the ground floor of the Huygens building. The [C&CZ
helpdesk](/en/howto/contact/) will still be directly accessible on the
main street with an entrance at HG00.051.
