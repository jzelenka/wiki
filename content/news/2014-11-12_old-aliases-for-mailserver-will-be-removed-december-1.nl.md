---
author: petervc
date: 2014-11-12 11:03:00
tags:
- studenten
- medewerkers
- docenten
title: Oude aliases voor mailserver worden 1 december verwijderd
---
C&CZ zal de aliassen voor imap, imap-srv, imap-server, pop, pop-srv,
pop-server, smtp, smtp-srv en smtp-server voor domeinen cs, math, nmr,
sci en theochem verwijderen per 1 december . De [automatische
configuratie van mailclients](/nl/howto/categorie%3aemail/) gebruikt
alleen post.science.ru.nl en smtp.science.ru.nl, die men ook moet
gebruiken bij het [handmatig configureren van een
mailclient](/nl/howto/categorie%3aemail/). Aangezien het certificaat van
de mailserver ook niet geldig is voor deze oude aliassen, is het uit
beveiligingsoogpunt beter om ze te verwijderen.
