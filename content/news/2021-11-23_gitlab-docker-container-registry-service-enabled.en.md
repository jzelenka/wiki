---
author: bram
date: 2021-11-23 14:50:00
tags:
- studenten
- medewerkers
title: GitLab Docker Container Registry service enabled
---
In the C&CZ operated [GitLab](/en/howto/gitlab/) service, GitLab Docker
Container Registry service has been enabled. For more information,
please have a look at the [GitLab
documentation](https://gitlab.science.ru.nl/help/user/packages/container_registry/index.md).
