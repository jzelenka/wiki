---
author: mkup
date: 2014-02-25 12:06:00
tags:
- medewerkers
- studenten
title: Eduroam and wired network adjustments
---
After complaints about the [Eduroam
coverage](http://www.ru.nl/draadloos) in the Huygens building, a plan
has been made to move three wireless access points and nine will be
added. We expect this to be realized before the end of April.

The planned [changes in the wired network outside working hours](/en/cpk/) in preparation of [IP
telephony](/en/tags/telefonie/), have been postponed, because it
appeared that the network had become less stable.

C&CZ invites everyone to report problems with the wireless and wired
network to netmaster\@science.ru.nl.
