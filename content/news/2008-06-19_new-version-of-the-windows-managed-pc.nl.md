---
author: wim
date: 2008-06-19 16:10:00
title: Nieuwe versie van de Windows Beheerde Werkplek PC
---
Er is een nieuwe installatiemethode voor de [Windows-XP Beheerde
Werkplek PCs](/nl/howto/windows-beheerde-werkplek/), die door diverse
groepen werkplekondersteuners op de RU samen gemaakt is en onderhouden
wordt in het kader van het SITO (Samenwerkende IT-Ondersteuning)
project. Het voornaamste verschil is dat er meer software geïnstalleerd
is, die ook minder vaak afhankelijk is van een netwerkschijf. Veel PC’s
in het [Studielandschap](/nl/howto/terminalkamers/) en diverse
afdelings-PCs zijn op de nieuwe manier geïnstalleerd.
