---
author: petervc
date: 2015-04-09 17:10:00
tags:
- software
cover:
  image: img/2015/labview.png
title: National Instruments LabVIEW on Install network share
---
To make it easier to install [NI LabVIEW](http://www.ni.com/labview/)
for departments that participate in the license, all DVDs of the “Spring
2015” version have been copied to the [Install](/en/howto/install-share/)
network share. License codes can be obtained from C&CZ helpdesk or
postmaster.
