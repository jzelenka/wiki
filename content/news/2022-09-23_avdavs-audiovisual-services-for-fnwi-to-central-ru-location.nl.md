---
author: bbellink
date: 2022-09-23 13:13:00
tags:
- medewerkers
- studenten
title: AVD/AVS Audiovisual Services voor FNWI naar centrale RU-locatie
cover:
  image: img/2022/beamer.jpg
---
Vanaf maandag 3 oktober zullen Sacha Djuric en Jelle de Koter van
AVD/AVS Audiovisual Services werken vanuit de centrale locatie Thomas
van Aquinostraat 1.00.22 voor alle faculteiten en diensten. Het nieuwe
telefoonnummer wordt [+31 24 36 12680](tel:+31 24 36 12680). De vaakst afgehaalde items kunnen
dan gehaald worden bij de [C&CZ helpdesk](/nl/howto/contact/):
verloopkabels, beamers voor de studieverenigingen, kabels, batterijen,
afstandbedieningen en een mobiele USB-camera op statief met microfoon
(o.a. voor hybride onderwijs). Bij vragen, benader
[C&CZ](/nl/howto/contact/).
