---
author: petervc
date: 2012-05-31 13:31:00
tags:
- studenten
- medewerkers
- docenten
title: 'Phishing mail: Geweldig nieuws\!'
---
Sinds 31 mei krijgen medewerkers van de RU een phishing mail met als
onderwerp “Geweldig nieuws!”. In de mail staat een link naar een
verkorte URL van <http://goo.gl/> . Wanneer men erin trapt en op de link
klikt, dan komt men op een site van [Google
Docs](https://docs.google.com/) terecht die eruit ziet alsof die van de
RU zou kunnen zijn. Indien men dan dom genoeg is om in te loggen met het
RU U-nummer en RU-wachtwoord, dan zijn die op dat moment bekend bij
internet-criminelen, die o.a. kunnen proberen om via deze
gebruikersgegevens spam te versturen.
