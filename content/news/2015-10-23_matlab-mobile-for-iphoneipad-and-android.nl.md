---
author: petervc
date: 2015-10-23 13:04:00
tags:
- studenten
- medewerkers
- docenten
title: Matlab Mobile voor iPhone/iPad en Android
---
Matlab [Mobile](http://nl.mathworks.com/mobile/) is een lichtgewicht
Matlab desktop voor iPhone/iPad en Android, die verbinding maakt met een
Matlab-sessie in de MathWorks Cloud of op je computer. Vanaf smartphone
of tablet kunnen typische Matlab-taken als evalueren van commando’s,
uitvoeren van scripts, maken van figuren en manipuleren en bekijken van
resultaten uitgevoerd worden. Speciale toetsenborden voor iOS en Android
helpen om eenvoudig Matlab-syntax in te voeren. Om het te gebruiken moet
de app gedownload worden van de App Store of Play Store en moet een
[Mathworks account gemaakt
worden](https://nl.mathworks.com/mwaccount/register?uri=%2Fmwaccount%2F)
met een email adres dat eindigt op: @science.ru.nl,
@student.science.ru.nl, @donders.ru.nl, @pwo.ru.nl, @let.ru.nl,
@fm.ru.nl, @ai.ru.nl, @socsci.ru.nl, @ru.nl, @student.ru.nl of
@radboudumc.nl .
