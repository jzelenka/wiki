---
author: caspar
date: 2013-03-13 18:11:00
tags:
- medewerkers
- docenten
title: New Functional Manager CMS
---
For many years everyone at FNWI who was involved in websites based on
the RU Content Management System ([CMS](/en/howto/cms/)) relied on Peter
Klok for help and advice. In IT terms Peter was our functional manager.
Because of Peter’s retirement this role was taken over by C&CZ. This
means that all support questions regarding the websites of the Faculty
of Science, regardless of the underlying technique, can be directed to
C&CZ. In practice the support task for the CMS is in the hands of Wim
G.H.M. Janssen, IT specialist at the IMAPP institute. C&CZ (especially
Fred Melssen) will function as Wim’s backup.
