---
author: wim
date: 2015-02-20 14:12:00
tags:
- medewerkers
- studenten
title: Terminalkamers upgrade naar Ubuntu 14.04 tijdens zomervakantie
---
In de zomermaanden zal het operating system van alle PC’s in de
[terminalkamers](/nl/howto/terminalkamers/) een upgrade krijgen van
Ubuntu 12.04 naar Ubuntu 14.04. Dit kan mogelijk consequenties hebben
voor software die u bij cursussen gebruikt. U kunt testen op de
loginserver lilo4.science.ru.nl, die al van Ubuntu 14.04 is voorzien.
