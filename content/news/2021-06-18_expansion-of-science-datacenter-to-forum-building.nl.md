---
author: sioo
date: 2021-06-18 11:18:00
tags:
- medewerkers
- studenten
title: Uitbreiding datacenter FNWI naar Forumgebouw
---
Omdat FNWI-onderzoeksgroepen in toenemende mate krachtige
computers/rekenclusters, met veel CPU’s en GPU’s, aanschaffen, heeft
C&CZ het datacenter uitgebreid naar de bestaande serverruimte van
Radboud Services, in het Forum aan de overkant van de Heijendaalseweg.
Daardoor zal er de komende jaren voldoende fysieke ruimte, stroom en
koeling zijn voor de door afdelingen aangeschafte servers.
