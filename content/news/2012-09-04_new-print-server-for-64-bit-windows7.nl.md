---
author: wim
date: 2012-09-04 16:14:00
tags:
- studenten
- medewerkers
- docenten
title: Nieuwe printserver voor 64-bits Windows7
---
Vanwege de overgang van Windows-XP naar 64-bits Windows7 in de
[terminalkamers](/nl/howto/terminalkamers/) hebben we een nieuwe
printserver met de naam ‘printto.science.ru.nl’ moeten installeren. Voor
alle [gebudgetteerde printers](/nl/howto/printers-en-printen/) moet deze
gebruikt worden. Zie [onze printerpagina](/nl/howto/printers-en-printen/)
voor details over hoe een printer onder Windows7 aangekoppeld moet
worden.
