---
author: petervc
date: 2008-01-23 10:59:00
title: New Maple license file, valid until January 2009
---
The license file for [Maple](/en/howto/maple/) was almost expired,
therefore a new version has been installed on the license server.
Employees and students with a home-use license can get a new version,
valid until January 2009, through C&CZ.
