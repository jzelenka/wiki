---
author: petervc
date: 2014-04-17 13:08:00
tags:
- studenten
- medewerkers
title: ArcGIS campus license?
---
The [ArcGIS](http://www.arcgis.com) (Geographical Information System)
software suite is used increasingly within the [Nijmegen School of
Management](http://www.ru.nl/nsm/), so much that the current number of
available licenses is no longer sufficient. They therefore want to know
whether other faculties want to participate in a campus license for
ArcGIS. A campus license for three years costs €5500 the first year and
€8,500 subsequent years. If interested, please [contact
C&CZ](/en/howto/contact/).
