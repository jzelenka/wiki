---
author: wim
date: 2016-08-08 12:13:00
tags:
- medewerkers
- studenten
title: "C&CZ printing system will be replaced by Péage"
---
Péage is the print/scan/copy system of Radboud University for staff and
students. General information can be found on the [Péage
website](http://www.ru.nl/fb/english/print/printing-peage-0/).

As of August 4, the [C&CZ printing and budget
system](/en/howto/printers-en-printen/) is being replaced by the campus
wide [Péage printing
system](http://www.ru.nl/fb/english/print/printing-peage-0/). It is
possible to setup Péage now and test ‘Follow-me’ printing on printers
that have already been switched to Péage.

On the [C&CZ Péage page](/en/howto/peage/), C&CZ gives information
specific for staff and students of FNWI.
