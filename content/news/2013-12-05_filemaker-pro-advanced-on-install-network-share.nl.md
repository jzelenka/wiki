---
author: john
date: 2013-12-05 12:39:00
tags:
- medewerkers
- docenten
title: Filemaker Pro Advanced op Install-schijf
---
Een nieuwe versie van [Filemaker Pro
Advanced](http://www.filemaker.com/nl/products/filemaker-pro/), 12, is
op de [Install](/nl/howto/install-share/)-netwerkschijf te vinden. De
licentievoorwaarden staan gebruik op RU-computers toe. Licentiecodes
zijn bij C&CZ helpdesk of postmaster te verkrijgen. Voor thuisgebruik
kan de software op [Surfspot](http://www.surfspot.nl) besteld worden.
