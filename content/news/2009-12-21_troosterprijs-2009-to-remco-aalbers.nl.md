---
author: petervc
date: 2009-12-21 12:07:00
title: Troosterprijs 2009 voor Remco Aalbers
---
De
[Troosterprijs](http://www.ru.nl/fnwi/physchem/solid_state_nmr/prof_dr_jan/)
2009 is toegekend aan {{< author "remcoa" >}}. Een van de systemen die
hij ontwikkelde is de webtoepassing RUMBA (RU Master BAchelor) voor het
maken en aanbieden van studieinformatie. Enkele andere aansprekende
voorbeelden van zijn werk zijn ROB (Rapportage OnderwijsBelasting), de
experimentele portfolio voor studenten, [de Virtual Classroom voor
Biologie](http://www.vcbio.science.ru.nl/) en de wiki’s. De
prijsuitreiking vond plaats op vrijdag 18 december, 16:00 uur in
HG00.304. In zijn presentatie ging Aalbers niet diep in op de
ontwikkelde systemen, maar legde de gevolgde ontwikkelmethode uit:
[Agile
software-ontwikkeling](http://nl.wikipedia.org/wiki/Agile-software-ontwikkeling)
van [web-applicaties](http://nl.wikipedia.org/wiki/Webapplicatie) op
basis van [open
standaarden](http://nl.wikipedia.org/wiki/Open_standaard) en [open
source](http://nl.wikipedia.org/wiki/Open_source).
