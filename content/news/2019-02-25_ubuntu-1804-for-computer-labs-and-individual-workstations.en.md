---
author: wim
date: 2019-02-25 18:00:00
tags:
- medewerkers
- docenten
- studenten
title: Ubuntu 18.04 for computer labs and individual workstations
---
During the summer break all [computer labs](/en/howto/terminalkamers/)
will be (re)installed with Ubuntu 18.04 LTS. Since months ago, one can
test course software using the Ubuntu 18.04
[loginserver](/en/howto/hardware-servers/) lilo6. All PC’s in HG00.075
and HG00.761 will be replaced by new ones, (All-in-One, dual boot with
Windows10 and Ubuntu 18.04 Linux). After this replacement, al PC’s in
the computer labs have fast SSD disks.

If you are using Linux PC’s in [computer labs](/en/howto/terminalkamers/)
as well as a C&CZ managed Linux PC in your office, we advise you to
consider upgrading your office PC to Ubuntu 18.04 as well. Running
Ubuntu 16.04 in your office on a managed PC and using an Ubuntu 18.04 PC
in the [computer labs](/en/howto/terminalkamers/) will result in
problems.
