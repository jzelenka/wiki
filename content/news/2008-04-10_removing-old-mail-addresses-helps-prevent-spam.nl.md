---
author: josal
date: 2008-04-10 12:48:00
title: Opruimen oude mail-adressen helpt spam voorkomen
---
Op dit moment (mei 2008) komt het regelmatig voor dat plotseling veel
spam verstuurd wordt vanaf diverse plekken op het Internet, met als
(vervalst) afzender-adres een oud “@sci.kun.nl” adres. Voor een eigenaar
van zo’n adres levert dat veel overlast op, omdat de spam die niet
afgeleverd kan worden “terug” gaat naar het gebruikte afzender-adres. Om
die overlast te verminderen is het een goed idee om op de [DHZ
website](http://dhz.science.ru.nl) te kijken welke oude “kun.nl”
afdressen weg kunnen en dat te mailen naar postmaster.
