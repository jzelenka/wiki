---
author: petervc
date: 2016-12-16 16:30:00
tags:
- studenten
- medewerkers
- docenten
title: Donatie aan vrije en open source software
---
C&CZ maakt voor het merendeel van de services gebruik van [vrije
software](https://nl.wikipedia.org/wiki/Vrije_software) en
[opensourcesoftware](https://nl.wikipedia.org/wiki/Opensourcesoftware).
Daarom is al enige tijd geleden het idee ontstaan dat de
C&CZ-medewerkers zouden stemmen welke projecten een donatie van C&CZ
zouden krijgen. Dit jaar is de keus gevallen op de [Free Software
Foundation
(FSF)](https://nl.wikipedia.org/wiki/Free_Software_Foundation) en
[LimeSurvey](https://www.limesurvey.org/). De FSF ondersteunt GNU/Linux,
dat op vrijwel alle door C&CZ beheerde servers en vele honderden
werkstations gebruikt wordt, waaronder alle
[PC-cursuszalen](/nl/howto/terminalkamers/). LimeSurvey wordt sinds 2008
gebruikt voor het houden van enquêtes. Sinds eind 2011 wordt het voor
alle ca. 500 FNWI-cursusevaluaties per jaar gebruikt. Daarnaast wordt
het ook door afdelingen (als PUC, ISIS en OWC) en individuele
medewerkers en studenten van FNWI gebruikt. Zie voor meer info onze
[LimeSurvey](/nl/howto/limesurvey/)-pagina.
