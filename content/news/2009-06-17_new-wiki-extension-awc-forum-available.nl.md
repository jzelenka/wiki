---
author: remcoa
date: 2009-06-17 16:02:00
title: Nieuwe extensie AWC Forum op wikis beschikbaar
---
Een nieuwe forum extensie is nu beschikbaar voor alle wikis, zie [de
geïnstalleerde MediaWiki
extensies](/nl/howto/geïnstalleerde-mediawiki-extensies#awc-forum/) voor
meer informatie.
