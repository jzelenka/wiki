---
author: wim
date: 2014-11-14 12:51:00
tags:
- studenten
- medewerkers
- docenten
title: 'Schijven xpsoftware en xpcursus nieuwe namen: software en cursus'
---
De op veel pc’s gebruikte [netwerkschijven](/nl/howto/netwerkschijf/)
`xpsoftware` en `xpcursus` zijn hernoemd naar `software` en `cursus`. De
oude namen werken nog tot maandag 5 januari 2015. Deze schijven worden
ook als software-netwerkschijf [S-schijf](/nl/howto/s-schijf/) en
cursussoftware-netwerkschijf [T-schijf](/nl/howto/t-schijf/) aangekoppeld
op door C&CZ beheerde pc’s. Docenten kunnen zelf hun cursussoftware op
de [T-schijf](/nl/howto/t-schijf/) bijhouden.
