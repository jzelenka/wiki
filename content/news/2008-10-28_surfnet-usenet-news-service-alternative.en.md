---
author: petervc
date: 2008-10-28 16:49:00
title: Surfnet Usenet News service alternative
---
The UCI
[announces](http://www.ru.nl/uci/actueel/nieuws/Usenet_via_surfspot/) an
alternative for the Surfnet Usenet News service that ended in July: the
[Hitnews](http://www.hitnews.eu/) newsgroups. Through
[SurfSpot](http://www.surfspot.nl) it costs 40 euro/year.
