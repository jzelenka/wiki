---
author: petervc
date: 2022-03-02 12:35:00
tags:
- medewerkers
- studenten
title: Nieuwe versie van Mathematica (13.0.1)
cover:
  image: img/2022/mathematica-13.png
---
Er is een nieuwe versie (13.0.1) van
[Mathematica](/nl/howto/mathematica/) geïnstalleerd op alle door C&CZ
beheerde Linux systemen, oudere versies zijn nog in `/vol/mathematica`
te vinden. De installatiebestanden voor Windows, Linux en macOS zijn op
de [Install](/nl/howto/install-share/)-schijf te vinden, ook voor oudere
versies. Licentie- en installatiegegevens zijn bij
[C&CZ](/nl/howto/contact/) te verkrijgen.
