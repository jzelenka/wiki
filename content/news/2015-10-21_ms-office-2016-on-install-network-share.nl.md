---
author: wim
date: 2015-10-21 16:05:00
tags:
- medewerkers
- docenten
title: MS Office 2016 op Install-schijf
---
De nieuwste versie van [Microsoft Office](http://office.microsoft.com),
2016, is op de [Install](/nl/howto/install-share/)-netwerkschijf te
vinden. De licentievoorwaarden staan installatie op RU-computers toe.
Licentiecodes zijn bij C&CZ helpdesk of postmaster te verkrijgen. Voor
eigen PC’s kan de software goedkoop op
[Surfspot](http://www.surfspot.nl) besteld worden.
