---
author: fmelssen
date: 2014-09-24 12:10:00
tags:
- medewerkers
- studenten
title: 'Shakespeak: online voting and asking questions during presentations'
---
As of today, Radboud University has a license to use
[Shakespeak](http://www.shakespeak.com). Shakespeak is a plugin for
Powerpoint, which lets you add a question or vote to a slide. Students
can give their votes or comments (Internet, Twitter and SMS) and the
results are directly available in your presentation. More questions:
blackboard\@science.ru.nl
