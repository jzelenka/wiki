---
author: petervc
date: 2009-12-21 12:07:00
title: Troosterprijs 2009 to Remco Aalbers
---
The [Trooster
Award](http://www.ru.nl/fnwi/physchem/solid_state_nmr/prof_dr_jan/) 2009
has been awarded to {{< author "remcoa" >}}. One of the systems he
developed is the web application RUMBA (RU Master Bachelor) for creating
and consulting course information. Some other highlights of his work:
the education pressure report system ROB, the experimental student
portfolio, [the Virtual Classroom for
Biology](http://www.vcbio.science.ru.nl/) and the wiki’s. The
presentation of the award was Friday December 18, 16:00 hours in
HG00.304. In his presentation Aalbers didn’t expand on the developed
systems, but explained his method of development: [Agile software
development](http://en.wikipedia.org/wiki/Agile_software_development) of
[web applications](http://en.wikipedia.org/wiki/Web_application) based
on [open standards](http://en.wikipedia.org/wiki/Open_standard) and
[open source](http://en.wikipedia.org/wiki/Open_source).
