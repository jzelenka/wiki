---
author: stefan
date: 2012-09-21 15:21:00
tags:
- studenten
- medewerkers
title: Opruiming oude PC's 2012
---
Update 27 september: *Alle PC’s zijn inmiddels weg.* Volgend jaar zal er
waarschijnlijk weer zo’n actie komen.

Vanaf 24 september kunnen bij C&CZ oude PC’s opgehaald worden, die o.a.
bij de vervanging van de PC’s in de
[terminalkamers](/nl/howto/terminalkamers/) afgelopen zomer vrijgekomen
zijn. Deze PC’s zijn i.h.a. zo’n 5 jaar oud. De harde schijf is
verwijderd. De PC’s worden zonder enige garantie weggegeven. Medewerkers
en studenten van FNWI die belangstelling hebben voor een van deze PC’s,
kunnen een afspraak maken via mail naar de [C&CZ
helpdesk](/nl/howto/helpdesk/).
