---
author: pberens
date: 2010-10-11 13:57:00
tags:
- docenten
title: Blackboard and the Student Portal
---
The [Student
Portal](/en/howto/blackboard#.5bblackboard-en-de-nieuwe-studentenportal.5d.5bblackboard-and-the-new-student-portal.5d/)
will become the central daily entry point to RU web services for
students. The portal shows, amongst other things, the most recent
Blackboard announcements.
