---
author: petervc
date: 2009-03-20 12:21:00
title: Van Dale dictionaries online
---
`The UB has a license for the Van Dale dictionaries`[`online`](http://www.ru.nl/ubn/op_bezoek/sociaal-culturele/nieuws/van_dale/)`. These can also be used at home.`
