---
author: petervc
date: 2013-09-03 14:06:00
tags:
- studenten
- medewerkers
- docenten
title: KM printers/copiers/scanners in bedrijf, Ricohs verwijderd
---
Alle geplande [Konica Minolta MFP’s](/nl/howto/km/) zijn nu in bedrijf,
alle Ricohs zijn uitgezet. Enkele printers zijn door KM op zwart/wit
ingesteld, zodat men niet onbedoeld kleur kan printen. Ook raden we aan
te proberen de printerinstelling op de eigen werkplek standaard op
zwart/wit te zetten. [Printen](/nl/howto/printers-en-printen/) en
[scannen](/nl/tags/scanners/) gaat vrijwel hetzelfde als op de Ricohs.
FNWI-medewerkers kunnen [van C&CZ](/nl/howto/contact/) een pincode
krijgen om te kunnen kopiëren op de nieuwe KM MFP’s. Studenten kunnen
[kopiëren op de
Peage-MFP](http://www.ru.nl/ictservicecentrum/studenten/peage/kopieren/)
bij het restaurant, of ze kunnen scannen en daarna printen.
