---
author: theon
date: 2009-06-17 14:26:00
title: Vrij beschikbare anti-RSI software
---
De vrij beschikbare anti-RSI software
[Workrave](http://www.workrave.org/welcome/) is op alle door C&CZ
beheerde Windows pc’s beschikbaar. Als Workrave niet vanzelf opstart,
kan men zelf `Start -> Programs -> Workrave` gebruiken. Wanneer men
Workrave niet automatisch op wil laten starten, kan dat via
`Start->Programs->Startup` en daar `Workrave` verwijderen.
