---
author: caspar
date: 2009-02-09 14:22:00
title: 1234567890 seconden sinds The Epoch
---
A.s. zaterdag 14 februari om 00:31:30 Nederlandse tijd bereikt de in de
Unix wereld gangbare
[tijdaanduiding](http://en.wikipedia.org/wiki/Unix_time), namelijk het
aantal seconden sinds 1 januari 1970 UTC (‘The
[Epoch](http://en.wikipedia.org/wiki/Epoch_(reference_date))’), het
“mooie” getal [1234567890](http://www.coolepochcountdown.com/).
Misschien nog wel spannender is dat in de vroege ochtend van 19 januari
2038 de tikker de maximale waarde bereikt die in een 32 bit signed
integer past. Gelukkig is er nog voldoende tijd om een oplossing te
bedenken voor deze uitdaging waarbij het [Y2K
probleem](http://en.wikipedia.org/wiki/Y2K) volkomen verbleekt.
