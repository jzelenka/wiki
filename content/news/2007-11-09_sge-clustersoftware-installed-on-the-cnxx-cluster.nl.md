---
author: petervc
date: 2007-11-09 17:52:00
title: "SGE Clustersoftware geïnstalleerd op het cnXX-cluster"
---
Op het cnXX-rekencluster met machines van diverse afdelingen is [Sun
GridEngine](http://gridengine.sunsource.net) clustersoftware
geïnstalleerd. Voor gebruik is het lezen van de
[clustersoftware](/nl/howto/software-cluster/) pagina aan te raden.
