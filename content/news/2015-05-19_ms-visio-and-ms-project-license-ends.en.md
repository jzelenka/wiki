---
author: wim
date: 2015-05-19 17:12:00
tags:
- medewerkers
- docenten
- studenten
title: MS Visio and MS Project license ends
---
MS [Visio](https://products.office.com/nl-nl/visio/flowchart-software)
and MS
[Project](https://products.office.com/en-us/project/project-and-portfolio-management-software)
are no longer part of the [previously announced](/en/news/) new
agreement 2015-2017 between SURFmarket and Microsoft. Both Visio and
Project have a grace period of 1 year, which means that previously
installed versions of these products may be used until the end of 2015
at no additional cost. To avoid surprises by January 2016, C&CZ will
already remove Visio and Project from C&CZ [Managed
pcs](/en/howto/windows-beheerde-werkplek/) at the end of July, during the
summer break.
