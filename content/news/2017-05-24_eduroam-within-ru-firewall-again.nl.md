---
author: petervc
date: 2017-05-24 19:32:00
tags:
- medewerkers
- studenten
title: Eduroam terug binnen RU firewall
---
Het [ISC](http://www.ru.nl/isc) heeft het [draadloze
Eduroam-netwerk](http://www.ru.nl/ict/medewerkers/wifi/) op de avond van
22 mei weer terug binnen de RU firewall gezet. In december 2015 was dit
erbuiten geplaatst, om de performanceproblemen van de oude firewall het
hoofd te bieden. De in december 2016 geplaatste nieuwe firewall heeft
veel meer performance, daarom kon deze actie gepland worden.
