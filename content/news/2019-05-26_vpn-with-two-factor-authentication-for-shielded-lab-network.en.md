---
author: visser
date: 2019-05-26 00:10:00
tags:
- studenten
- medewerkers
- docenten
title: VPN with two-factor authentication for shielded lab-network
---
The [Science VPN](/en/howto/vpn/) has a new technique, in which
individual users can get access to restricted parts of the network with
two-factor authentication (username/password and certificate). This has
been developed on request of a department that wants to give maintenance
partners access to only the equipment that they service. Departments
that are interested in this access, can [contact
C&CZ](/en/howto/contact/).
