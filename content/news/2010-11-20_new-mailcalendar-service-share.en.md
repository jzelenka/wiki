---
author: caspar
date: 2010-11-20 21:05:00
title: New mail/calendar service Share
---
The UCI has developed a new combined mail and calendar service for the
RU. The service called Share is based on the open standards product
Zimbra. All RU students and a selection of RU employees are already
using Share. Deployment of Share at FNWI depends on the migration of the
FNWI Exchange accounts to Share. This migration in turn requires a
number of shortcomings in Share to be resolved. At the moment it is
unclear when Share will be available for FNWI.
