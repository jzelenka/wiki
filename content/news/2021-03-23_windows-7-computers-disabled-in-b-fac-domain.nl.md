---
author: wim
date: 2021-03-23 10:22:00
tags:
- medewerkers
- docenten
title: Windows 7 computers disabled in B-FAC domain
---
I.v.m. het verscherpen van de beveiliging worden de laatste Windows 7
machines per 24-03-20221 in het Active Directory Domain B-FAC
gedisabled. Verzoek is al sinds lang om de betreffende machines naar een
meer up-to-date OS te upgraden. Zie evt. eerdere aankondigingen over
[Windows
10](https://wiki.cncz.science.ru.nl/Nieuws#.5BMicrosoft_Windows_10_upgrade.5D.5BMicrosoft_Windows_10_upgrade.5D)
en [het einde van Windows
7](https://wiki.cncz.science.ru.nl/Nieuws_archief#.5BWindows_7_stopt_januari_2020:_Upgrade_nu.21.5D.5BWindows_7_ends_January_2020:_Upgrade_now.21.5D).
