---
author: petervc
date: 2008-09-03 12:23:00
title: Overlast voor collega's door reactie op phishing mail (waarin om wachtwoord
  gevraagd wordt)
---
Een week geleden reageerde een al te argeloze medewerker op een phishing
mail door het sturen van loginnaam en wachtwoord. Even later werd dat
misbruikt om grote hoeveelheden spam via onze webmailserver te
versturen. Daardoor kwam een mailserver van ons op een zwarte lijst te
staan, waardoor allerlei organisaties tijdelijk geen mail meer van ons
accepteerden. Graag wil C&CZ daarom \*nogmaals\* benadrukken dat het
uiterst onverstandig is om voetstoots alles te geloven wat in een brief
of e-mail staat en dat \*geen enkele officiele instantie\* u ooit per
email of telefonisch om uw wachtwoord zal vragen, net zomin als om uw
pincode van uw bankrekening.
