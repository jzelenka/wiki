---
author: petervc
date: 2012-03-13 23:43:00
tags:
- studenten
- medewerkers
- docenten
title: New Matlab license server (old one goes away\!) and Matlab R2012a
---
There is a new license server for the use of [Matlab](/en/howto/matlab/).
Users entitled to a license can request the new license codes by sending
a mail to postmaster. **The old license server will be brought
down in the not too distant future**, so to avoid problems,
all installations of Matlab have to be updated, also older versions! The
latest version of [Matlab](/en/howto/matlab/), R2012a, is available for
departments that have bought licenses. The software and license codes
can be acquired through a mail to postmaster for those entitled to it.
The software can also be found on the [install](/en/tags/software)-disc.
The C&CZ-managed 64-bit Linux machines have this version installed, an
older version (/opt/matlab-R2011b/bin/matlab) is still available
temporarily. The C&CZ-managed Windows machines will receive this version
too.
