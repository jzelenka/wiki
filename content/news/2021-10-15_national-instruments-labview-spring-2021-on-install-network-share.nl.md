---
author: petervc
date: 2021-10-15 11:59:00
tags:
- software
cover:
  image: img/2021/labview.png
title: National Instruments LabVIEW Spring 2021 op Install-schijf
---
Om het voor de afdelingen die meebetalen aan de licentie van [NI
LabVIEW](https://www.ni.com/nl-nl/shop/labview.html) makkelijker te
maken om LabVIEW te installeren, is de zojuist binnengekomen versie
“Spring 2021” op de [Install](/nl/howto/install-share/)-netwerkschijf
gezet. Installatiemedia zijn ook te leen. Licentiecodes zijn bij C&CZ
helpdesk of postmaster te verkrijgen. Voor [macOS/Linux is [deze versie
noodzakelijk](https://www.ni.com/nl-nl/innovations/white-papers/15/install-ni-academic-software-for-mac-os-x-and-linux.html),
voor MS Windows kan de [installatie ook via de website van
NI](https://knowledge.ni.com/KnowledgeArticleDetails?id=kA00Z0000019Ke3SAE&l=nl-NL).
