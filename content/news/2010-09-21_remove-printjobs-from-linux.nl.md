---
author: sommel
date: 2010-09-21 18:44:00
title: Print-opdrachten verwijderen onder Linux
---
Het verwijderen van een aangeleverde print-opdracht is nu ook vanaf
Linux mogelijk. Dit kan m.b.v. het
[lprm](http://ss64.com/bash/lprm.html) commando. Het werkt op alle door
C&CZ beheerde Linux [servers en
werkplekken](/nl/howto/hardware-servers/), zowel 32-bit als 64-bit.
