---
author: wim
date: 2015-04-28 16:01:00
tags:
- medewerkers
- docenten
- studenten
title: Linux loginserver lilo wordt alias voor lilo4
---
De naam `lilo`, die altijd naar de aangeraden loginserver wijst, zal op
woensdag 6 mei omgezet worden van de meer dan drie jaar oude `lilo3`
naar de nieuwere [lilo4](/nl/news/). Dit is een stapje in de
upgrade naar Ubuntu 14.04 LTS, in de zomervakantie worden ook de pc’s
uit de [Terminalkamers](/nl/howto/terminalkamers/) met Ubuntu 14.04 LTS
ge(her)installeerd. Het zal noodzakelijk zijn de oude
lilo(.science.ru.nl) entry uit het bestand \~/.ssh/known\_hosts te
verwijderen. Voor wie de vingerafdruk van de publieke RSA-sleutel wil
controleren voordat het Science wachtwoord aan de nieuwe server gegeven
wordt: `aa:ad:c0:2e:60:9d:d3:cd:ca:a4:59:7d:d0:d8:4c:68`. De naam
`stitch` voor de tweede loginserver, die men kan gebruiken als er een
probleem mocht zijn met `lilo`, zal dan omgezet worden naar de oudere
`lilo3`.
