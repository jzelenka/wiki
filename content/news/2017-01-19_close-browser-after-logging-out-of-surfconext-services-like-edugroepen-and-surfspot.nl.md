---
author: petervc
date: 2017-01-19 18:01:00
tags:
- medewerkers
- studenten
title: Sluit browser na "uitloggen" op SURFconext-diensten als Edugroepen en SURFspot
---
Bij inloggen op websites als [Edugroepen](https://www.edugroepen.nl),
[SURFspot](https://www.surfspot.nl/),
[SURFdrive](https://www.surfdrive.nl/) of
[SURFfilesender](https://www.surffilesender.nl/) met U-/s-nummer en
RU-wachtwoord wordt gebruik gemaakt van
[SURFconext](https://www.surf.nl/diensten-en-producten/surfconext/index.html).
Als u dan op “Uitloggen” klikt, lijkt u wel uitgelogd te worden, maar de
browser heeft nog toegang. Als u of iemand anders in deze browser weer
naar deze of een andere door SURFconext ontsloten webservice gaat, bent
u direct ingelogd. Het dringende advies is dan ook: gebruikt u een
dienst die ontsloten wordt via SURFconext en maken er meerdere mensen
gebruik van uw werkplek, sluit dan na gebruik de browser af.
