---
author: mkup
date: 2018-07-04 14:39:00
tags:
- medewerkers
- docenten
- studenten
title: 'Netwerkonderhoud RU: woensdagavond/nacht 4 op 5 juli vanaf 22:00 uur'
---
Op woensdagavond 4 juli zal onderhoud plaatsvinden aan centrale
netwerkonderdelen van de RU. Vanaf 22:00 uur zullen centrale RU-diensten
en -netwerk meestal niet beschikbaar zijn. Telefonie ondervindt geen
storing. Voor de C&CZ-services kan verstoring optreden: enige tijd geen
connectie met Internet. Denk s.v.p. na of deze mogelijke verstoring voor
u gevolgen heeft en bereid u indien nodig voor. Toekomstige
onderhoudsvensters zijn op de [website van het
ISC](http://www.ru.nl/systeem-meldingen/) te zien.
