---
author: mkup
date: 2012-02-28 14:00:00
tags:
- studenten
- medewerkers
title: Network problems after change of January 28 solved
---
January 28, the [UCI network management](http://www.ru.nl/uci) installed
new software and a new configuration on the Science Faculty router,
among other things to prepare for IP telephony. The following week some
network problems were reported, which have been traced to a [high cpu
load](http://cricket.science.ru.nl/?target=%2Fswitches%2Fdr-huyg.heyendaal.net%2Fperformance;ranges=m;view=cpu)
of the router. After a few temporary measures were taken, on Monday
January 28 18:00-18:45 a new configuration was installed, after which
the cpu load hasn’t been high again and the network problems appear to
have been solved more permanently.
