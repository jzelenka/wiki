---
author: sommel
date: 2008-04-14 14:32:00
title: Printbudget command for Unix
---
On [Linux/Solaris workstations and login
servers](/en/howto/hardware-servers/) one can use the command
**printbudget** to get an overview of the budget groups and the money on
it. One can also change the default budgetgroup and get an overview of
the last print jobs, like it is on the
[Do-It-Yourself](https://dhz.science.ru.nl/index?Language=en) website.
