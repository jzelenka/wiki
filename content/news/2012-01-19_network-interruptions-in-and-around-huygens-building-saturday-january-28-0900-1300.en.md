---
author: mkup
date: 2012-01-19 15:12:00
tags:
- studenten
- medewerkers
title: Network interruptions in and around Huygens building, Saturday January 28 09:00-13:00
---
The router for the Huygens building and nearby buildings will have a few
service interruptions, since the [UCI](http://www.ru.nl/uci) needs to
install new connections and new software. All network services with
clients or servers connected through that router will have service
interruptions.
