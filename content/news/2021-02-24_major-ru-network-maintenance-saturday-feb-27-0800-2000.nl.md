---
author: petervc
date: 2021-02-24 14:07:00
tags:
- medewerkers
- studenten
title: Groot RU netwerkonderhoud zaterdag 27 februari 08:00-20:00
---
ISC netwerkbeheer [kondigde aan](https://www.ru.nl/systeem-meldingen/)
dat a.s. zaterdag 27 februari gepland groot onderhoud aan het RU-netwerk
uitgevoerd zal worden, waardoor alle RU-diensten diverse keren maximaal
een uur lang niet bereikbaar zullen zijn. Dit gaat om alle RU-diensten,
inclusief die van FNWI/C&CZ: e-mail, VPN, wifi, BASS, OSIRIS,
Brightspace, Syllabus+, Corsa, etc.
