---
author: petervc
date: 2008-07-01 13:56:00
title: Adobe Creative Suite on install-disc and can be lent
---
The DVD’s of Adobe CS3 Design Premium UK and NL for
[MS-Windows](/en/howto/microsoft-windows/) and for UK
[Macintosh](/en/howto/macintosh/) have been copied to the
[install](http://www.cncz.science.ru.nl/software/installscience) network
disc. Postmaster can provide serial numbers. The DVDs can also be
borrowed at HG03.055. The other versions (Web) probably are less
popular. May be used on campus by employees and students. Employees can
purchase at [Surfspot](http://www.surfspot.nl) a DVD set for use at
home. Home use by students is not covered by this license.
