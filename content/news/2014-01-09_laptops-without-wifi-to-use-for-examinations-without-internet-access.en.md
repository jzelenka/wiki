---
author: john
date: 2014-01-09 16:34:00
tags:
- medewerkers
- docenten
title: Laptops without wifi to use for examinations without Internet access
---
It is now possible to borrow laptops from the [Science laptop
pool](/en/howto/laptop-pool/) with [wireless
network](http://en.wikipedia.org/wiki/Wifi) switched off. These laptops
can be used for examinations to allow students to use a computer without
giving easy access to the internet. Wifi has been switched off by
default on two laptops in the pool by request of the [Education
Center](http://www.ru.nl/fnwi/onderwijs/onderwijscentrum/) in order to
assist disabled students with examinations. For larger examinations,
please [reserve](/en/howto/laptop-pool/) the laptops in time, specifying
the need for wireless networking switched off if necessary.
