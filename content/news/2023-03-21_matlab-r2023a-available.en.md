---
title: Matlab R2023a available
author: petervc
date: 2023-03-21
tags:
- medewerkers
- studenten
cover:
  image: img/2023/matlab.png
---
The latest version of [Matlab](/en/howto/matlab/), R2023a, is available
for departments that have licenses. The software and license codes can
be obtained through a mail to postmaster for those entitled to it. The
software can also be found on the [install](/en/tags/software)-disc. All
C&CZ-managed Linux machines have this version installed, an older
version (/opt/matlab-R2022b/bin/matlab) is still available temporarily.
The C&CZ-managed Windows machines will not receive a new version during
the semester to prevent problems with version dependencies in current
lectures.
