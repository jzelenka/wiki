---
author: wim
date: 2011-02-16 10:53:00
tags:
- studenten
- medewerkers
- docenten
title: Workstation Unlocker on student PCs
---
On all student PCs (study area, PC rooms, and the info PCs near the
library) the *Workstation Unlocker* has been
installed.

This software enables everyone to release a locked PC after a fixed
timeout.

The user currently logged on to the PC will be logged off without the
need to reboot the PC.

Information about this software can be found at
[www.alexsapps.com](http://www.alexsapps.com/Apps/W/WorkstationUnlocker/Default.aspx)
