---
author: stefan
date: 2017-09-04 12:33:00
tags:
- studenten
- medewerkers
- docenten
title: C&CZ 3D print service now 4 color
---
The [four color
extension](https://all3dp.com/prusa-i3-multi-material-upgrade/) for the
[Prusa i3 MK2](https://www.3dhubs.com/3d-printers/original-prusa-i3-mk2)
3D Printer, that we ordered long ago, has finally arrived. Therefore we
now can print objects with 4 colors, or 3 colors and soluble support
material. For more details, see the [Printers
page](/en/howto/printers-en-printen/).
