---
author: petervc
date: 2012-10-18 16:10:00
tags:
- studenten
- medewerkers
title: Adobe Creative Suite 6 Web/Design Premium
---
The most recent version of the [Adobe Creative Suite Design/Web Premium
Bundle](http://www.adobe.com/products/creativesuite), 6, is available
for MS-Windows and Mac OS X. It can be found on the
[Install](/en/howto/install-share/) network share. After we burnt DVD’s
it can also be [borrowed](/en/howto/microsoft-windows/). The license
permits use on university computers and also home use by employees.
License codes can be requested from C&CZ helpdesk or postmaster. One can
also order the DVD’s on [Surfspot](http://www.surfspot.nl).
