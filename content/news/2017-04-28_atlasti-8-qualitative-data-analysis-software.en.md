---
author: petervc
date: 2017-04-28 13:17:00
tags:
- studenten
- medewerkers
title: ATLAS.ti 8 qualitative data analysis software
---
Version 8 of the ATLAS.ti qualitative data analysis software is
available on the [Install](/en/howto/install-share/) network share.
License codes are available from C&CZ helpdesk or postmaster. According
to the Radboud educational license, this software may only be used for
education and research.
