---
author: wim
date: 2011-02-16 10:53:00
tags:
- studenten
- medewerkers
title: Ubuntu login server
---
In het kader van een gefaseerde overstap van Fedora naar Ubuntu zijn er
enkele Ubuntu clients en servers geinstalleerd. Zo ook is er is een
experimentele *Ubuntu 10.04 LTS (Lucid Lynx)*
login server **lilo1.science.ru.nl** geinstalleerd
voor algemeen gebruik.

Er is gekozen voor Ubuntu 10.04 LTS (Lucid Lynx) vanwege
**L**(ong) **T**(ime)
**S**(upport), 3 jaar voor desktops en 5 jaar voor
servers. Dit in tegenstelling tot pakweg 1 jaar updates en support voor
Fedora. Basis software, zoals veelvuldig in gebruik binnen de Beta
Faculteit, is aanwezig.

Het kan bijna niet anders dan dat er nog zaken onbreken of anders werken
dan op de huidige Fedora 11 systemen. Ook kan het zijn dat instellingen
voor de Fedora installatie niet probleemloos werken op een Ubuntu
systeem. Aarzel echter niet om omissies of problemen te melden. Helaas
kunnen we niet toezeggen dat we alle problemen per omgaande zullen
verhelpen.
