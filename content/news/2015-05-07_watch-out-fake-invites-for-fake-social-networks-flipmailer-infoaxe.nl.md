---
author: petervc
date: 2015-05-07 13:23:00
tags:
- medewerkers
- docenten
- studenten
title: 'Pas op: nep-invitaties voor nep-sociale netwerken (flipmailer, infoaxe)'
---
De laatste tijd horen we van gebruikers die mail krijgen van hen bekende
mail-adressen met onderwerp of inhoud iets als “You have a new
notification from … View?” of “… is waiting for your response. Respond?”
of “I would like to add you as a friend”. Accept?" De link (URL) in deze
mails wijst naar iets in “flipmailer punt com” of “infoaxe punt com”.
Wanneer men daarop klikt, wordt in de browser Firefox/Chrome/… een
spyware plugin/add-on/extension geïnstalleerd. Wanneer je je
mail-wachtwoord op hun website invult, krijgen al je contacten dezelfde
nep-invitatie. Trap hier niet in! Als je al op deze mails gereageerd
hebt, [neem dan contact op met de C&CZ helpdesk](/nl/howto/contact/) of
zoek op het Internet naar wat je moet doen om de spyware te verwijderen,
zoals [deze
how-to-remove-fliporainfoaxenet-spam-extension](http://emmanuelcontreras.com/content/how-remove-fliporainfoaxenet-spam-extension).
