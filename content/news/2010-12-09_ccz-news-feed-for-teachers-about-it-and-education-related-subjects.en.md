---
author: pberens
date: 2010-12-09 11:04:00
tags:
- docenten
title: C&CZ News feed for teachers about IT and education related subjects
---
[http://wiki.science.ru.nl/cncz/index.php?title=Nieuws&action=feed&lang=en&feed=rss&tags=docenten](/en/news/)

This feed contains news for FNWI teachers about IT and education related
subjects, especially E-learning and Blackboard.
