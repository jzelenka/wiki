---
author: petervc
date: 2022-05-17 15:05:00
tags:
- studenten
- medewerkers
- docenten
title: CCZ verhuist 22 juni naar HG00.051
---
Om ruimte te maken voor de groei van afdelingen van IMM, zal C&CZ op 22
juni naar de begane grond van het Huygensgebouw verhuizen. De [C&CZ
helpdesk](/nl/howto/contact/) houdt in HG00.051 een toegangsdeur aan de
centrale straat.
