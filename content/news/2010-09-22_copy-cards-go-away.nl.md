---
author: petervc
date: 2010-09-22 11:06:00
title: Kopieerkaartjes verdwijnen
---
De kopieerkaartjes met een magneetstrip, die men kan gebruiken bij de
[multifunctionele kopieerapparaten](/nl/howto/copiers/), zullen over
enkele maanden gaan verdwijnen. Om te printen en kopieren gebruikt men
in het toekomstige
[Peage-systeem](http://www.ru.nl/studenten/voorzieningen/peage-%28proef%29/)
een chipkaart en een netwerk-budget. Binnenkort zal een eerste apparaat
in de centrale hal van het Huygensgebouw hiermee uitgerust worden. De
eerste maanden is het systeem alleen bruikbaar voor studenten, dus
zullen de overige FNWI-multifunctionals pas later naar dit systeem
omgezet worden. Aan afdelingen het advies om voor niet meer dan enkele
maanden kopieerkaartjes in voorraad te nemen.
