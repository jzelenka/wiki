---
author: polman
date: 2013-10-14 11:16:00
tags:
- medewerkers
- docenten
- studenten
title: 'Webmail: Roundcube vervangt Horde'
---
De verouderde webmail service [Horde](https://horde.science.ru.nl) zal
op maandag 18 november gestopt worden. De opvolger, die nu al
beschikbaar is, is een moderne webmail-omgeving
[Roundcube](https://roundcube.science.ru.nl), die bij het eerste
inloggen de contacten uit Horde importeert. Roundcube heeft allerlei
mogelijkheden, zoals het beheren van het delen van mailmappen met andere
gebruikers en het filteren van mail direct bij binnenkomst op de server.
Zie voor meer info onze [mail pagina](/nl/tags/email/).
