---
author: theon
date: 2009-09-28 14:05:00
title: Student printer changes
---
Next to printer `jansteen` another HP4350 black-and-white laserprinter
`jansteen2` has been placed. This makes printing easier for students: if
one of these printers has a problem, one can just pick the other one.
Repairs of these printers can take a long time, because there is no
support contract anymore for these printers. The HP4350 printers `monet`
and `chagall` have been removed.
