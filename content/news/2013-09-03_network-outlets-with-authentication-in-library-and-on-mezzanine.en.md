---
author: mkup
date: 2013-09-03 15:17:00
tags:
- studenten
- medewerkers
- docenten
title: Network outlets with authentication in Library and on mezzanine
---
As of today, the reading room of the Library of Science and the
mezzanine have 32 and 64 authenticated network outlets respectively,
with network speeds of 100 Mbps. Connect your laptop with a UTP cable
and start a browser. You will be redirected to a website where you can
register with your personnel number (`U123456`) or student number
(`s123456`). After that, you will have Internet access. In the near
future, you will also be able to register with your
`scienceloginname@science.ru.nl`. We intend to implement authentication
for all publicly accessible active network outlets in the future. The
technique behind this is Qmanage by
[Quarantainenet](http://www.quarantainenet.com/).
