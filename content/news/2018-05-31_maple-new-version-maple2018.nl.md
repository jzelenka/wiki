---
author: polman
date: 2018-05-31 14:13:00
tags:
- studenten
- medewerkers
- docenten
title: 'Nieuwe versie Maple: Maple2018'
---
De nieuwste versie van [Maple](/nl/howto/maple/), Maple2018, is te vinden
op de [Install](/nl/howto/install-share/)-netwerkschijf en is op door
C&CZ beheerde Linux-computers geïnstalleerd. Licentiecodes zijn bij C&CZ
helpdesk of postmaster te verkrijgen voor de deelnemende afdelingen.
