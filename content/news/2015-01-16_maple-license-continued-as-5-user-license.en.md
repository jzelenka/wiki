---
author: polman
date: 2015-01-16 15:39:00
tags:
- studenten
- medewerkers
- docenten
title: Maple license continued as 5-user license
---
The license of [Maple](/en/howto/maple/) has been continued as a 5-user
license. Of these, 4 are financed by the Faculty of Science departments
[EHEF](http://www.ru.nl/ehef/) and [THEF](http://www.ru.nl/thef/).
Because 1 license is paid by [C&CZ](/), all FNWI employees and students
are allowed to use Maple once in a while. If you want to use Maple more
often, please contact [C&CZ](/en/howto/contact/).
