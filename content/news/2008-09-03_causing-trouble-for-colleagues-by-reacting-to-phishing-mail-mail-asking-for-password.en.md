---
author: petervc
date: 2008-09-03 12:23:00
title: Causing trouble for colleagues by reacting to phishing mail (mail asking for
  password)
---
A week ago a too naive colleague reacted to a phishing mail by sending
his loginname and password. Soon after that loginname was misused to
send large amounts of spam through our webmail server, which lead to the
appearance of one of our servers in a blacklist. That made it impossible
for that server to send mail to some organisations. C&CZ likes to stress
\*again\* that it is not very wise to believe something just because is
written in a letter or e-mail and that \*no official institution\* will
ever ask you for your password, just like a bank never will ask you for
your pincode for your bank account.
