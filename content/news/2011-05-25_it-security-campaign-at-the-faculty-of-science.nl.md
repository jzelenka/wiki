---
author: petervc
date: 2011-05-25 11:49:00
tags:
- studenten
- medewerkers
- docenten
title: ICT-beveiligingscampagne bij FNWI
---
In april, mei en juni 2011 voeren C&CZ, GDI en UCI een campagne over
ICT-beveiliging in de hele RU. In de weken dat de campagne duurt, is er
extra aandacht voor zaken als beveiligingssoftware, het up-to-date
houden van alle software, het veiliger gebruik van Blackboard, het
verzinnen van een goed wachtwoord, het veilig gebruiken van USB-sticks
en ‘phishing’. De campagne bij FNWI is begonnen op maandag 23 mei 2011.
Op de [RU ICT-beveiligingssite](http://www.ru.nl/ict-beveiliging) vindt
u nog veel meer informatie over ICT-beveiliging. Alles onder het motto:
RU Secure!
