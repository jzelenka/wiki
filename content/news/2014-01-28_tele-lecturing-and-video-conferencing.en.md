---
author: caspar
date: 2014-01-28 12:53:00
tags:
- medewerkers
- studenten
title: Tele lecturing and video conferencing
---
There is a [tele lecture room](/en/howto/telecollege/) at the Faculty of
Science: HG00.108. This facility makes it possible for students who are
elsewhere in the world, to participate *virtually* and *interactively*
in a lecture given in this room. The other way around is also possible.
Moreover in the large conference room HG01.060 a video conferencing
facility has been created. This setup also serves as backup for the tele
lecture facility. For help and advice please contact the [Audiovisual
Department](http://www.ru.nl/fnwi/ihz/avd/audio-visuele-dienst/) of our
faculty.
