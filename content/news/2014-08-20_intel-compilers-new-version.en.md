---
author: polman
date: 2014-08-20 17:42:00
tags:
- medewerkers
- studenten
title: Intel compilers new version
---
C&CZ has bought two licences for concurrent use of the most recent
version of the [Intel Cluster Studio for
Linux](http://software.intel.com/en-us/articles/intel-cluster-studio-xe/).
This has been installed in `/vol/opt/intelcompilers` and is available on
a.o. [clusternodes](/en/howto/hardware-servers/) en
[loginservers](/en/howto/hardware-servers/). The old (2011) version will
also be moved to `/vol/opt/intelcompilers`. To set the environment
variables correctly, users must first run:

source /vol/opt/intelcompilers/intel-2014/composerxe/bin/compilervars.sh
intel64

After that, `icc -V` gives the new version number as output.
