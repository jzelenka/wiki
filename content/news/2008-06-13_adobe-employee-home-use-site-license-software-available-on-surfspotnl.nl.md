---
author: petervc
date: 2008-06-13 15:39:00
title: 'Adobe: medewerker thuisgebruik site-license via surfspot.nl te koop'
---
Op [Surfspot](http://www.surfspot.nl) zijn de site-license producten van
[Adobe](http://www.adobe.com) voor thuisgebruik door medewerkers nu te
koop voor 35 euro per bundel. Men kan kiezen tussen Engelse of
Nederlandse versies van de ‘web’ of ‘design’ bundel. In beide bundels
zit o.a. Photoshop, Illustrator, Acrobat Professional, Dreamweaver.
Alleen de ‘design’ bundel bevat InDesign. De site-license producten voor
installatie op de campus zijn met spoed besteld.
