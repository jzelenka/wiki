---
author: petervc
date: 2022-09-01 14:16:00
tags:
- medewerkers
- studenten
title: Nieuwe versie van OriginLabPro (2022b SR1)
cover:
  image: img/2022/originlab2022b.png
---
Er is een nieuwe versie (2022b SR1) van
[OriginLabPro](/nl/howto/originlab/), software voor wetenschappelijke
grafieken en data-analyse,beschikbaar op de
[Install](/nl/howto/install-share/)-schijf. Zie [de website van
OriginLab](https://www.originlab.com/2022b) voor alle info over de
nieuwe versie. De licentieserver ondersteunt deze versie. Afdelingen van
FNWI kunnen bij C&CZ de licentie- en installatie-informatie krijgen, ook
voor standalone gebruik. Installatie op door C&CZ beheerde PC’s wordt
gepland. Bij vragen, benader [C&CZ](/nl/howto/contact/).
