---
author: petervc
date: 2008-09-02 17:24:00
title: Opening hours C&CZ extended
---
C&CZ has extended the [opening hours](/en/howto/helpdesk/), our new
opening hours are from 08:00 to 18:00.
