---
author: wim
date: 2014-05-23 17:14:00
tags:
- medewerkers
- studenten
title: New print server
---
Practically all printers are now available through a new server. If you
still attach a printer through the old “printer-srv”, you will receive
an email with the request to move to the new server, as detailed on [the
printer page](/en/howto/printers-en-printen/). Please [report all
problems](/en/howto/contact/).
