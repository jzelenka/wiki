---
author: polman
date: 2015-02-13 14:25:00
tags:
- studenten
- medewerkers
title: IMAP mailserver upgrade a.s. maandagnacht
---
De IMAP server wordt a.s. maandagnacht vervangen door een nieuwe server.
Gedurende de nacht kan geen Science mail gelezen worden van de server,
omdat dan de meest recente mails gesynchroniseerd zullen worden naar de
nieuwe server. Mail kan wel verstuurd worden gedurende deze tijd, maar
nieuwe mail zal pas na 07:30 op de nieuwe server afgeleverd worden. N.B.
op de oude server kon men inloggen met de eerste 8 tekens van het
wachtwoord, op de nieuwe server moet het volledige wachtwoord ingetikt
worden.
