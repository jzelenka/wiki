---
author: sommel
date: 2008-11-18 13:40:00
title: Algemene aanwijzingen/waarschuwingen bij het printen
---
Op de pagina [Printers en printen](/nl/howto/printers-en-printen/) zijn
de volgende aanwijzingen/waarschuwingen opgenomen:

-   Maak *geen stapels* kopie-afdrukken zonder vooraf te testen.
-   Print niet vanaf *Adobe Acrobat 8* naar een HP4250/4350.
-   Gebruik *geen kleurenprinter* voor zwart/wit afdrukken van een
    kleurenbestand.
