---
author: caspar
date: 2009-11-19 14:19:00
title: 'Linux login servers: upgrade and addition'
---
The operating system of the general purpose Linux login server `lilo`
(formally `porthos.science.ru.nl`) has been upgraded to [Fedora
11](http://nl.wikipedia.org/wiki/Fedora). Also a second Linux login
server named `stitch` (formally `lijfwacht.science.ru.nl`) is now
available. `Lilo` and `stitch` are identical as far as the software is
concerned but `stitch` has newer, more powerful hardware (8 cores, 2.5
GHz each, 8GB internal memory). In case of a problem with one of these
servers the other one serves as backup. These changes were initiated
because of a security issue on `lilo` before the upgrade and agree with
our goal to eliminate the old Solaris login servers `solo` and `solost`
eventually. For more info, see the [servers
page](/en/howto/hardware-servers/).
