---
author: wim
date: 2018-04-10 10:47:00
tags:
- medewerkers
- studenten
title: RU mail-addresses (@ru.nl, @student.ru.nl, ...) in Science LDAP address book
---
For those who forward all Science mail to an RU Exchange address
(@ru.nl, @student.ru.nl, @fnwi.ru.nl or @donders.ru.nl), the [Science
LDAP address book](/en/howto/ldap-adresboek/) from now on lists that
forward address instead of the Science mail address. The advantage of
using this Science LDAP address book is that you can easily find
addresses of FNWI employees and students, without having to choose from
all RU namesakes.
