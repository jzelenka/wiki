---
author: petervc
date: 2018-11-27 14:26:00
tags:
- medewerkers
- studenten
- docenten
title: Temporarily free Stata licence on RU pc's
---
::: {#.5BTijdelijk_gratis_Stata-licentie_op_RU-werkplekken.5D.5BTemprarily_free_Stata_licence_on_RU_pc.27s.5D}
:::

Together with Radboudumc, Radboud University has signed a campus license
for [Stata](https://www.stata.com/). Stata is statistical software that
is in use at a number of faculties - especially at FSW and FM. The costs
for the use of Stata have been relatively high up to now. Because now
there is a campus license, it has become much cheaper for new users:
until November 2019 it can be used free of charge on pc’s owned by
Radboud University. After that it will cost a maximum of € 150 per user
per year, depending on the number of users.
