---
author: petervc
date: 2012-03-13 15:57:00
tags:
- studenten
- medewerkers
title: New paper for copiers and printers
---
The new Oce BlacklabelZero paper that won the European tender, is being
used since recently also for the [C&CZ
printers](/en/howto/printers-en-printen/), including the [Ricoh
multifunctionals](/en/howto/ricoh/). The new paper is whiter, cheaper and
moreenvironment-friendly. The less white Biotop paper can still be
ordered as long as there is RU letter paper of the same color.
