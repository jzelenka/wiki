---
author: sommel
date: 2010-03-23 10:55:00
title: Linux cups server en printen met gebruik van Kerberos tickets
---
Vanaf november 2009 moest voor het [afdrukken vanaf
Linux](/nl/howto/printers-en-printen#.5bafdrukken-op-beheerde-linux-werkplekken.5d.5bprinting-using-a-managed-linux-pc.5d/)
naar een gebudgetteerde printer telkens uw accountwachtwoord worden
ingetikt. Dit gaf soms problemen als de toepassing niet vanuit een
terminal was gestart. Nu is er een [CUPS](http://www.cups.org/)-server
`drukwerk.science.ru.nl` ingericht die voor gebudgetteerde printers
authenticatie met een geldig [Kerberos](http://www.kerberos.org/) ticket
vereist, waardoor het telkens intikken van het wachtwoord niet meer
nodig is. Beheerde Linux PC’s met Fedora 8 en hoger maken al gebruik van
deze CUPS-server. Een Kerberos ticket granting ticket kan men krijgen
door **kinit** te tikken.

We zullen proberen deze CUPS-server zo aan te passen dat deze ook voor
Windows
[IPP-printen](http://en.wikipedia.org/wiki/Internet_Printing_Protocol)
gebruikt kan worden.
