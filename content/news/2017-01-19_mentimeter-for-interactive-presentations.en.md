---
author: fmelssen
date: 2017-01-19 18:18:00
tags:
- medewerkers
- studenten
title: Mentimeter for interactive presentations
---
Radboud University has a license for
[Mentimeter](https://www.mentimeter.com/join/ru/) in the academic year
2016-2017. Possibly the license will be continued after that. Mentimeter
is a tool to make a presentation interactive: listeners can vote
anonymously with their phone, laptop or tablet on questions or
statements. Feedback can be displayed immediately. Mentimeter resembles
the older [Shakespeak](https://www.shakespeak.com/) tool, but offers
more functionality and is fully browser-oriented (does not work with a
Powerpoint plugin like Shakespeak). Anyone with a mailadres within the
RU domain can [sign up](https://www.mentimeter.com/join/ru/).
