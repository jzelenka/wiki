---
author: petervc
date: 2012-09-25 16:16:00
tags:
- studenten
- medewerkers
- docenten
title: Einde aan transparantenprinter nicolas
---
Jarenlang was de inkjet
transparanten-[printer](/nl/howto/printers-en-printen/) nicolas voor
gebruikers van belang, om mooie kleurentransparanten te maken voor
presentaties. Omdat deze printer een hardwareprobleem vertoont en nog
slechts zeer incidenteel gebruikt werd, is besloten de printer uit te
faseren. Indien men nog transparanten wil printen, kan men
\*lasergeschikte\* transparanten gebruiken in de
[laserprinters](/nl/howto/printers-en-printen/).
