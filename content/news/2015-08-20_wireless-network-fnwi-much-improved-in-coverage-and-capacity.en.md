---
author: mkup
date: 2015-08-20 16:32:00
tags:
- medewerkers
- docenten
- studenten
title: Wireless network FNWI much improved in coverage and capacity
---
In the past months, the number of wireless access points in the Huygens
building, Mercator 1 and HFML has been much increased, from 97 to 219.
Almost all of these access points also support the new
[802.11ac](https://en.wikipedia.org/wiki/IEEE_802.11ac) standard. This
has much improved both coverage and capacity of the wireless network
‘eduroam’ in these buildings. To make sure that both coverage and
capacity is optimal in important locations, the
[ISC](http://www.ru.nl/isc) will take measurements in the next weeks. We
also ask users within FNWI that still have problems with the wireless
network, to report this to the C&CZ helpdesk (helpdesk\@science.ru.nl).
Where necessary, adjustments will then be made. More information about
the wireless network can be found at [the ISC
website](http://www.ru.nl/ict-uk/staff/wifi/). The other FNWI buildings
will get an upgrade in 2016, if budget is available.
