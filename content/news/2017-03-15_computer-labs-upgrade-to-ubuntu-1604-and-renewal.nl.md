---
author: john
date: 2017-03-15 18:35:00
tags:
- medewerkers
- docenten
- studenten
title: 'Terminalkamers: upgrade naar Ubuntu 16.04 en vernieuwing'
---
In de zomervakantie worden alle pc’s uit de
[Terminalkamers](/nl/howto/terminalkamers/) met Ubuntu 16.04 LTS
ge(her)installeerd. Cursus-software kan al lang getest worden op de
Ubuntu 16.04 [loginserver](/nl/howto/hardware-servers/) lilo5. Alle vier
jaar oude PC’s in TK029 (HG00.029), TK206 (HG00.206), TK253 (HG02.253),
bibliotheek, studielandschap, projectkamers, collegezalen en
colloquiumkamers worden in die periode vervangen door nieuwe PC’s
(All-in-One, dual boot met Windows7 en Ubuntu 16.04 Linux.
