---
author: petervc
date: 2016-08-19 18:17:00
tags:
- studenten
- medewerkers
title: Temporarily free security software for students and staff
---
Between August 15 and September 15, RU students and staff can get the
security software F-Secure SAFE for 1 device for 1 year for free.
Normally the price is € 2 per year. The software is available for
Windows, Mac, Android, iOS and Windows Phone. For more info see the
[Surfspot
website](https://www.surfspot.nl/f-secure-safe-1-apparaat-radboud-universiteit-nijmegen-en-fontys.html).
