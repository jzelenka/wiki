---
author: wim
date: 2008-09-03 11:53:00
title: H:-disk of students moved and enlarged
---
Because of the new [computer lab](/en/howto/terminalkamers/) TK023 with
dual-boot Windows/Linux PC’s, the H:-disks (home directories under
Linux/Unix) of students have been moved to a new server. This made it
also possible to enlarge the [quota](/en/howto/quota-bekijken/) on that
disk to 1 GB per student.
