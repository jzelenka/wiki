---
author: petervc
date: 2008-03-28 15:02:00
title: Surfnet Usenet News voorziening wordt gestopt?
---
Tijdens de [RIO](http://www.ru.nl/rio)-vergadering van 27 maart deelde
het [UCI](http://www.ru.nl/uci) mee dat [SURFnet](http://www.surfnet.nl)
de [Surfnet Usenet
News-dienst](http://www.surfnet.nl/nl/diensten/cc/Pages/news.aspx) in de
toekomst alleen nog aan universiteiten wil leveren als die er ca 12
k€/jaar voor betalen. Wie deze service van belang vindt voor het werk en
niet eenvoudig bij alternatieven als [Google
groups](http://groups.google.com) terecht kan, kan zich bij C&CZ melden.
