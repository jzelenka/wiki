---
author: sommel
date: 2008-12-09 15:24:00
title: HP4350 printers naar vakgroepen?
---
Een aantal [HP4350 dubbelzijdige zwart-wit Postscript
printers](/nl/howto/printers-en-printen/) die nog naast de nieuwe
[Ricoh](/nl/howto/ricoh/) multifunctionele machines staan, kunnen
verplaatst en/of verkocht worden aan vakgroepen. Wie belangstelling
heeft: melden bij C&CZ!
