---
author: petervc
date: 2021-12-06 00:24:00
tags:
- software
cover:
  image: img/2021/maple.png
title: 'Nieuwe versie Maple: Maple2021.2 en licentie verlengd'
---
De door C&CZ beheerde “vier gelijktijdige gebruikers”-licentie van Maple
is verlengd tot 31 december 2022. De nieuwste versie van
[Maple](/nl/howto/maple/), Maple2021.2, is voor Windows/macOS/Linux te
vinden op de [Install](/nl/howto/install-share/)-netwerkschijf en is op
door C&CZ beheerde Linux-computers geïnstalleerd. Licentiecodes zijn bij
C&CZ helpdesk of postmaster te verkrijgen.
