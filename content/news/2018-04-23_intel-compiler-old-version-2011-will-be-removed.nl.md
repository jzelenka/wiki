---
author: petervc
date: 2018-04-23 16:28:00
tags:
- medewerkers
- studenten
title: 'Intel compilers: oude versie (2011) verdwijnt'
---
Sinds 2014 heeft C&CZ twee licenties voor gelijktijdig gebruik van de
2014 versie van de [Intel Cluster Studio voor
Linux](http://software.intel.com/en-us/articles/intel-cluster-studio-xe/),
te vinden in `/vol/opt/intelcompilers`. Daarom zal per 1 mei 2018 de
oude (2011) versie van die plaats verwijderd worden, waardoor de oude
license server gestopt kan worden.
