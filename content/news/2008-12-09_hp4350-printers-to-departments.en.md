---
author: sommel
date: 2008-12-09 15:24:00
title: HP4350 printers to departments?
---
A number of [HP4350 double-sided black and white Postscript
printers](/en/howto/printers-en-printen/) that are located next to a new
[Ricoh](/en/howto/ricoh/) multifunctional machine, can be moved or sold
to departments. Interested? Contact C&CZ!
