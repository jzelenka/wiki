---
author: petervc
date: 2008-04-03 18:18:00
title: Matlab new version (R2008a)
---
Starting March 2008 a new version of [Matlab](/en/howto/matlab/) is
available on all C&CZ managed PCs with Linux or Windows. For the
Solaris9 Suns R2007a is the last version. For more information about
this release, see [the Mathworks
website](http://www.mathworks.com/products/new_products/latest_features.html)
