---
author: petervc
date: 2013-09-04 10:45:00
tags:
- studenten
- medewerkers
- docenten
title: Trap niet in phishing mails\!
---
Als u een mail krijgt met als zogenaamde afzender iets als “C&CZ” of “RU
Administrator”, die u vraagt om uw gebruikersnaam en wachtwoord op een
vage website in te vullen of te mailen, doe dat dan s.v.p. niet. Men
schrijft dan meestal dat dat nodig is om problemen te voorkomen, maar
als u reageert, gaat u juist problemen veroorzaken! Internet-criminelen
gebruiken die gegevens namelijk o.a. om spam te versturen via onze
mailservers. Ook hebben ze dan toegang tot al uw mail op de IMAP-server
en al uw bestanden. De afgelopen weken zijn niet minder dan vijf
FNWI-medewerkers en -studenten in zo’n mail getrapt, met overlast voor
allen tot gevolg.
