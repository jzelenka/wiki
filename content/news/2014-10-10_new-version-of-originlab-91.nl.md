---
author: polman
date: 2014-10-10 13:08:00
tags:
- medewerkers
- studenten
title: Nieuwe versie van OriginLab (9.1)
---
Er is een nieuwe versie (9.1) van [OriginLab](/nl/howto/originlab/),
software voor wetenschappelijke grafieken en data-analyse,beschikbaar op
de [Install](/nl/howto/install-share/)-schijf. De licentieserver heeft
een update naar deze versie gehad. Afdelingen die meebetalen aan de
licentie kunnen bij C&CZ de licentie- en installatie-informatie krijgen,
ook voor standalone gebruik. Installatie op door C&CZ beheerde PC’s moet
nog gepland worden. Met de leverancier is een nieuw onderhoudscontract
afgesloten tot augustus 2017.
