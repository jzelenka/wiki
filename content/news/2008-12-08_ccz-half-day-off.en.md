---
author: petervc
date: 2008-12-08 23:46:00
title: C&CZ (half) day off
---
Wednesday December 17 from 14:00 hours, C&CZ will have their (half) day
out. In case of emergencies C&CZ can even then be [contacted by
phone](/en/howto/helpdesk/).
