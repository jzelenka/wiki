---
author: petervc
date: 2019-02-26 13:01:00
tags:
- medewerkers
- studenten
title: Peage en MFP's in maart vervangen
---
Konica Minolta heeft opnieuw de Europese aanbesteding voor
multifunctionals (MFP’s) gewonnen. Alle MFP’s, de bijbehorende software
en het betaalsysteem zullen in maart 2019 vervangen worden. Iedere
nieuwe MFP heeft een paslezer. In plaats van de campuskaart kan ook een
andere pas, b.v. de OV-chipkaart, gekoppeld worden aan het RU-account
(U-, S- of E-nummer). Inloggen kan daarna zonder pincode met de
gekoppelde pas. Voor studenten/studieverenigingen wordt het mogelijk om
groepsbudgetten te gebruiken. Studenten gaan de [Skuario
app](https://www.skuario.com/) gebruiken voor opwaarderen/overboeken.
[Meer informatie](https://wiki.cncz.science.ru.nl/Peage) zal in maart
bekendgemaakt worden.
