---
author: caspar
date: 2013-03-12 15:36:00
tags:
- studenten
- docenten
- medewerkers
title: Replacement of Ricoh multifunctionals by Konica Minolta
---
The European tender for multifunctionals (printer/copier/scanner) for
the university was won by [Konica Minolta](/en/howto/km/). This means
that the current [Ricoh](/en/howto/ricoh/) machines will be replaced.
There is a project to make the print/copy/scan system
[Péage](http://www.ru.nl/peage) suitable for employees. The printing and
copying costs for employees will be charged to their
“aanstellingskostenplaats”, i.e. the current budget groups will
disappear. Until this project is completed successfully, the [C&CZ
printbudget system](/en/howto/printers-en-printen/) will be used.

-   Since the new Peage system will not support Xafax copycards, it is
    advised to minimize your local stock of Xafax copycards.
-   The first Konica Minolta MFP’s will be placed for the expansion to
    Huygens 2.0 (Mercator 1).
-   The first replacement of Ricoh-machines in FNWI will be in April:
    the [Peage](http://www.ru.nl/peage) printer Watt near the
    restaurant.
