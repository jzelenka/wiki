---
author: polman
date: 2015-09-18 10:41:00
tags:
- studenten
- medewerkers
- docenten
title: 'Nieuwe versie Maple: Maple2015'
---
De nieuwste versie van [Maple](/nl/howto/maple/), Maple2015, is vanaf nu
beschikbaar op de door C&CZ beheerde Linux PC’s en zal binnenkort op de
Windows PC’s geïnstalleerd worden. De licentie van Maple is een
5-gebruikers licentie. Hiervan worden 4 licenties betaald door de
FNWI-afdelingen [EHEF en THEF](http://www.ru.nl/highenergyphysics/).
Doordat [C&CZ](/) ook 1 licentie betaalt, kan elke FNWI-medewerker en
-student incidenteel Maple gebruiken. Belangstellenden die Maple vaker
willen gebruiken, kunnen zich melden bij [C&CZ](/nl/howto/contact/). De
software is te vinden op de
[Install](/nl/howto/install-share/)-netwerkschijf. Licentiecodes zijn bij
C&CZ helpdesk of postmaster te verkrijgen.
