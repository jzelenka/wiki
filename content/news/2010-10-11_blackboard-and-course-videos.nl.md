---
author: pberens
date: 2010-10-11 13:57:00
tags:
- docenten
title: Blackboard en Onderwijs In Beeld
---
Voor Onderwijs in Beeld is een [nieuwe Flash
player](/nl/howto/blackboard#.5bonderwijs-in-beeld:-nieuwe-flash-player.5d.5bnew-flash-player.5d/)
beschikbaar met extra handigheidjes.
