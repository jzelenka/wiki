---
author: mkup
date: 2017-02-22 14:56:00
tags:
- medewerkers
- studenten
title: Science and RU-Internet network maintenance
---
Monday evening March 6, there will be maintenance on RU network
equipment. Between ca. 22:00-22:30 no network traffic will be possible
at the Faculty of Science on the wired and wireless networks. From 23:00
until March 7 02:00 there will be a period where no traffic between RU
and Internet is possible. Please think which processes depend on network
connectivity and prepare for this outage if necessary.
