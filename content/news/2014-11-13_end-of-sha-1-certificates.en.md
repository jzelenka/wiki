---
author: polman
date: 2014-11-13 12:03:00
tags:
- studenten
- medewerkers
- docenten
title: End of SHA-1 certificates
---
All [SHA-1](http://en.wikipedia.org/wiki/Secure_Hash_Algorithm)
certificates of C&CZ managed servers will be [replaced by new SHA-2
certificates](https://www.digicert.com/transitioning-to-sha-2.htm) on
short notice. This is necessary because SHA-1 is no longer considered
[secure enough for the
future](https://www.schneier.com/blog/archives/2012/10/when_will_we_se.html).
The [Chrome browser will display a warning as early as November
2014](http://googleonlinesecurity.blogspot.nl/2014/09/gradually-sunsetting-sha-1.html)
at a website with a SHA-1 certificate, other browsers [will follow
later](https://www.digicert.com/sha-2-ssl-certificates.htm).
