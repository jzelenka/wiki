---
title: Science Jitsi Meet video conferencing only authenticated
author: sioo
date: 2023-01-19
tags:
- medewerkers
- studenten
cover:
  image: img/2023/jitsimeet.png
---
# Science Jitsi Security

As of January 18th, 2023, starting a [jitsi](https://jitsi.science.ru.nl/)
meeting room requires you to authenticate with your [science
login](/en/howto/login) and password. Guests do not require any
authentication once the session is running. [Jitsi](https://en.wikipedia.org/wiki/Jitsi) is a collection of
free and open-source multiplatform voice (VoIP), video conferencing and
instant messaging applications for the web platform, Windows, Linux,
macOS, iOS and Android, of which Jitsi Meet videoconferencing is the most well-known.
