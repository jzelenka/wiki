---
author: polman
date: 2008-04-25 12:56:00
title: New storage possibility for passing on large files
---
To facilitate the local copying of large files (for which mail is not
suitable) a new network share is available, see
[TempDisk](/en/howto/tempdisk/).
