---
author: polman
date: 2010-03-03 18:09:00
title: Endnote X3 beschikbaar
---
[Endnote](http://www.endnote.com/) versie X3 is beschikbaar voor
MS-Windows en Macintosh. Het kan voor Windows geïnstalleerd worden vanaf
de [install](http://www.cncz.science.ru.nl/software/installscience)
netwerk schijf. De CD’s kunnen ook door medewerkers en studenten geleend
worden, ook voor thuisgebruik. Op de [Windows-XP Beheerde Werkplek
PCs](/nl/howto/windows-beheerde-werkplek/) is de software ook
geïnstalleerd.
