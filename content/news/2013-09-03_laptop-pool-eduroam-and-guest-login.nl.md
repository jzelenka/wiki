---
author: petervc
date: 2013-09-03 15:45:00
tags:
- studenten
- medewerkers
- docenten
title: 'Laptop-pool: Eduroam en gastlogin'
---
In de zomervakantie zijn de laptops van de [laptop
pool](/nl/howto/laptop-pool/) aangepast, zodat ze van het [draadloze
netwerk Eduroam](/nl/howto/netwerk-draadloos/) gebruik maken. Tevens is
er een gastlogin op alle laptops gemaakt, zodat de laptops ook zonder
authenticatie en draadloos netwerk te gebruiken zijn.
