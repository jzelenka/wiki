---
author: wim
date: 2018-08-27 17:58:00
tags:
- medewerkers
- studenten
title: Windows10, Software Center en TK pc's
---
In de zomervakantie hebben alle [pc’s in de pc-cursuszalen
(terminalkamers)](/nl/howto/terminalkamers/) een upgrade van Windows7
naar Windows10 gehad. Ook zijn de 4 jaar oude pc’s uit TK625/HG00.625 en
HG00.201/studielandschap/bibliotheek vervangen. In TK053/HG02.053 en
HG00.201/studielandschap zijn ook pc’s bijgeplaatst. Via het Software
Center kunnen gebruikers zelf software installeren uit de catalogus.
Alle [vragen en opmerkingen zijn welkom](/nl/howto/contact/).
