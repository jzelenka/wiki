---
author: petervc
date: 2016-11-23 13:02:00
tags:
- medewerkers
- studenten
title: RU network down Monday night Dec. 12 22:00-03:00
---
The [ISC](http://www.ru.nl/isc) let know that the central RU network
will be down in the night of Monday December 12 22:00 till Tuesday
morning 03:00 because the RU firewall will be replaced. From 22:00 hours
RU-central services such as Exchange (e-mail and calendar), BASS, Osiris
and Blackboard will not be available. From 23:00 hours no traffic
through the RU firewall will be possible, the RU will then be
disconnected from the Internet. Internal wired FNWI traffic will not be
hindered. Internal and external telephony will not be hindered too. If
there are serious objections to this maintenance for ongoing research,
one can [contact us](/en/howto/contact/).
