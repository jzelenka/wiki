---
author: wim
date: 2018-04-10 10:47:00
tags:
- medewerkers
- studenten
title: RU mail-adressen (@ru.nl, @student.ru.nl, ...) in Science LDAP adresboek
---
Voor wie alle Science-mail doorstuurt naar een RU Exchange adres
(@ru.nl, @student.ru.nl, @fnwi.ru.nl of @donders.ru.nl), wordt in het
[Science LDAP-adresboek](/nl/howto/ldap-adresboek/) vanaf nu dat
doorstuuradres vermeld i.p.v. het Science mail adres. Het voordeel van
het gebruik van dit Science LDAP adresboek kan zijn dat men eenvoudig
adressen van FNWI-medewerkers en FNWI-studenten kan vinden, zonder te
moeten kiezen uit alle RU-naamgenoten.
