---
author: petervc
date: 2011-05-25 17:44:00
tags:
- studenten
- medewerkers
- docenten
title: Lock screen when leaving the room\!
---
Leaving your office for a few minutes? Then make sure to have your
screen locked! If you leave your computer unattended (using no
screensaver with password protection) and your room hasn’t been locked,
there is a chance that the data on your PC could be compromised, or that
illegal actions could be carried out by someone. During the campaign, we
will devote extra attention to unattended PC’s. You might be surprised
to find a ‘static’ sticker on your monitor. You can turn on screen lock
on a Windows computer by pressing the Windows and the L-button
simultaneously.
