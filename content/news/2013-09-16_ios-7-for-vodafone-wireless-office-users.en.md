---
author: bertw
date: 2013-09-16 13:56:00
tags:
- medewerkers
- docenten
title: iOS 7 for Vodafone Wireless Office users
---
On September 18 Apple introduced iOS 7. The upgrade to iOS 7 requires
that iPhone users with a [Vodafone Wireless Office](/en/tags/telefonie)
subscription from the RU re-enter their Internet settings (APN)
manually. You can do this through: Settings / General / Mobile Network /
Mobile Datanetwork / Mobile Data / APN, after the upgrade to iOS7. The
internet settings (APN) are: office.vodafone.nl (Username: vodafone and
Password: vodafone).
