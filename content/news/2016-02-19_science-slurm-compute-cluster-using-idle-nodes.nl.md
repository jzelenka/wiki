---
author: polman
date: 2016-02-19 17:41:00
tags:
- medewerkers
- studenten
title: 'Science SLURM rekencluster: gebruik van vrije nodes'
---
De [configuratie van SLURM](/nl/howto/slurm/) van het
[cn-cluster](/nl/howto/hardware-servers/) is aangepast. Hierdoor krijgen
jobs van eigenaren hoge prioriteit in hun eigen partition (op hun eigen
nodes) en het maakt het mogelijk dat alle clustergebruikers met lage
prioriteit alle nodes gebruiken. Daarnaast kan iedereen jobs aanleveren
voor de node die C&CZ aangeschaft heeft, waarbij kortere jobs hogere
prioriteit krijgen.
