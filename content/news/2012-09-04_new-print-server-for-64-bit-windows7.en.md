---
author: wim
date: 2012-09-04 16:14:00
tags:
- studenten
- medewerkers
- docenten
title: New print server for 64-bit Windows7
---
Because we upgraded the [computer labs](/en/howto/terminalkamers/) from
Windows-XP to 64-bit Windows7, we had to install a new printserver, with
the name ‘printto.science.ru.nl’. For all [budgetted
printers](/en/howto/printers-en-printen/) this server must be used. See
[our printer page](/en/howto/printers-en-printen/) for detailed
information how to attach a printer ffrom this server.
