---
author: mkup
date: 2011-05-26 23:21:00
tags:
- studenten
- medewerkers
- docenten
title: Spammende PC's eerder opgespoord
---
[CERT-RU](http://www.ru.nl/cert), die ICT-beveiligingsincidenten binnen
de RU coördineert, ontvangt met enige regelmaat meldingen over besmette
PC’s die spam-mail staan te verspreiden. Vaak zijn deze opgemerkt door
[Senderbase](http://www.senderbase.org/senderbase_queries/detailip?search_string=131.174.0.0%2F16;amp;max_rows=50;amp;dnext_set=0;amp;tddorder=lastday+desc;amp;which_others=%2F16#page)
omdat het dagelijkse volume mail veel hoger is dan het maandelijks
gemiddelde. De routers bij FNWI loggen het initiëren van een
SMTP-verbinding naar buiten de RU. Bij een abnormaal hoog volume gaat er
een alarm af. Daardoor zijn besmette PC’s meestal al weer schoongemaakt
als de melding bij CERT-RU binnenkomt.
