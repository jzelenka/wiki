---
author: wim
date: 2010-03-23 10:35:00
title: New pc's in pc rooms dual-boot Windows/Linux
---
All new pc’s in the [pc rooms and study
landscape](/en/howto/terminalkamers/) are dual-boot: an individual
student or employee can choose to start Windows-XP or Linux Fedora 11.
The rooms can now also be used for courses that require Linux. Only the
older pc’s in TK075 (HG00.075) are still single-boot Windows-XP. In the
course of 2010 the migration to Windows 7 and Ubuntu 10.04 Linux will
start.
