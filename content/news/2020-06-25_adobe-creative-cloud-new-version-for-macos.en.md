---
author: john
date: 2020-06-25 17:25:00
tags:
- studenten
- medewerkers
- docenten
title: Adobe Creative Cloud new version for macOS
---
A new version of the Adobe [Creative Cloud
software](https://www.adobe.com/creativecloud.html) (Acrobat,
Illustrator, Photoshop etc., now also Premiere Pro and Audition) is
available for macOS, including macOS 10.15 Catalina. The license permits
use on devices owned by Radboud University. The installation proceeds
through the Adobe Creative Cloud Desktop app, which gives the choice of
RU licensed Adobe products after logging in with an Adobe account.
Detailed install instructions can be requested from [the C&CZ
helpdesk](/en/howto/contact/).
