---
author: petervc
date: 2022-03-02 12:35:00
tags:
- medewerkers
- studenten
title: New version of Mathematica (13.0.1)
cover:
  image: img/2022/mathematica-13.png
---
A new version of [Mathematica](/en/howto/mathematica/) (13.0.1) has been
installed on all C&CZ managed Linux systems, older versions can still be
found in `/vol/mathematica`. The installation files for Windows, Linux
and macOS can be found on the [Install](/en/howto/install-share/)-disk,
also for older versions of Mathematica. Installation and license info
can be [requested from C&CZ](/en/howto/contact/).
