---
author: caspar
date: 2012-01-03 14:25:00
title: Faculty Education Award 2011
---
The Faculty of Science Education Award 2011 was awarded to C&CZ, for its
“long and excellent service record with regards to the provision of
support to students and teachers”, according to the unanimous jury. The
dean, who presented the award, mentioned several developments in the IT
infrastructure and the teaching support systems and the close
cooperation with staff and students, which makes that products are
developed that are needed. Since 2001, the Faculty Education Award is
awarded by representatives from the [Faculty Student
Council](http://www.ru.nl/fnwi/fsr) and both student and staff faculty
degree programme committees. The jury is chaired by the Vice-Dean for
Education. The award consists of a certificate and a work of art by
[Anneke van Bergen](http://www.annekevanbergen.nl/). C&CZ is very
honoured by this splendid award.
