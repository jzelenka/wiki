---
author: petervc
date: 2013-01-07 13:34:00
tags:
- studenten
- medewerkers
title: 'Euroglot: nieuwe licentie op Install-schijf'
---
De nieuwe licentie van [Euroglot](http://www.euroglot.nl) voor 2013 is
op de [Install](/nl/howto/install-share/)-netwerkschijf te vinden.
