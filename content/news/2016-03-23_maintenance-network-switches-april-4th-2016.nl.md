---
author: mkup
date: 2016-03-23 10:53:00
tags:
- studenten
- medewerkers
- docenten
title: Onderhoud netwerkswitches op 4 april 2016
---
Op maandagavond 4 april 2016 tussen 22:30 en 24:00 uur vindt onderhoud
plaats op de netwerkswitches bij FNWI. Dit betekent dat tussen genoemde
tijden het bedrade en draadloze netwerk enige tijd niet beschikbaar zal
zijn op de locaties Huygens, Linnaeus, FELIX, Nanolab, A1-kelder,
Logistiek Centrum, Goudsmit, Proeftuin, Mercator 1/2/3, UBC en
Kinderdagverblijf. Alleen in HFML zal het netwerk operationeel blijven.
Het advies is om zelf na te gaan welke systemen of opstellingen hiervan
hinder kunnen ondervinden en zo mogelijk maatregelen te nemen door
netwerkactiviteit op een andere tijd te laten plaatsvinden. Denk evt.
ook aan systemen met alarmering via Internet.
