---
author: sioo
date: 2021-04-20 11:10:00
tags:
- medewerkers
- docenten
- studenten
title: Linux login server lilo5 decommissioned
---
The support for Ubuntu 16.04 LTS ends on April 30th, 2021. Therefore we
will turn off lilo5 with this version of Ubuntu on May 1st, 2021. The
default login server (lilo.science.ru.nl) points to lilo7 with Ubuntu
20.04 LTS, but lilo6 with Ubuntu 18.04 LTS will remain available until
support for this version ends in 2023. The [signatures of the C&CZ
loginservers](https://wiki.cncz.science.ru.nl/Hardware_servers#Linux_.5Bloginservers.5D.5Blogin_servers.5D)
can be checked. Please also check these tips for improving your SSH
settings [tips for improving your SSH
settings](https://wiki.cncz.science.ru.nl/SSH#.5Ben.5DSSH_tips_and_settings.5B.2Fen.5D.5Bnl.5DSSH_Instellingen_en_tips.5B.2Fnl.5D).
