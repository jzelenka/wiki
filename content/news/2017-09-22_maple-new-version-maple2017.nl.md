---
author: polman
date: 2017-09-22 16:02:00
tags:
- studenten
- medewerkers
- docenten
title: 'Nieuwe versie Maple: Maple2017'
---
De nieuwste versie van [Maple](/nl/howto/maple/), Maple2017, is te vinden
op de [Install](/nl/howto/install-share/)-netwerkschijf en is op door
C&CZ beheerde Linux-computers geïnstalleerd. Licentiecodes zijn bij C&CZ
helpdesk of postmaster te verkrijgen voor de deelnemende afdelingen.
