---
author: wim
date: 2011-02-16 10:53:00
tags:
- medewerkers
title: Windows print server
---
A new Windows 2008 printer server has been installed to support
departemental printing. The server is named
**drukker.nwi.ru.nl** – edit 2014-11-18 renamed to
**drukker.b-fac.ru.nl**. This server can support
printing from Windows XP, Windows Vista (32 and 64 bit) and Windows 7
(32 and 64 bit). Printer drivers are automatically installed by
connecting to the printer server and selecting the print queue.

Pros:

-   Currently very little problems occurred

Cons:

-   **Only** for standard, frequent installed,
    common available printers (HP LaserJet, Xerox, Ricoh)
-   **Little** protection against abuse
-   Cannot make use of the CnCZ print budget software, so
    **no** accounting or print budget facilities

This implies that only printers that are not publically available should
be considered adding to this server. Please contact CnCZ to figure out
if a printer is suitable for installation.

See [Printers\_en\_printen](/en/howto/printers-en-printen/) for an
explanation how to connect printers.
