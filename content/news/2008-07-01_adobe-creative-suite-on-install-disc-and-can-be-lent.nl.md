---
author: petervc
date: 2008-07-01 13:56:00
title: Adobe Creative Suite op install-schijf en te leen
---
De DVD’s van Adobe CS3 Design Premium UK en NL voor
[MS-Windows](/nl/howto/microsoft-windows/) en voor UK
[Macintosh](/nl/howto/macintosh/) zijn op de
[install](http://www.cncz.science.ru.nl/software/installscience) netwerk
schijf gezet. Voor serienummers kan men zich tot Postmaster wenden. De
DVDs zijn ook te leen op HG03.055. Voor de andere versies (Web) lijkt
minder belangstelling te bestaan. Mag op de campus door medewerkers en
studenten gebruikt worden. Medewerkers kunnen voor thuisgebruik een DVD
set voor 35 euro aanschaffen bij [Surfspot](http://www.surfspot.nl).
Thuisgebruik door studenten is niet toegestaan onde deze
licentievoorwaarden.
