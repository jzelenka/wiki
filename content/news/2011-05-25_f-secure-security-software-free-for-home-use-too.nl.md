---
author: petervc
date: 2011-05-25 15:36:00
tags:
- studenten
- medewerkers
- docenten
title: 'F-Secure beveiliging: gratis, ook voor thuis\!'
---
Medewerkers en studenten weten soms niet dat de F-Secure
beveiligingssoftware gratis is voor alle medewerkers en studenten, ook
voor thuisgebruik. Licentiecodes voor
[F-Secure](http://www.f-secure.com) zijn te vinden op
[Radboudnet](http://www.radboudnet.nl/fsecure).
