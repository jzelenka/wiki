---
author: petervc
date: 2017-01-19 18:01:00
tags:
- medewerkers
- studenten
title: Close browser after "logging out" of SURFconext services like Edugroepen and
  SURFspot
---
Logging in to websites as [Edugroepen](https://www.edugroepen.nl),
[SURFspot](https://www.surfspot.nl/),
[SURFdrive](https://www.surfdrive.nl/) or
[SURFfilesender](https://www.surffilesender.nl/) with U-number and
RU-password, uses
[SURFconext](https://www.surf.nl/diensten-en-producten/surfconext/index.html).
If you hit “Logout”, you seem to be logged out, but the browser still
has access. Within the browser, you or any other user can connect to any
website with SURFconext authentication and will be automatically logged
in. Therefore we strongly urge you to close the browser after using a
SURFconext service, particularly when using a shared device or
workstation.
