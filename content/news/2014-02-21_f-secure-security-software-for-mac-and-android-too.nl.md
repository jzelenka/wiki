---
author: petervc
date: 2014-02-21 16:38:00
tags:
- medewerkers
- studenten
title: 'F-Secure beveiligingssoftware: ook voor Mac en Android'
---
Voor apparatuur die met RU-fondsen of via derde geldstroom ten behoeve
van RU-werkzaamheden is aangeschaft, kan een door de RU betaalde versie
van de F-Secure beveiligingssoftware geïnstalleerd worden. Er is een
[versie voor MS Windows 7 en
8](http://www.radboudnet.nl/informatiebeveiliging/secure/download-secure/)
en er is een
[Mac-versie](http://www.radboudnet.nl/informatiebeveiliging/secure/aanvraagformulier/).

De prijs van F-Secure voor apparatuur (Windows PC’s, Macs en Android)
die privé eigendom is, is zowel voor medewerkers als studenten verlaagd
naar € 2 per jaar. Zoek hiervoor bij [Surfspot](http://www.surfspot.nl)
naar Secure. Natuurlijk kan men in plaats hiervan een ander
beveiligingspakket installeren, zoals de gratis [Panda voor Windows
8](http://www.pandasecurity.com/netherlands/windows8/), de gratis [Avast
voor Windows 7](http://www.avast.com/nl-nl/index) of evt. de algemeen
als minder beoordeelde [Microsoft Security
Essentials](http://windows.microsoft.com/nl-nl/windows/security-essentials-download).
