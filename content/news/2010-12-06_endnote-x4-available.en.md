---
author: petervc
date: 2010-12-06 16:35:00
title: Endnote X4 available
---
[Endnote](http://www.endnote.com/) version X4 is available for
MS-Windows and Macintosh. It can be installed on Windows from the
[install](http://www.cncz.science.ru.nl/software/installscience) network
disk. The CDs can also be borrowed by employees and students, for home
use too. It will also be installed shortly on the [Windows-XP Managed
PCs](/en/howto/windows-beheerde-werkplek/).
