---
author: polman
date: 2014-07-02 14:25:00
tags:
- medewerkers
- studenten
title: Limit of number of groups increased drastically
---
The limit of the number of groups with whom one can share files on
[network discs](/en/howto/diskruimte/), has been increased from 16 to
100. This historical problem in the design of
[NFS](http://en.wikipedia.org/wiki/Network_File_System) has been [solved
in modern versions of
Linux](http://www.xkyle.com/solving-the-nfs-16-group-limit-problem).
