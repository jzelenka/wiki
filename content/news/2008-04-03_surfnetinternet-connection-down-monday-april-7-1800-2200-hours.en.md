---
author: petervc
date: 2008-04-03 19:00:00
title: SURFnet/Internet connection down Monday April 7 18:00-22:00 hours
---
The [UCI](http://www.ru.nl/uci) will again try to upgrade the connection
between the RU and SURFnet next Monday evening from 1 Gb/s to 10 Gb/s.
The connection between RU and SURFnet will be interrupted during the
maintenance. C&CZ will plan the upgrade of the FNWI router to 10 Gb/s
afterwards.
