---
author: petervc
date: 2010-03-04 17:05:00
title: Tijdsbesparing bij het maken van afspraken (agendabeheer)
---
Mocht u veel tijd kwijt zijn met het plannen van afspraken, vooral
wanneer dat met vrij vaste en/of grote groepen medewerkers is, lees dan
verder.

De afgelopen paar weken is bij het Faculteitsbureau met succes getest
met de [Mozilla
SunBird](http://www.mozilla.org/projects/calendar/sunbird/)
kalendersoftware. Hierin ziet men de bezet-informatie van op dit moment
enkele tientallen medewerkers die gebruik maken van Outlook, Google
Calendar of een FNWI E-Groupware kalender.

Mocht u belangstelling hebben om ook van deze kalendersoftware gebruik
te maken, dan horen we dat graag, met erbij een lijst van de personen
met wie u het vaakst (groeps)afspraken maakt. Deze personen kunnen
alvast zelf de [instellingen van hun agenda
wijzigen](/nl/howto/bezet-info-publiceren/).

Overigens is het de bedoeling dat in 2010 de RU overstapt op een modern
mail/agendasysteem voor alle medewerkers en studenten. Dat systeem is te
gebruiken met een prima webclient, maar ook met Outlook, Thunderbird
e.d. Dat moet het maken van afspraken met RU-medewerkers pas echt
eenvoudig en efficiënt maken. Als u daarop kunt/wilt wachten, hoeft u
natuurlijk niet te reageren.
