---
author: petervc
date: 2018-10-12 15:21:00
tags:
- medewerkers
- docenten
title: MS Office 2019 instructions on Install network share
---
Instructions to install the most recent version of [Microsoft
Office](http://office.microsoft.com), 2019, are available on the
[Install](/en/howto/install-share/) network share. There is a new
installation method, with the [Office Deployment
Tool](https://www.microsoft.com/en-us/download/details.aspx?id=49117).
The MAK license code is known to the C&CZ helpdesk. They can share the
screen of your PC and then enter the code. They only do that if the PC
is owned by FNWI. For privately owned computers you can order this
software relatively cheap at [Surfspot](http://www.surfspot.nl).
