---
author: petervc
date: 2012-09-24 11:00:00
tags:
- studenten
- medewerkers
- docenten
title: TeX Collection 2012
---
The most recent version of the [TeX
Collection](http://www.tug.org/texcollection), June 2011, is available
for MS-Windows, Mac OS X and Unix/Linux. It can be found on the
[Install](/en/howto/install-share/) network share and can also be
[borrowed](/en/howto/microsoft-windows/).
