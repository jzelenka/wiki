---
author: caspar
date: 2015-06-25 15:13:00
tags:
- medewerkers
- docenten
- studenten
title: 'Information session: "Onderwijs in Beeld" becomes "Weblectures"'
---
Monday June 29, from 12:30 to 13:30 (sharp), in LIN1 (Linnaeus
building).

We hereby cordially invite you to the information session about the new
weblectures system which will become operational at the Faculty of
Science in September 2015. This new Radboud University service will
replace the current Onderwijs in Beeld system for recording lectures and
viewing the recordings. In a presentation we will zoom in on what this
means for you: what will change for students and lecturers and what are
the new features when your course is recorded.

We hope to see you on Monday June 29.

See also [the information site on
weblectures](http://www.radboudnet.nl/weblectures) (log on with your
u-number).

With kind regards, Anna Galema (Weblectures functional manager) and
Peter van der Wolk (Coordinator weblectures Mediatechniek)
