---
author: petervc
date: 2013-11-08 10:48:00
tags:
- medewerkers
- docenten
title: Windows 8.1 op Install-schijf
---
De nieuwste versie van [Microsoft Windows](http://office.microsoft.com),
8.1, is op de [Install](/nl/howto/install-share/)-netwerkschijf te
vinden. De licentievoorwaarden staan gebruik op RU-computers toe.
Licentiecodes zijn bij C&CZ helpdesk of postmaster te verkrijgen. Voor
thuisgebruik kan de software op [Surfspot](http://www.surfspot.nl)
besteld worden.
