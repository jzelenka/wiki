---
author: remcoa
date: 2015-11-24 18:11:00
tags:
- medewerkers
- studenten
title: Digitaal toetsen/tentamineren in een veilige browseromgeving in de PC-cursuszalen
---
Sinds kort is het mogelijk om [digitaal te
toetsen](/nl/howto/digitaal-toetsen/) (tentamens af te nemen) in de door
C&CZ beheerde [PC-cursuszalen](/nl/howto/terminalkamers/). Diverse
vraag/antwoord-vormen worden ondersteund, momenteel met name open vragen
en multiple choice. Neem [contact op met C&CZ](/nl/howto/contact/) voor
nadere informatie.
