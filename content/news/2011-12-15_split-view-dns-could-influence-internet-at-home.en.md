---
author: petervc
date: 2011-12-15 15:08:00
tags:
- studenten
- medewerkers
- docenten
title: Split-view DNS could influence Internet at home
---
Anyone using at home a `science.ru.nl` nameserver like
`ns1.science.ru.nl`, should remove these DNS-servers from the list of
DNS-servers, because we started using [split-view
DNS](http://en.wikipedia.org/wiki/Split-horizon_DNS). With split-view
DNS, we can give internal (RU on campus) pc’s answers differing from
those given to external pc’s. The [UCI](http://www.ru.nl/uci) introduced
this a few months ago for e.g. the DNS zones `ru.nl` and `heyendaal.net`
to increase [IT security](http://www.ru.nl/ict-beveiliging/).
