---
author: petervc
date: 2009-12-15 18:08:00
title: Mail onterecht in Spam-folder
---
Op 7 december is bij de reguliere update van het spamfilter, een nieuwe
filter-set geactiveerd die in sommige situaties wat al te enthousiast
mail als spam bestempelde. Dit leidde ertoe dat sommige valide mail ten
onrechte in de Spam-folder terecht kwam; false positives. Wij raden
iedereen aan in de Spam-folder (onder Inbox) te kijken of daar false
positives in terecht zijn gekomen. Eens per week een blik op de
Spam-folder werpen is altijd een goed idee als men geen enkele mail wil
missen, maar voor 7 en 8 december is er dus ook directe aanleiding toe .

Een uitgebreide verhandeling over onze [virus- en
spamfiltering](/nl/howto/email-spam/) en de mogelijkheden voor
persoonlijke instellingen is aanwezig.

Onze excuses voor eventuele overlast.
