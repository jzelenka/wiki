---
author: petervc
date: 2008-04-04 15:42:00
title: Nieuwe disk-server met 90 GB partities
---
Er zijn weer nieuwe
[disk-servers](/nl/howto/hardware-servers#disk.5b-.5d.5b-.5dservers/) met
door FNWI-afdelingen te huren 90 GB partities. De vorige RAID-arrays
waren uitverkocht en een enkele is al uit onderhoud. Er is gekozen voor
een nieuw type Linux disk server met interne controller en harde
schijven. De partities van de RAID-array die uit onderhoud is gegaan,
zullen in overleg met de huurders verhuisd worden naar een nieuwe
server.
