---
author: wim
date: 2016-02-23 17:25:00
tags:
- medewerkers
- studenten
title: Overgang naar RU-printsysteem Peage
---
C&CZ bereidt samen met het [ISC](http://www.ru.nl/isc) de vervanging
voor van het C&CZ printsysteem door het RU-brede
[Peage](http://www.ru.nl/facilitairbedrijf/printen/printen-studenten/printen-campus/)
printsysteem, dat al enkele jaren door studenten gebruikt kan worden.
Zodra medewerkers en gasten ook Peage kunnen gebruiken, kan het C&CZ
printsysteem uitgefaseerd gaan worden. De vervanging zal vermoedelijk
vanaf april plaatsvinden. Voordelen van Peage zijn: follow-me-printing
en scan-to-me scanning. Een nadeel is dat men niet meerdere
budgetgroepen kan hebben. Doorbelasting voor medewerkers gaat naar de
kostenplaats van de hoofdaanstelling. De Konica Minolta printer in het
restaurant van het Huygensgebouw is al lang een Peage-printer. Als Peage
verder ingevoerd wordt, zal begonnen worden met de Konica Minolta
printers op de begane grond van het Huygensgebouw. Na de begane grond
zijn de verdiepingen aan de beurt en tenslotte de gebouwen rond Huygens.
