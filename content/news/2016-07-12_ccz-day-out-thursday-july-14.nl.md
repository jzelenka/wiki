---
author: petervc
date: 2016-07-12 17:11:00
tags:
- medewerkers
- studenten
title: 'C&CZ dagje uit: donderdag 14 juli'
---
Het jaarlijkse dagje uit van C&CZ is gepland voor donderdag 14 juli.
Voor bereikbaarheid in geval van ernstige storingen wordt gezorgd.
