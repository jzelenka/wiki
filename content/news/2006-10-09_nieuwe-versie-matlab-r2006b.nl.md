---
author: polman
date: 2006-10-09 14:54:00
title: Nieuwe versie Matlab (R2006b)
---
Met ingang van oktober 2006 is er een nieuwe versie (7.3, R2006b) van
Matlab beschikbaar op alle door C&CZ beheerde Suns en PCs met Linux of
Windows-XP.

De oude versie (R2006a) wordt over enkele weken verwijderd, wanneer
duidelijk is dat de overgang naar R2006b geen grote problemen
veroorzaakt. Om de oude versie tot die tijd nog te kunnen gebruiken moet
het zoekpad uitgebreid worden met /vol/matlab-R2006a/bin (onder Unix).

Voor meer info over deze release, zie
<http://www.mathworks.com/products/new_products/latest_features.html?ref=fp2006b>

Bij vragen/problemen: postmaster\@science.ru.nl, tel 53535/56666,
HG03.055.
