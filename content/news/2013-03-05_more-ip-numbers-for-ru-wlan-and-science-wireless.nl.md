---
author: mkup
date: 2013-03-05 11:02:00
tags:
- studenten
- medewerkers
- docenten
title: Meer IP-nummers voor ru-wlan en Science (draadloos)
---
Op maandag 4 maart 2013 om 18.00 uur is, [zoals kort vooraf
aangekondigd](/nl/cpk), het aantal IP-nummers dat in
de FNWI-gebouwen beschikbaar is voor ru-wlan en Science, verdubbeld.
Omdat ru-wlan naar een nieuwe nummerreeks verhuisde, hadden gebruikers
van ru-wlan daardoor verlies van connectiviteit gedurende enkele
minuten. Er was al een plan om ru-wlan en Science binnen de
FNWI-gebouwen te vervangen door het RU-brede Eduroam en ru-wlan. Maar
het gebruik van het draadloze netwerk groeide zo snel, dat we niet
konden wachten tot dit plan uitgevoerd zou zijn. De week ervoor konden
enkele gebruikers soms zelfs geen IP-nummer meer krijgen, terwijl de
lease-tijd al naar 30 minuten teruggebracht was. Daarom werd deze
tijdelijke maatregel toch noodzakelijk, met invoering op korte termijn.
Door de verdubbeling kunnen meer gebruikers [nu al overstappen op het
gebruik van ru-wlan i.p.v. Science](/nl/howto/netwerk-draadloos/), ter
voorbereiding op de uitvoering van het genoemde plan.
