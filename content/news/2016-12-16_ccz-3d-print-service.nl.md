---
author: stefan
date: 2016-12-16 16:15:00
tags:
- studenten
- medewerkers
- docenten
title: C&CZ 3D-printservice
---
Diverse afdelingen binnen FNWI hebben een 3D-printer voor eigen gebruik.
Bij [TeCe](http://www.ru.nl/fnwi/technocentrum/) staan twee 3D-printers
die op twee verschillende technieken gebaseerd zijn, voor het maken van
onderdelen voor experimentele opstellingen. C&CZ start nu een
3D-printservice voor FNWI-medewerkers en -studenten, primair bedoeld
voor gebruik in het onderwijs. Zie voor meer details de
[Printers-pagina](/nl/howto/printers-en-printen/).
