---
author: caspar
date: 2013-04-12 18:19:00
tags:
- studenten
- medewerkers
- docenten
title: CMS-beheerder Wim Janssen gestationeerd bij C&CZ
---
Op 15 april 2013 verhuist Wim G.H.M. Janssen (IMAPP en facultaire
CMS-beheerder) zijn werkplek naar C&CZ. Wim blijft in dienst van IMAPP
maar kan vanuit C&CZ zijn werkzaamheden, waarbij steeds meer wordt
samengewerkt met C&CZ, effectiever uitvoeren. Ook is het zo mogelijk om
bepaalde taken onder te brengen bij C&CZ zodat er meer ruimte ontstaat
voor nieuwe ontwikkelingen. Er kan enige verwarring ontstaan: bij C&CZ
werkt al jaren Wim P.J. Janssen, waardoor vaker een Wim-Wim situatie zal
ontstaan.
