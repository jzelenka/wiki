---
author: alinssen
date: 2010-03-12 17:25:00
title: Web-applicatie voor aanwezigheid medewerkers
---
Binnen diverse afdelingen van de Algemene Dienst van FNWI wordt sinds
enige tijd gebruik gemaakt van een aanwezigheidsregistratie/kloksysteem
gebaseerd op het open source systeem
[TimeTrex](http://www.timetrex.com/). Andere afdelingen die interesse
hebben om het systeem te gebruiken voor een of meer medewerkers, kunnen
zich melden bij C&CZ. Registratie gebeurt met pasjes of via het web. Het
kan een goed hulpmiddel zijn voor het administreren van variabele
werktijden. Het is een enigszins flexibel systeem, dat bijvoorbeeld ook
op “automatisch klokken” ingesteld kan worden, zodat het dan alleen
gebruikt wordt voor registratie van verlof, ziekte e.d. Er is overigens
(nog) geen koppeling met de RU BASS-HRM registratie, die minstens
eenmaal per kwartaal bijgewerkt moet worden.
