---
author: petervc
date: 2014-03-12 12:55:00
tags:
- studenten
- medewerkers
- docenten
title: Password lock-out only after 10 false attempts
---
Until today, a Science account was locked out after 3 consecutive false
login attempts. This has changed: now only after 10 false attempts the
account is locked out for 30 minutes. This way the behaviour for the
Science account and for the RU account are identical.

Note that you are not forced to change the Science or RU password on a
regular basis. Yet we advise everyone to do so as changing passwords
once in a while adds to the security. But please think of all devices
where this password is stored (laptop, smartphone, home PC, etc.).
