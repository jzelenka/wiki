---
author: caspar
date: 2008-12-09 13:47:00
title: Adobe Creative Suite 4 Web/Design Premium
---
Op [Surfspot](http://www.surfspot.nl) is de [Adobe Creative Suite 4
Design/Web Premium Bundel](http://www.adobe.com/products/creativesuite)
voor thuisgebruik door medewerkers te koop voor € 40, zowel voor
MS-Windows als voor Apple Mac. De produkten voor instellingsgebruik zijn
besteld en zullen binnenkort beschikbaar zijn.
