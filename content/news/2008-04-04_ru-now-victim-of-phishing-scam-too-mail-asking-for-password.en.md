---
author: petervc
date: 2008-04-04 13:08:00
title: RU now victim of phishing scam too (mail asking for password)
---
Now also a lot of RU employees received emails, that at first sight
looked as if they were sent by a RU Helpdesk, in which was asked for a
reply with loginname and password. The reply of a naive user again went
to a “@hotmail.co.uk” mail-address. C&CZ likes to stress again that \*no
official institution\* will ever ask you for your password, just like a
bank never will ask you for your pincode for your bank account. This is
almost an exact copy of the incident with TU Delft that C&CZ reported on
January 29. If someone was unwise enough to reply with username and
password, then it is wise to change the password immediately. C&CZ will
contact users that sent replies through a C&CZ mailserver and has
blocked mail to the “@hotmail.co.uk” mail-adres in question on their
smtp-servers.
