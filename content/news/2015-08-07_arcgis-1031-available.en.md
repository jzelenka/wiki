---
author: bram
date: 2015-08-07 16:58:00
tags:
- medewerkers
- docenten
- studenten
title: ArcGIS 10.3.1 available
---
A campus license of [ArcGIS](http://www.arcgis.com/) 10.3.1 is
available. ArgGIS is being installed on all pcs in the [computer
labs](/en/howto/terminalkamers/). On request, it can be installed on
managed Windows 7 pc’s as well. Setup files and installation
instructions are available on the [Install](/en/howto/install-share/)
network share for home use by students and employees. Home users need to
setup a [VPN connection](/en/howto/vpn/) in order to reach the license
server.
