---
author: petervc
date: 2010-03-05 17:50:00
title: Openingstijden C&CZ aangepast
---
C&CZ kan de in 2008 verruimde [openingstijden](/nl/howto/helpdesk/) niet
langer handhaven, onze nieuwe openingstijden zijn van 08:30 uur tot na
18:00 uur.
