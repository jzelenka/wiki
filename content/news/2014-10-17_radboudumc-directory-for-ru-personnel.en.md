---
author: bertw
date: 2014-10-17 14:14:00
tags:
- medewerkers
- docenten
title: RadboudUMC directory for RU personnel
---
Radboud University personnel with a valid U-number and RU-password can
also find [the RadboudUMC
directory](https://www.extern.umcn.nl/ru_umcn_tel/ru.html) in the
RadboudUMC intranet. An [overview of all
directories](/en/howto/telefoon-en-e-mail-gidsen/) is also available.
