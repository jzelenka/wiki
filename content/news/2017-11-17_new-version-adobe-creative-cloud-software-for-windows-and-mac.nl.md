---
author: petervc
date: 2017-11-17 15:56:00
tags:
- medewerkers
- docenten
title: Nieuwe versie Adobe Creative Cloud software Windows en Mac
---
Een nieuwe versie van de [Adobe Creative Cloud
software](https://www.adobe.com/nl/creativecloud/catalog/desktop.html)
voor MS-Windows en Mac is beschikbaar op de
[Install](/nl/howto/install-share/)-netwerkschijf. Licentiecodes voor de
periode 2017-2020 zijn reeds in deze pakketten ingebouwd, daarom zijn ze
beschikbaar als zip-bestanden die met een wachtwoord beveiligd zijn. De
wachtwoorden zijn bij C&CZ helpdesk of postmaster te verkrijgen. Volgens
de site-license mag deze Adobe Creative Cloud for Enterprise (Specified
Apps Only Non-Video) op alle computers in eigendom van de RU worden
geïnstalleerd zonder bijkomende kosten. Voor thuisgebruik van deze
software kunnen medewerkers en studenten terecht op
[Surfspot](http://www.surfspot.nl). De software bestaat uit de volgende
onderdelen: Acrobat DC 2017-2020-ML, Audition CC 2017 (alleen voor
Windows), Bridge CC 2017, Dreamweaver CC 2017, Fireworks CS6 (alleen
voor Windows), Flash Builder Premium (alleen voor Windows), Illustrator
CC 2017, InCopy CC 2017, InDesign CC 2017, Media Encoder CC 2017, Muse
CC 2017 (alleen voor Windows) en Photoshop CC 2017.
