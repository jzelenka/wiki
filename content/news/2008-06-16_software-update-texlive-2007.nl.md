---
author: petervc
date: 2008-06-16 18:06:00
title: 'Software update: TeXlive 2007'
---
Vandaag is op de door C&CZ beheerde Windows en Unix computers TeXlive
2007 de standaard versie van (La)TeX geworden, als opvolger van TeXlive
2005. Als er geen problemen gemeld worden, zal de oude versie over
enkele enige maanden verwijderd worden.
