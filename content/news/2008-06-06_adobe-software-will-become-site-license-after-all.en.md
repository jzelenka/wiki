---
author: petervc
date: 2008-06-06 15:32:00
title: Adobe software will become site-license after all\!
---
Today [SURFdiensten](http://www.surfdiensten.nl) reported that the site
license agreement between SURFdiensten and [Adobe](http://www.adobe.com)
for Dutch schools and universities reached a final status. The products
and codes for the licensed products are expected to become available
next week.
