---
author: pberens
date: 2011-01-26 16:25:00
tags:
- docenten
title: 'Get more out of Blackboard: Evaluation tools'
---
Did you see the tools to follow your students behaviour in your
Blackboard course already? Please try Control Panel, Evaluation

-   Performance Dashboard: when did students last visit your Blackboard
    course?
-   Course Statistics: shows all students activity in your
    Blackboard-course
-   Early Warning System: sends you an email when a student fails an
    exam, might mis a deadline etc. Might come in handy!

Need some help? Feel free to mail the FNWI Blackboard Helpdesk
