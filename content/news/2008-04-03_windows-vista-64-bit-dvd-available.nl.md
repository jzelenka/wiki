---
author: petervc
date: 2008-04-03 18:54:00
title: Windows Vista 64-bit DVD beschikbaar
---
Ook de 64-bits versie van MS Vista Enterprise upgrade is nu [te
leen](/nl/howto/microsoft-windows/) voor gebruik op de campus, al raadt
C&CZ het gebruik nog af i.v.m. mogelijke problemen met drivers,
netwerkprinters etc.
