---
author: petervc
date: 2015-06-01 14:04:00
tags:
- medewerkers
- docenten
- studenten
title: Watch out\! Scammers send mail on behalf of Ziggo
---
Since a couple of weeks, people receive e-mails that seem to be sent by
cable company Ziggo, but which in reality are phishing mails, sent by
Internet criminals. The subject of the email is “Uw betalingstermijn is
verstreken” (Your payment has expired). The e-mail suggests that the
recipient has an invoice open and threatens with collection costs. The
purpose of such a fraud is to persuade the recipient to click on a link
or open an attachment. For more information about this particular scam,
see
[dichtbij.nl](http://www.dichtbij.nl/eindhoven/112/artikel/4027753/let-op-oplichters-versturen-email-uit-naam-van-ziggo.aspx).
This time it’s fake e-mails on behalf of Ziggo, but other crooks are
active too. A useful website that provides information on scams is
[fraudehelpdesk.org](http://www.fraudhelpdesk.org/).
