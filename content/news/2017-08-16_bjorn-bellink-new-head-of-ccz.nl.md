---
author: petervc
date: 2017-08-16 16:19:00
tags:
- studenten
- medewerkers
title: Bjorn Bellink nieuw hoofd C&CZ
---
Per 1 september start {{< author "bbellink" >}} als
C&CZ-afdelingshoofd. {{< author "petervc" >}}, die sinds het vertrek
van Caspar Terheggen waarnam, gaat weer terug naar zijn oude functie.
