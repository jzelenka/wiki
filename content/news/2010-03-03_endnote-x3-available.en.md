---
author: polman
date: 2010-03-03 18:09:00
title: Endnote X3 available
---
[Endnote](http://www.endnote.com/) version X3 is available for
MS-Windows and Macintosh. It can be installed on Windows from the
[install](http://www.cncz.science.ru.nl/software/installscience) network
disk. The CDs can also be borrowed by employees and students, for home
use too. It has also been installed on the [Windows-XP Managed
PCs](/en/howto/windows-beheerde-werkplek/).
