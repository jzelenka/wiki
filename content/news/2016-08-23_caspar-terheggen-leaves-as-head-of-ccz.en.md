---
author: petervc
date: 2016-08-23 15:09:00
tags:
- studenten
- medewerkers
title: Caspar Terheggen leaves as head of C&CZ
---
Caspar Terheggen will resign als head of C&CZ on September 1. He has
accepted the new function of enterprise and information architect of
Radboud University. Peter van Campen will be acting head of C&CZ, until
a new head of C&CZ has been appointed.
