---
author: petervc
date: 2008-01-28 18:14:00
title: Surfgroepen voor samenwerken met anderen
---
Voor het samenwerken (delen van bestanden etc.) met andere medewerkers
en studenten van universiteiten en hogescholen en zelfs externen is
[Surfgroepen](http://www.surfgroepen.nl) een goed gratis middel dat bij
te weinig mensen bekend is.
