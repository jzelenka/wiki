---
author: petervc
date: 2008-04-23 18:16:00
title: Netwerk upgrade geslaagd
---
Op zondagochtend 20 april tussen 09:30 en 11:30 uur is de oude
router/switch die de netwerken op het Toernooiveld met elkaar en met de
rest van het RU campusnetwerk (en daardoor ook met Surfnet/Internet)
verbindt, vervangen door een nieuw, krachtiger exemplaar. De actie van
C&CZ en [UCI](http://www.ru.nl/uci) was op dit tijdstip gepland om zo
weinig mogelijk overlast te veroorzaken voor medewerkers en studenten.

De komende weken zullen nog enkele aanpassingen aan het netwerk gemaakt
worden, daarvan zal de overlast veel geringer zijn.

Enkele aspecten van deze upgrade: 1 router/switch die over 2 locaties
verspreid is met redundante 10Gb uplinks naar het UCI. De router/switch
kan betere afscherming bieden, o.a. door meer ruimte voor “access
control lists” en door het gebruik van “reverse path forwarding”. Door
gebruik van FlexLinks kunnen de desktop-switches eenvoudig en
bedrijfszeker redundant aangesloten worden. Doordat er meer interfaces
zijn, kunnen de cascades van desktop-switches verkleind worden en
achterhaalde aansluitingen van perifere gebouwen gemoderniseerd worden.

Over het nieuwe netwerk (NiNe) schreef het UCI al in [Enter
magazine](http://www.ru.nl/aspx/download.aspx?File=/contents/pages/12801/enter76_okt2007.pdf).
