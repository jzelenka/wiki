---
author: petervc
date: 2007-11-09 18:10:00
title: "StarOffice 8 voor Solaris geïnstalleerd"
---
De nieuwste versie van het kantoorpakket
[StarOffice](http://en.wikipedia.org/wiki/StarOffice) (8) is
geïnstalleerd voor Solaris. Tik `soffice` om het te gebruiken.
