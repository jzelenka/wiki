---
author: petervc
date: 2010-11-20 19:51:00
title: MS Office, latest versions for campus use for Windows and Macintosh
---
The DVD of [MS Office Professional Plus
2010](http://office.microsoft.com/en-gb/professional-plus/) for Windows
XP/Vista/7 NL/UK 32/64 and those of the UK and NL versions of [MS Office
for Mac
2011](http://store.microsoft.com/microsoft/Office-for-Mac-Home-and-Student-2011/product/C565DB0E)
are available for computers owned by the Faculty of Science. The [DVD
for Windows](/en/howto/microsoft-windows/) can be borrowed and is
available on the [install](/en/tags/software) disc. The DVD’s for
[Macintosh](/en/howto/macintosh/) can be borrowed.
