---
author: caspar
date: 2015-07-07 13:49:00
tags:
- medewerkers
title: 'New employee systems development and administration: Simon Oosthoek'
---
As of July 1, {{< author "simon" >}} started to work for FNWI as C&CZ
employee [systems development and
administration](/en/howto/systeemontwikkeling/).
