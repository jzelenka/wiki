---
author: wim
date: 2008-05-20 17:02:00
title: MS Office for Mac 2008 NL/UK \*campus\* can be borrowed
---
A CD of [MS Office 2008 for Macintosh](/en/howto/macintosh/) can be
borrowed for campus use and on Macs owned by departments, the NL- as
well as the UK-version. For home use one can buy it relatively cheap at
[Surfspot](http://www.surfspot.nl), althoug it is always more expensive
than the free [OpenOffice](http://www.openoffice.org).
