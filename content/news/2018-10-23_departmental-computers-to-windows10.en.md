---
author: stefan
date: 2018-10-23 11:12:00
tags:
- medewerkers
title: Departmental computers to Windows10
---
New departmental computers can be installed with Windows10 by C&CZ. Also
existing pc’s can get an upgrade from Windows7 to Windows10. You can
make an appointment for this with the [C&CZ
helpdesk](/en/howto/contact/). After that, users can install software
themselves with the catalogue of the Software Center. N.B.: the
Outlook2016 installed with Windows10 doesn’t work well with the Science
IMAP mailserver. Alternatives: mailclients like Roundcube and
Thunderbird or a different mailserver like the RU Exchange mail and
calendar service.
