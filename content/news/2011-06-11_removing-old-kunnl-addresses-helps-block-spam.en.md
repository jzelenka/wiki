---
author: petervc
date: 2011-06-11 19:41:00
tags:
- studenten
- medewerkers
title: Removing old kun.nl addresses helps block spam\!
---
The last few days some users receive more spam than normally. In many
cases the spam has been sent to an old kun.nl email-address. On the [Do
It Yourself website](http://diy.science.ru.nl) you can see what old
addresses you still have. If you send a mail to
postmaster\@science.ru.nl with the list of addresses you want removed,
that can make a big difference for you. A plan is being made to remove
all kun.nl addresses, but maybe you would like your old addresses to be
removed sooner.
