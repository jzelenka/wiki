---
author: petervc
date: 2016-02-23 17:26:00
tags:
- medewerkers
- studenten
title: IPv6 pilot project
---
C&CZ en [ISC Netwerken](http://www.ru.nl/isc) zijn begin 2016
gezamenlijk een pilot project gestart met
[IPv6](https://nl.wikipedia.org/wiki/Internet_Protocol_versie_6)
netwerken. De doelstelling van de pilot is het opdoen van kennis van en
ervaring met IPv6 om dit op het gehele campusnetwerk succesvol te
implementeren. Op dit moment zijn er twee [dual-stack
Vlans](https://en.wikipedia.org/wiki/IPv6#Dual_IP_stack_implementation),
1 voor clients en 1 voor servers. Omdat het een pilot betreft, wordt er
geen enkele garantie gegeven en kan de IPv6-connectiviteit onderbroken
worden voor aanpassingen. Een verwachte aanpassing is de verhuizing van
de koppeling met SURFnet van het Huygensgebouw naar het Forum
(ISC-gebouw). Tot die tijd kunnen alleen outlets in de FNWI-gebouwen
aangesloten worden op dit netwerk. Aanmelden voor de pilot kan nu dus
nog alleen bij C&CZ netwerkbeheer, netmaster\@science.ru.nl, voor
outlets in de FNWI-gebouwen.
