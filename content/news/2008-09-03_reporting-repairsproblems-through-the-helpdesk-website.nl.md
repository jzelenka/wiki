---
author: theon
date: 2008-09-03 17:20:00
title: Aanmelden reparaties/storingen via de helpdesk website
---
Sinds enige maanden gebruiken diverse medewerkers en studenten al de
[C&CZ helpdesk website](https://helpdesk.science.ru.nl/) om
[reparaties](/nl/howto/reparaties/) aan te vragen en ook andere storingen
te melden. Men moet hier inloggen met de [Science
loginnaam](/nl/howto/login/). Voordelen zijn dat men bij een bekend
apparaat, zoals een [Beheerde Werkplek
PC](/nl/howto/windows-beheerde-werkplek/) geen serienummers e.d. hoeft te
vermelden en dat men de afhandeling van de storing zelf via de website
kan volgen.
