---
author: petervc
date: 2013-02-14 10:52:00
tags:
- studenten
- medewerkers
- docenten
title: 'Phishing mail: "Belangrijke boodschap met betrekking tot uw Radboud Universiteit
  Webmail" en "Fwd: Your e-mail will expire soon"'
---
Recently a number of employees of the Faculty of Science received a
phishing email with the subject “Belangrijke boodschap met betrekking
tot uw Radboud Universiteit Webmail” and content “Nieuw Belangrijk
Veiligheid Bericht Alert! Inloggen om het probleem op te lossen.” Others
received mail with subject “Fwd: Your e-mail will expire soon”. The link
provided in the mail points to a copy of our Horde webmail service. Big
differences are:

-   the URL is not within science.ru.nl
-   it is not a secure https connection, the lock is not present
-   the account name and password that you type in, do not arrive at
    C&CZ servers, but at Internet criminals. They can then access all
    files and mail of that Science account. Usually the criminals try to
    use the user credentials to send spam via our mail servers. This is
    something we notice; in such a case C&CZ will disable the user
    account a.s.a.p. to prevent further abuse of the account and to
    prevent our mail servers from ending up on black lists.

It is not the first time we see a phishing attack specifically aimed at
Science-users and it will most probably not be the last time either.
Please stay alert! If you want to read more about phishing, please see
[this monthly security
newsletter](http://www.securingthehuman.org/newsletters/ouch/issues/OUCH-201302_en.pdf).
