---
author: wim
date: 2008-09-03 00:12:00
title: Nieuwe PC-zaal TK023
---
Er is een nieuwe [terminalkamer](/nl/howto/terminalkamers/) TK023
(HG00.023), met 41 moderne PC’s met grote 24" schermen met hoge
resolutie. Voor C&CZ is dit een nieuw type: de gebruiker kan zowel
Windows-XP als Fedora Linux starten. Zo’n dual- of multi-boot PC is de
standaard voor de toekomst van de terminalkamers, met later vast ook
Windows-Vista naast Windows-XP.
