---
author: petervc
date: 2014-02-20 17:39:00
tags:
- medewerkers
- studenten
title: Scannen op Konica Minolta MFP's
---
Update: per 27 februari zijn de instellingen van de switchpoorten van
alle MFP’s aangepast. Overblijvende problemen s.v.p. [melden bij
C&CZ](/nl/howto/contact/).

Scannen naar e-mail gaat (ging?) soms fout op de KM MFP’s. Meestal helpt
het uit- en weer aanzetten van de MFP. Maar ook het scannen naar een
USB-stick is natuurlijk een alternatief. Log hiervoor in op de MFP met
de scan-pincode (1111), leg een origineel in de papierinvoer of op de
glasplaat, kies `Scan` en stop daarna de USB-stick in de USB-poort aan
de rechterzijkant van een MFP, bovenaan. Na een paar seconden herkent de
MFP de USB-stick en presenteert de keus “Save a document to external
memory”. Kies `OK` en druk op `Start`. Zie ook de
[RU-handleiding](http://www.ru.nl/publish/pages/687597/uitrol-mf-scan.pdf)
op de [RU-pagina over de
MFP’s](http://www.ru.nl/ictservicecentrum/medewerkers/werkplek-campus/uitrol/).
