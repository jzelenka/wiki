---
author: wim
date: 2019-02-25 18:00:00
tags:
- medewerkers
- docenten
- studenten
title: Ubuntu 18.04 voor pc-onderwijszalen en persoonlijke werkstations
---
In de zomervakantie worden alle pc’s uit de
[pc-onderwijszalen](/nl/howto/terminalkamers/) met Ubuntu 18.04 LTS
ge(her)installeerd. Cursus-software kan al maanden getest worden op de
Ubuntu 18.04 [loginserver](/nl/howto/hardware-servers/) lilo6. Alle PC’s
in HG00.075 en HG03.761 worden in deze periode vervangen door nieuwe
PC’s (All-in-One, dual boot met Windows10 en Ubuntu 18.04 Linux). Na
deze vervanging hebben alle pc’s in de pc-onderwijszalen snelle
SSD-schijven.

Indien u gebruikt maakt van zowel PC’s in de
[pc-onderwijszalen](/nl/howto/terminalkamers/) als een door C&CZ beheerde
PC met Linux op uw werkplek, dan is het raadzaam te overwegen om uw
werkplek PC ook een upgrade naar Ubuntu 18.04 te laten geven. Gebruik
maken van Ubuntu 16.04 in uw kantoor en Ubuntu 18.04 op een beheerde
werkplek in de [pc-onderwijszalen](/nl/howto/terminalkamers/) kan
problemen veroorzaken.
