---
title: Onbeperkte Mathematica-licentie in 2023 voor RU-medewerkers en -studenten
author: petervc
date: 2023-02-14
tags:
- medewerkers
- studenten
cover:
  image: img/2023/mathematica-13.png
---
De licentie voor [Mathematica](/nl/howto/mathematica/) is tijdelijk
voor heel 2023 uitgebreid naar een ongelimiteerde campuslicentie: alle
medewerkers en studenten mogen Mathematica in 2023 onbeperkt gebruiken
op het RU-netwerk. Voor thuisgebruik moet men dus een [VPN](/nl/howto/vpn/) gebruiken.

De software is te vinden op de [C&CZ Install schijf](https://install.science.ru.nl/science/Mathematica/). Bij de installatie moet je aangeven:

    License server: mathematica.science.ru.nl
    License: L4601-6478

De introductiecursus ["Mathematica Basic Principles: An Introduction"](https://can.nl/events/mathematica-basic-principles/)
kan bij CANdiensten in Amsterdam gratis gevolgd worden.

Bij vragen of problemen, mail [mathematica@science.ru.nl](mailto:mathematica@science.ru.nl).
