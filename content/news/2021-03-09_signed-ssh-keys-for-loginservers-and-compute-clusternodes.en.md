---
author: polman
date: 2021-03-09 23:53:00
tags:
- medewerkers
- studenten
title: Signed SSH keys for loginservers and compute clusternodes
---
From now on, C&CZ signs the SSH keys of e.g. [loginservers and compute
clusternodes](https://wiki.cncz.science.ru.nl/index.php?title=Hardware_servers&setlang=en#Linux_.5Bloginservers.5D.5Blogin_servers.5D).
That makes it possible for users, as explained on that page, to add the
public signing key in the private .ssh/known\_hosts, which makes one
trust all C&CZ signed SSH servers. Whenever C&CZ deploys a new ‘lilo’,
the question whether this might be spoofing will not occur then.
