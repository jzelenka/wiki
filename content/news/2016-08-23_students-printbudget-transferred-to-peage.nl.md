---
author: wim
date: 2016-08-23 14:32:00
tags:
- studenten
title: 'Studenten: printbudget overgeboekt naar Peage'
---
Alle FNWI Konica Minolta MFP’s die doorbelast werden via het
C&CZ-printbudgetsysteem, zijn overgezet naar Peage. Het
C&CZ-printbudgetsysteem wordt alleen nog gebruikt voor de HP-printer
escher (Library of Science) en de posterprinter kamerbreed. Op 23
augustus heeft C&CZ daarom alle studenten printbudgetten die gekoppeld
zijn aan een Science studentenlogin van een ingeschreven student, leeg
gemaakt. Het ISC heeft dat budget op 24 augustus bijgeboekt op het Peage
budget van de betreffende student. Eigenaren van voldoende positieve
groepsbudgetten zal gevraagd worden wat er met het resterende budget
moet gebeuren. Budget voor escher/kamerbreed kan alleen nog bijgeboekt
worden bij C&CZ (postmaster\@science.ru.nl) met vermelding van
kostenplaats of evt. met cash.
