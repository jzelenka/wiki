---
author: polman
date: 2018-06-26 14:16:00
tags:
- software
cover:
  image: img/2018/labview.png
title: National Instruments LabVIEW Spring 2018 op Install-schijf
---
Om het voor de afdelingen die meebetalen aan de licentie van [NI
LabVIEW](http://netherlands.ni.com/labview) makkelijker te maken om
LabVIEW te installeren, is de zojuist binnengekomen versie “Spring 2018”
op de [Install](/nl/howto/install-share/)-netwerkschijf gezet.
Installatiemedia zijn ook te leen. Licentiecodes zijn bij C&CZ helpdesk
of postmaster te verkrijgen.
