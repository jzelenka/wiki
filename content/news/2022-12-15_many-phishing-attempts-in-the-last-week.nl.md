---
title: Veel phishing pogingen de laatste week
author: bram
date: 2022-12-15
tags: []
---
De laatste week zien we een sterke toename aan [phishing](/nl/howto/know-about-phishing/) pogingen in het Science domein. Het valt op dat deze pogingen gebruik maken van goed nagebouwde Science omgevingen zoals Roundcube die moeilijk van de echte omgeving te onderscheiden zijn. Vaak staat er in de phishing mail iets over salaris of een betaling waardoor er veel op geklikt wordt. Vanuit C&CZ wordt er alles aan gedaan om de overlast zoveel mogelijk te beperken door deze phishing mails zoveel mogelijk te blokkeren.

Graag het verzoek extra alert te zijn op mails en vooral niet in te gaan op mails waarin gevraagd wordt je logingegevens in te vullen voor een salarisbetaling, een andere betaling, een account reset etc.. Dit zijn mails die over het algemeen nooit verstuurd worden. Mocht je onverhoopt toch ergens zijn ingelogd waar je achteraf twijfels bij hebt, meldt dit dan zo snel mogelijk bij [C&CZ](/en/howto/contact).
