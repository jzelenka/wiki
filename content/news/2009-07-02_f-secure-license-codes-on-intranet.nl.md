---
author: petervc
date: 2009-07-02 16:08:00
title: F-Secure licentiecodes op Intranet
---
Om de overstap van [McAfee](http://www.mcafee.com) naar
[F-Secure](http://www.f-secure.com) makkelijker te maken is op de
website van de [RU
ICT-beveiliging](http://www.radboudnet.nl/ict-beveiliging/) alle
benodigde informatie over [F-Secure
software](http://www.radboudnet.nl/fsecure) vermeld. Na inloggen met U-
of S-nummer zijn de licentiecodes direct te zien.
