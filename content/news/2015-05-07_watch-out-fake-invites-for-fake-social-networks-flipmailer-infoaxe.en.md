---
author: petervc
date: 2015-05-07 13:23:00
tags:
- medewerkers
- docenten
- studenten
title: 'Watch out: fake invites for fake social networks (flipmailer, infoaxe)'
---
Recently C&CZ hears from users that they receive mail from mail
addresses they know with subjects like “You have a new notification from
… View?” or “… is waiting for your response. Respond?” or “I would like
to add you as a friend”. Accept?" The link (URL) in these mails points
to something in “flipmailer dot com” or “infoaxe dot com”. Clicking the
link results in the installation of a spyware plugin/add-on/extension in
the browser Firefox/Chrome/… If you fill in your mail-password on their
website, all your contacts will get the same fake invitation. Do not
fall for this! If you have responded to these mails, please [contact the
C&CZ helpdesk](/en/howto/contact/) or search the Internet what to do to
remove the spyware, such as [deze
how-to-remove-fliporainfoaxenet-spam-extension](http://emmanuelcontreras.com/content/how-remove-fliporainfoaxenet-spam-extension).
