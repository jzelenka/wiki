---
author: petervc
date: 2008-09-02 17:08:00
title: Personnel changes at C&CZ
---
As of September 1, {{< author "keesk" >}} will mainly concern himself
with leading the university project for the introduction of [Syllabus
Plus](http://www.scientia.com/nl/). Therefore he resigns as head of
C&CZ, although he stays at C&CZ in the role of advisor. The new head of
C&CZ is {{< author "petervc" >}}. The vacancy within System
Administration/Development is filled by {{< author "caspar" >}}.
