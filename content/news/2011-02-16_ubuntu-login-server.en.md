---
author: wim
date: 2011-02-16 10:53:00
tags:
- studenten
- medewerkers
title: Ubuntu login server
---
In the process of gradually replacing Fedora by Ubuntu, there have been
several Ubuntu clients and servers installed. One of these is an
experimental *Ubuntu 10.04 LTS (Lucid Lynx)*
login server **lilo1.science.ru.nl** for general
use.

The choice for Ubuntu 10.04 LTS (Lucid Lynx) has been made because of
the **L**(ong) **T**(ime)
**S**(upport), 3 years for desktops and 5 years for
servers. This in contrast to approximately 1 year updates and support
for Fedora. Software, commonly used within the faculty, is available.

More likely than not, packages or programs will be missing or work in
slightly different ways than on a Fedora system. It’s also possible that
settings for Fedora might cause problems on the Ubuntu systems. Please
do not hesitate to report problems. Regrettably, we cannot garantee that
all problems can or will be solved immediately.
