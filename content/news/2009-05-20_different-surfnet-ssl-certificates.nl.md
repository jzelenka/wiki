---
author: polman
date: 2009-05-20 15:55:00
title: Andere Surfnet SSL-certificaten
---
Via [Surfnet](http://www.surfnet.nl) zijn gratis SSL-certificaten van
[Cybertrust](http://www.cybertrust.com) te verkrijgen. Surfnet heeft
gemeld het contract op te zeggen per 1-1-2010 en over te gaan op
certificaten van [Comodo](http://comodo.com). Er is [meer
informatie](http://www.terena.org/news/fullstory.php?news_id=2405)
beschikbaar. Comodo certificaten zijn op dit moment nog niet te krijgen.
Cybertrust-certificaten worden waarschijnlijk 3 maanden na het opzeggen
van het contract, dus per 1 april 2010, ongeldig. Voordelen van Comodo
zou zijn: wildcard-certificaten en prive-certificaten. Meer info volgt
zodra de nieuwe service beschikbaar is.
