---
author: petervc
date: 2015-12-15 17:03:00
tags:
- medewerkers
- docenten
- studenten
title: Dell/Intel HPC code optimaliseringsbijeenkomst Amsterdam
---
Op 14 januari 2016 organiseert Dell in Amsterdam een high performance
computing (HPC) community bijeenkomst. Martin Hilgeman (Dell) zal
spreken over software optimalisatie, Laurent Duhem (Intel) over
developer tools. Medewerkers en studenten die hiervoor interesse hebben,
kunnen zich aanmelden op [de website van het
event](https://www.eventbrite.nl/e/registratie-the-hpc-community-event-19421646668).
Onderwerpen als parallellisering, GPU’s en co-processoren zullen de
revue passeren.
