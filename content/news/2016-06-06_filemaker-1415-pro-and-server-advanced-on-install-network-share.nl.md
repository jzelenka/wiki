---
author: petervc
date: 2016-06-06 17:33:00
tags:
- medewerkers
- docenten
title: Filemaker 14/15 Pro en Server Advanced op Install-schijf
---
Nieuwe versies van [Filemaker
Pro](http://www.filemaker.com/nl/products/filemaker-pro/) en van
[Filemaker Server
Advanced](http://www.filemaker.com/nl/products/filemaker-server/), 14 en
15, voor Windows (32- en 64-bit) en Mac zijn op de
[Install](/nl/howto/install-share/)-netwerkschijf te vinden. De
licentievoorwaarden staan gebruik op RU-computers toe. Licentiecodes
zijn bij C&CZ helpdesk of postmaster te verkrijgen. Voor thuisgebruik
kan de software op [Surfspot](http://www.surfspot.nl) besteld worden.
