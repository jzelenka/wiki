---
author: petervc
date: 2013-10-18 15:43:00
tags:
- medewerkers
- docenten
- studenten
title: Long filenames cause printing problems
---
A printjob may fail to print when the filename is more than approx. 110
characters. One of our print servers does not handle this correctly. A
warning about this problem was added to the [printer
page](/en/howto/printers-en-printen/). We are working on a fix.
