---
author: john
date: 2016-07-07 13:36:00
tags:
- medewerkers
- docenten
- studenten
title: Quiet keyboards at renewal computer labs HG00.023 and HG00.153
---
During the summer break the PCs in the [computer
labs](/en/howto/terminalkamers/) TK023 (HG00.023) and TK153 (HG00.153)
will be replaced by new ones, of the type type [Dell OptiPlex 7440
AIO](http://www.dell.com/us/business/p/optiplex-24-7000-aio/pd) with
24-inch Full-HD screen, a Core i5 6500 (3.2GHz,QC) processor, 1x8GB
memory and 512GB SSD hard disc. In order to reduce noise disturbance,
especially during exams relatively quiet [Cherry Stream 3.0
keyboards](https://www.techpowerup.com/216759/cherry-announces-the-stream-3-0-keyboard)
have been chosen. For other places where many keyboards are used in the
same room, this will be a good choice too.
