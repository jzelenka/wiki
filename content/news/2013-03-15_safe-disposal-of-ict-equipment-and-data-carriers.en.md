---
author: john
date: 2013-03-15 14:45:00
tags:
- medewerkers
- docenten
title: Safe disposal of ICT equipment and data carriers
---
For the disposal of ICT-equipment (PCs, laptops, servers, etc.) and data
carriers (hard disks, tapes) which may contain confidential data, a safe
waste channel is now available. These can be handed over to an employee
of [Logistics
Center](http://www.ru.nl/fnwi/ihz/logistiek/logistiek_centrum/). So,
it’s not necesarry to remove hard disks from PCs yourself.
