---
author: petervc
date: 2011-09-19 17:41:00
tags:
- medewerkers
title: Reference Manager licentie stopt eind 2011
---
Jarenlang had het [UMCN](http://www.umcn.nl) een licentie voor gebruik
van [Reference Manager](http://www.refman.com/) binnen UMCN en RU.
Binnen FNWI hebben sommige medewerkers gebruik gemaakt van deze
licentie, in plaats van de RU-standaard
[EndNote](http://www.endnote.com/). Per eind 2011 stopt het UMCN met
deze licentie, omdat men over is gegaan op EndNote. Meer info over de
overstap naar EndNote is te vinden op de [website van de
UB](http://www.ru.nl/ubn/bibliotheeklocaties/medische/endnote/).
