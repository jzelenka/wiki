---
author: caspar
date: 2015-06-10 16:55:00
tags:
- studenten
- docenten
title: 'Nieuwe medewerker operations/helpdesk: Bjorn Honing'
---
Vanaf 1 juni jl. is {{< author "bjorn" >}} bij FNWI in dienst getreden
als C&CZ-medewerker operations/helpdesk. Bjorn is daarmee de direct
directe collega van {{< author "john" >}} en
{{< author "stefan" >}}.
