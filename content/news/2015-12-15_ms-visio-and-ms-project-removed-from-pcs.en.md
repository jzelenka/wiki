---
author: wim
date: 2015-12-15 11:12:00
tags:
- medewerkers
- docenten
- studenten
title: MS Visio and MS Project removed from pcs
---
Because the grace period for the use of MS
[Visio](https://products.office.com/nl-nl/visio/flowchart-software) and
MS
[Project](https://products.office.com/en-us/project/project-and-portfolio-management-software)
ends January 1, 2016, this software will be removed from C&CZ [Managed
pcs](/en/howto/windows-beheerde-werkplek/). Employees wanting to stay or
start using Project and/or Visio should [contact
C&CZ](/en/howto/contact/) for a license that must be paid for per
installation.
