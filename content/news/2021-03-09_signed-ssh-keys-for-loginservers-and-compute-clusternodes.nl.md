---
author: polman
date: 2021-03-09 23:53:00
tags:
- medewerkers
- studenten
title: Getekende SSH-sleutels voor loginservers en rekenclusternodes
---
Vanaf nu tekent C&CZ de SSH-sleutels van o.a. [loginservers en
rekenclusternodes](https://wiki.cncz.science.ru.nl/index.php?title=Hardware_servers&setlang=nl#Linux_.5Bloginservers.5D.5Blogin_servers.5D).
Dat maakt het voor gebruikers zoals daar uitgelegd mogelijk om de
publieke tekensleutel op te nemen in de eigen .ssh/known\_hosts waardoor
men automatisch alle door C&CZ getekende SSH servers vertrouwt. Als C&CZ
een nieuwe ‘lilo’-server inzet, krijgt men dan nooit meer de vraag of er
sprake kan zijn van spoofing.
