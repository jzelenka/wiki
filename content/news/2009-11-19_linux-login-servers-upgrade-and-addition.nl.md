---
author: caspar
date: 2009-11-19 14:19:00
title: 'Linux loginserver: upgrade en uitbreiding'
---
De algemeen toegankelijke Linux loginserver `lilo` (formeel
`porthos.science.ru.nl`) heeft een upgrade van het operating systeem
naar [Fedora 11](http://nl.wikipedia.org/wiki/Fedora) gekregen. Tevens
is een tweede Linux loginserver ingericht met de naam `stitch` (formeel
`lijfwacht.science.ru.nl`). De software op `lilo` en `stitch` is
identiek, maar `stitch` heeft modernere, krachtigere hardware (8 cores,
2.5 GHz elk, 8GB intern geheugen). Doordat er nu twee Linux loginservers
zijn, heeft men een uitwijkmogelijkheid in geval van problemen. Deze
wijzigingen waren noodzakelijk i.v.m. een security probleem met `lilo`
voor de upgrade en passen in ons streven om de oude Solaris loginservers
`solo` en `solost` op termijn uit te faseren. Voor meer info zie de
[pagina over servers](/nl/howto/hardware-servers/).
