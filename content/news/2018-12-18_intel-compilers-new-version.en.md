---
author: petervc
date: 2018-12-18 15:20:00
tags:
- medewerkers
- studenten
title: Intel compilers new version
---
C&CZ has bought together with TCM and Theoretical Chemistry two licenses
for the concurrent use of the most recent version of the [Intel Parallel
Studio XE for
Linux](https://software.intel.com/en-us/parallel-studio-xe). This has
been installed in `/vol/opt/intelcompilers` and is available on a.o.
[clusternodes](/en/howto/hardware-servers/) en
[loginservers](/en/howto/hardware-servers/). The old (2014) version can
also be found there. To set the environment variables correctly,
BASH-users must first run:

source /vol/opt/intelcompilers/intel-2019/composerxe/bin/compilervars.sh
intel64

After that, `icc -V` gives the new version number as output. For more
info see [the page on Intel
compilers](https://wiki.cncz.science.ru.nl/index.php?title=Intel_compilers&setlang=en)
