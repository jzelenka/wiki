---
author: petervc
date: 2014-11-25 14:57:00
tags:
- medewerkers
- docenten
title: Notice of change in Microsoft licenses as of 2015
---
The [ISC](http://www.ru.nl/isc) let us know that by January 1, 2015
there will be a new licensing agreement with Microsoft through
Surfmarket. The main difference appears to be that the license for
Microsoft Office on Windows PCs owned by the University, will be
followed by a license for Office 365 Plan E3 (formerly A3) for all RU
staff. This will most likely mean that installing Office on Windows PCs
and iPads/iPhones that are private property of RU staff will be possible
under the license as of January 1, 2015. More information will follow in
the first quarter of 2015.
