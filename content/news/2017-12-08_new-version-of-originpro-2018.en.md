---
author: petervc
date: 2017-12-08 17:23:00
tags:
- medewerkers
- studenten
title: New version of OriginPro (2018)
---
A new version (2018) of [OriginLab](/en/howto/originlab/), software for
scientific graphing and data analysis, can be found on the
[Install](/en/howto/install-share/)-disk. The license server supports
this version. Departments that take part in the license can request
installation and license info from C&CZ, also for standalone use.
Installation on C&CZ managed PCs still is being planned. Departments
that want to start using OriginLab should contact
[C&CZ](/en/howto/contact/).
