---
author: mkup
date: 2018-11-22 15:04:00
tags:
- medewerkers
- docenten
- studenten
title: 'Netwerkonderhoud RU: maandagavond/nacht 26 op 27 november 22:00-03:00 uur'
---
Op maandagavond 26 november zal onderhoud plaatsvinden aan centrale
netwerkonderdelen van de RU: een aantal routers krijgt een
software-upgrade. Vanaf 22:00 uur zullen delen van het RU-netwerk en
centrale RU-diensten niet beschikbaar zijn. Binnen Huygens en andere
FNWI-gebouwen zullen tussen 23:30 en 00:30 uur het internet en draadloos
netwerk niet beschikbaar zijn. Denk s.v.p. na of deze mogelijke
verstoring voor u gevolgen heeft en bereid u indien nodig voor.
Toekomstige onderhoudsvensters zijn op de [website van het
ISC](http://www.ru.nl/systeem-meldingen/) te zien.
