---
author: wim
date: 2011-02-16 10:53:00
tags:
- studenten
- medewerkers
title: Microsoft Office 2007
---
Op dit moment is Microsoft Office 2007 het standaard office pakket op de
campus. We hebben getracht om zoveel mogelijk plekken reeds te voorzien
van deze software. Indien u op dit moment nog een oudere versie van
Office gebruikt is het raadzaam om over te stappen.

Indien de PC die u gebruikt eigendom is van de faculteit kunt u gebruik
maken van de campus licentie en kunt U de software bij ons lenen of
rechtstreeks installeren van de install disk
(\\\\install-srv.science.ru.nl\\install).

Voor PC’s die prive eigedom zijn verwijzen we naar
[Surfspot](http://www.surfspot.nl).
