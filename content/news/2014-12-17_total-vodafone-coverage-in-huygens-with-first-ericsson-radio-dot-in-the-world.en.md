---
author: bertw
date: 2014-12-17 23:58:00
tags:
- medewerkers
- docenten
- studenten
title: Total Vodafone coverage in Huygens with first Ericsson Radio Dot in the world
---
Coverage for mobile phones on the Vodafone network, as used for
[Vodafone Wireless Office](/en/tags/telefonie/) has been extended to the
basements and transport corridors of the Huygens building. This became
possible, because Ericsson and Vodafone were interested in doing a pilot
in the Huygens building with [the first live enterprise deployment of
the Radio Dot system worldwide](http://www.ericsson.com/news/1878703).
This is in addition to the [distributed antenna
system](/en/howto/gsm-/-fax-/-dect/) built in 2011, which was necessary
because of the similarity of the Huygens building with a [Faraday
cage](http://en.wikipedia.org/wiki/Faraday_cage). This reminds some
veterans of June 1987, when a pilot was carried out at the Faculty of
Science by PTT (now KPN), Philips, SURF and the university with the
first
[PABX](http://en.wikipedia.org/wiki/Private_branch_exchange#History)
with a large amount of data traffic in The Netherlands. This PABX is
still in use for speech phone calls in the periphery of the Huygens
building.
