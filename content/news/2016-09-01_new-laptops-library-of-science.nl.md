---
author: john
date: 2016-09-01 15:47:00
tags:
- studenten
- medewerkers
title: Nieuwe laptops Library of Science
---
Van de [laptop pool van de Library of Science](/nl/howto/laptop-pool/)
zijn 23 laptops vervangen door een nieuw en sneller type: HP EliteBook
850 G3. Met name de SSD (solid state disk) en het full HD scherm zijn
een aanzienlijke verbetering ten opzichte van het oude type. De overige
(46) oude laptops blijven voorlopig nog in gebruik.
