---
author: stefan
date: 2013-05-29 17:35:00
tags:
- medewerkers
- studenten
title: A0 fotoprinter weer operationeel
---
Voor het maken van afdrukken van posters e.d. op hoge kwaliteit
fotopapier (maximale papierbreedte 90 cm) kan men nu bij C&CZ terecht.
De betreffende printer “[Kamerbreed](/nl/howto/kamerbreed/)” is dezelfde
die tot voor kort via de afdeling Grafische Vormgeving beschikbaar was.
De prijs voor een A0 afdruk is 32 euro, kleinere afdrukken zijn ook
mogelijk.
