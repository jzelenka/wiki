---
author: polman
date: 2015-02-13 14:25:00
tags:
- studenten
- medewerkers
title: IMAP mail server upgrade next monday night
---
The IMAP server will be replaced by a new server next Monday night.
During the night, no Science mail can be read from the server, because
then the most recent mails will be synchronized to the new server. Mail
can be sent in this period, but new mail will start being delivered on
the new server from 07:30. A word of warning, one could login on the old
server with the first 8 characters of the password, the new server only
accepts the full password.
