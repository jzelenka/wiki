---
author: fmelssen
date: 2016-09-30 11:50:00
tags:
- medewerkers
- docenten
- studenten
title: Idee voor ICT in het onderwijs? Proeftuinen krijgen 7500 euro\!
---
De nieuwe ronde voor ICT-proeftuinen in het onderwijs is gestart.
Sluitingsdatum voor de aanvragen is 4 november 2016. Een docent, student
of ondersteuner met een innovatief idee kan een aanvraag doen voor een
bijdrage van maximaal € 7500. Meer informatie over de voorwaarden en het
aanvraagformulier is te vinden op de [ICT in het
onderwijs](http://www.radboudnet.nl/ictinhetonderwijs/) website op
Radboudnet.
