---
author: petervc
date: 2015-08-27 18:09:00
tags:
- software
cover:
  image: img/2015/labview.png
title: National Instruments LabVIEW Fall 2015 on Install network share
---
To make it easier to install [NI LabVIEW](http://www.ni.com/labview/)
for departments that participate in the license, the newly arrived “Fall
2015” version has been copied to the [Install](/en/howto/install-share/)
network share. License codes can be obtained from C&CZ helpdesk or
postmaster.
