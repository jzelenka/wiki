---
author: bbellink
date: 2018-01-22 10:00:00
tags:
- medewerkers
- studenten
title: 'C&CZ away day: Tuesday January 23'
---
C&CZ scheduled an away day on Tuesday January 23. C&CZ can be reached in
case of serious disruptions of services.
