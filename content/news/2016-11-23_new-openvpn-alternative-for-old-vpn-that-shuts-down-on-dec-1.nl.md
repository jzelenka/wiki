---
author: visser
date: 2016-11-23 13:53:00
tags:
- medewerkers
- studenten
title: Nieuw OpenVPN alternatief voor oude VPN die stopt per 1 december
---
Omdat enkele Linux- en MacOS-gebruikers problemen hadden met de nieuwe
op IPsec gebaseerde [VPN](/nl/howto/vpn/) en ook met de al bestaande
alternatieven, hebben we een nieuwe [OpenVPN](/nl/howto/vpn/) service
ingericht. De hoop en verwachting is dat we hiermee de oude op PPTP
gebaseerde VPN.science.ru.nl, die niet meer als veilig beschouwd wordt,
zoals aangekondigd, op 1 december uit kunnen zetten.
