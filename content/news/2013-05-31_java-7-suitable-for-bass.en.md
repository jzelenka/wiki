---
author: petervc
date: 2013-05-31 16:21:00
title: Java 7 suitable for BASS
---
As of June 1, Java 7 can be used to access [BASS](/en/howto/bass/),
because then the BASS server software has had an upgrade. After that
clients can install the regular Java updates, which makes them more
secure.
