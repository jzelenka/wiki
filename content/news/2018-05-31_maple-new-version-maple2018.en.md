---
author: polman
date: 2018-05-31 14:13:00
tags:
- studenten
- medewerkers
- docenten
title: 'Maple new version: Maple2018'
---
The latest version of [Maple](/en/howto/maple/), Maple2018, can be found
on the [Install](/en/howto/install-share/) network share and has been
installed on C&CZ managed Linux computers. License codes can be
requested from C&CZ helpdesk or postmaster by departments that take part
in the license.
