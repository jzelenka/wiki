---
author: caspar
date: 2012-02-17 19:03:00
tags:
- studenten
title: Peage available at FNWI for RU students
---
As of February 13, 2012 FNWI students can use the new
[Peage](/en/howto/print-project-peage/) printing and copying system of
the Radboud University. FNWI staff cannot use Peage yet. Until everyone
at FNWI can use Peage both the [current
system](/en/howto/printers-en-printen/) and [Peage](/en/howto/peage/) are
available.
