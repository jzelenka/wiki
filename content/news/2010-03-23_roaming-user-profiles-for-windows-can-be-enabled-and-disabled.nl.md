---
author: sommel
date: 2010-03-23 15:48:00
title: Zwervende gebruikersprofielen aan en uit te zetten
---
Op de [Doe-Het-Zelf](http://dhz.science.ru.nl)-website is het mogelijk
om [zwervende gebruikersprofielen (roaming user
profiles)](http://en.wikipedia.org/wiki/Roaming_user_profile) voor de
domeinen NWI en B-FAC aan en uit te zetten. Standaard zijn zwervende
profielen wel ingeschakeld voor het NWI domein en niet voor het B-FAC
domein. Aanmelden zonder zwervend gebruikersprofiel zal in het algemeen
ietsje sneller zijn, maar een nadeel is dat gebruikersinstellingen niet
bewaard blijven. U kunt hierbij denken aan het Desktop
achtergrondplaatje, documenten op de Desktop, Email instellingen,
‘Favorieten’, ‘Recente documenten’, etc. Met name bestanden die op het
bureaublad worden gezet worden niet bewaard!
