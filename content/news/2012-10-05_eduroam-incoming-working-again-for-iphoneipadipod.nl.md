---
author: mkup
date: 2012-10-05 17:48:00
title: Eduroam inkomend werkt weer voor iPhone/iPad/iPod
---
Het [UCI netwerkbeheer](http://www.ru.nl/uci) meldt dat het [probleem](/nl/cpk/) voor gebruikers van een
iPhone/iPad/iPod met het
[inkomend](http://www.ru.nl/gdi/voorzieningen/campusbrede-systemen/eduroam/)
gebruik van [Eduroam](http://www.eduroam.nl) opgelost is. Eduroam
inkomend betekent dat men gebruik maakt van het draadloze netwerk van
een andere instelling in de wereld, met authenticatiegegevens
(login/wachtwoord) van RU of Science.
