---
author: wim
date: 2011-02-16 10:53:00
tags:
- studenten
- medewerkers
title: New names for Linux login servers
---
Currently the login servers are named
**lilo.science.ru.nl**
(**LI**nux **LO**gin server)
and **stitch.science.ru.nl**. However, the actual
machines have different names,
**porthos.science.ru.nl** and
**lijfwacht.science.ru.nl** respectively. This
leads to confusion with some of our users. To address this issue we will
phase out and rename some machines.

The oldest server **lilo.science.ru.nl** will be
phased out no sooner than **lilo1.science.ru.nl**
can take over. The current Fedora login server
**stitch.science.ru.nl** will be renamed to
**lilo2.science.ru.nl**.

The name **lilo** will eventually become an alias
for the fastest login server and the name
**stitch** will, for the time being, become an
alias for **lilo2**. Whenever replacing login
servers, new machine will receive a successive number, so
**lilo3**, **lilo4**, etc.
