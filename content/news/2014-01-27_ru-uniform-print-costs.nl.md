---
author: petervc
date: 2014-01-27 16:48:00
tags:
- medewerkers
- studenten
title: RU-uniformering printprijzen
---
Per 1 februari 2014 zal C&CZ de
[printprijzen](/nl/howto/printers-en-printen/) aanpassen, conform het
CvB-besluit om print- en kopieerprijzen binnen de RU te uniformeren.
Zwart/wit A4 prints zullen hierdoor 5.7% duurder worden. A4-kleur, A3
zwart/wit en A3-kleur worden resp. 18%, 54% en 31% goedkoper. Omdat de
hoop is dat het C&CZ printsysteem op niet te lange termijn vervangen zal
worden door het RU-brede [Peage](/nl/howto/peage/), zal vooralsnog geen
prioriteit gegeven worden aan het herzien van het C&CZ-printsysteem om
enkelzijdig printen duurder te maken dan dubbelzijdig, zoals elders op
de campus is ingevoerd.
