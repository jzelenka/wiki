---
author: bram
date: 2015-01-27 16:51:00
tags:
- studenten
- medewerkers
title: 'Nieuwe dienst: GitLab versiecontrole'
---
Als opvolger van onze [Subversion](/nl/howto/subversion/) dienst, is nu
ook [GitLab](/nl/howto/gitlab/) beschikbaar.
[GitLab](https://about.gitlab.com/features/) is een open source platform
voor het delen van code, het beheren van git archieven, bijhouden van
issues/bugs en het reviewen van code. Studenten en medewerkers kunnen
zich op <https://gitlab.science.ru.nl> aanmelden met hun Science login.
Het is mogelijk om bestaande in svn beheerde projecten naar GitLab over
te zetten.
