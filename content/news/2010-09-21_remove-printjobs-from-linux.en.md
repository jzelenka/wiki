---
author: sommel
date: 2010-09-21 18:44:00
title: Remove printjobs from Linux
---
The removal of submitted print jobs is now possible from Linux. It can
be done with the [lprm](http://ss64.com/bash/lprm.html) command. It
works from all C&CZ managed Linux [servers and
workstations](/en/howto/hardware-servers/), 32-bit as well as 64-bit.
