---
author: petervc
date: 2008-06-19 16:24:00
title: 'Software update: Maple 12'
---
A new version (12) of [Maple](/en/howto/maple/) has been installed on
Linux/Solaris. Early next week it will also be available on the
[Windows-XP Managed PCs](/en/howto/windows-beheerde-werkplek/). It can be
installed on Windows from the
[install](http://www.cncz.science.ru.nl/software/installscience) network
disk, but the CDs can also be borrowed by employees and students, for
home use too.
