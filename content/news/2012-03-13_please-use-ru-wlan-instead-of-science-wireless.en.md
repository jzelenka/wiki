---
author: mkup
date: 2012-03-13 16:37:00
tags:
- studenten
- medewerkers
title: Please use ru-wlan instead of Science wireless\!
---
In the near future the wireless access points in the Huygens building
and the buildings around it, will be replaced by new ones, based on a
new technique. The Science wireless network will then no longer be
available. Therefore please switch over to using the
[`ru-wlan`](http://www.ru.nl/gdi/voorzieningen/campusbrede-systemen/wireless-ru/)
wireless network, that is available before and after. One can use
ru-wlan with U-number and RU-password, but also with
`science-username@science.ru.nl` and Science-password. After the switch,
faster ([wireless-N](http://nl.wikipedia.org/wiki/IEEE_802.11#802.11n))
access points wille be used in busy areas. After the switch the
[eduroam](http://www.eduroam.org/) wireless network will be available
too, one can use that with `U-number@ru.nl` and RU-password, but also
with `science-username@science.ru.nl` and Science-password. Before the
switch, eduroam is only available near the Huygens building on the
terrace behind the restaurant. In the future `ru-wlan` will be switched
off, only eduroam will survive. Furthermore, there is a plan to have
more wireless access outside of the Huygens building, i.e. at the
central entrance and the grass behind the Huygens building.
