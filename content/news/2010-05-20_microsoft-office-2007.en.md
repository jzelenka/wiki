---
author: wim
date: 2010-05-20 13:24:00
title: Microsoft Office 2007
---
C&CZ will deploy Microsoft Office 2007 Enterprise to all [Managed
PC’s](/en/howto/windows-beheerde-werkplek/). Up till now Office2007 was
only installed on request of departments, because of the completely
different look and feel and compatibility issues. Office2007 will be
installed on the Study Landscape PC’s shortly, the [PC
rooms](/en/howto/terminalkamers/) used for courses will follow during the
summer holidays.

Departments can contact C&CZ to start an automatic deployment of the
software to their managed PC’s. For self-administered PC’s, one can use
the [install](http://www.cncz.science.ru.nl/software/installscience)
network disk to install Office 2007. Necessary license codes are mailed
by Postmaster on request from a faculty or RU mail-address.

Help is available for the move to Office2007:

-   A [Learning
    Guide](http://sito.science.ru.nl/LearningGuide/Publications/start/default.htm)
    for all parts of Office2007 (Word, Excel, PowerPoint, Outlook,
    Access) and also for Windows7. This can only be used on campus and
    is only available in Dutch.
-   The (Dutch) [information site of the Office 2007
    project](http://www.ru.nl/gdi/projecten/office_2007/) of the
    [GebruikersDienst ICT](http://www.ru.nl/gdi)
-   Walk-in sessions with a teacher explaining Office2007 and the
    Learning Guide: Thursday May 27 in E.1 2.50 (Erasmus building,
    low-rise, room 2.50). Hours: 10:00-11:00, 11:00-12:00, 14:00-15:00,
    15:00-16:00. Tuesday June 8 in Gymnasion GN3. Hours: 10:00-11:00,
    11:00-12:00.
