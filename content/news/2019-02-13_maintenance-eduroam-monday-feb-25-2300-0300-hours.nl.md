---
author: mkup
date: 2019-02-13 12:23:00
tags:
- medewerkers
- docenten
- studenten
title: 'Onderhoud eduroam: maandag 25 feb 23:00-03:00 uur'
---
Op maandagavond 25 februari tussen 23:00-03:00 uur zal onderhoud
plaatsvinden aan het draadloze netwerk van de RU en zal eduroam
tijdelijk niet beschikbaar zijn. Denk s.v.p. na of dit voor u gevolgen
heeft en bereid u indien nodig voor.
