---
author: petervc
date: 2012-08-21 18:15:00
tags:
- studenten
- medewerkers
- docenten
title: Windows7 en Linux Ubuntu 12.04 in alle onderwijsruimtes
---
In de zomervakantie heeft C&CZ alle
[onderwijs-pc’s](/nl/howto/terminalkamers/) in terminalkamers,
collegezalen en colloquiumkamers opnieuw geïnstalleerd, met nieuwe
versies van Windows (7) en Ubuntu Linux (12.04). Allerlei bestaande
software (voor Windows op de S: en T: schijven) is beschikbaar, maar
niet altijd getest. Voor Windows7 is ook een [lijst van geïnstalleerde
software](/nl/howto/windows-beheerde-werkplek/) aanwezig.

Graag alle problemen z.s.m. doorgeven aan C&CZ, bv. via mail naar
postmaster\@science.ru.nl, zodat we de tijd hebben om problemen op te
lossen.
