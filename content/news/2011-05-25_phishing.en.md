---
author: petervc
date: 2011-05-25 16:56:00
tags:
- studenten
- medewerkers
- docenten
title: Phishing
---
Sometimes even university staff and students are tricked into replying
to a phishing email: you receive an email that looks like someone from
support asks to reply with your password, to prevent your email account
from being blocked. Never reply to these fake senders! Support personnel
would never ask you for your password!
