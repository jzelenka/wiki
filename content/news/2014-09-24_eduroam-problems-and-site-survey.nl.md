---
author: mkup
date: 2014-09-24 10:35:00
tags:
- medewerkers
- studenten
title: Eduroam problemen en site survey
---
Er worden [opnieuw](/nl/news/) problemen gemeld met het
draadloze netwerk [Eduroam](http://www.ru.nl/draadloos), zowel met de
authenticatie als met de dekking. In geval van problemen met
RU-authenticatie (U123456\@ru.nl, s123456\@ru.nl, …) kan men als
alternatief scienceloginnaam\@science.ru.nl proberen. Voor het
verbeteren van de dekking in het Huygensgebouw zal er de komende vier
weken door Ronald Versteeg van [JS Network
Solutions](http://www.js-networksolutions.nl/) in het hele Huygensgebouw
gemeten worden. Voor niet vrij toegankelijke ruimten zal hij overleggen
met [C&CZ Netwerkbeheer](/nl/howto/netwerkbeheer/) en/of de betreffende
afdeling. Op de resultaten van die site survey zal [ISC
netwerken](http://www.ru.nl/isc) een verbeterplan baseren.

C&CZ verzoekt iedereen om problemen met het draadloze en bedrade netwerk
te melden via netmaster\@science.ru.nl.
