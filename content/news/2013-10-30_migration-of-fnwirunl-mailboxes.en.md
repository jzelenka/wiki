---
author: petervc
date: 2013-10-30 17:42:00
tags:
- medewerkers
- docenten
title: Migration of @fnwi.ru.nl mailboxes
---
The [ISC](http://www.ru.nl/isc) will migrate all mailboxes with an
`@fnwi.ru.nl` mail address from old Exchange 2003 servers to new
Exchange 2010 servers in the evening of November 20. PC’s managed by the
ISC will have their settings changed automatically by the ISC. For home
PC’s, smartphones, tablets and laptops and PC’s managed by C&CZ or by
the user him/herself, the settings will have to be changed after the
migration. Most often it suffices to replace the server name
`outlookweb.ru.nl` by `mail.ru.nl`. In all cases C&CZ can offer
assistance.
