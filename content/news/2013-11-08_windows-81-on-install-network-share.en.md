---
author: petervc
date: 2013-11-08 10:48:00
tags:
- medewerkers
- docenten
title: Windows 8.1 on Install network share
---
The most recent version of [Microsoft
Windows](http://office.microsoft.com), 8.1, is available on the
[Install](/en/howto/install-share/) network share. The license permits
the use on university computers. License codes can be obtained from C&CZ
helpdesk or postmaster. For home PCs the software is available through
[Surfspot](http://www.surfspot.nl).
