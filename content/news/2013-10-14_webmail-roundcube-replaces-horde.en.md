---
author: polman
date: 2013-10-14 11:16:00
tags:
- medewerkers
- docenten
- studenten
title: 'Webmail: Roundcube replaces Horde'
---
The outdated webmail service [Horde](https://horde.science.ru.nl) will
be shut down on Monday, November 18. The successor is available now
already: [Roundcube](https://roundcube.science.ru.nl). At first login,
contacts from Horde are imported. Roundcube has lots of possibilies,
like managing the sharing of mail folders with other users and the
filtering of mail directly at the moment of arrival at the server. For
more information, see our [mail page](/en/tags/email/).
