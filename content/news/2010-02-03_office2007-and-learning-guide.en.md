---
author: polman
date: 2010-02-03 18:12:00
title: Office2007 and Learning Guide
---
Departments that would like to migrate from Microsoft Office XP to 2007
with their [Windows managed pc’s](/en/howto/windows-beheerde-werkplek/)
can send a request to C&CZ. To help users with this migration, Radboud
University has bought a [Learning Guide (in
Dutch)](http://sito.science.ru.nl/LearningGuide/Publications/start/default.htm),
that can be used from the campus network. The guide also contains
information on Windows7.
