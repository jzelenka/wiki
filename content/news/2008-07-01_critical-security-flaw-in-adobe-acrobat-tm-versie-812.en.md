---
author: petervc
date: 2008-07-01 14:09:00
title: Critical security flaw in Adobe Acrobat t/m versie 8.1.2
---
Adobe Acrobat has a [critical security
flaw](http://www.adobe.com/support/security/bulletins/apsb08-15.html),
all users of version 8 are recommended to update to version “8.1.2
Security Update 1”. Since the site-license for Adobe software has
finally come through, this update can probably best be combined with the
update to the Professional version.
