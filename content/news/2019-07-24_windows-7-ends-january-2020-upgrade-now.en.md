---
author: wim
date: 2019-07-24 13:45:00
tags:
- medewerkers
- docenten
- studenten
title: 'Windows 7 ends January 2020: Upgrade now\!'
---
Microsoft [ends the support of Windows
7](https://support.microsoft.com/en-us/help/4057281/windows-7-support-will-end-on-january-14-2020)
early 2020. Therefore we urge everyone to upgrade all pc’s with Windows
7 to Windows 10 or another supported operating system or to replace
these pcs. For C&CZ managed pcs please [contact the C&CZ
helpdesk](/en/howto/contact/) for an upgrade to Windows 10 or Ubuntu
18.04. For other pcs, they can also offer assistance.
