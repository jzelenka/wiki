---
author: petervc
date: 2014-11-16 10:45:00
tags:
- studenten
- medewerkers
- docenten
title: Uitfaseren SSLv3 protocol
---
Vanwege [kwetsbaarheden](https://www.us-cert.gov/ncas/alerts/TA14-290A)
in het beveiligingsprotocol
[SSLv3](http://nl.wikipedia.org/wiki/Secure_Sockets_Layer) is het
gebruik van dit protocol op alle door C&CZ beheerde webservers uitgezet.
Een beveiligde verbinding wordt daardoor alleen opgebouwd op basis van
de opvolger [TLS](http://nl.wikipedia.org/wiki/Secure_Sockets_Layer).
Browsers als
[Chrome](http://venturebeat.com/2014/10/30/google-plans-to-disable-fallback-to-ssl-3-0-in-chrome-39-and-remove-ssl-3-0-completely-in-chrome-40/),
[Internet
Explorer](https://technet.microsoft.com/en-us/library/security/3009008.aspx)
en
[Firefox](https://blog.mozilla.org/security/2014/10/14/the-poodle-attack-and-the-end-of-ssl-3-0/)
zullen ieder vroeger of later stoppen met de ondersteuning van SSLv3. Er
is ook een [handleiding voor handmatige
aanpassingen](https://scotthelme.co.uk/sslv3-goes-to-the-dogs-poodle-kills-off-protocol/)
om de kwetsbaarheden per direct op te heffen. Dat kan echter tot
problemen leiden bij websites die nog niet zonder SSLv3 kunnen, zoals
[BASS](/nl/howto/bass/). P.S.: vanaf half december is BASS nu ook zonder
SSLv3 te gebruiken.
