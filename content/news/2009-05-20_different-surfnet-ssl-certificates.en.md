---
author: polman
date: 2009-05-20 15:55:00
title: Different Surfnet SSL-certificates
---
One can/could acquire free SSL-certificates of
[Cybertrust](http://www.cybertrust.com) through
[Surfnet](http://www.surfnet.nl). The contract period with Cybertrust
ends 1-1-2010 and all certificates will become invalid 3 months later.
Starting from July new certicates can be acquired from
[Comodo](http://comodo.com). See [for more
information](http://www.terena.org/news/fullstory.php?news_id=2405).
Advantages of comodo are wilcard certificates and private certificates.
More information will follow as soon as the service is available.
