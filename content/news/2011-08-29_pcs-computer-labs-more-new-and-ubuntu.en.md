---
author: wim
date: 2011-08-29 17:59:00
tags:
- studenten
- medewerkers
- docenten
title: 'PC''s computer labs: more, new and Ubuntu'
---
The page about the [computer labs](/en/howto/terminalkamers/) lists all
details about the PC’s in the computer labs. The most important changes
are:

-   All PC’s have been reinstalled during the summer break.
-   All PC’s now are dual-boot Ubuntu Linux and Windows XP. Fedora Linux
    is no longer available in these labs.
-   In TK075 66 new PC’s have been installed during the summer break.
-   The new lab TK137 has been created with 49 PC’s from other labs.
