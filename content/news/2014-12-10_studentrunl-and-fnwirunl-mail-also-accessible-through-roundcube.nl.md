---
author: petervc
date: 2014-12-10 11:36:00
tags:
- medewerkers
- docenten
- studenten
title: Student.ru.nl en fnwi.ru.nl mail ook beschikbaar via Roundcube
---
In het weekend van 12, 13 en 14 november verplaatst het
[ISC](http://www.ru.nl/isc) de mail van @student.ru.nl van de oude RU
Share voorziening naar de nieuwe [RU Exchange
voorziening](http://www.ru.nl/ict/medewerkers/mail-agenda/). Toegang via
een browser kan via [RU Exchange webmail](https://mail.ru.nl), maar ook
via [C&CZ Roundcube webmail](https://roundcube.science.ru.nl), door het
kiezen van `mail.ru.nl` in het drop-down menu. Voordelen daarvan zijn
dat verschillende afzender-adressen instelbaar zijn (via
Settings-\>Identities) en dat het Science LDAP-adresboek beschikbaar is
met alle FNWI-medewerkers en -studenten. Het Exchange adresboek van de
RU AD bevat vrijwel geen FNWI-onderzoekers. Ook heeft Roundcube
browser-onafhankelijk een krachtiger en moderner interface en hoeft men
geen `RU\` voor het U- of S-nummer te tikken. Nadeel t.o.v. de standaard
[Outlook Web Access](https://mail.ru.nl) kan zijn dat Roundcube geen
agenda-ondersteuning heeft, het is alleen een
[IMAP](http://nl.wikipedia.org/wiki/Internet_Message_Access_Protocol)
mailclient.
