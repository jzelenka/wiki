---
author: petervc
date: 2010-12-20 13:46:00
title: NL Language Pack voor MS Office 2010 UK
---
De [Language Packs](http://office.microsoft.com/en-us/language/) voor de
32- en 64-bits versie van [MS Office Professional Plus
2010](http://office.microsoft.com/en-gb/professional-plus/) zijn
beschikbaar voor computers die eigendom van de faculteit zijn. De
[CD](/nl/howto/microsoft-windows/)’s kunnen geleend worden, tevens staan
deze op de [install](/nl/tags/software) schijf.
