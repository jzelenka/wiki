---
author: bram
date: 2011-09-28 17:16:00
tags:
- studenten
- medewerkers
- docenten
title: 'Versiebeheer van tekst en broncode: Subversion hosting'
---
Ontwikkelt u software? Schrijft u code? Dan maakt u vast wel eens
zip/tgz bestanden van iets wat (eindelijk) werkt om eventueel later op
terug te vallen. Herkenbaar? Dan kunt u nu gebruik maken van de
[Subversion dienst van C&CZ](/nl/howto/subversion/).
[Subversion](http://subversion.apache.org/) ondersteunt u met het
versiebeheer van broncode bestanden. Vorderingen worden inzichtelijk, en
‘undo’ kan altijd. Bovendien maakt Subversion het bijzonder prettig om
met meerdere personen samen te werken aan dezelfde set van bestanden. De
Subversion dienst is beschikbaar voor mensen met een Science account.
Tevens kan toegang worden verleend aan mensen van buiten de faculteit.
