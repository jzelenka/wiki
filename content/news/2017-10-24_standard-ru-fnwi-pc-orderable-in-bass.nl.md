---
author: john
date: 2017-10-24 16:08:00
tags:
- medewerkers
title: Standaard RU-FNWI PC bestelbaar in BASS
---
In de webwinkel van [Scholten Awater](http://www.sa.nl) in
[BASS](http://bass.ru.nl) is nu een “Standaard PC RU-FNWI” te kiezen.
C&CZ heeft deze getest op het via het netwerk installeren van Windows 7
en Ubuntu 16.04. Dit is een Dell OptiPlex 3050 SFF met een Core i5 6500
(3.2GHz,QC) processor, 1X8GB geheugen, een 256GB SSD harde schijf en een
DVD-schrijver, met 3 jaar ProSupport en Next Business Day garantie. Op
dit systeem, met een zesde generatie Intel-processor, wordt Windows 7
nog ondersteund.
