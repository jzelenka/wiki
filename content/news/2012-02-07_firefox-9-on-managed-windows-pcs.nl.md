---
author: polman
date: 2012-02-07 15:13:00
tags:
- studenten
- medewerkers
- docenten
title: Firefox 9 op beheerde Windows PC's
---
Er is een nieuwe versie van de webbrowser
[Firefox](http://www.mozilla.org/en-US/firefox/fx/) (9) geïnstalleerd op
alle [beheerde Windows PC’s](/nl/howto/windows-beheerde-werkplek/),
inclusief alle plugins die ook voor de oude versie beschikbaar waren. Op
dit moment is de nieuwe versie alleen te vinden via het Start-menu,
binnenkort ook via de snelkoppeling op het bureaublad.
