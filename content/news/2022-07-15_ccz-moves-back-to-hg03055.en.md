---
author: petervc
date: 2022-07-15 10:43:00
tags:
- studenten
- medewerkers
- docenten
title: CCZ moves back to HG03.055
---
Because there was too much noise in the new rooms, C&CZ will temporarily
move back to the third floor of the Huygens building. We expect that the
final move to the ground floor will be in December 2022.
