---
author: petervc
date: 2013-12-13 15:01:00
tags:
- medewerkers
- studenten
title: Upgrade F-Secure beveiligingssoftware
---
De nieuwste versie van F-Secure is per nu voor thuisgebruik door RU
medewerkers voor ca. € 5,25 te koop op
[Surfspot](http://www.surfspot.nl), gebruik als zoekterm “secure”. Het
betreft F-Secure Internet Security 2014 voor 1 jaar, voor 1 gebruiker.
Deze versie ondersteunt ook Windows 8. F-Secure voor Android en Mac zijn
ook beschikbaar onder dezelfde licentie. De ondersteuning voor versie 9,
die thuis gratis gebruikt kon worden, stopt per 6 februari 2014. De op
de campus gebruikte business suite van F-Secure, die op de
[Install](/nl/howto/install-share/) netwerkschijf te vinden is, is niet
meer geschikt voor thuisgebruik, omdat deze moet kunnen communiceren met
een policy manager. N.B.: De 1 gebruikerlicentie dekt niet installatie
op devices van huisgenoten of gezinsleden. Zie evt. [Radboudnet
nieuws](http://www.radboudnet.nl/actueel/nieuws/@928616/beveiligingssoftware/)
voor meer info en updates.
