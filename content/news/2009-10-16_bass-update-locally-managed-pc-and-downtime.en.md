---
author: petervc
date: 2009-10-16 16:53:00
title: BASS update locally managed pc and downtime
---
If one uses a locally managed pc, one should [update
BASS](/en/howto/bass/) before November 9 to avoid problems. The C&CZ
Managed Windows pc’s are or will be updated by C&CZ. This update is
necessary to use the new BASS servers. Because of this update, BASS
cannot be used between Friday November 6, 12:00 hours and Monday
November 9, 08:00 hours.
