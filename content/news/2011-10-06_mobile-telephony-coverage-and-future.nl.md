---
author: bertw
date: 2011-10-06 14:01:00
tags:
- studenten
- medewerkers
- docenten
title: 'Mobiele telefonie: dekkingsverbetering en toekomst'
---
De dekking van het [Vodafone](http://www.vodafone.nl) mobiele netwerk is
in het hele Huygensgebouw sterk verbeterd, de bereikbaarheid moet nu
overal goed zijn. Het plan is om ook de mobiele netwerken van KPN en
T-Mobile aan te sluiten op het nieuw aangelegde [Distributed Antenna
System](http://en.wikipedia.org/wiki/Distributed_Antenna_System). De
dekkingsverbetering is een voorwaarde voor het gebruik van [Vodafone
Wireless
Office](http://enterprise.vodafone.com/products_solutions/voice_roaming/wireless_office.jsp),
waarmee men met een mobieltje kan bellen en gebeld kan worden met de
RU-interne vijfcijferig nummers, ook buiten het gebouw. Dit past in het
toekomstbeeld voor bedrijfstelefonie aan de RU, dat bestaat uit
IP-Telefonie (Voice over IP) voor vaste toestellen en Wireless Office
voor mobiel.
