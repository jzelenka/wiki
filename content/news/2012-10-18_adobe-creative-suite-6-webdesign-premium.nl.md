---
author: petervc
date: 2012-10-18 16:10:00
tags:
- studenten
- medewerkers
title: Adobe Creative Suite 6 Web/Design Premium
---
De nieuwste versie van de [Adobe Creative Suite Design/Web Premium
Bundel](http://www.adobe.com/products/creativesuite), 6, is beschikbaar
voor MS-Windows en Mac OS X. Zowel de Engelstalige als de
Nederlandstalige versie is op de
[Install](/nl/howto/install-share/)-netwerkschijf te vinden. Als we DVD’s
gebrand hebben is het ook [te leen](/nl/howto/microsoft-windows/). De
licentievoorwaarden staan gebruik op RU-computers toe en ook
thuisgebruik door medewerkers. Licentiecodes zijn bij C&CZ helpdesk of
postmaster te verkrijgen. Eigen DVD’s kunnen ook op
[Surfspot](http://www.surfspot.nl) besteld worden.
