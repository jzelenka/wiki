---
author: petervc
date: 2014-11-25 14:52:00
tags:
- studenten
- medewerkers
- docenten
title: 'Weer phishing mails: "WAARSCHUWING" van "Admin"'
---
De afgelopen dagen zijn weer een tiental studenten en medewerkers van
FNWI in een phishing mail getrapt. In krom Nederlands stond in de mail
dat de mailbox bijna vol was. Het onderwerp was vaak “WAARSCHUWING” en
de afzender noemde zichzelf “Admin”. Er werd een link gegeven naar
<http://www.heliohost.org/> , een leverancier van gratis websites, waar
een nagemaakte Science webmail pagina gemaakt was. Op deze website waren
overigens de ingevulde loginnamen en wachtwoorden, van alle reacties van
alle aangevallen domeinen, voor iedereen te lezen. Daarop heeft C&CZ
deze logins geblokkeerd en de beheerders van HelioHost gevraagd de
website te verwijderen. Trap niet in phishing mails, het veroorzaakt
overlast voor jezelf en anderen!
