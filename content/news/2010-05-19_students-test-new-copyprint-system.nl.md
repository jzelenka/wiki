---
author: petervc
date: 2010-05-19 17:22:00
title: Studenten testen nieuw print/kopieer-systeem
---
De onder genoemde test door studenten is afgesloten, het [Peage
project](http://www.ru.nl/uci/peage/peage/) loopt door. C&CZ zocht voor
het project Peage, dat een RU-breed print/kopieersysteem op moet
leveren, 25 studenten. Deze studenten kregen € 50,- print/kopieerbudget
in het nieuwe systeem. De voorwaarden om mee te doen waren:

-   student FNWI
-   in bezit van OV-chipkaart
-   aanwezig zijn bij de instructiebijeenkomst op donderdag 27 mei 2010
    tussen 12:30 - 13:30 uur in CC3
-   bereid campusbreed (FMW, Letteren, FNWI en Managementwetenschappen)
    zwart/wit print/kopieeropdrachten uit te voeren tijdens de
    testperiode (tot 2 juli 2010)
-   bereid ervaringen te delen in de evaluatie

De eerste 25 die zich via mail aanmelden bij Peter van Campen werden
geselecteerd, sluitingsdatum was 25 mei.
