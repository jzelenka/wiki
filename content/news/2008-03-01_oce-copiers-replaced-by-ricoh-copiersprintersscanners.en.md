---
author: petervc
date: 2008-03-01 00:03:00
title: Oce copiers replaced by Ricoh copiers/printers/scanners
---
The Oce copiers are being replaced by Ricoh machines that can print and
scan too. There is a [Ricoh-overview page](/en/howto/ricoh/), but of
course they are also mentioned on the pages dealing with
[copiers](/en/howto/copiers/), the [network
printers](/en/howto/printers-en-printen/) and the
[scanners](/en/tags/scanners/).
