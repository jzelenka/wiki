---
author: petervc
date: 2011-12-22 16:42:00
tags:
- studenten
- medewerkers
- docenten
title: Matlab R2011b
---
De nieuwste versie van [Matlab](/nl/howto/matlab/), R2011b, is
beschikbaar voor afdelingen die licenties voor het gebruik van Matlab
aangeschaft hebben. De software en de licentiecodes zijn via een mail
naar postmaster te krijgen voor wie daar recht op heeft. De software
staat overigens ook op de [install](/nl/tags/software)-schijf. Op de
door C&CZ beheerde 64-bit Linux machines is R2011b beschikbaar, een
oudere versie (/opt/matlab-R2011a/bin/matlab) is nog tijdelijk te
gebruiken. Op de door C&CZ beheerde Windows-machines zal R2011b ook
beschikbaar komen.
