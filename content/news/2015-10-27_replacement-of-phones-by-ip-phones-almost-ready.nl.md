---
author: bertw
date: 2015-10-27 12:27:00
tags:
- medewerkers
title: Vervanging telefoons door IP-telefoons bijna gereed
---
De vervanging van telefoons door [IP
telefoons](/nl/howto/categorie%3atelefonie/) is in volle gang en zal over
enkele weken afgerond zijn. Behalve IP telefoons kunnen afdelingen ook
kiezen voor [mobiele bedrijfstelefoons op basis van Vodafone Wireless
Office](/nl/howto/categorie%3atelefonie/).
