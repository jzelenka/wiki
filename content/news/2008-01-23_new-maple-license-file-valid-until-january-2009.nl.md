---
author: petervc
date: 2008-01-23 10:59:00
title: Maple licentiefile vernieuwd, geldig tot januari 2009
---
Het licentiefile van [Maple](/nl/howto/maple/) was bijna verlopen, daarom
is het vernieuwd op de license server. Medewerkers en/of studenten die
Maple met een thuislicentie gebruiken, kunnen via C&CZ een nieuwe
licentiefile krijgen, die tot eind januari 2009 geldig is.
