---
author: remcoa
date: 2015-11-24 18:11:00
tags:
- medewerkers
- studenten
title: Digital testing/exams in a safe browser environment in the computer labs
---
It is now possible to do [digital testing](/en/howto/digitaal-toetsen/)
(take exams) in the C&CZ managed [computer
labs](/en/howto/terminalkamers/). Various question/answer forms are
supported, at the moment mainly open questions and multiple choice.
Please [contact C&CZ](/en/howto/contact/) for further information.
