---
author: petervc
date: 2017-05-24 19:50:00
tags:
- medewerkers
title: Departments can order card readers for Peage
---
The [RU Facility Services (Facilitair
Bedrijf)](http://www.ru.nl/facilitairbedrijf/printen/printen-campus/)
let us know that if a department wants to speed up the use of the Konica
Minolta multifunctionals, a department can order a campus card reader
for € 300,-. After that, a staff member only has to type the PIN (4
digits) and swipe the campus card to login, just like students do on the
ground floor of the Huygens building. Because a new European tender is
expected to be finished January 2018, this will only be of interest for
very busy multifunctionals.
