---
author: polman
date: 2008-02-20 11:53:00
title: New license period for Mathematica coming up
---
The current license period for [Mathematica](http://www.wolfram.com)
will end soon, departments interested in using
[Mathematica](/en/howto/mathematica/) in the next contract period of 3
years can contact C&CZ.
