---
author: sioo
date: 2021-10-01 15:40:00
tags:
- medewerkers
- docenten
- studenten
title: Update BigBlueButton videoconferencing
---
De BigBlueButton experimentele videoconferencing website is verhuisd en
heeft een update gehad. De nieuwe URL is
<https://bigbluebutton.science.ru.nl/> , je kunt er zelf een login op
aanmaken, als je die nog niet had van de oude site
<https://bigbb.science.ru.nl> die nu naar de nieuwe site doorgestuurd
wordt. De site blijft “experimenteel”, zonder garanties.
