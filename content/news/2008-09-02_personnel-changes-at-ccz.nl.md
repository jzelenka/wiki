---
author: petervc
date: 2008-09-02 17:08:00
title: Personele wijzigingen bij C&CZ
---
Per 1 september gaat {{< author "keesk" >}} zich voornamelijk bezig
houden met het leiden van het project voor de invoering van het
universiteitsbrede roostersysteem op basis van [Syllabus
Plus](http://www.scientia.com/nl/). Daarom treedt hij terug als
afdelingshoofd, al blijft hij wel als adviseur aan C&CZ verbonden. Hij
wordt als afdelingshoofd opgevolgd door {{< author "petervc" >}}, die
op zijn beurt bij systeembeheer/ontwikkeling wordt opgevolgd door
{{< author "caspar" >}}.
