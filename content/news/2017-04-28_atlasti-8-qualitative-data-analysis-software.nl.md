---
author: petervc
date: 2017-04-28 13:17:00
tags:
- studenten
- medewerkers
title: ATLAS.ti 8 kwalitatieve data-analysesoftware
---
Versie 8 van de ATLAS.ti kwalitatieve data-analysesoftware voor Windows
is beschikbaar op de [Install](/nl/howto/install-share/)-netwerkschijf.
Volgens de Radboud educational license mag deze software alleen gebruikt
worden voor onderwijs en onderzoek.
