---
author: polman
date: 2019-11-26 12:13:00
tags:
- medewerkers
- studenten
title: Nieuwe licentieperiode Mathematica aanstaande
---
De huidige licentie periode voor [Mathematica](http://www.wolfram.com)
loopt binnenkort af, afdelingen die geïnteresseerd zijn in het gebruik
van [Mathematica](/nl/howto/mathematica/) voor de nieuwe 3-jarige
contractperiode kunnen [contact](/nl/howto/contact/) opnemen met C&CZ.
