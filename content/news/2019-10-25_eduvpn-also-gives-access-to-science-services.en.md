---
author: petervc
date: 2019-10-25 15:57:00
tags:
- studenten
- medewerkers
- docenten
title: EduVPN also gives access to Science services
---
For quite some time, [SURF EduVPN](https://www.surf.nl/en/eduvpn) has
been the standard [VPN for central RU
services](https://www.ru.nl/ict-uk/staff/working-off-campus/vpn-virtual-private-network/).
Now all VPN-protected Science services that are also accessible from the
[Science VPN services](/en/howto/vpn/) and/or from [RU
Eduroam](https://www.ru.nl/ict-uk/staff/wifi/set-up-wifi-eduroam-campus/),
can be accessed from SURF RU EduVPN. This includes network drives,
license servers, websites, protected labs, access to workplaces and
special services such as
[telgids](https://wiki.cncz.science.ru.nl/index.php?title=FNWI_Telefoon_en_E-mail_gids&setlang=en)/ldap/kerberos.
