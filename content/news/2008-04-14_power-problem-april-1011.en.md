---
author: petervc
date: 2008-04-14 12:43:00
title: Power problem April 10/11
---
Thursday evening April 10 at 21:30 a power glitch for wings 1 and 2 of
the Huygens building caused power switches to triple. Thanks to a UPS in
wing 1 the router and network switches kept on running until 22:30. The
[UVB](http://www.ru.nl/uvb) fixed the power to the router and swityches
in wing 1. Power to the switches in wing 2 was not fixes until 10:00,
which made the problem last the longest in wing 2 and the eastside of
the central street between wings 2 and 4 (no network). C&CZ will contact
the UVB to try to give more priority to restoring power for network
equipment.
