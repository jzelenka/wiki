---
author: petervc
date: 2017-08-16 16:19:00
tags:
- studenten
- medewerkers
title: Bjorn Bellink new head of C&CZ
---
As of September 1, {{< author "bbellink" >}} will start as head of
C&CZ. {{< author "petervc" >}}, who was acting head since Caspar
Terheggen moved, will go back to his old position.
