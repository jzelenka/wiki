---
author: petervc
date: 2016-10-24 22:34:00
tags:
- studenten
- medewerkers
- docenten
title: "Wachtwoord-hygiëne: persoonlijk en niet te oud, net een tandenborstel"
---
Op de RU is een [Treat your password like your
toothbrush](http://www.ru.nl/privacy/nieuws/nieuws/@1052921/campagne-treat-your-password-like-your-toothbrush/)
campagne gevoerd. Ook C&CZ zal binnenkort actie ondernemen:
ICT-contactpersonen krijgen een lijst van hun gebruikers die al jaren
hun Science-wachtwoord niet veranderd hebben. Oude Science-wachtwoorden
hebben niet de nieuwste versleuteling, dat willen we graag veranderd
zien. Zelfs het kiezen van een nieuw Science-wachtwoord dat gelijk is
aan het oude wachtwoord is dus al een verbetering maar echt een nieuw
wachtwoord kiezen heeft natuurlijk sterk de voorkeur. Science
wachtwoorden veranderen kan op de [DHZ-site](https://dhz.science.ru.nl).
Bij wijzigen van een wachtwoord moet men vooraf even bedenken welke (ook
privé-)apparaten automatisch het oude wachtwoord blijven proberen, om
overlast te voorkomen.
