---
author: petervc
date: 2017-08-19 00:44:00
tags:
- studenten
title: Tijdelijk gratis F-Secure beveiligingssoftware voor RU-studenten
---
Van 21 augustus tot 21 september 2017 kun je als student aan de Radboud
Universiteit gratis F-Secure SAFE downloaden op je
pc/laptop/smartphone/tablet/iMac/Macbook. Zie voor meer info de [ISC
studentensoftwarepagina](http://www.ru.nl/ict/studenten/software/).
