---
author: bbellink
date: 2022-09-23 13:13:00
tags:
- medewerkers
- studenten
title: AVD/AVS Audiovisual Services for FNWI to central RU location
cover:
  image: img/2022/beamer.jpg
---
From Monday, October 3, Sacha Djuric and Jelle de Koter of AVD/AVS
Audiovisual Services will operate from the RU central location Thomas
van Aquinostraat 1.00.22 for all faculties and service departments. The
new phone number will be [+31 24 36 12680](tel:+31 24 36 12680). The most frequent picked-up
items can then be picked up at the [C&CZ helpdesk](/en/howto/contact/):
adapter cables, beamers for study associations, cables, batteries,
remote controls and a a mobile USB camera on a tripod with microphone
(e.g. for hybrid education). In case of questions contact
[C&CZ](/en/howto/contact/).
