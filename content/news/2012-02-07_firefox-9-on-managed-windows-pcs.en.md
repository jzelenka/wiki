---
author: polman
date: 2012-02-07 15:13:00
tags:
- studenten
- medewerkers
- docenten
title: Firefox 9 on Managed Windows PC's
---
A new version of [Firefox](http://www.mozilla.org/en-US/firefox/fx/) (9)
has been installed on the [managed Windows
PC’s](/en/howto/windows-beheerde-werkplek/), including all plugins that
were available for the old version. Currently the new version is only
available through the Start menu, but soon also through the shortcut on
the desktop.
