---
author: mkup
date: 2014-05-08 12:12:00
tags:
- medewerkers
- studenten
title: Maintenance wireless infrastructure at May 19
---
At Monday May 19, a major upgrade of the wireless infrastructure and its
maintenance tool (called Prime) will take place by the
[ISC](http://www.ru.nl/isc). Therefore the wireless service (i.e.
Eduroam) will be interrupted several times from 8:00 pm. Existing
wireless connections will be lost and new connections will not be
possible for some time. Due to the complexity of this operation this
maintenance will possibly be continued at Monday, May 26, again with
interruptions from 8:00 pm.
