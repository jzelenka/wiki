---
author: caspar
date: 2010-11-20 19:05:00
title: Terena TLS certificaten
---
Voor domeinnamen die zijn
[geregistreerd](/nl/howto/domeinnaam-registratie/) via C&CZ kan zonder
kosten een gesigneerd [TLS certificaat](/nl/howto/tls-certificaten/)
worden aangevraagd door een mail te sturen naar postmaster. Het betreft
een certificaat gesigneerd door Terena, een Certificate Authority die
door alle standaard browsers wordt vertrouwd.
