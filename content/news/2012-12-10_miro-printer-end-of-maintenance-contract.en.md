---
author: petervc
date: 2012-12-10 13:14:00
tags:
- studenten
- medewerkers
- docenten
title: 'Miro printer: end of maintenance contract'
---
Year after year some users preferred the Xerox Phaser color
[printer](/en/howto/printers-en-printen/) miro over the Ricoh Aficio
[laserprinters](/en/howto/printers-en-printen/) for its different (wax)
color technique. It is not possible to have a maintenance contract in
2013 for miro. That doesn’t mean that it’s impossible to use miro, but
it does mean that miro will be removed when a major hardware failure
occurs in 2013. The plan is to have new multifunctionals all over the RU
in the first half of 2013.
