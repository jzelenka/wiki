---
author: bertw
date: 2008-10-09 17:49:00
title: Telefoongids RU automatisch bijgewerkt
---
De koppeling tussen de [FNWI
Telefoongids](/nl/howto/fnwi-telefoon-en-e-mail-gids/) en het [RU Relatie
Beheer Systeem (RBS)](http://www.ru.nl/rbs) en daarmee de [RU
telefoongids](/nl/howto/telefoon-en-e-mail-gidsen/) is een feit. Dat wil
zeggen dat de gidsen automatisch aangepast worden doordat de
telefooncontactpersoon (vaak secretariaat) van de afdeling binnen FNWI
de gegevens online aanpast na inloggen op de FNWI Telgids Mutatie
website. In de FNWI-telefoongids kan men ook gegevens opnemen van labs,
liften, secretariaten etc., alleen de gegevens bij een U-nummer kunnen
in RBS. Voor meer informatie zie de
[Telefoniepagina](/nl/tags/telefonie).
