---
author: mkup
date: 2018-02-20 16:58:00
tags:
- medewerkers
- docenten
- studenten
title: Old Sopho telephone exchange switched off
---
After more than 30 years of loyal service, the old Philips Sopho S2500
telephone exchange of the SKU (RU and Radboudumc) has been switched off
and removed. This ‘first digital telephone exchange’ was commissioned by
FNWI in August 1987, as a pilot project of PTT, Philips and the RU (then
KUN). Aad Hulsbosch of C&CZ was one of the driving forces during
commissioning. In 1991, the Sopho switch was expanded for the entire
SKU, to approximately 15,000 connections. The [telephone functionality
on the SKU](http://www.ru.nl/ict-uk/staff/telephones/) has recently been
completely taken over by an IP telephone exchange of [Mitel
Mitel](https://nl.wikipedia.org/wiki/) and the
[Vodafone](http://www.vodafone.nl) Wireless Office wireless telephony.
