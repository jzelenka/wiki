---
author: caspar
date: 2009-02-09 14:22:00
title: 1234567890 seconds since The Epoch
---
Next Saturday February 14 at 00:31:30 local time the [time
counter](http://en.wikipedia.org/wiki/Unix_time) that is commonly used
in the Unix world, i.e. the number of seconds since January 1, 1970 UTC
(‘The [Epoch](http://en.wikipedia.org/wiki/Epoch_(reference_date))’),
reaches the pleasing value of
[1234567890](http://www.coolepochcountdown.com/). Perhaps even more
exciting is the fact that in the early morning of January 19, 2038 the
counter reaches the maximum value that can be stored in a 32 bit signed
integer. Luckily there still is enough time to find a solution for this
daunting challenge compared to which the [Y2K
problem](http://en.wikipedia.org/wiki/Y2K) was just a stroll in the
park.
