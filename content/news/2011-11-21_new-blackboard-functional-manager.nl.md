---
author: caspar
date: 2011-11-21 17:18:00
tags:
- studenten
- docenten
title: Nieuwe Blackboardbeheerder
---
Vanaf 15 november jl. is Fred Melssen bij FNWI in dienst getreden.
Fred’s belangrijkste werkzaamheden worden: ontwikkeling en beheer van
ICT-oplossingen tbv het onderwijs en facultair
[Blackboard](/nl/howto/blackboard/)-beheerder voor FNWI. Fred is daarmee
de opvolger van Pauline Berens en directe collega van Remco Aalbers.
Fred is afkomstig van de Faculteit der Managementwetenschappen, waar hij
als ICTO specialist, programmeur en facultair Blackboard-beheerder
werkte en het UCI waar hij als programmeur in dienst was. Fred’s functie
valt deels onder het onderwijscentrum en deels onder C&CZ. Zijn werkplek
bevindt zich bij C&CZ.
