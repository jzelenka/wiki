---
author: polman
date: 2015-09-24 11:49:00
tags:
- medewerkers
- studenten
title: New version of OriginLab (2015, 9.2)
---
A new version (2015, 9.2) of [OriginLab](/en/howto/originlab/), software
for scientific graphing and data analysis, can be found on the
[Install](/en/howto/install-share/)-disk. The license server has been
upgraded to this version. Departments that take part in the license can
request installation and license info from C&CZ, also for standalone
use. Installation on C&CZ managed PCs still has to be planned.
Departments that want to start using OriginLab should contact
[C&CZ](/en/howto/contact/).
