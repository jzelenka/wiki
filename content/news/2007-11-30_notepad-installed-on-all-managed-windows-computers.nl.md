---
author: polman
date: 2007-11-30 14:37:00
title: "Notepad++ geïnstalleerd op alle Windows beheerde werkplekken"
---
Notepad++ is een open source source code editor en Notepad vervanger,
met onder meer syntax ondersteuning voor een groot aantal
programmeertalen. Voor uitgebreide beschrijving zie
[Notepad++](http://notepad-plus.sourceforge.net/uk/site.htm)
