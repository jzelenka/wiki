---
author: wim
date: 2011-02-16 10:53:00
tags:
- studenten
- medewerkers
- docenten
title: Workstation Unlocker op onderwijs PC's
---
Op alle onderwijs PC’s (studielandschap, PC zalen en de info PC’s bij de
bibliotheek) is *Workstation Unlocker*
geïnstalleerd.

Deze software maakt het mogelijk om, na een ingestelde timeout, een
gelockte PC vrij te geven.

De gebruiker, ingelogd op PC, wordt netjes uitgelogd zonder dat een
reboot noodzakelijk is.

Informatie over de software is te vinden op
[www.alexsapps.com](http://www.alexsapps.com/Apps/W/WorkstationUnlocker/Default.aspx)
