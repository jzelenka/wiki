---
author: bertw
date: 2013-09-24 16:40:00
tags:
- medewerkers
- docenten
title: 'Nieuw telefoonnummer C&CZ Helpdesk: 20000'
---
Vanaf heden is de C&CZ Helpdesk bereikbaar via telefoonnummer 20000. Het
huidige nummer 56666 zal worden uitgefaseerd. Tot 1 november 2013 is de
helpdesk nog via het oude nummer bereikbaar, daarna zult u een bericht
horen over de wijziging en automatisch worden doorverbonden naar het
nieuwe nummer. Vanaf de eerste week van 2014 zal het oude nummer
definitief buiten dienst gaan.
