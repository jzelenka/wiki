---
author: petervc
date: 2009-10-16 16:53:00
title: BASS aanpassing zelfbeheerde werkplek en downtijd
---
Indien men een zelfbeheerde werkplek heeft waarop [BASS](/nl/howto/bass/)
gebruikt wordt, moet men voor 9 november de [BASS software
aanpassen](/nl/howto/bass/) om problemen te vermijden. De door C&CZ
beheerde Windows-werkplekken zijn/worden op tijd aangepast. Deze
aanpassing is nodig om van de nieuwe BASS-servers gebruik te kunnen
maken. BASS zal ontoegankelijk zijn van vrijdag 6 november 12:00 uur tot
maandag 9 november 08:00 uur.
