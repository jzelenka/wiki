---
author: remcoa
date: 2009-06-17 16:02:00
title: New wiki extension AWC Forum available
---
A new forum extension can now be used in all wikis, see [the installed
MediaWiki
extensions](/en/howto/geïnstalleerde-mediawiki-extensies#awc-forum/) on
how to enable this extension.
