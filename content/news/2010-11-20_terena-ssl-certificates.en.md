---
author: caspar
date: 2010-11-20 19:05:00
title: Terena SSL certificates
---
For domain names [registered](/en/howto/domeinnaam-registratie/) through
C&CZ a signed [TLS certificate](/en/howto/tls-certificaten/) can be
requested free of charge via a mail to postmaster. These certificates
are signed by Terena, a Certificate Authority trusted automatically by
all major browsers.
