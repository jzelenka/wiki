---
author: bbellink
date: 2020-09-23 23:15:00
tags:
- medewerkers
title: AWS Research Immersion Day voor RU dinsdag 29 sep 13:00-16:00
---
Sinds begin dit jaar heeft de RU een contract gesloten met SURFcumulus
voor de toegang tot 2 cloudleveranciers: AWS (Amazon Web Services) en
Azure (Microsoft). C&CZ wil graag de mogelijkheden van het AWS
cloudplatform breder onder de aandacht brengen bij onderzoekers en
organiseert daarom een ‘Research Immersion Day’ i.s.m. AWS. Het
programma voor deze Engelstalige middag:

`13:00 – 13:05: Welcome`
`13:05 – 13:45: Research Computing on AWS (including short introductions on cost management, data privacy and sustainability)(various speakers)`
`13:45 – 14:25: Jupyter Notebooks on AWS (Nicolas Metallo)`
`14:25 – 14:30: Break` `14:30 – 15:45: HPC on AWS (John Segers)`
`15:45 – 16:00: Q&A and closing`

Het is een online meeting met behulp van Amazon Chime. Meeting info op
verzoek. Achtergrondinformatie:
<https://aws.amazon.com/government-education/research-and-technical-computing>.
