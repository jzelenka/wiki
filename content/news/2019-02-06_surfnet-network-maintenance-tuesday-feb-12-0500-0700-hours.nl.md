---
author: mkup
date: 2019-02-06 08:40:00
tags:
- medewerkers
- docenten
- studenten
title: 'Netwerkonderhoud SURFnet: dinsdagochtend 12 feb 05:00-07:00 uur'
---
Op dinsdagochtend 12 februari zal onderhoud plaatsvinden aan de
internetverbindingen tussen de RU en SURFnet. Hoewel er geen
onderbrekingen in het internetverkeer worden verwacht, worden die niet
volledig uitgesloten. Denk s.v.p. na of deze mogelijke verstoring voor u
gevolgen heeft en bereid u indien nodig voor.
