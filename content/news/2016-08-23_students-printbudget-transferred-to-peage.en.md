---
author: wim
date: 2016-08-23 14:32:00
tags:
- studenten
title: 'Students: printbudget transferred to Peage'
---
All FNWI Konica Minolta MFPs within the C&CZ-printbudget system have
been switched to Peage. The C&CZ printbudget system is now only used for
the HP printer escher (Library of Science) and the posterprinter
kamerbreed. August 23, C&CZ has therefore emptied all student print
budgets that are associated with an active Science student login. The
ISC added that last budget to the corresponding Peage student budget on
August 24. Owners of sufficiently positive group budgets will be asked
what to do with the remaining budget. Budget for escher/kamerbreed can
as of now only be topped up or settled at C&CZ
(postmaster\@science.ru.nl), indicating charge account (kostenplaats) or
if necessary with cash.
