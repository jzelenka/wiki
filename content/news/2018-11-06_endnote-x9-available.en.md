---
author: petervc
date: 2018-11-06 11:41:00
tags:
- medewerkers
- studenten
title: Endnote X9 available
---
[Endnote](http://www.endnote.com/) version X9 is available for
MS-Windows and Mac OS X. It can be found on the
[install](http://www.cncz.science.ru.nl/software/installscience) network
disk. The license permits use by employees and students, also at home.
Within short notice it will also be available in the Software Center of
the [C&CZ managed pc with Windows
10](https://wiki.cncz.science.ru.nl/index.php?title=Windows_beheerde_werkplek&setlang=en).
