---
author: caspar
date: 2012-09-25 10:33:00
tags:
- studenten
- medewerkers
- docenten
title: Laptop pool
---
As of October 2012, the Faculty of Science will have a [pool of 64
laptops](/en/howto/laptop-pool/) that can be borrowed by employees and
students of this Faculty. There are 64 standard-RU laptops ([HP Probook
6560b](http://www8.hp.com/nl/nl/products/laptops/product-detail.html?oid=5045605)).
The laptops can be reserved and borrowed at the [Library of
Science](http://www.ru.nl/fnwi/bibliotheek/). Reservations for Faculty
education purposes have precedence, especially for the groups that
contributed financially to the pool: [PUC of
Science](http://www.ru.nl/pucofscience/) and the [Chemistry
Lab](http://www.ru.nl/omw/). It is also possible to borrow complete
[laptop
trolleys](http://www.kruunenberg-shop.nl/index.php/kantoor-artikelen/laptop-kasten/laptoptrolley-24-stuks.html)
containing around 21 laptops. There are [lending
regulations](/en/howto/laptop-uitleenreglement/). Individuals who borrow
a laptop need to sign a [lending
agreement](/en/howto/laptop-leenovereenkomst/). The laptops can be used
anywhere in the faculty, e.g. on the mezzanine; a study landscape
without desktop pc’s that will be opened soon.
