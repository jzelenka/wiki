---
author: pberens
date: 2011-01-26 16:25:00
title: Information about service interruptions and maintenance
---
Feel free to use our new page [Service Interruptions and
Maintainance](/en/tags/storingen). Be quickly informed via the [CPK
mailinglist](http://mailman.science.ru.nl/mailman/listinfo/CPK) or the
[RSS feed](/en/howto/storingen/). There’s an archive containing [Recent
Service Interruptions](/en/cpk/) as well.
