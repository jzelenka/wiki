---
author: polman
date: 2017-02-21 23:53:00
tags:
- medewerkers
- studenten
title: Nieuwe licentieperiode Mathematica aanstaande
---
De huidige licentie periode voor [Mathematica](http://www.wolfram.com)
loopt binnenkort af, afdelingen die geïnteresseerd zijn in het gebruik
van [Mathematica](/nl/howto/mathematica/) voor de nieuwe 3-jarige
contractperiode kunnen [contact](/nl/howto/contact/) opnemen met C&CZ.
