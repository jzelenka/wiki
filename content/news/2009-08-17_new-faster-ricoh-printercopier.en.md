---
author: sommel
date: 2009-08-17 09:10:00
title: New faster Ricoh printer/copier
---
The [printer](/en/howto/printers-en-printen/),
[scanner](/en/tags/scanners/) en [copier](/en/howto/copiers/) Ricoh
Aficio MP 5000 with the name *watt* is replaced by a faster B&W Ricoh
Aficio MP 7000 with the name *volta*.

The address book of *watt* is largely copied to *volta*. *Volta* can
only scan in B&W.
