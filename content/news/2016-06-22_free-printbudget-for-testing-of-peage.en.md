---
author: petervc
date: 2016-06-22 14:48:00
tags:
- medewerkers
- docenten
- studenten
title: "Free printbudget for testing of Péage"
---
In the next months, the [C&CZ managed Konica Minolta
MFP’s](/en/howto/copiers/) will be transferred to the [RU wide
print/copy/scan system
Péage](http://www.ru.nl/fb/english/print/printing-peage-0/). Students
and staff of the Faculty of Science are requested to test this, in order
to tackle problems in an early stage. To encourage this, the first
dozens of these that register at the Science Postmaster will receive
free Péage budget when they share their experiences after testing. One
Péage MFP has been available for several years near the cafetaria in the
Huygens building. During the test, another one is placed in the central
street near wing 5 on the second floor. After the test, the first group
KM’s to be switched to Péage are located on the ground floor and
mezzanine. C&CZ will maintain FNWI-specific instructions at [the C&CZ
Péage page](/en/howto/peage/).
