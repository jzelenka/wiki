---
author: petervc
date: 2008-01-29 12:24:00
title: TU Delft slachtoffer van phishing scam
---
De [CERT-RU](http://www.ru.nl/cert), die security incidenten binnen de
RU coordineert, meldde dat medewerkers en studenten van de TU Delft
mails ontvangen hadden die op het eerste gezicht afkomstig leken te zijn
van de TU Delft, waarin om reactie met loginnaam en wachtwoord werd
gevraagd, anders zou de account opgeheven worden. Het antwoord van een
onnadenkende naieve gebruiker ging naar een “@hotmail.co.uk” mail-adres.
C&CZ hecht eraan te benadrukken dat \*geen enkele officiele instantie\*
u ooit per email of telefonisch om wachtwoord of pincode voor uw
bankrekening zal vragen. Voor de originele melding van de CERT van de TU
Delft, zie de

[TU Delft Security
Alert](http://www.tudelft.nl/live/pagina.jsp?id=17d7e6d4-a6ae-475a-8a63-70d0ab4629a1&lang=nl).
