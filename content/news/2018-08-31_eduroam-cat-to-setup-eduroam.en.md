---
author: petervc
date: 2018-08-31 12:33:00
tags:
- medewerkers
- studenten
title: Eduroam CAT to setup Eduroam
---
The [ISC](https://www.ru.nl/ict/) made setting up the [wireless
network](https://www.ru.nl/wireless) simpler and safer by switching to
[Eduroam CAT](https://cat.eduroam.org). Employees and students of
Radboud University are offered an installer tailored to the RU campus
network. Eduroam CAT currently supports various versions of Windows,
MacOS, OS X, Linux, Chrome OS, Android and a generic EAP configuration.
In order to use Eduroam CAT, Internet connectivity is necessary. On
campus, this might be the network Eduroam-config, but then one must use
[the RU-specific
instructions](https://www.ru.nl/ict-uk/staff/wifi/setting-up-eduroam/)
