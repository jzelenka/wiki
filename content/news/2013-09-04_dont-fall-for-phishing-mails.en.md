---
author: petervc
date: 2013-09-04 10:45:00
tags:
- studenten
- medewerkers
- docenten
title: Don't fall for phishing mails\!
---
When you get a mail pretending to come from “C&CZ” or “RU Administrator”
asking you to mail or provide your username and password to some website
in order to prevent problems: do not fall for this. Because if you do
react, you will cause problems! Internet criminals will use the
credentials to spread spam through our mailservers. They will also have
access to all of your mail on the IMAP server and all of your files. The
past few weeks no less than five FNWI employees and students fell for
such a phishing mail, with serious consequences for all users.
