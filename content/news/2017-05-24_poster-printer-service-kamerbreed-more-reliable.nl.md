---
author: stefan
date: 2017-05-24 17:22:00
tags:
- medewerkers
- studenten
title: Poster print service kamerbreed betrouwbaarder
---
De in mei 2013 door C&CZ gratis verkregen [A0 foto
printer](/nl/howto/kamerbreed/) heeft de afgelopen jaren veel posters
voor medewerkers en studenten geprint. De laatste tijd zijn er wel
diverse storingen geweest, die langere tijd duurden. Daarom is besloten
een nieuw exemplaar met garantie aan te schaffen. Bij calamiteiten is
ook nog de oude inzetbaar. Daarmee verwachten we dat we de komende jaren
een gegarandeerde service kunnen leveren en niet meer door hoeven te
verwijzen naar [RU
copyshop](http://www.ru.nl/facilitairbedrijf/winkels/copyshop/).
