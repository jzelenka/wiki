---
author: caspar
date: 2012-09-25 10:33:00
tags:
- studenten
- medewerkers
- docenten
title: Laptop-pool
---
Vanaf begin oktober 2012 beschikt FNWI over een [uitleen
laptop-pool](/nl/howto/laptop-pool/) van 64 stuks standaard-RU laptops
([HP Probook
6560b](http://www8.hp.com/nl/nl/products/laptops/product-detail.html?oid=5045605)).
Reserveren en lenen zal via de [Library of
Science](http://www.ru.nl/fnwi/bibliotheek/) gaan. Gebruik voor
FNWI-onderwijsdoeleinden heeft voorrang, speciaal voor de groepen die de
laptop-pool meegefinancierd hebben zoals [PUC of
Science](http://www.ru.nl/pucofscience/) en [Practicum
Chemie](http://www.ru.nl/omw/). Het is ook mogelijk een of meer [laptop
trolleys](http://www.kruunenberg-shop.nl/index.php/kantoor-artikelen/laptop-kasten/laptoptrolley-24-stuks.html)
met ongeveer 21 laptops ineens te lenen. Er is een
[uitleenreglement](/nl/howto/laptop-uitleenreglement/). Individuele
leners moeten een [leenovereenkomst](/nl/howto/laptop-leenovereenkomst/)
tekenen. De laptops kunnen overal in de faculteit gebruikt worden, b.v.
op de “insteekverdieping”, het studielandschap zonder vaste werkplekken
dat binnenkort geopend wordt.
