---
author: wim
date: 2019-06-25 14:44:00
title: Retiring Windows 2008 Terminal Server ts2
---
Mid july 2019 the Windows 2008 Terminal Server ts2 will be switched off.
This terminal server used to be necessary for non-Windows users to use
BASS or Tracelab. Please [contact C&CZ](/en/howto/contact/) if you think
this might cause problems for you.
