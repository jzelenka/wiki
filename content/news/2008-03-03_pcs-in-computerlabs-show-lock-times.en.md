---
author: wim
date: 2008-03-03 14:01:00
title: PC's in computerlabs show lock times
---
PC’s in computers labs now show the duration of the screenlock. Using
this information you can determine whether you want to reboot the PC.
This program has been built by one of our students: J. Groenewegen.
