---
author: mkup
date: 2017-11-22 10:03:00
tags:
- medewerkers
- docenten
title: 'Network maintenance at Science Faculty: upgrade router'
---
Monday evening November 27, there will be maintenance on RU network
equipment. Between ca. 23:30-02:00 new firmware will be loaded into the
Heyendaal-East distribution router. During this period the wired and
wireless network will be unavailable for approx. 30 minutes. Also the
Mitel phones will be out of order, only the green emergency phones will
be functional. Please think which processes depend on network
connectivity and prepare for this outage if necessary.
