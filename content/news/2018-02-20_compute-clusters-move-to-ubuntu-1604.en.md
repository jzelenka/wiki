---
author: polman
date: 2018-02-20 14:35:00
tags:
- medewerkers
- studenten
title: Compute clusters move to Ubuntu 16.04
---
C&CZ installed a new [head
node](https://superuser.com/questions/400927/specific-difference-between-head-node-and-gate-way-to-a-cluster)
for the [cn-cluster](/en/howto/hardware-servers/) with Ubuntu 16.04 and a
new version of SLURM. This makes it possible to upgrade all
cn-clusternodes to these versions. Other clusters (coma, neuroinf, mlp)
had this upgrade already. The C&CZ-managed cn compute cluster consists
of 60 nodes with 1560 cores and 8 TB RAM. The Astrophysics coma cluster,
with 920 cores, 600 TB fileserver storage, GPU’s and fast
[Infiniband](http://en.wikipedia.org/wiki/InfiniBand) interconnect is a
separate cluster. For more info, see the [cn compute
cluster](/en/howto/hardware-servers/) page.
