---
title: Migration of external GitLab users to Science logins with access only to GitLab
author: bram
date: 2023-03-10
cover:
  image: img/2023/gitlab-logo.svg
---
Starting March 2023, external GitLab users of
[gitlab.science.ru.nl](https://gitlab.science.ru.nl) will be migrated
to Science logins that only have access to GitLab.

Until now, external GitLab logins were created in GitLab itself,
separate from the Science login administration. By migrating the
external GitLab users to Science logins with access to GitLab only,
the management of these logins is improved. The requester (responsible for the Science login) can
manage these logins in [DIY](https://dhz.science.ru.nl).

For external user of GitLab that are still required after April 1, 2023,
Science logins with GitLab access only can be requested, see [Science
login requests](/en/howto/login/#request-a-science-login). Specify which
existing GitLab account is to be migrated, then we will link it to the
new Science login.

As soon as the new Science login has been created, a password must first be set in [DIY](https://dhz.science.ru.nl). Only then is it possible to log in to gitlab.science.ru.nl.
Completely new Science logins for GitLab are only visible to others after the first login to GitLab. For example, if you want to add these people to a project.
