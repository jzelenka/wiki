---
author: polman
date: 2010-09-22 19:44:00
tags:
- studenten
- docenten
title: Instellingen bewaren op B-FAC PC's
---
Wie gebruikersinstellingen bewaard wil zien bij inloggen op de [PC’s in
studielandschap, cursuszalen etc.](/nl/howto/terminalkamers/), moet
eenmalig op de [Doe-Het-Zelf website](/nl/howto/dhz/) bij Profiel
aanvinken “Zwervend profiel voor het B-FAC domein”. Hierdoor wordt dan
wel het inloggen iets trager, omdat de instellingen van een server
opgehaald moeten worden. Het voordeel is dat alle gebruikersinstellingen
dan wel bewaard worden. Een voorbeeld hiervan is het vertrouwen van alle
“ru.nl” sites in de [NoScript Firefox
uitbreiding](http://noscript.net/), dat nodig is om bv.
[Blackboard](http://www.ru.nl/blackboard/) te gebruiken. Nieuwe logins
zullen vanaf nu een zwervend profiel krijgen in alle domeinen.
