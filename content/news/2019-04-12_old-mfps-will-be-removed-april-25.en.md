---
author: petervc
date: 2019-04-12 14:38:00
tags:
- medewerkers
- docenten
- studenten
title: Old MFPs will be removed April 25
---
The [old Konica Minolta
MFPs](https://www.ru.nl/fb/english/print/printing-campus/) will be
removed from April 25. Users have had several weeks time to move over to
the new system, that is much faster and more user friendly. For
students: try to use up your old credit and do not top up. Can’t use up
all your credit? Then you can ask to get a refund at the Library of
Science until June 1, 2019. For all users:
[EveryOnePrint](https://eop.ru.nl) will be shutdown too, this
functionality can be found in the [SafeQ web
upload](https://peage.ru.nl/).
