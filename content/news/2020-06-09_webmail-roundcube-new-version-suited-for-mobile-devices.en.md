---
author: polman
date: 2020-06-09 23:05:00
tags:
- medewerkers
- docenten
- studenten
title: 'Webmail: Roundcube new version suited for mobile devices'
---
The webmail service [Roundcube](https://roundcube.science.ru.nl) had an
major update, with a new user interface, suited for smartphones.
Resending/bouncing mails is now also possible. For all new
functionality, see [the project website](https://roundcube.net/).
Roundcube has lots of possibilies, like managing the sharing of mail
folders with other users and the filtering of mail directly at the
moment of arrival at the server. For more information, see our [mail
page](/en/tags/email/).
