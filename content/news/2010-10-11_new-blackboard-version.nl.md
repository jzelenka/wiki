---
author: pberens
date: 2010-10-11 13:57:00
tags:
- docenten
title: Blackboard nieuwe versie
---
Er is een [nieuwe versie van
Blackboard](/nl/howto/blackboard#.5bblackboard-9.1:-nieuwe-versie.5d.5bblackboard-9.1:-new-version.5d/)
in gebruik genomen. Verder is de [Blackboard](/nl/howto/blackboard/)
pagina in de C&CZ Wiki vernieuwd en er is een nieuwe facultaire
Blackboard-beheerder.
