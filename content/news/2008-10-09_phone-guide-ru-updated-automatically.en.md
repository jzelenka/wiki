---
author: bertw
date: 2008-10-09 17:49:00
title: Phone Guide RU updated automatically
---
The interface between the [Science phone
guide](/en/howto/fnwi-telefoon-en-e-mail-gids/) and the [RU Relation
Management System (RBS)](http://www.ru.nl/rbs) and through that with the
[phone guide](/en/howto/telefoon-en-e-mail-gidsen%7cru/) is working now.
That means that the guides will automatically be updated when the
telefone contact person (usually the secretary) of a department within
the Science Faculty logs into and updates the Science Phone Guide
website. In the Science Phone Guide one can also enter information of
labs, elevators, secretariats etc., only the data for a U-number will
end up in RBS. For more information see the [Telephony page](/en/tags/telefonie/).
