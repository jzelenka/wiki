---
author: polman
date: 2018-07-17 13:57:00
tags:
- studenten
- medewerkers
- docenten
title: Nieuwe versie Mailman maillijst software
---
De [Mailman maillijst software](https://mailman.science.ru.nl/) heeft
een upgrade gehad naar een nieuwe versie op een nieuwe server. Vanwege
aanmeld-spam was aanmelden tijdelijk afgezet. De nieuwe versie gebruikt
[reCAPTCHA](https://www.google.com/recaptcha/) als maatregel tegen
aanmeld-spam. Daarom is het aanmelden via het web weer mogelijk gemaakt.
