---
author: caspar
date: 2008-12-09 13:47:00
title: Adobe Creative Suite 4 Web/Design Premium
---
On [Surfspot](http://www.surfspot.nl) the [Adobe Creative Suite 4
Design/Web Premium Bundle](http://www.adobe.com/products/creativesuite)
for home use by employees can be bought for € 40, for MS-Windows and for
Apple Mac. The products for use within the university have been ordered
and will be available soon.
