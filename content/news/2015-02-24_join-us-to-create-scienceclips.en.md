---
author: caspar
date: 2015-02-24 16:02:00
tags:
- medewerkers
- studenten
title: Join us to create ScienceClips
---
We are looking for lecturers who would like to experiment with video for
their lectures. On the [ScienceClips](http://clips.science.ru.nl/)
website we have collected more information and some examples of the
possibilities. Are you interested in creating a ScienceClip for your
course or would you like to join the pilot project with your own
concept/idea? Please contact Caspar Terheggen, C&CZ.
