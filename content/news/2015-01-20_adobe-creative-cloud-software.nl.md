---
author: petervc
date: 2015-01-20 12:24:00
tags:
- studenten
- medewerkers
title: Adobe Creative Cloud software
---
Adobe Creative Cloud is de [opvolger van de Creative Suite
software](http://www.cnet.com/news/adobe-kills-creative-suite-goes-subscription-only/).
De [Adobe Creative Cloud
software](https://www.adobe.com/nl/creativecloud/catalog/desktop.html)
is beschikbaar op de [Install](/nl/howto/install-share/)-netwerkschijf.
Licentiecodes zijn bij C&CZ helpdesk of postmaster te verkrijgen.
Volgens de site-license mag deze Adobe Creative Cloud for Enterprise
(Specified Apps Only Non-Video) op alle computers in eigendom van de RU
worden geïnstalleerd zonder bijkomende kosten. Voor thuisgebruik van
deze software kunnen medewerkers en studenten terecht op
[Surfspot](http://www.surfspot.nl). De software bestaat uit de volgende
onderdelen: Photoshop, Illustrator, InDesign, Dreamweaver, Muse Pro,
Acrobat Pro, Bridge, Encore, Fireworks, Flash Builder Premium, InCopy,
Media Encoder, Edge Animate, Edge Code (preview) en Edge Reflow
(preview).

Met bijbetaling van € 15,68 ex BTW per jaar per installatie heeft men de
Adobe Institution Bundle (Add-On), met de pakketten: Photoshop Elements,
Premiere Elements, Captivate en Presenter.

Met bijbetaling van € 31,35 ex BTW per jaar per installatie heeft men de
Adobe Upsell to Creative Cloud Enterprise (Add-On), met de pakketten:
After Effects, Premiere Pro, Audition, Flash Professional, Lightroom,
Prelude en Speedgrade.
