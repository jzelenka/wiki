---
author: petervc
date: 2008-08-27 10:37:00
title: New version Endnote available
---
A CD of Endnote version X2 is available for MS-Windows, version X1 for
Macintosh. It can be installed on Windows from the
[install](http://www.cncz.science.ru.nl/software/installscience) network
disk, but the CDs can also be borrowed by employees and students, for
home use too. In a few weeks it will also be available on the
[Windows-XP Managed PCs](/en/howto/windows-beheerde-werkplek/).
