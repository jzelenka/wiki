---
author: caspar
date: 2013-09-17 11:45:00
tags:
- medewerkers
- studenten
- docenten
title: Prezi Enjoy Edu licentie
---
[Prezi](/nl/howto/prezi/) is een cloud gebaseerde applicatie om
presentaties te maken en te geven. Het is een alternatief voor
powerpoint. Voor docenten en studenten is gratis een uitgebreidere
licentievorm beschikbaar, de zogenaamde “Enjoy Edu” license. Deze biedt
meer diskruimte, het recht om presentaties niet openbaar te maken en een
eigen logo te gebruiken i.p.v. het Prezi logo.
