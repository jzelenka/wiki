---
author: petervc
date: 2015-08-28 11:01:00
tags:
- studenten
- medewerkers
- docenten
title: Online cursus Matlab
---
Als u Matlab interactief wilt leren gebruiken, in uw eigen tempo, bezoek
dan [de Matlab Academy](https://matlabacademy.mathworks.com/) en maak
een account aan met een e-mailadres eindigend op: @science.ru.nl,
@student.science.ru.nl, @donders.ru.nl, @pwo.ru.nl, @let.ru.nl,
@fm.ru.nl, @ai.ru.nl, @socsci.ru.nl, @ru.nl of @student.ru.nl en gebruik
de Activation Key die u kunt krijgen van [C&CZ](/nl/howto/contact/) als
uw afdeling deelneemt aan de campus [Matlab](/nl/howto/matlab/)-licentie.
