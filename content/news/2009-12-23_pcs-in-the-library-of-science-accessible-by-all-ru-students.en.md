---
author: caspar
date: 2009-12-23 16:48:00
title: PCs in the Library of Science accessible by all RU students
---
As of December 23, 2009 all students of Radboud University can logon to
the [PCs in the Library of Science](/en/howto/terminalkamers/) in the
Huygens building with their RU account (s-number). Make sure to choose
the RU logon domain at the logon screen.

Please Note:

-   Personal configuration settings are not preserved between sessions
    on these PCs.
-   Only the black-and-white printers in the vicinity of the library are
    automatically available. Other printers need to be configured
    manually for each session. For instructions please come to the
    library front desk.

FNWI students and staff can also logon with their science account
(domain B-fac), yet without their personal profile. However, the
U-network drive with their personal data is available. On PC’s in the
terminal rooms the personal profiles are still available.

In January the PCs in the *Studielandschap* will be replaced by brand
new hardware. The new PCs will be accessible by all RU students.
