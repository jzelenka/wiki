---
author: mkup
date: 2009-07-20 12:06:00
title: Redundant fiber uplinks
---
From today all network switches in the Huygens building and other
buildings at Toernooiveld are linked to the FNWI router by a second,
redundant fiber uplink. This router consists of two linked autonomous
switches at different locations. As result of this a defective fiber
link or failing router switch will not instantly effect the existing
network connections. Obviously an improved availability of the network
services.
