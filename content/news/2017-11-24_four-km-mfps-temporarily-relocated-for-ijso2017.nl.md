---
author: petervc
date: 2017-11-24 12:14:00
tags:
- medewerkers
- studenten
title: Vier KM MFP's tijdelijk verplaatst voor IJSO2017
---
Om aan de grote print- en kopieerbehoefte van
[IJSO2017](http://www.ijso2017.nl) te voldoen, worden 4 KM MFP’s in de
periode 29 november tot 11 december verplaatst. Hierbij is gekozen voor
de MFP’s die het minst gebruikt worden:

`HG00.038    BizHubC554-40` `HG00.645    km-pr0926`
`HG01.108    km-pr0930` `HG00.084    km-pr0928`

Excuses voor evt. overlast.
