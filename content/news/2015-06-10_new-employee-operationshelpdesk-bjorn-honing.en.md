---
author: caspar
date: 2015-06-10 16:55:00
tags:
- studenten
- docenten
title: 'New employee operations/helpdesk: Bjorn Honing'
---
As of June 1, {{< author "bjorn" >}} started to work for FNWI as C&CZ
employee operations/helpdesk. As such Bjorn is the immediate colleague
of {{< author "john" >}} and {{< author "stefan" >}}.
