---
author: mkup
date: 2015-08-20 16:32:00
tags:
- medewerkers
- docenten
- studenten
title: Wireless network ru-guest will be replaced September 10 by eVA
---
As of September 10, the wireless network ‘ru-guest’ will no longer be
available. Guests of Radboud University can now use the wireless network
‘eduroam’ through ‘eVA’ (eduroam Visitor Access). All employees and
students of FNWI can request one or more eVA guest accounts at the C&CZ
helpdesk (helpdesk\@science.ru.nl). When requesting eVA accounts please
supply your mail address and your mobile number. More information on eVA
can be found at [the SURF
website](https://www.surf.nl/kennis-en-innovatie/innovatieprojecten/startdatum-2012/draadvrij/wifi-op-de-campus-met-eduroam/eduroam-visitor-access/index.html).
