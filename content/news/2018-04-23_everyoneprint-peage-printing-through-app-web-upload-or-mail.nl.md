---
author: bram
date: 2018-04-23 13:34:00
tags:
- medewerkers
- studenten
title: 'EveryOnePrint: Peage printen via app, web upload of mail'
---
Sinds kort heeft het ISC
[EveryOnePrint](http://www.ru.nl/facilitairbedrijf/printen/printen-vanaf-smartphone-tablet-etc-everyoneprint/)
beschikbaar gemaakt. Hiermee kan vanaf smartphone, tablet of elk ander
Internet-device geprint worden naar [Peage](/nl/howto/peage/), via
web-upload, app of mail. Jammer genoeg is ervoor gekozen om de
mail-optie alleen met een @ru.nl mail-adres te laten werken, niet met
een @science.ru.nl of ander mail-adres.
