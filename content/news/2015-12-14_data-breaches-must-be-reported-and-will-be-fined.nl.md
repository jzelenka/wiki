---
author: petervc
date: 2015-12-14 17:55:00
tags:
- medewerkers
- studenten
title: 'Datalekken: melden verplicht, boete volgt'
---
Per 1 januari 2016 zijn organisaties [bij wet verplicht datalekken te
melden](https://www.cbpweb.nl/nl/melden/meldplicht-datalekken). Als u
merkt dat u onterecht toegang heeft tot persoonsgegevens, dan hoort u
dat te melden, b.v. bij [C&CZ](/nl/howto/contact/) en/of
[CERT-RU](http://www.ru.nl/informatiebeveiliging/ru-professionals/cert-ru/).
Als u zelf werkt met persoonsgegevens, dan moet u veilig met die
gegevens omgaan. Onveilig omgaan met persoonsgegevens kan tot een hoge
boete leiden. De komende tijd zal extra aandacht geschonken worden aan
het hierbij betrekken van de RU-gemeenschap.
