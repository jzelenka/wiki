---
author: theon
date: 2008-10-10 15:35:00
title: Ricoh copiers/printers
---
Nu de [Ricoh copiers/printers](/nl/howto/copiers/) een half jaar in
gebruik zijn en er binnenkort een extra machine aangeschaft gaat worden,
vindt C&CZ het een goed idee om de wensen/problemen binnen FNWI te
inventariseren. Reacties zijn welkom bij de [C&CZ
helpdesk](/nl/howto/helpdesk/).
