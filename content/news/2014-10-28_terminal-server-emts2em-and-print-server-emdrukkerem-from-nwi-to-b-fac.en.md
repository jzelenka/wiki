---
author: wim
date: 2014-10-28 14:57:00
tags:
- medewerkers
- docenten
title: Terminal server <em>ts2</em> and print server <em>drukker</em> from NWI to
  B-FAC
---
Due to decommissioning of the *NWI.RU.NL*
Active Directory domain, on November 16 the terminal server
*ts2* and the printer server for departmental
printers *drukker* will be moved from
*NWI.RU.NL* to
*B-FAC.RU.NL*. This change is of consequence
for logging on to the terminal server *ts2* and
for connecting to printers from the print server
*drukker*.

If you are logging on to the terminal server
*ts2* using
*NWI\\loginname*, you will have to change this
to ***B-FAC**\\loginname*.
In the case of the print server *drukker*, if
connecting to printers using
*\\\\drukker.nwi.ru.nl\\printername*, you have
to change this to
*\\drukker.**b-fac**.ru.nl\\\\printername*.
The easiest way to do this is to remove and reconnect the printer from
the server.
