---
author: petervc
date: 2016-04-18 12:56:00
tags:
- studenten
- medewerkers
- docenten
title: 'Encrypt your laptop: don''t be the source of a data breach'
---
Loss and theft of laptops are a common problem. If a lost laptop
contains personal data this is considered a data breach that must be
reported and can possibly lead to a fine for Radboud University. To
prevent data breaches and fines, we strongly encourage you to [encypt
the hard disk of your
laptop](http://www.ru.nl/privacy/english/data/encrypt-laptop/). This can
be done easily, for Windows with
[BitLocker](https://en.wikipedia.org/wiki/BitLocker) or
[VeraCrypt](https://veracrypt.codeplex.com/), for Mac with
[FileVault](https://support.apple.com/en-us/HT204837) and for Linux with
[VeraCrypt](https://veracrypt.codeplex.com/). The [C&CZ
helpdesk](/en/howto/contact/) can give advice.
