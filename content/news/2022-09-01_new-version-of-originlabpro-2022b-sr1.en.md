---
author: petervc
date: 2022-09-01 14:16:00
tags:
- medewerkers
- studenten
title: New version of OriginLabPro (2022b SR1)
cover:
  image: img/2022/originlab2022b.png
---
A new version (2022b SR1) of [OriginLabPro](/en/howto/originlab/),
software for scientific graphing and data analysis, can be found on the
[Install](/en/howto/install-share/)-disk. See [the website of
OriginLab](https://www.originlab.com/2022b) for all info on the new
version. The license server supports this new version. Departments
within FNWI can request installation and license info from C&CZ, also
for standalone use. Installation on C&CZ managed PCs is being planned.
In case of questions contact [C&CZ](/en/howto/contact/).
