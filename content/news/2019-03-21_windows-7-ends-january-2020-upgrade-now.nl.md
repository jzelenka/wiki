---
author: wim
date: 2019-03-21 13:02:00
tags:
- medewerkers
- docenten
- studenten
title: 'Windows 7 stopt januari 2020: Upgrade nu\!'
---
Microsoft [stopt met de ondersteuning van
Windows7](https://support.microsoft.com/nl-nl/help/4057281/windows-7-support-will-end-on-january-14-2020)
begin 2020. Daarom raden we iedereen aan om voor die tijd alle pc’s met
Windows 7 een upgrade naar Windows 10 of een ander ondersteund operating
system te geven of deze pc’s te vervangen. Voor door C&CZ beheerde
afdelings-pc’s kan men met de [C&CZ helpdesk](/nl/howto/contact/) een
afspraak maken voor een upgrade naar Windows 10 of Ubuntu 16.04/18.04.
Ook voor andere pc’s kunnen zij hulp bieden.
