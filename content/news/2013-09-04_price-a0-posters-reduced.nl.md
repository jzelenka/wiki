---
author: stefan
date: 2013-09-04 10:17:00
tags:
- medewerkers
- studenten
title: Prijs A0 posters verlaagd
---
De prijs van een A0-posterprint van de [A0 foto
printer](/nl/howto/kamerbreed/) is verlaagd van 32 naar 24 euro, omdat de
kosten lager uitvallen dan vooraf verwacht.
