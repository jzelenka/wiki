---
author: pberens
date: 2011-01-26 16:25:00
tags:
- docenten
title: Blackboard traag? Gebruik Firefox of IE8
---
Het Grade Center kan traag wezen in IE 7, vooral met veel studenten in
een cursus. We werken eraan om Google Chrome beschikbaar te stellen op
beheerde werkplekken.
