---
author: fmelssen
date: 2014-09-24 12:10:00
tags:
- medewerkers
- studenten
title: 'Shakespeak: online stemmen en vragen stellen tijdens presentaties'
---
Met ingang van vandaag heeft de Radboud Universiteit een licentie om
[Shakespeak](http://www.shakespeak.com) te gebruiken. Shakespeak is een
plugin voor Powerpoint, waarmee u een vraag of een stemming kunt
toevoegen aan een dia. Students kunnen stemmen en opmerkingen (Internet,
Twitter en SMS) geven en de resultaten zijn direct beschikbaar in de
presentatie. Meer vragen: blackboard\@science.ru.nl
