---
author: wim
date: 2009-12-23 15:13:00
title: 'F-Secure beveiliging: nieuwe versie, minder belastend'
---
Versie 9 van de beveiligingssoftware [F-Secure](http://www.f-secure.com)
is [beschikbaar op Radboudnet](http://www.radboudnet.nl/fsecure). De
nieuwe versie is [minder
belastend](http://www.f-secure.com/en_EMEA/about-us/pressroom/news/2009/fs_news_20091130_01_eng.html)
voor de pc. De RU-licentie omvat ook thuisgebruik door medewerkers en
studenten. In januari 2010 zal C&CZ starten met het installeren van deze
versie op de [C&CZ-beheerde MS-Windows
werkplek](/nl/howto/windows-beheerde-werkplek/).
