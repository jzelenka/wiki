---
author: petervc
date: 2011-05-25 15:36:00
tags:
- studenten
- medewerkers
- docenten
title: F-Secure security software, free for home use too\!
---
Some employees and students may be unaware that the F-Secure security
software can be used freely, also at home. License codes for
[F-Secure](http://www.f-secure.com) can be found on
[Radboudnet](http://www.radboudnet.nl/fsecure).
