---
author: polman
date: 2008-06-03 13:34:00
title: New version of Mathematica (6.0.2.1)
---
A new version of [Mathematica](/en/howto/mathematica/) has been
installed. The CDs can be borrowed by departments that take part in the
license.
