---
author: petervc
date: 2008-10-14 18:10:00
title: McAfee licentie wordt opgevolgd door F-Secure
---
In het [RU ICT Overleg (RIO)](http://www.ru.nl/@750491/ru_ict-overleg/)
is besloten dat de RU voor 1 september 2009 overstapt van
[McAfee](http://www.mcafee.com) op [F-Secure](http://www.f-secure.com)
voor werkplekbeveiliging. De F-Secure Client Security software is op de
[install](http://www.cncz.science.ru.nl/software/installscience)
netwerkschijf te vinden. Licentiecodes kunnen bij C&CZ verkregen worden.
