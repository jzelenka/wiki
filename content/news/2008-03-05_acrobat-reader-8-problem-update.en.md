---
author: petervc
date: 2008-03-05 17:03:00
title: Acrobat Reader 8 problem update
---
Because Acrobat Reader 8 for MS-Windows produces PostScript that the HP
LaserJet4250/4350 [printers](/en/howto/printers-en-printen/) can’t
handle, C&CZ switched to using the (slower) PCL-drivers. Also
[FoxIt\_PDF\_Reader](/en/howto/foxit-pdf-reader/) has been copied to the
[S disk](/en/howto/s-schijf/) as an alternative for Acrobat Reader.
