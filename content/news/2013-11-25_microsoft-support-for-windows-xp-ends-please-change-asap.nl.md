---
author: petervc
date: 2013-11-25 17:42:00
tags:
- medewerkers
- docenten
title: Microsoft support voor Windows XP stopt, stap z.s.m. over\!
---
Vanaf 8 april 2014 wordt Windows XP [niet meer door Microsoft
ondersteund](http://windows.microsoft.com/nl-nl/windows/products/lifecycle).
C&CZ adviseert daarom met klem om voor die tijd overgestapt te zijn op
een ander besturingssysteem. Voor werkplekken binnen FNWI is dat b.v.
een door C&CZ beheerde [Windows 7](/nl/howto/windows-beheerde-werkplek/)
en/of Ubuntu 12.04 werkplek. Op een beheerde Windows 7 werkplek kan een
afdeling lokale beheersrechten (localadmin) krijgen. Ook is het mogelijk
om een PC of laptop door C&CZ te [laten
installeren](/nl/howto/windows-beheerde-werkplek/) om daarna het beheer
zelf uit te voeren. Neem s.v.p. [contact op met de C&CZ
helpdesk](/nl/howto/contact/) indien u hiervan gebruik wilt maken. N.b.
Als een PC niet voldoet aan de
[hardware-eisen](/nl/howto/windows-beheerde-werkplek/), dan zal men
nieuwe hardware aan moeten schaffen, soms alleen geheugen, soms een
nieuwe PC.
