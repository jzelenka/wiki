---
author: petervc
date: 2021-11-05 16:38:00
tags:
- studenten
- medewerkers
- docenten
title: 'Donatie aan vrije en open source software: Mozilla'
---
C&CZ maakt voor het merendeel van de services gebruik van [vrije
software](https://nl.wikipedia.org/wiki/Vrije_software) en
[opensourcesoftware](https://nl.wikipedia.org/wiki/Opensourcesoftware).
Daarom is al enige tijd geleden het idee ontstaan dat de
C&CZ-medewerkers elk jaar zouden stemmen welke projecten een donatie van
C&CZ zouden krijgen. Dit jaar is de keus gevallen op [de Mozilla
Foundation](https://foundation.mozilla.org/nl/). Veel mensen zullen de
webbrowser [Mozilla
Firefox](https://nl.wikipedia.org/wiki/Mozilla_Firefox) en de mailclient
[Mozilla Thunderbird](https://nl.wikipedia.org/wiki/Mozilla_Thunderbird)
kennen en er misschien elke dag urenlang gebruik van maken. Maar [de
Mozilla Foundation doet nog veel
meer](https://foundation.mozilla.org/nl/what-we-do/) om te proberen te
zorgen dat het internet een publieke hulpbron blijft, open en
toegankelijk voor iedereen.
