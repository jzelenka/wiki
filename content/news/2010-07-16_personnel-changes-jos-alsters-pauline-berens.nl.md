---
author: petervc
date: 2010-07-16 16:49:00
title: 'Personele wijzigingen: Jos Alsters, Pauline Berens'
---
Jos Alsters heeft per 15 juli 2010 C&CZ verlaten. Hij is gestart met een
eigen ICT-adviesbedrijf [Itsal.nl](http://www.itsal.nl), “IT Services
vanuit Open Standaarden”. C&CZ heeft de bedoeling om Jos nog wel voor
enkele opdrachten in te huren.

Per dezelfde datum is {{< author "pberens" >}} begonnen als medewerker
van zowel OWC als C&CZ, als ICT-beheerder/ontwikkelaar voor
FNWI-onderwijs.
