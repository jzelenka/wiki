---
title: NI LabVIEW 2022Q3 abonnementslicentie
author: petervc
date: 2023-02-14
tags:
- medewerkers
- studenten
cover:
  image: img/2023/labview.png
---
Er is een nieuwe versie van [LabVIEW](/nl/howto/labview/) van [NI.com](https://ni.com), versie 2022Q3.

NI.com heeft gemeld dat ze overstappen op een
[abonnementsmodel voor LabVIEW
licenties](https://www.ni.com/en/landing/subscription-software.html) vanaf versie 2022Q3.
C&CZ beheert een FlexNet-licentieserver voor LabVIEW 2022Q3, met een
beperkte aantal licenties en een deadline in april voor verlenging van de licentie.
Eerdere versies van LabVIEW werken standalone en onbeperkt in tijd.

De software is te vinden op de [C&CZ Install schijf](https://install.science.ru.nl/science/NI%20LabVIEW/). De licentieserver is:

         labview.science.ru.nl:28000

Om een licentie te kunnen uitchecken, moet C&CZ namen van pc's en/of gebruikers
invoeren in de licentieserver.
Voor pc's buiten science.ru.nl moet dit DNS-achtervoegsel worden toegevoegd
aan de IPv4 DNS-configuratie: science.ru.nl
