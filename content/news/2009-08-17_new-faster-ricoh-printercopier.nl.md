---
author: sommel
date: 2009-08-17 09:10:00
title: Nieuwe snellere Ricoh printer/copier
---
De [printer](/nl/howto/printers-en-printen/),
[scanner](/nl/tags/scanners/) en [kopieermachine](/nl/howto/copiers/)
Ricoh Aficio MP 5000 met de naam *watt* vervangen door een snellere z/w
machine Ricoh Aficio MP 7000 met de naam *volta*.

Het adresboek van *watt* is grotendeels overgezet op *volta*. Let op:
*volta* kan alleen maar in z/w scannen i.t.t. *watt*.
