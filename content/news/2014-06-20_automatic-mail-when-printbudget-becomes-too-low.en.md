---
author: polman
date: 2014-06-20 10:59:00
tags:
- medewerkers
- studenten
title: Automatic mail when printbudget becomes too low
---
When the [printbudget](/en/howto/printbudget/) is not sufficient, print
jobs are no longer processed. Because people do not always know quickly
that this is the cause, C&CZ will send an email when the printbudget has
become too low. For a personal budget the [mail](/en/howto/emailcodes/)
is sent to the personal email address. For a group budget the
[mail](/en/howto/emailcodes/) is sent to the owners of the budget group.
Send a mail to postmaster if you want to be warned earlier. Please
include the name of the budgetgroup and the value of the printbudget at
which you want to receive a mail.
