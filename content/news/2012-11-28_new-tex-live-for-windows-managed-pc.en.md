---
author: polman
date: 2012-11-28 16:52:00
tags:
- studenten
- medewerkers
- docenten
title: New TeX Live for Windows managed pc
---
On the S-disk [texlive2012](http://www.tug.org/texcollection) has been
installed, next to it we have added
[texmaker](http://www.xm1math.net/texmaker/), texmaker is a LaTeX
editor. To set the paths to the underlying programs in texmaker, one can
use a predefined settings file. Start S:\\texmaker\\texmaker.exe and
then choose Options menu -\> Settings File -\> “Replace the settings
file by a new one”, then select S:\\texmaker\\texmaker.ini and click OK.
You only have to do this once if you use a roaming profile, otherwise
you have to repeat this every time you login, see [Dhz](/en/howto/dhz/).
