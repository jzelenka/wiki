---
author: petervc
date: 2010-08-18 13:31:00
title: 'Bij vraag om wachtwoord via mail: niet reageren\!'
---
Met enige regelmaat krijgt men tegenwoordig
[phishing](http://nl.wikipedia.org/wiki/Phishing)-mail, waarin men
gevraagd wordt om loginnaam en wachtwoord door te geven. Graag wil C&CZ
\*nogmaals\* benadrukken dat het uiterst onverstandig is om voetstoots
alles te geloven wat in een brief of e-mail staat en dat \*geen enkele
officiële instantie\* u ooit per e-mail of telefonisch om uw wachtwoord
zal vragen, net zomin als om uw pincode van uw bankrekening. Wanneer een
argeloze gebruiker er toch in trapt, wordt zijn account meestal snel
daarna misbruikt om spam te sturen, waardoor onze mailservers op zwarte
lijsten komen, met overlast voor alle gebruikers als gevolg.
