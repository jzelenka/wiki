---
author: petervc
date: 2014-08-20 14:41:00
tags:
- studenten
title: Office 365 ProPlus almost free of charge for students
---
The [ISC](http://www.ru.nl/ictservicecentrum/) has [informed
us](http://www.ru.nl/ictservicecentrum/actueel/nieuws/berichten/studenten-office-365/)
that [Microsoft Office 365
ProPlus](http://office.microsoft.com/nl-nl/business/office-365-proplus-office-online-FX103213513.aspx)
is now available almost free of charge for RU students. There is only an
administration fee of € 2,99 per year by <http://www.surfspot.nl>
SURFspot.nl]. See [the SURFspot.nl Office365
page](https://www.surfspot.nl/officestudent) for all info.
