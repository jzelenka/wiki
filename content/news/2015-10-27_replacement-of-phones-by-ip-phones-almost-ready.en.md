---
author: bertw
date: 2015-10-27 12:27:00
tags:
- medewerkers
title: Replacement of phones by IP phones almost ready
---
The replacement of telephones by [IP
phones](/en/howto/categorie%3atelefonie/) is in full gear and will be
completed in a few weeks. Next to IP phones, departments also have the
option of [mobile corporate phones based on Vodafone Wireless
Office](/en/howto/categorie%3atelefonie/)
