---
author: bertw
date: 2017-10-23 11:32:00
tags:
- medewerkers
- studenten
title: Green telephones out of order for 15 minutes on November 22
---
The green emergency phones will be out of order on November 22 for 15
minutes between 11:30 and 12:00. During that time [T38 (Fax over
IP)](https://en.wikipedia.org/wiki/T.38) licences will be installed on
the calamity servers. All other phones will not be hindered by this
maintenance, so can be used in this period.
