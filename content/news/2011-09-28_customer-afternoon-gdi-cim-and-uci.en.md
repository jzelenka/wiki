---
author: caspar
date: 2011-09-28 13:38:00
tags:
- medewerkers
title: Customer afternoon GDI, CIM and UCI
---
The [IT User Service Group (GDI)](http://www.ru.nl/gdi), the department
[Concern Information Management (CIM)](http://www.ru.nl/cim) and the
[University IT Centre (UCI)](http://www.ru.nl/uci) organize a common
customer afternoon on Thursday October 20, 14.00-17.00 hours in the
Mohrmanzaal (Aula, Comeniuslaan 2). Project leaders and other RU-experts
will talk about recent developments w.r.t. digitization of documents on
campus, the inclusion of personal particulars in concern systems, the
e-mail- and calendar system Share and the facility management
information system Planon. You can register for the event (before
October 1) by sending an e-mail to the UCI secretariat or call them
(17999).
