---
title: 'C&CZ heidag dinsdag 28 maart'
author: petervc
date: 2023-03-26
tags:
- medewerkers
- studenten
cover:
  image: img/2023/heidag.jpg
---
Een heidag voor de afdeling C&CZ is gepland voor dinsdag 28 maart. Voor bereikbaarheid in geval van ernstige storingen wordt
gezorgd.
