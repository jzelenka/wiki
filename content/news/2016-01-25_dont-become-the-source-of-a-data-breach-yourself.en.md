---
author: petervc
date: 2016-01-25 18:18:00
tags:
- medewerkers
- studenten
title: Don't become the source of a data breach yourself\!
---
In order not to become the source of a data breach yourself, it is wise
to consider what personal information you have of others (colleagues,
students, …). If you do not really need this information, deleting it is
the safest option. Encrypting personal data, e.g. by using the free
software [7zip](http://www.7-zip.org/) (open source) or
[VeraCrypt](https://veracrypt.codeplex.com/), both available for
Windows/Linux/MacOSX, is a good choice too, especially if you have the
data on a mobile data carrier, such as a laptop or a USB stick. The
filesystem of smartphones/tablets/laptops can often be encrypted too
([Windows](http://www.tomsguide.com/us/encrypt-files-windows,news-18314.html),
[Android](http://www.howtogeek.com/141953/how-to-encrypt-your-android-phone-and-why-you-might-want-to/),
[iPad/iPhone](http://searchmobilecomputing.techtarget.com/tip/How-iOS-encryption-and-data-protection-work)).
Furthermore in case of theft or loss, these can often be remotely wiped
([Android](https://support.google.com/accounts/answer/6160500?hl=en),
[iPad/iPhone](https://support.apple.com/kb/PH2701?viewlocale=en_US)).
Reports of a data breach can be done at [C&CZ](/en/howto/contact/), but
also 24/7 at the [ILS helpdesk](/en/howto/contact/@ils-helpdesk).
