---
author: petervc
date: 2015-09-25 17:21:00
tags:
- medewerkers
- studenten
title: Do not fall for phone scam
---
Today we received messages from a few employees, that they had received
cold phone calls from persons offering help with computer problems, or
who told them that they could see everything the employee did on the
computer. \*Do not fall for this, do not answer their questions, do not
take the actions they request.\* In case of doubt, please refer them to
[C&CZ](/en/howto/contact/).
