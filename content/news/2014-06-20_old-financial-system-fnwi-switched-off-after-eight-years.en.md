---
author: petervc
date: 2014-06-20 12:25:00
tags:
- medewerkers
title: Old financial system FNWI switched off after eight years
---
With the introduction of [BASS](/en/howto/bass/) in 2006, it was decided
to not take over data from the financial system of the Faculty of
Science, Airbase. Because of tax liabilities and to make it possible to
view details of old purchase orders, C&CZ had to keep these systems
running until now. Because these systems have been phased out now, the
DNS domain name `dbf.kun.nl`, which still reminds of the Directorate
B-Faculties of the Catholic University of Nijmegen, can also be removed.
