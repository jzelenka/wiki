---
author: petervc
date: 2015-01-14 23:37:00
tags:
- studenten
- medewerkers
- docenten
title: Maple licentie tijdelijk verlengd
---
De licentie van [Maple](/nl/howto/maple/) is tijdelijk verlengd met een
5-gebruikers licentie. Dit geeft [IMAPP](http://www.ru.nl/imapp/nl/) en
[C&CZ](/) gelegenheid om te inventariseren welke andere afdelingen ook
financieel willen bijdragen aan het voortzetten van zo’n licentie. Per
licentie zou dit 900 € per jaar excl. BTW kosten. Belangstellenden
kunnen zich melden bij [C&CZ](/nl/howto/contact/).
