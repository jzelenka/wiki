---
author: caspar
date: 2009-12-14 10:24:00
title: Stroomstoring op zaal
---
Op 13 december 2009 om 18:50 is de stroomvoorziening van een aantal
servers op onze computerzaal uitgevallen. Het gaat om de toevoer naar de
servers \*achter\* de UPS, de batterij die tot doel heeft de servers
ongevoelig te maken voor storingen aan het elektriciteitsnet. Het UVB
onderzoekt de oorzaak. De storing trof een groot aantal file-servers,
web-servers (waaronder squirrel), een LDAP-server, enz. We hebben
inmiddels alle systemen omgeprikt naar een andere toevoer. De laatste is
omstreeks 10:20 uur vanmorgen gestart. Het komende uur zouden alle
services weer beschikbaar moeten komen.
