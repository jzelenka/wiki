---
author: petervc
date: 2017-02-21 17:14:00
tags:
- studenten
title: MobaXterm in alle RU studielandschappen
---
Het [ISC](http://www.ru.nl/isc) heeft in alle door hen beheerde
studielandschappen [MobaXterm](/nl/howto/ssh/) geïnstalleerd. Hiermee kan
men verbinding maken met [Linux servers](/nl/howto/hardware-servers/) en
daar dan ook X11-grafische programmatuur gebruiken. Dit was een verzoek
van studenten FNWI, omdat de [centrale
bibliotheek](http://www.ru.nl/ubn/) ruimere openingstijden heeft dan de
[Library of
Science](http://www.ru.nl/ubn/bibliotheek/locaties/library-of-science/).
