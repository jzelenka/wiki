---
author: bram
date: 2011-10-07 16:35:00
tags:
- medewerkers
title: BASS and single logon
---
For security reasons direct access to the concern application BASS used
to be blocked, even from within the RU internal network; one had to
logon to the Port Forwarder first which meant two logons in a row. With
the RU central firewall in place the first logon is not necessary
anymore (although still possible) for PC’s which are located behind the
firewall from a network topology point of view. A significant amount of
the PC’s in FNWI have already been relocated behind the firewall. On the
[BASS](/en/howto/bass/) page under *System Configuration* you can see the
current status of your PC and how to configure your PC to be able to
switch to single logon.
