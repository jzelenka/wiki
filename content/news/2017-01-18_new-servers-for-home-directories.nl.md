---
author: polman
date: 2017-01-18 17:37:00
tags:
- medewerkers
- studenten
title: Nieuwe servers voor home directory
---
De meer dan vier jaar oude home-directory/U:-schijf servers `pile` en
`bundle` worden vervangen door nieuwe servers met de namen `home1` en
`home2`. Het verhuizen van de home directories (U:-schijven) van
gebruikers gebeurt buiten werktijd. Met groepen gebruikers waarvoor dit
overlast zou geven (met Outlook .pst bestanden of
rekencluster-gebruikers) worden apart afspraken gemaakt wanneer dit zou
kunnen gebeuren. Bij de verhuizing wordt de minimale
[quota](/nl/howto/quota-bekijken/) voor de Science [home
directory/U:-schijf](/nl/howto/diskruimte/) verhoogd van 2 GB tot 5 GB.
Workstations/PC’s hebben mogelijk een extra reboot nodig als niet
succesvol kan worden ingelogd onder Ubuntu.
