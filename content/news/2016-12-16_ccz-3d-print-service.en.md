---
author: stefan
date: 2016-12-16 16:15:00
tags:
- studenten
- medewerkers
- docenten
title: C&CZ 3D print service
---
Several departments within the Faculty of Science have a 3D printer for
their own use. At the [TeCe](http://www.ru.nl/fnwi/technocentrum/) there
are two 3D printers with different techniques, to produce parts for
Science experiments. Now C&CZ starts a 3D print service for staff and
students of the Faculty of Science, primarily meant for use in
education. For more details, see the [Printers
page](/en/howto/printers-en-printen/).
