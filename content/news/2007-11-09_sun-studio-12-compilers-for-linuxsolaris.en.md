---
author: petervc
date: 2007-11-09 17:57:00
title: Sun Studio 12 compilers for Linux/Solaris
---
The newest version of the [Sun
Studio](http://developers.sun.com/sunstudio/) compilers and tools (C,
C++, Fortran) has been installed. They can be found in

/opt/sun/sunstudio12/ (Linux) or /vol/sunstudio12/SUNWspro
(Solaris).
