---
title: 'Maple nieuwe versie: Maple2023'
author: petervc
date: 2023-03-16
tags:
- medewerkers
- studenten
cover:
  image: img/2023/maple.png
---
De nieuwste versie van [Maple](/nl/howto/maple/), Maple2023, is voor
Windows/macOS/Linux te vinden op de
[Install](/nl/howto/install-share/)-netwerkschijf en is op door C&CZ
beheerde Linux-computers geïnstalleerd. Licentiecodes zijn bij C&CZ
helpdesk of postmaster te verkrijgen.

