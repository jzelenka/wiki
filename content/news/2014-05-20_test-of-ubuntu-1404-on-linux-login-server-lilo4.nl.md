---
author: visser
date: 2014-05-20 17:02:00
tags:
- medewerkers
- studenten
title: Test van Ubuntu 14.04 met Linux loginserver lilo4
---
Als test voor de nieuwe versie van Ubuntu, 14.04 LTS, hebben we een
nieuwe loginserver beschikbaar, onder de naam `lilo4.science.ru.nl`.
Over enige tijd zal deze de vijf jaar oude loginserver `lilo2`
vervangen. De naam `lilo`, die altijd naar de nieuwste/snelste
loginserver wijst, zal pas dan omgezet worden van de twee jaar oude
`lilo3` naar de nieuwe `lilo4`. De nieuwe `lilo4` is een Dell PowerEdge
R420 met 2 Xeon E5 2430L v2 6-core processoren en 32 GB geheugen. Met
hyperthreading aan lijkt dit dus een 24-processor machine. Voor wie de
vingerafdruk van de publieke RSA-sleutel wil controleren voor het
Science wachtwoord aan de nieuwe server te geven:
`aa:ad:c0:2e:60:9d:d3:cd:ca:a4:59:7d:d0:d8:4c:68`. Alle problemen met
deze nieuwe versie van Ubuntu Linux [graag melden](/nl/howto/contact/).
