---
author: petervc
date: '2022-09-09T11:01:58Z'
keywords: []
lang: en
tags:
- software
title: Microsoft windows
wiki_id: '160'
---
## Windows software for employees and students of the Faculty of Science

### To install on your own Windows PC

-   [Install-share](/en/howto/install-share/): a network drive with
    installable software.
-   The [ILS](http://www.ru.nl/ils) has a
    [<https://www.ru.nl/ict/medewerkers/software/>

`page on software] and maintains a `[`list`](https://gosoftware.hosting.ru.nl/)` of RU licensed software.`

-   Surfspot.nl: All kinds of licensed software (which can also be used
    at home) can be bought at [Surfspot](http://www.surfspot.nl/) when
    one logs in with employee/student number and
    [RU-wachtwoord](http://www.ru.nl/wachtwoord). Departments can order
    these software packages via the [Purchase
    Department](http://www.ru.nl/purchasing). The
    [ILS](http://www.ru.nl/ils/) coordinates most RU-wide software
    licenses, mainly through
    [SURFdiensten](http://www.surfdiensten.nl/).
    -   The upgrade of Windows 10 to the Educational version with e.g.
        BitLocker encryption is recommended.
-   The [ChemBioOffice](/en/howto/chembiooffice/) software must be
    installed directly from the site of the supplier.
-   C&CZ lends (to students and staff of the Faculty of Science) DVD/CDs
    of [Microsoft Windows](/en/howto/microsoft-windows/),
    [Unix/Linux](/en/howto/unix/) or [Apple MacOS](/en/howto/macintosh/)
    software with a RU or FNWI license agreement.
-   Software for the RU Concern systems ([BASS-Finlog,
    HRM](/en/howto/bass/), …).

### Software available on C&CZ managed Windows-computers

It is not possible to exactly state what software is installed on a C&CZ
managed Windows-computer. This greatly depends on the purpose of the PC
or the department that owns the PC. Licensed software for which no
campus license exists, is often distributed to only a small number of
PCs.

On the Windows PC’s in the [computer labs](/en/howto/terminalkamers/) a
[T-disk](/en/howto/t-schijf/) with course software is attached from the
network.

Below the software packages that are available.

`<include src="/var/www1/wiki/live/htdocs/cncz/include/software.wiki" nocache nopre wikitext />`{=html}
