---
author: petervc
date: '2013-04-18T09:56:57Z'
keywords: []
lang: nl
tags:
- software
title: Van Dale
wiki_id: '653'
---
## Van Dale woordenboeken

Op alle oude [door C&CZ beheerde MS-Windows
PCs](/nl/howto/windows-beheerde-werkplek/) \*met Windows XP\* zijn de
volgende [van Dale](http://www.vandale.nl) woordenboeken beschikbaar:

-   Groot Woordenboek van de Nederlandse taal
-   Nederlands-Duits, Duits-Nederlands
-   Nederlands-Engels, Engels-Nederlands
-   Nederlands-Frans, Frans-Nederlands

Deze zijn automatisch vanuit MS-Word te gebruiken.

Op niet door C&CZ beheerde windows pc’s kan men deze woordenboeken
gebruiken door de [S schijf](/nl/howto/s-schijf/) aan te koppelen en dan
de clientinstallatie per woordenboek uit te voeren. Voor Duits is dat
bijvoorbeeld S:\\VanDale\\Grote woordenboeken\\Duits\\client\\setup.exe
en voor het Groot Woordenboek der Nederlandse taal is dat
S:\\VanDale\\Clients\\Setup.exe.

-   Bekende problemen: Helaas treedt er zo nu en dan een vervelend
    probleem op met Word. De woordenboeken worden in Word toegevoegd
    d.m.v. template bestanden in C:\\Documents and
    Settings\\username\\Application Data\\Microsoft\\Word\\STARTUP\\.
    Bij het opstarten van Word maakt deze in deze directory backup
    bestanden aan van de template bestanden (deze beginnen met
    \~$) en deze worden als Word normaal afsluit ook weer opgeruimd. Helaas gaat dat niet altijd goed en blijven deze \~$
    bestanden in de STARTUP directory staan. Bij een volgende start van
    Word wil deze ook deze \~\$ bestanden inlezen wat tot vervelende
    foutmeldingen leidt. Deze zijn onschuldig, Word blijft normaal
    werken, maar irritant. Op dit moment is dit alleen op te lossen door
    deze bestanden zelf te verwijderen.

-   Licentie: Nadat eerder al enkele faculteiten (waaronder FNWI)
    beperkte licenties afgesloten hadden, werd juli 2008 meegedeeld dat
    de [UB](http://www.ru.nl/ub) een licentie heeft voor de
    woordenboeken Nederlands, Engels, Frans, en Duits.

Toegang via een web browser: [Vandale
woordenboeken](http://pro.vandale.nl/) Dit werkt alleen vanaf de Campus,
via een vpn verbinding of via de proxyserver van de UB [Vandale
Woordenboeken](http://proxy.ubn.kun.nl:8080/login?url=http://pro.vandale.nl)
