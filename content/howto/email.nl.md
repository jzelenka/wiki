---
title: Email
author: bram
date: 2022-10-14
keywords:
- email
tags:
- medewerkers
- studenten
- email
cover:
  image: img/2022/email.png
ShowToc: true
TocOpen: true
---
{{< notice tip >}}

# Webmail

Gebruik een van de  webmailers als je snel vanuit je browser toegang wilt hebben tot je email:

## https://roundcube.science.ru.nl/

## https://rainloop.science.ru.nl/

{{< /notice >}}


# Mailinstellingen

Bij het instellen van de meeste mailprogramma's, volstaat het om alleen je mailadres in te vullen. Zo ziet je Science mailadres eruit:

> `I.Achternaam@science.ru.nl`

Alle technische instellingen worden vervolgens automatisch voor je ingevuld.
Mocht dat niet lukken, of als het nodig is om eea zelf in te vullen, gebruik dan de instellingen van onderstaande tabel:


## Binnenkomende mail

| item           | instelling           | toelichting                                                                                   |
| -------------- | -------------------- | --------------------------------------------------------------------------------------------- |
| mailprotocol   | `IMAP`               | Mailprotocol waarbij je mail in princiepe op de server bewaard blijft.                        |
| servernaam     | `post.science.ru.nl` | Server voor het lezen van mail. Hier staan je mail-mappen.                                    |
| poort          | `993`                |                                                                                               |
| versleuteling  | `TLS`                | Deze versleuteling wordt gebruikt bij de communicatie van je email-programma en de mailserver.|
| gebruikersnaam | `scienceloginnaam`   | Gebruik hiervoor je [Science account](/nl/howto/login).                                        |


## Uitgaande mail

| item           | instelling           | toelichting                                             |
| -------------- | -------------------- | ------------------------------------------------------- |
| servernaam     | `smtp.science.ru.nl` | Deze server wordt gebruikt bij het versturen van email. |
| poort          | `587`                |                                                         |
| versleuteling  | `TLS`                |                                                         |
| gebruikersnaam | `scienceloginnaam`   | Gebruik hiervoor je [Science account](/nl/howto/login).  |


## Adresboek

| item            | instelling
| --------------- | --------------------
| adresboekserver | `ldap.science.ru.nl`
| poort           | 389
| basedn          | `DN: o=addressbook`


# Science mail eigenschapen

> Lees op de [DHZ pagina](/nl/howto/dhz) wat je zoal voor je mail kunt instellen.

- C&CZ gebruikt open protocollen voor de mail. Dat betekent dat je mail kunt checken met een programma naar je eigen voorkeur.
- Op elk mailbericht wordt bij binnenkomst een eenvoudig [antivirus-](/nl/howto/email-antivirus/) en [antispam-filter](/nl/howto/email-spam) toegepast.
- Mailboxen kunnen tussen gebruikers [gedeeld](#mailboxen-delen) worden.
- Ook [labels](/nl/howto/thunderbird-tags/) voor mail kunnen met andere gebruikers gedeeld worden.
- Met behulp van [Sieve](/nl/howto/email-sieve/) kunnen mailberichten al op de server gefilterd worden.
- Er zijn twee mogelijkheden om mailadreslijsten op te zetten: [mailadres-lijsten](#mailing-lijsten) en [mailman](/nl/howto/mailman-lists).
- De opslagruimte voor je mail is standaard `5 GB`. Dit is op [verzoek](/nl/howto/contact) uit te breiden.

{{< notice tip >}}

De mailmap "verzonden" wordt vaak over het hoofd wordt gezien bij het opruimen van email. Zoek hier naar de mails met de grootste attachments.

{{< /notice >}}


## Mailboxen delen

Dit kan eenvoudig ingesteld worden in [Roundcube webmail](http://roundcube.science.ru.nl), linksonder bij het tandwiel-icon, via `Mapopties` en dubbelklikken op een map, maar ook in een mailprogramma als Kmail. Degene die de map mag gebruiken, moet zich vaak nog wel abonneren op die map.
Wanneer een gebruiker een mail leest, wordt die niet automatisch voor alle gebruikers gemarkeerd als gelezen.


## Mailing lijsten

Er zijn eenvoudige lijsten van mail-adressen mogelijk, die men aan kan
vragen bij [Postmaster](/nl/howto/contact) en die daarna door de eigenaren zelf bijgehouden
kan worden op [de Doe-Het-Zelf website](/en/howto/dhz),


## Alternatieve mailinstellingen

- Naast het `IMAP`-protocol is het ook mogelijk om het `POP3`-protocol te gebruiken. Als je nog nooit van het `POP3` protocol hebt gehoord, gebruik het niet! Als je het toch wilt gebruiken: het `POP3`-poortnummer is `995`.
- Vanuit het netwerk van het Huygensgebouw is het niet verplicht om je inloggegevens te gebruiken voor het versturen van mail. Gebruik dan poort `25` van de smtp server.


# `@ru.nl` mail

{{< notice tip >}}

Het webmailadres voor @ru.nl mail is: https://mail.ru.nl

{{< /notice >}}

Elke student of medewerker van de Radboud Universiteit heeft ook een [centraal `@ru.nl` mail-adres](http://www.ru.nl/ict/medewerkers/mail-agenda/) in de vorm:

> `Voornaam.Achternaam@ru.nl`


## @ru.nl mail doorsturen

Het is mogelijk om mail naar dit adres door te laten sturen naar een ander adres. Het instellen hiervan gaat als volgt in het werk:
- log in op https://mail.ru.nl
- bij instellingen, ga naar "Options" > "Organize email"
- klik op de `+`
- kies "Create a rule for arriving messages"
- Kies voor "redirect all incoming" naar een ander emailadres
