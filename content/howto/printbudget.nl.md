---
author: petervc
date: '2016-08-17T11:34:41Z'
keywords: []
lang: nl
tags:
- studenten
- medewerkers
- contactpersonen
title: Printbudget
wiki_id: '33'
---
Om gebruik te kunnen maken van de [C&CZ
printers](/nl/howto/printers-en-printen/) moet de loginnaam voorkomen in
een budgetgroep voor de betreffende groep van printers. Ook moet het
budget van de budgetgroep voldoende positief zijn om 1 bladzijde af te
mogen drukken. De kosten worden automatisch van het budget afgeboekt. Op
de [Doe Het Zelf website](http://dhz.science.ru.nl) kan men de
budgetgroep kiezen waarvan de kosten standaard afgeschreven zullen
worden. Daar is ook een overzicht te zien van het tegoed op de budgetten
waarvan men gebruik kan maken en van de twintig laatst afgedrukte
printopdrachten.

Om een nieuwe budgetgroep te laten maken dient men bij C&CZ
[systeembeheer](/nl/howto/systeemontwikkeling/) langs te
gaan.

C&CZ stuurt een mail wanneer het printbudget te laag wordt. Bij een
persoonlijk budget wordt [de mail](/nl/howto/emailcodes/) naar het
persoonlijke mail-adres gestuurd. Bij een groepsbudget wordt [de
mail](/nl/howto/emailcodes/) naar de eigenaren van de budgetgroep
gestuurd. Stuur een mail naar postmaster als u al eerder gewaarschuwd
wilt worden. Vermeld hierbij de naam van de budgetgroep en de hoogte van
het printbudget waarbij u een mail wilt krijgen.

Geld storten op een bestaande budgetgroep kan op een paar manieren:

-   voor groepen met een budgetnummer door een mail te sturen naar
    postmaster\@science.ru.nl onder vermelding van het bedrag, het
    budgetnummer en de budgetgroep.
-   eventueel contant bij C&CZ
    [systeembeheer](/nl/howto/systeemontwikkeling/).
