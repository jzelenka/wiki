---
author: remcoa
date: '2008-01-31T18:20:02Z'
keywords: []
lang: nl
tags: []
title: Firefox Add-ons
wiki_id: '663'
---
-   Tab Mix Plus
    -   Adblock Plus
    -   Flashblock
    -   Mouse Gestures
    -   MultiSidebar
    -   Vertigo
    -   PDF Download
    -   del.icio.us Complete
    -   Google Browser Sync

-   Digger
-   Dom Inspector
-   Firebug
-   Live HTTP Headers
-   Tamper Data
-   User Agent Switcher
-   View Cookies
-   Web Developer
-   YSlow

Meer add-ons zijn te vinden op [addons.mozilla.org](https://addons.mozilla.org/en-US/firefox)
