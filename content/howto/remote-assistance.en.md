---
title: Remote Assistance
author: sioo
date: 2023-01-27
keywords: []
tags: []
cover:
  image: img/2023/remote-assistance.png
ShowToc: false
TocOpen: false
---
C&CZ helpdesk can assist you by taking over your screen using *TeamViewer*.
In case you are requested to download TeamViewer, use the following link:

> https://get.teamviewer.com/ischelpdesk