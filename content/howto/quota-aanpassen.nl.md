---
author: remcoa
date: '2019-04-12T11:28:56Z'
keywords: []
lang: nl
tags:
- contactpersonen
title: Quota aanpassen
wiki_id: '7'
---
## Quota aanpassen

Alleen [afdelingscontactpersonen](/nl/tags/eerstelijns) kunnen
wijzigingen van quota aanvragen. Omdat het in principe niet mogelijk is
om te verifiëren of degene die, via een WWW formulier zoals dit, een
mailtje stuurt ook daadwerkelijk is wie hij of zij beweert te zijn, zal
een aanvraag via dit formulier altijd eerst gecontroleerd worden en dus
langer duren. De snelste manier om een quotaverandering aan te vragen is
daarom door het rechtstreeks sturen van een mailtje naar
postmaster\@science.ru.nl. Dit formulier kan daarbij als voor beeld
gebruikt worden.

------------------------------------------------------------------------

Gegevens van de aanvrager:

|                           \| \|
| ————————: \| \|
|              ’‘’naam:’’’ \| \|
| ’‘’afdeling/vakgroep:’’’ \| \|
|        ’‘’emailadres:’’’ \| \|

Gegevens van de aanvraag:

|                            \| \|
| ————————-: \| \|
|          ’‘’loginnaam:’’’ \| \|
| ’‘’disk-quota - zacht:’’’ \| \|
| ’‘’file-quota - zacht:’’’ \| \|

Eventuele opmerkingen:
