---
author: petervc
date: '2015-10-14T13:11:38Z'
keywords: []
lang: nl
tags: ["software"]
title: Software cluster
wiki_id: '512'
---
## Cluster software

{{< notice warning >}}
All clusternodes have been moved to
[SLURM](/nl/howto/slurm/). 
{{< /notice >}}


