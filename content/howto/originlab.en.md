---
author: petervc
date: '2022-11-08T16:37:04Z'
keywords: []
lang: en
tags:
- software
title: OriginLab
wiki_id: '943'
---
### OriginLab

Origin by [Originlab](http://www.originlab.com/) is scientific graphing
and data analysis software.

### License

C&CZ acquired a network license for FNWI for the use of OriginPro. One
can get the software and installation/licensing keys of OriginPro from
[C&CZ system administration](/en/howto/systeemontwikkeling/).

The network license can only be used by computers on the Internet. For
standalone use, there are home-use licenses included. The current
contract ends August 2023.

If you get a message that the license will expire soon and you are using
a standalone version you can update the license by clicking on Activate
in the Help menu.

`The use of OriginPro is logged on the license server. `

`OriginPro is available on the MS-Windows machines managed by C&CZ.`
