---
author: petervc
date: '2022-03-18T13:43:09Z'
keywords: []
lang: en
tags:
- software
title: LabVIEW
wiki_id: '1084'
---
C&CZ administers the licenses for the use of
[LabVIEW](https://en.wikipedia.org/wiki/LabVIEW) within FNWI. LabVIEW by
[NI](https://www.ni.com/) is a graphical programming environment
engineers use to develop automated research, validation, and production
test systems.

From version 2022Q3 LabVIEW no longer works standalone and unlimited
in terms of time. C&CZ manages a LabVIEW license server with a small number of
licenses that have to be renewed every year. Users of this license must
be registered on the license server.
