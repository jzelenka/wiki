---
author: petervc
date: '2022-06-24T12:54:04Z'
keywords: []
lang: nl
tags:
- telefonie
title: FNWI Telefoon en E-mail gids
wiki_id: '174'
aliases:
- /nl/howto/fnwi-telefoon-en-e-mail-gids
---
## Telefoon- en emailgids

{{< telgids >}}

U kunt in bovenstaande gids zoeken op (een gedeelte van) naam,
telefoonnummer, kamer, afdeling etc. Als u van buiten de campus belt, draait u eerst `024 36` en dan het gevonden interne telefoonnummer!

## RBS
Afdelingen binnen FNWI kunnen zelf via [RBS](http://www.radboudnet.nl/rbs/) hun gidsvermeldingen invoeren of aanpassen. Alle Telefooncontactpersoon beschikken over een login om rechtstreeks in RBS gidsgegevens aan te passen.

De telefooncontactpersoon ontvangt ook de telefoonkostenspecificaties, kan mutaties en abonnementen aanvragen of opheffen en ontvangt, als daar aanleiding toe is van ILS informatie over de telefonievoorzieningen binnen de RU (bijvoorbeeld tariefswijzigingen, storingen, onderhoud).


[ILS](http://www.ru.nl/ils) beheert de telefooncentrale. Denk hierbij
aan aan/afsluiten van telefoons. Ook verzorgen zij de doorbelasting van
de gespreks- en abonnementskosten.


## Fout in de gids gevonden?
Neem dan contact op met het secretariaat van uw afdeling.
- Hier vindt u een [lijst van telefooncontactpersonen](https://www.ru.nl/ict/medewerkers/telefonie/).
- Voor vragen kunt u mailen naar [Postmaster](/nl/howto/contact).
- FNWI telefooncontactpersonen passen de gidsgegevens van hun afdeling aan in [RBS](https://rbs.ru.nl).
