---
author: petervc
date: '2017-08-17T13:19:21Z'
keywords: []
lang: en
tags:
- medewerkers
title: Medewerkers huisstijl
wiki_id: '81'
---
## RU Corporate Identity Information

General information can be found at the [RU Corporate Identity
site](http://www.ru.nl/huisstijl/).

In September 2004, a new corporate identity has been introduced in which
the name and logo of the university changed, and therefore also the
layout of all printed material. For MS-Word, formats and manuals have
been developed centrally. For LaTeX, C&CZ developed a [RU Corporate
Identity letter layout](/en/howto/medewerkers-latexbrief/). Within ICIS,
templates for [RU Corporate Identity LaTeX
presentations](http://www.ru.nl/icis/research/presentation/) have been
developed, that are also available on the C&CZ Linux systems in
/usr/local/share/texmf/tex/latex/rustyle/.

### RU Letter Layout for Word, new version (4.1.) of December, 2005

Attach the network disk: \\\\software-srv\\software in the following
way:

Windows Explorer -\> Tools -\> Map Network Drive. Drive: S (or some
other, free drive letter). Folder: \\\\software-srv\\software. Check
“Reconnect at logon”.

Open the network disk that
you just attached.

In the folder RU-Huisstijl you can find three manuals supplied by the
ISC (formerly UCI): An Installation Manual, a User Manual and a
Conversion Manual. The last one is only needed if one has worked with an
earlier version of the templates.

Carefully read both (Dutch) manuals. The User Manual specifically
expands on the different templates that are offered. Below is a short
explanation how to make these templates work for yourself.

In the folder RU-Huisstijlyou can find 9 .dot files and two .ini files.

`RU_Badge.dot            RU_Etiket_Large.dot     RU_Settings.ini`\
`RU_Brief.dot            RU_Fax.dot              RU_VisiteKaartje.dot`\
`RU_COG.dot              RU_Huisstijl.dot        RU_WithCompliments.dot`\
`RU_Brief_MM.dot         RU_User_Settings.ini`

To use the Corporate Identity for a specific department, one has to copy
the files to a place where the user can write them. This could be one’s
own home-directory (H-disk), a directory on a network disk that can be
written by somebody from the department or a directory on the local PC.

Just once, one has to do the following (see the Installation Manual, Per
User Settings, page 3): Open an empty file in Word, select in the
drop-down menu: Tools -\> Templates and Add-Ins -\> Add. Browse to the
directory where RU\_Huisstijl.dot has been copied to. Select the file
RU\_Huisstijl.dot.

**Word 2010:** Open the ‘File’ menu -\> Options -\> Add-Ins. Select
*Word add-ins* from the drop-downmenu and click ‘Start’. In the pop-up
window ‘Templates and add-ins’, click ‘Add’; browse to the directory
where RU\_Huisstijl.dot has been copied to. Select the file
RU\_Huisstijl.dot.

After that one has to exit Word. This makes sure that the templates can
find the file RU\_Settings.ini, in which address information etc. is
stored. Is is essential that the file RU\_Settings.ini can be found in
the same folder as where RU\_Huisstijl.dot can be found.

If the latest step doesn’t work, the most probable cause is that the
template Security settings does not allow unsigned macros. In that case
open in Word Tools -\> Macro -\> Security and choose Average. Then every
time a template file is used, a dialog box will appear that asks
explicitly if you want to turn on the macros or not.

New in this version is that the macro’s have been signed with a
certificate. That makes it possible to add once and for all this
certificate to the list of trusted certificates, after which the Macro
security can be switched back to high, this also gets rid of the always
appearing dialog box. To add the certificate: in the dialog box click
“Details” and in the next dialog click “View Certificate” and after that
“Install Certificate”. After that click a few times “Next” in the wizard
“Import Certificate”. Finally check in Macro security “Always trust this
source”. After that Macro security can be switched back to high.

Next do Step 4 op the Installation Manual: open RU\_COG.dot (**Word
2010:** click on ‘Add-ins’ in the upper right) and choose from the menu
item RU Huisstijl the option Edit User Ini Folder, enter the complete
path to the directory where you copied the files in the dialog, press
‘Apply’ and then close this dialog. After this you can fill in the
address of the department in question, these will be saved into the file
RU\_User\_Settings.ini in the directory you have just entered.

For the [RU letter layout with
Office2007](/en/howto/huisstijl-office2007/) there is a seperate page.

From this point onwards you can use the User Manual. The personal
profiles that you make are saved in RU\_User\_Settings.ini, so be
careful with this file.
