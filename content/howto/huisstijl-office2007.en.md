---
author: petervc
date: '2011-07-05T10:31:40Z'
keywords: []
lang: en
tags: []
title: Huisstijl Office2007
wiki_id: '775'
---
### Corporate Identity in Office 2007

To be able to use the RU letter layout in Office 2007 one has to add
once the ru\_huisstijl template.

-   Open Word 2007
-   Click on the Microsoft Office button (topleft) and click on Options
    for Word (bottom of window)
-   Select Advanced
-   Below General (scroll down to the bottom) select File Locations
-   Click on Workgroup templates and then select Modify and browse to
    the location of the RU letter folder

of your department and select OK

-   Click on the Microsoft Office button (topleft) and click on Options
    for Word (bottom of window)
-   Select Add-Ins
-   Select Templates from the Manage dropdown menu en select Go
-   Select Add and browse once more to the location of the RU letter
    folder

of your department and select ru\_huisstijl.dot

-   Select OK and once more OK

The template has a date problem, whenever a letter contains more than
one page the date on the second and subsequent pages is not
automatically set to the date chosen in the edit page. A workaround is
to open the Print preview page, this sets the date correctly.
