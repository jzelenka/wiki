---
author: petervc
date: '2018-01-10T15:14:49Z'
keywords: []
noage: true
lang: en
tags:
- software
title: Install share
wiki_id: '811'
---
Most campus license software that can be freely used by students and staff of the Science faculty, can be found in the `install` network share.

{{< notice info >}}
Consult [this page](../storage/#network-share-paths) on how to connect to a network share.
{{< /notice >}}

It is often possible to install this software directly from the install share. Often a license key is needed, it can
be requested by mail to [postmaster](../contact). Every software package has its own license requirements. Sometimes it can only be used on university owned
computers, sometimes it can even be used by students at home.
