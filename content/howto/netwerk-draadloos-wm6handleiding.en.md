---
author: mkup
date: '2013-10-09T12:52:40Z'
keywords: []
lang: en
tags: []
title: Netwerk draadloos WM6handleiding
wiki_id: '643'
---
## Wireless Settings for Windows Mobile

1.  **Windows Settings**:

    \* If you haven’t changed your password for a long time, do so now.
    For “Science” go to [Do-It-Yourself](http://diy.science.ru.nl), for
    “ru-wlan” to [the RU-password website](http://www.ru.nl/wachtwoord).

    -   Click “Start” → “Settings”.

        {{< figure src="/img/old/wm5-uk-wifi-screen1.jpg" title="Start-Settings" >}}

    -   A window named “Settings” appears. In this window, click on the
        tab “Connections” and within that tab on the icon for Wi-Fi. For
        an HP iPAQ this is called “iPAQ Wireless”.

        {{< figure src="/img/old/wm5-uk-wifi-screen2.jpg" title="Settings" >}}

    -   In the next window, click on Wi-Fi settings. For an HP iPAQ the
        window looks like:

        {{< figure src="/img/old/wm5-uk-wifi-screen3.jpg" title="iPAQ Wireless" >}}

    -   If you are within reach of the “Science” or “ru-wlan” wireless
        networks, click long on the “Science” or “ru-wlan” network and
        click “Turn off Wi-Fi”.

    -   Choose “Add New …”.

    -   In the next window, subtitled “Configure Wireless Networks”,
        fill in the name of the network, “Science” or “ru-wlan”.

        {{< figure src="/img/old/wm5-uk-wifi-screen4.jpg" title="Configure Wireless Networks" >}}

    -   In the next screen, click the middle tab on the bottom of the
        screen “Network Key”.

        [Configure Wireless Networks General ](WM5-UK-wifi-screen5.jpg
        “Configure Wireless Networks General”)

    -   In the next screen, titled “Configure Network Authentication”,
        pick WPA/TKIP and click on the right tab on the bottom of the
        screen “802.1x”.

        {{< figure src="/img/old/wm5-uk-wifi-screen6.jpg" title="Configure Network Authentication Network Key" >}}

    -   Pick as EAP type “PEAP” in the next screen.

        {{< figure src="/img/old/wm5-uk-wifi-screen7.jpg" title="Configure Network Authentication 802.1x" >}}

    -   Probably the PEAP properties cannot be changed. This is no
        problem, since the PEAP-settings then will be PEAPv0/MSCHAPv2,
        which is correct for our network. If you try to change the
        settings, you get a warning.:

        {{< figure src="/img/old/wm5-uk-wifi-screen8.jpg" title="Warning" >}}

    -   Clicking ’OK" will give you, after you turned Wi-Fi on again if
        necessary, a logon screen, in which you have to type your
        loginname/password (for the “Science” network) or your
        U-number/S-number/RU-password to get connected to the network.

        {{< figure src="/img/old/wm5-uk-wifi-screen9.jpg" title="User Logon" >}}
