---
author: petervc
date: '2017-10-05T15:43:27Z'
keywords: []
lang: en
tags: []
title: Android
wiki_id: '949'
---
## Network

### Wi-Fi Eduroam

The Eduroam wireless network is available in all campus buildings. It is
also available in other universities, see
[Eduroam.nl](http://www.eduroam.nl).

Browse to <http://www.ru.nl/wireless> for all info and configuration
manuals.

-   When asked for username and password, this can be one of the
    following combinations:

{\| style=“border-collapse: separate; border-spacing: 0;
background-color:\#ffffee;” cellpadding=“4” cellspacing=“0” border=“1”
\| \| Username \| Password \|- \| choice 1 \| *u-number\@ru.nl* \|
*RU-password (as is used for Radboudnet)* \|- \| choice 2 \|
*sciencelogin*\@science.ru.nl \| *Science-password (as is used on the
[Do-It-Yourself site](http://dhz.science.ru.nl)* \|}\
You can check the IP address by visiting www.whatsmyip.org it will show:

-   IP address: an address in the range 145.116.128.0 - 145.116.191.255

## VPN

To be allowed to access network disks, it is usually sufficient to be
connected to the RU campus network, wired or wireless via Eduroam. Some
network shares are stored in an extra secure location, these can only be
accessed from certain workstations, but also through the Science VPN.
The next steps are needed to install the Science VPN.

### Science VPN

See [Vpn](/en/howto/vpn/)

### RU VPN

The [RU VPN](http://www.ru.nl/ict-uk/staff/working-off-campus/vpn/) can
be used with an Android client.

## Access to a network share

See the [page on network disks](/en/howto/netwerkschijf/) for the apps
needed and the naming of network disks.

## Mail

Sorry, only the Dutch version of this text is available.

### Science mail

Sorry, only the Dutch version of this text is available.

### Exchange (ISC)

Sorry, only the Dutch version of this text is available.
