---
author: petervc
date: '2022-09-09T10:58:09Z'
keywords: []
lang: nl
tags:
- software
title: Macintosh
wiki_id: '146'
---
## OS X software

### Om zelf te installeren op een zelfbeheerde Apple Mac computer

-   [Install-share](/nl/howto/install-share/): een netwerkschijf met
    installeerbare software.
-   Het [ILS](http://www.ru.nl/ils) houdt een [lijst van
    RU-softwarelicenties](http://gosoftware.hosting.ru.nl/) bij.
-   Surfspot.nl: Allerlei licentie software (die ook thuis gebruik mag
    worden) is bij [Surfspot](http://www.surfspot.nl/) te koop wanneer
    men met personeels- of studentnummer en
    [RU-wachtwoord](http://www.ru.nl/wachtwoord) inlogt. Afdelingen
    kunnen deze software via de normale procedure
    ([BASS](https://www.ru.nl/cif/cfa/bass-finlog/)) bestellen. Meestal
    betreft het software waarvoor
    [SURFdiensten](http://www.surfdiensten.nl/) voor
    onderwijs-instellingen een
    [SURFlicentie-overeenkomst](http://www.surfdiensten.nl/info/licenties/index.html)
    afgesloten heeft, die door [SLIM](http://www.slim.nl/) als “Slimme
    ROM” uitgebracht wordt.
-   F-Secure: Licentiecodes voor het beveiligingspakket
    [F-Secure](http://www.f-secure.com) zijn te vinden op
    [Radboudnet](http://www.radboudnet.nl/fsecure).
-   [ChemBioOffice](/nl/howto/chembiooffice/) moet men installeren vanaf
    de site van de leverancier.
