---
author: petervc
date: '2018-11-27T09:41:23Z'
keywords: []
lang: nl
tags:
- studenten
- medewerkers
- software
title: Proquota
wiki_id: '601'
---
## ProQuota, wat is dat?

Als je op een beheerde PC inlogt, worden vanaf de server je
privé-instellingen (*Profile*) naar die PC gekopieerd. Gedurende de
sessie worden op de PC deze gekopieerde instellingen gebruikt en
veranderd als dat nodig is. Als je vervolgens uitlogt, worden de
(veranderde) instellingen terug naar de server gekopieerd. Log je later
in op een andere beheerde PC dan worden deze instellingen naar die
nieuwe PC gekopieerd en heb je je eigen instellingen weer beschikbaar.

ProQuota monitort de ruimte die je *Profile* op de disk inneemt. Omdat
het kopiëren van het *Profile* tijd kost tijdens het in- en uitloggen en
omdat de fysieke diskruimte beperkt is, is de hoeveelheid *Profile*
ruimte beperkt.

Bedenk dat de backup software onderscheid maakt tussen de Home
directories (H-schijf) en andere spullen, waarbij de Home directories
voorrang krijgen. Zorg dus dat je belangrijke data altijd in je Home
directory opslaat!

Wat kun je doen als je een ProQuota melding krijgt:

-   Voor Firefox en Thunderbird is het verstandig om ervoor te kiezen je
    gebruikersprofiel op te slaan in My Documents (= home directory =
    U-schijf)
-   Sla je documenten niet op op je desktop maar doe dat in je
    Home-directory.

Verder lijkt het me een goed idee om een pagina te maken waar “profile”
nog even wordt uitgelegd, welke mappen daar precies onder vallen en wat
veilig is om te verwijderen als je je profile wilt opschonen. Daar kan
dan ook het gedeelte “wat te doen bij ee proquota melding” bij.
