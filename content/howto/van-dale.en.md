---
author: petervc
date: '2013-04-18T09:56:57Z'
keywords: []
lang: en
tags:
- software
title: Van Dale
wiki_id: '653'
---
## Van Dale dictionaries

All old [C&CZ managed MS-Windows
PCs](/en/howto/windows-beheerde-werkplek/) \*with Windows-XP\* have the
following [van Dale](http://www.vandale.nl) dictionaries:

-   Dutch dictionary:
    dikke van Dale
-   Dutch-German, German-Dutch
-   Dutch-English,
    English-Dutch
-   Dutch-French, French-Dutch

These can be used
automatically from within MS-Word.

For other PCs one can use these dictionaries by attaching the
[S-disk](/en/howto/s-schijf/) and do the installation for each
dictionary. For German that means running S:\\VanDale\\Grote
woordenboeken\\Duits\\client\\setup.exe and for Dutch
S:\\VanDale\\Clients\\Setup.exe.

-   Known problems: Unfortunately sometimes there is a problem with
    Word. The dictionaries are added to Word by means of template files
    in C:\\Documents and Settings\\username\\Application
    Data\\Microsoft\\Word\\STARTUP\\. When Word starts, it makes backup
    copies of the template files in this directory that have filenames
    starting with
    \~$. If Word finishes abnormally, these files can be left in the STARTUP directory, causing irritating, but harmless error messages on the next startup of Word. Removing these \~$
    files makes these irritating, but harmless error messages go away.

-   License: First some faculties (including the Faculty of Science) had
    some partial licenses, but the [University
    Library](http://www.ru.nl/ub) informed us in July, 2008 that they
    had licenses for the Dutch, English, French, and German
    dictionaries.

Access is via a web site: [Van Dale
Dictionaries](http://pro.vandale.nl/). This will only work on Campus,
through a vpn connection or via the proxy server of the UB [Vandale
Dictionaries](http://proxy.ubn.kun.nl:8080/login?url=http://pro.vandale.nl)
