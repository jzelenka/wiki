---
author: petervc
date: '2022-04-01T12:10:56Z'
keywords: []
lang: en
tags:
- overcncz
title: Oud medewerkers
wiki_id: '837'
---
Former C&CZ employees

-   Jos Alsters
-   Pauline Berens
-   Mariet Bimshtein-Oskam
-   Mathieu Bouwens
-   Hans van Driel
-   Marc van Elferen
-   Wim Evers
-   Aad Hulsbosch
-   Willem Jan Karman
-   Kees Keijzers
-   Marcel Kuppens
-   Hans Mahler
-   Theo Neuij
-   Henk van Nieuwkerk
-   Ron Sommeling
-   Tom van der Sommen
-   Khamba Staring
-   Caspar Terheggen
-   Joost van Wel
