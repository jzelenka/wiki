---
author: remcoa
date: '2019-04-12T11:28:56Z'
keywords: []
lang: en
tags:
- contactpersonen
title: Quota aanpassen
wiki_id: '7'
---
## Change of Quota

Only [department contacts](/en/tags/eerstelijns) may request changes of
quota. In principle, it is not possible to verify whether a person is
the person he/she claims to be, in the case he/she sends a mail via a
WWW form like this one. Hence, a request via this form will always be
checked before it is executed and will, therefore, take longer. The
quickest way to request a change of quota is therefore: send a mail
directly to postmaster\@science.ru.nl. This form may be used as an
example.

------------------------------------------------------------------------

Data of the requestor:

|                                   \| \|
| ——————————–: \| \|
|                      ’‘’name:’’’ \| \|
| ’‘’department/research group:’’’ \| \|
|             ’‘’email address:’’’ \| \|

Data of the request:

|                           \| \|
| ————————: \| \|
|        ’‘’login name:’’’ \| \|
| ’‘’disk-quota - soft:’’’ \| \|
| ’‘’file-quota - soft:’’’ \| \|

Possible remarks:
