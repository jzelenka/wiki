---
author: petervc
date: '2018-08-15T16:13:41Z'
keywords: []
lang: en
tags: []
title: Laptop uitleenreglement
wiki_id: '894'
---
### Laptop lending regulations

## General

Students and employees of the Faculty of Science can borrow a laptop at
the counter of the Library of Science, if a laptop is available for the
period wanted.

-   Security:The borrower is responsible for the
    laptop; don’t leave it unattended, not even for quickly getting
    coffee or a visit to the bathroom. At the counter of the Library of
    Science a laptop lock can be borrowed when needed.

-   Lending period:A laptop is lent for use within
    the Faculty for a well-defined period of time, usually less than 1
    day.Borrowing a laptop for more days and/or for use outside the
    Huygens building is occasionally possible. A laptop be be acquired
    on working days from 08:30 hours and has to be returned Monday
    through Thursday before 20:00 hours or Friday before 17:30 hours.

-   Laptop hardware:A power adapter and a USB
    mouse can be borrowed as an option.

-   Laptop software: The laptop has a.o. the
    following software installed: Windows 10, Adobe Reader, MS Office,
    F-Secure Anti virus.

There also is a quite complete [list of all
software](/en/howto/windows-beheerde-werkplek/).

## Laptop lending regulations Faculty of Science

Article 1 The laptop is the property of the Faculty of
Science): the lender. The laptop is lent to a member of the group
described in Article 2: the borrower.

Article 2 The laptop is only lent to staff and students
of the Faculty of Science. Membership of the Faculty community must be
proven by showing the staff or student pass. In addition, the borrower
must show a valid ID (driver’s license, passport or identity card).

Article 3 The laptop is lent for a predetermined period
(usually up to 1 day). Borrowing and returning the laptop can only be
done during opening hours of the front desk of the Library of Science.

Article 4 The laptop may only be used for
educational/research purposes.

Article 5 The borrower may not lend or give the laptop to
others. The borrower is only after prior arrangement allowed to take the
laptop outside the building.

Article 6 If the borrower fails to return the laptop in
time or in original condition at the desk of the Library of Science, the
Faculty reserves the right to refuse to lend laptops to that person in
the future.

Article 7 Damage and/or loss of the laptop - with the
exception of damage resulting from normal use - is entirely borne by the
borrower. Theft is always reported to the police by the borrower. In
case of theft of the laptop the borrower must immediately notify the
Faculty withsubmission of the police report.

Article 8 The borrower may not modify the configuration
of the laptop. The installation of software is not permitted.

Article 9 The lender (Faculty of Science) is authorized
to require the immediate return of the laptop interim and without notice
if the borrower neglected, abused, or used it for a purpose other than
that for which it is intended or if the borrower violates the provisions
of these regulations in any way.

Article 10 The borrower uses the laptop at his own risk.
The lender never will be liable for any damage whatsoever.
