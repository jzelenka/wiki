---
author: petervc
date: '2022-07-05T11:29:47Z'
keywords: []
lang: en
tags:
- netwerk
title: Netwerk ethergids
wiki_id: '92'
---
## C&CZ Ethernet guide

All network info is, mainly automatically, saved in a database. Some
info can be looked up below by searching on MAC address, IP address,
room number, outlet number or hostname.

[FNWI Ethergids](https://rbsgids.science.ru.nl/cgi-scripts/ethergids.php)

Did you find an inaccuracy? Please tell [Postmaster](mailto:Postmaster@science.ru.nl) or call 53737.
