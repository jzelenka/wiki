---
author: polman
date: '2009-05-08T13:40:04Z'
keywords: []
lang: nl
tags:
- internet
- wiki
title: Mediawiki Extensie Installeren
wiki_id: '688'
---
Mediawiki Extensie Installeren

Checkout de extensie Extension-name middels:

     svn co http://svn.wikimedia.org/svnroot/mediawiki/trunk/extensions/Extension-name 

Kopieer de opgehaalde bestanden naar de extensions directory

Denk eraan een .htaccess met RewriteEngine Off erin in de directory te
zetten
