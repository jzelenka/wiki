---
author: polman
date: '2019-02-13T15:38:54Z'
keywords: []
lang: en
tags:
- studenten
- medewerkers
- internet
title: Informatiebeveiliging
wiki_id: '938'
---
## Information security

European directives and Dutch law prohibit the storage of personal data
(persoonsgegevens) outside the [European Economic
Area](http://en.wikipedia.org/wiki/European_Economic_Area) (EEA). Most
of the well known cloud services are US based: Dropbox, all Google
services such as Gmail and Google+, Hotmail, iCloud. This implies that
(data collections containing) personal data may not be stored on these
cloud services. In addition to the law, Radboud University has issued an
internal security policy that prohibits the storage of data that is
classified as critical (this includes all personal data) on any (public)
cloud service, even those which are EEA based. However, RU considers
SURFnet’s [SURFdrive](https://www.surfdrive.nl/en),
[SURFfilesender](https://www.surffilesender.nl/en) and
[edugroepen](https://www.edugroepen.nl/) as safe community cloud
services for data exchange between RU employees, students, and external
parties.

In addition to this, it is advised to consider whether you trust the
cloud provider well enough to handle your data. Take into account the
possible problems which may arise when the service is discontinued or
taken over by a third party, when the service is hacked or forced by law
to provide data to a (local) authority. Moreover one can never be sure
about the goals a provider may have with your data.

The [RU information security
policy](http://www.ru.nl/privacy/ru//beleid) and [data
classification](http://www.radboudnet.nl/informatiebeveiliging/classificaties_en/inleiding/)
methodology are available online (in Dutch only).
