---
author: bram
date: '2022-09-19T16:23:09Z'
keywords: []
lang: nl
tags:
- hardware
title: Afvoer
wiki_id: '948'
---
## Afvoer

-   Veilige afvoer van ICT-apparatuur en datadragers

-   De afdeling Huisvesting & Logistiek (HL) beheert
	een contract met een hierin gespecialiseerd
	extern bedrijf voor het op locatie vernietigen/versnipperen van
	datadragers. De beveiligde metalen container die hierbij als tijdelijke
	opslag gebruikt wordt, is met ingang van september 2022 bij C&CZ
	te vinden. FNWI-medewerkers en -studenten kunnen zelf bij C&CZ
	datadragers met gevoelige gegevens in de container deponeren. De C&CZ
	helpdesk kan helpen bij het herkennen en verwijderen van datadragers uit
	desktops, laptops etc.


-   Defecte monitoren en andere elektronica kunnen afgevoerd via het
    ‘Logistiek Centrum’:

-   <https://www.ru.nl/fnwi/faculteit/organisatie/ondersteunende-diensten/interne/>

    De onbeveiligde afvalcontainers zijn te vinden op verdieping -2 van het
    Huygensgebouw, dan rechtsaf (zuid) tot eind en dan linkaf (oost).

