---
author: polman
date: '2007-10-16T20:09:57Z'
keywords: []
lang: en
noage: true
tags:
- internet
title: Url
wiki_id: '70'
---
## Universal Resource Locators

a Universal Resource Locator (URL) tells you were you can find a World Wide Web page. Each URL is
constructed as:

> `protocol`://`host`.`domain`:`port`/`path`

## Protocol

The `protocol` determines in what way the
information has to be obtained. Valid protocols are e.g.

-   http: Hyper Text Transfer Protocol. The protocol to get Web pages.\
    file or ftp: The document referred to will be downloaded as a normal
    file.\
    telnet: Starts a telnet session to
    host`.`domain.
    The `/`path part is empty.\
    news: A link to a Usenet newsgroup. The
    host plus
    domain part determine the news-server
    (for instance news.surfnet.nl) and the
    path part is de newsgroup.\
    mailto: In this case the remaninder of the URL forms a e-mail
    address to which a mail will be sent.

## Host and Domain

The fully qualified name of the computer serving the document. It is
common practice to define an *alias* for computers operating as www
servers. This has two advantages. Firstly one can use an easy to
remember alais, for instance `wwww`, and secondly the service can be
easily moved to an other computer by letting the alias point to another
computer.

## Port

WWW traffic across the Internet uses a set of rules commonly called a
protocol: Internet Protocol or simply IP. This protocol also defines how
the different connection types (ftp, telnet, http, smtp, nntp, etc.) can
be recognized. This is done using a number, the so called port number.
The default port for HTTP-traffic is 80. The
`:`port part of the URL may be left out
when the WWW service runs on this port number.

## Path

Depending on the
protocol this is the path to a
document or the name of a newsgroup

## Examples

-   <http://www.science.ru.nl/cncz/>: The homepage
    of C&CZ.
-   <ftp://ftp.science.ru.nl/> The ftp-service of the Faculty of Sciences.
-   <http://opc.ubn.ru.nl> The online Public Catalogue of the
    University Library.
-   [mailto:Postmaster\@science.ru.nl](mailto:Postmaster@science.ru.nl)
    A mail to the person acting as
    Postmaster at C&CZ, typically used to send requests or problem
    reports
