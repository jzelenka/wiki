---
author: remcoa
date: '2011-08-30T08:56:34Z'
keywords: []
lang: en
tags:
- wiki
title: NieuweWiki
wiki_id: '682'
---
## Configuration

For every wiki a separate LocalSettingsExtra.php file exists which
enables you to change the default behavior of your wiki. This file can
be found in /vol/wikis/YOURWIKI/ on Unix login servers and is accessible
on Windows pc’s via \\\\wikiconf\\wikisettings\\YOURWIKI Changes in this
file may take up to one minute before becoming active. It is advisable
to read through this file to see what is possible. Beware that (syntax)
errors in this file may cause it to be ignored completely.

### Logo

To change your logo you have to complete the following steps

-   Convert your logo to 135x135 pixel size or smaller
-   Upload your logo image to your wiki (Upload file in Toolbox)

Obtain the location of the uploaded image by clicking on the link just
below *No higher resolution available.* and just above *File History*.
Typically the link will look like
https://wiki.science.ru.nl/YOURWIKI/images/8/82/yourlogo.jpg

-   use this link in your LocalSettingsExtra.php file like this

``` php
 $wgLogo = "/YOURWIKI/images/8/82/yourlogo.jpg";
```

Note that you should remove everything before /YOURWIKI in the obtained
link.

### Extensions

-   [Overview of locally installed extensions](/en/howto/geïnstalleerde-mediawiki-extensies/) ready to
    be used.
-   [overview of available extensions](http://www.mediawiki.org/wiki/Extension_Matrix) which
    may need local installation for which you can submit a request to
    postmaster@@science.ru.nl .

### Default Skin

To see the list of available skins, login
to your wiki, go to *My Preferences* and select the Skin Tab. You can
preview each of the available skins by clicking the corresponding
preview link.

To set one of the available skins as default for *new* and anonymous
(not logged in) users change the line containing the word
*\$wgDefaultSkin* in your LocalSettingsExtra.php file

``` php
 $wgDefaultSkin = 'fnwi';
```

replacing the value by the lowercase name of the skin.

### User management

## Help
