---
author: petervc
date: '2022-06-24T12:54:04Z'
keywords: []
lang: en
tags:
- telefonie
title: Faculty telephone and email guide
wiki_id: '174'
aliases:
- /en/howto/fnwi-telefoon-en-e-mail-gids
---
# Telephone and email guide

{{< telgids >}}

You can search this guide for (part of the) name, phone number, room,
department etc. If you are calling from outside The Netherlands first
dial `+31 24 36` and then the internal phone number.

## RBS
Departments within the Faculty of Science can enter or adjust their guide entries themselves via [RBS](http://www.radboudnet.nl/rbs/). All *telephone contact persons* have a login to edit directory data directly in RBS.

The telephone contact person also receives the telephone cost specifications, can request or cancel changes and subscriptions and, if there is reason to do so, receives information from ILS about the telephony facilities within the RU (eg rate changes, malfunctions, maintenance).

[ILS](/en/howto/contact/#ils-helpdesk) manages the telephone switch. They also
charge subscriptions and call costs.

## Found an error in the guide?
Please contact your secretariat.
- Here you can find a [list of telephone contact persons](https://www.ru.nl/ict/medewerkers/telefonie/).
- For questions, mail to [postmaster](/en/howto/contact).
- FNWI phone contact persons manage their departments phone directory
entries in [RBS](https://rbs.ru.nl).
