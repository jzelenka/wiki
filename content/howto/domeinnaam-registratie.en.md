---
author: petervc
date: '2019-09-06T16:18:30Z'
keywords: []
lang: en
tags:
- internet
title: Domeinnaam registratie
wiki_id: '721'
---
C&CZ has obtained a partner account at domain name registrar
[QDC](http://www.qdc.nl), allowing us to register a `.nl`, `.com`,
`.org` or `.eu` domain name quickly and easily. Many other extensions
are possible but sometimes at significantly higher cost. The (technical)
administration of such a domain name is controlled by C&CZ (the C&CZ
Internet domain name servers). In this way Radboud University Nijmegen
becomes the owner of the domain name instead of an individual employee
of a department).

The (yearly) costs of the registration of a domain name will normally be
paid by C&CZ, at least for departments within the Faculty of Science.

-   For new domain names: please send an email to
    postmaster\@science.ru.nl with the required information
-   For existing domain names: it is possible to relocate existing
    domain names (to C&CZ/QDC), but this involves some additional
    paperwork. Please contact C&CZ System Administration or send an
    email to postmaster\@science.ru.nl
-   Signed [TLS Certificates](/en/howto/tls-certificaten/) for these
    domain names can also be acquired through C&CZ by sending a mail to
    postmaster.
