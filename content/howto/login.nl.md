---
author: petervc
date: '2022-11-24T17:03:10Z'
keywords: []
noage: true
lang: nl
title: Login
aliases:
- gastlogin
- login-aanvragen
- dhz
wiki_id: '163'
ShowToc: true
TocOpen: true
---
# Science-login
Elke medewerker of student van de Science faculteit krijgt een persoonlijke *Science-login*.
Hiermee kun je gebruik maken van diensten zoals 
[eduroam](https://www.ru.nl/medewerkers/services/campusvoorzieningen/werk-en-studie-ondersteunende-diensten/ict/wifi),
[storage](../storage), [VPN](../vpn), [terminalkamer PC's](../terminalkamers), [JupyterHub](../jupyterhub) en
[GitLab](../gitlab).

> Een Science-login lijkt meestal op je naam en bevat geen punten, hoofdletters of een apestaartje (@).
> Bijvoorbeeld `johndoe`.

{{< notice info >}}

Alle instellingen die te maken hebben met je Science-login, zoals het instellen van je wachtwoord en het beheren van groepen, mailaliassen, mail doorsturen en vakantieberichten, regel je in [DHZ](https://dhz.science.ru.nl)

{{< /notice  >}}

## Studenten
Studenten krijgen na inschrijving automatisch een login gebaseerd op
de informatie uit [Osiris](https://www.ru.nl/studenten/services/onderwijs-volgen/studievoortgang-inzien-in-osiris). Op 
het moment dat de Science-login is aangemaakt ontvang je een email met verdere instructies. Deze mail wordt gestuurd 
naar het mailadres dat in Osiris geregistreerd staat.

Het emailadres dat bij je science login hoort eindigt op `@student.science.ru.nl` en wordt 
automatisch doorgestuurd naar je `@ru.nl` adres. Dit is eventueel aan te passen op [DHZ](https://dhz.science.ru.nl).

{{< notice info >}}

Als je de Science-login nog even wil aanhouden terwijl je hier niet meer studeert, dan zal C&CZ dit alleen doen op 
verzoek van de contactpersoon van je studierichting. Informeer hierover bij de
[Student Service Desk](https://www.ru.nl/fnwi/@1317126/openingstijden-student-service-desk/) van het Onderwijscentrum.

{{< /notice  >}}

## Medewerkers, gasten en stagiairs
Nieuwe medewerkers, stagiairs en gasten krijgen een Science-login op verzoek van de contactpersoon van de afdeling. Bij
een Science-login hoort ook een mail-adres dat eindigt op `@science.ru.nl`. Binnenkomende mail is te lezen vanaf
[onze mailvoorziening](../email), of middels [DHZ](https://dhz.science.ru.nl) door te laten sturen naar bijvoorbeeld het `@ru.nl` adres.
Voor stagiairs die hier ook student zijn, is het niet nodig om een Science-login aan te vragen.

{{< notice info >}}

Neem contact op met de contactpersoon van je afdeling als je de Science-login nog even wil aanhouden terwijl je hier
niet meer werkt.

{{< /notice  >}}

### Science-login aanvragen
Alleen contactpersonen kunnen Science-logins aanvragen. Mail hiervoor de volgende gegevens naar [Postmaster](./contact):

|                         |                                                                                                                                                                                              |
|-------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **loginnaam**           | Voorgestelde loginnaam. Bijvoorbeeld: `ajakobsen`, `pieterb` of `janjanssen`.                                                                                                                |
| **mailadres**           | Een suggestie voor het nieuwe mail-adres, meestal iets als `Initialen.Achternaam@science.ru.nl`                                                                                              |
| **volledige naam**      | Naam van de nieuwe medewerker, gast of stagiair                                                                                                                                              |
| **U- of S-nummer**      | Als het gaat om een medewerker of student. Met dit account kan ook worden ingelogd op [DHZ](https://dhz.science.ru.nl) om een (nieuw) wachtwoord in voor je Science-login in te stellen      |
| **prive mailadres**     | Het prive mailadres waar een initieel wachtwoord naar toe gestuurd wordt. In deze mail staan instructies voor het instellen van een nieuw wachtwoord, maar geen vermelding van de loginnaam. |
| **loginnaam aanvrager** | Als aanvrager heb je mogelijkheden om het account bijvoorbeeld op te heffen of te verlengen.                                                                                                 |


## Gasten
Wil je een of meerdere gasten tijdelijk toegang wilt geven tot bijvoorbeeld het
[draadloze netwerk](/nl/howto/netwerk-draadloos/) of tot de PC’s in de [terminalkamers](/nl/howto/terminalkamers/), dan 
zijn er diverse mogelijkheden:

|                             |                       |                                                                                                                                                                                                                                                                                                                    |
|-----------------------------|-----------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| WiFi dag-code               | één dag               | Ga naar <https://portal.ru.nl> en bekijk de instructie voor *Wifi-toegang Gasten*                                                                                                                                                                                                                                  |
| Wifi voor groepen           | één of meerdere dagen | Vraag [Eduroam Visitor Access](https://eva.eduroam.nl/inloggen) aan bij de [Library of Science](https://www.ru.nl/ubn/bibliotheek/locaties/library-science/) of bij de [C&CZ helpdesk](../contact).                                                                                                                |
| Science-login               | één dag               | Science-logins voor vandaag liggen klaar bij de [Library of Science](http://www.ru.nl/fnwi/bibliotheek/library_of_science/).                                                                                                                                                                                       |
| Science-login               | meerdere dagen        | Vraag een persoonlijke gast-login zoals [hierboven beschreven](#science-login-aanvragen) is.                                                                                                                                                                                                                       |
| GitLab/Mattermost login     | tijdelijk             | Vraag een Science-login aan als [hierboven beschreven](#science-login-aanvragen) met beperkte toegang tot [GitLab](../gitlab#externe-gebruikers)/Mattermost.                                                                                                                                                       |
| Science-logins voor groepen | meerdere dagen        | Science-logins voor groepen kunnen per mail bij [Postmaster](../contact) worden aangevraagd. Vermeld daarbij het aantal logins en tot wanneer ze geldig moeten blijven. Als er ook gebruik gemaakt gaat worden van de pc’s in de [terminalkamers](../terminalkamers/), vermeld dan ook welke software is er nodig. |
