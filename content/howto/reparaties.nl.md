---
author: petervc
date: '2022-06-28T14:59:09Z'
keywords: []
lang: nl
tags:
- storingen
- hardware
- contactpersonen
title: Reparaties
wiki_id: '103'
---
De procedure voor reparatie is afhankelijk van het bedrijf dat de
apparatuur geleverd heeft. Of er kosten aan deze reparatie verbonden
zijn, hangt af van de precieze garantievoorwaarden en of de reparatie
binnen de garantie valt:

1.  Door [de PC huisleverancier Dustin](/nl/howto/huisleverancier-pcs/)
    geleverde PC’s hebben normaal gesproken een garantietermijn van 3
    jaar. Maar pas op: dit geldt niet voor alle machines, kijk hiervoor
    goed op de [website van Dustin (voorheen
    Centralpoint)](http://www.dustin.nl/).
2.  Voor andere leveranciers en producten kan een andere garantietermijn
    gelden.

-   Reparaties kunt u zowel **binnen als buiten de garantietermijn** via
    C&CZ laten verlopen. Ook voor advies over mogelijke defecten kunt u
    bij ons terecht.
-   De leverancier of C&CZ zal u berichten op welke dag de reparatie zal
    plaatsvinden. U dient er zorg voor te dragen dat de technicus van de
    leverancier op die dag toegang heeft tot het betreffende apparaat.

-   Het aanmelden van een storing kan via [email, telefoon of een bezoekje](/nl/howto/contact/)

------------------------------------------------------------------------

N.B.1 De gebruiker gaat akkoord met eventuele kosten die voortvloeien
uit deze opdracht. Een raming van € 0 betekent niet automatisch dat er
geen kosten uit kunnen voortvloeien. Dit geldt ook voor reparaties
binnen de garantietermijn.

N.B.2 Indien de reparatiekosten meer zijn dan € 125,- zal eerst een
gespecificeerde kostenopgave per e-mail naar de gebruiker gestuurd
worden, met vermelding van het reparatienummer. Na kennisneming van de
kostenopgave moet de gebruiker berichten of al dan niet tot reparatie
moet worden overgegaan.

------------------------------------------------------------------------
