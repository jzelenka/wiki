---
author: stefan
date: '2019-07-11T06:55:08Z'
keywords: []
lang: en
tags: []
title: Kamerbreed
wiki_id: '917'
---
{{< notice info >}}
The posterprinter is temporary out of service, see [Poster printer kamerbreed broken](../../cpk/1321)
{{< /notice >}}

C&CZ offers the possibility to make prints on a ‘HP DesignJet Z3200ps
44" Photo’ poster printer, on 3 kinds of material:

-   high quality photo paper (HP Everyday Pigment Ink Satin Photo),
    maximum width 110 cm, price € 24 for an A0 print (1
    m{{< sup "2" >}}).
-   canvas (HP Artist Matte Canvas), maximum width 91 cm, price € 36 for
    an A0 print (1 m{{< sup "2" >}}). Canvas prints do
    not show cracks as easily as paper prints, can even be carefully
    folded and included in the hand luggage.
-   stickers (HP Everyday Adhesive Matte Polypropylene), maximum width
    105 cm, price € 30 for an A0 print (1 m{{< sup "2" >}}).

The printer is operated by [C&CZ](/en/howto/contact/), i.e. it is not
possible to send jobs to the printer via the network.

The costs of the prints are determined by the surface area of the print
in mm{{< sup "2" >}}. For transport/protection we also
sell tubes for 1.50 euro/piece. The costs are deducted from your current
printbudget [print budget](/en/howto/printbudget/), (either personal or
departmental).
