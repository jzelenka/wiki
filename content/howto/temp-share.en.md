---
title: Temp share
author: bram
date: 2023-01-13
keywords: []
tags:
- storage
cover:
  image: img/2023/temp-share.png
ShowToc: true
TocOpen: true
---

## Shared temporary storage

It sometimes happens that you want to send one or more large files to someone within the faculty, e-mail is not
suitable for this. To make this easier, the network disk `temp` is available. You can temporarily store large
files here. These can then be copied from this location by someone else.

| C&CZ systems      | Path                            | Instructions                                              |
| ----------------- | ------------------------------- | --------------------------------------------------------- |
| Microsoft Windows | `\\temp-srv.science.ru.nl\temp` | [connect network share](../mount-a-network-share#windows) |
| Linux             | `/vol/temp`                     |                                                           |

| Own systeem       | Path                                | Instructions                                              |
| ----------------- | ----------------------------------- | --------------------------------------------------------- |
| Microsoft Windows | `\\temp-srv.science.ru.nl\temp`     | [connect network share](../mount-a-network-share#windows) |
| macOS             | `smb://temp-srv.science.ru.nl/temp` | [connect network share](../mount-a-network-share#macos)   |
| Linux             | `smb://temp-srv.science.ru.nl/temp` | [connect network share](../mount-a-network-share#linux)   |


## Personal temporary storage

The network share name for temporarily storing personal files is `onlyme`. This drive is only available as a Windows network drive:
| C&CZ systems      | Path                                | Instructions                                        |
| ----------------- | ----------------------------------- | --------------------------------------------------- |
| Microsoft Windows | `\\onlyme-srv.science.ru.nl\onlyme` | [schijf koppelen](../mount-a-network-share#windows) |

{{< notice info >}}
The network share `onlyme` is not available on C&CZ Linux systems. For temporary storage of personal data from Linux
systems, please use `/vol/temp` and please take care of the file permissions yourself.
{{< /notice >}}

| Own system        | Path                                    | Instructions                                        |
| ----------------- | --------------------------------------- | --------------------------------------------------- |
| Microsoft Windows | `\\onlyme-srv.science.ru.nl\onlyme`     | [schijf koppelen](../mount-a-network-share#windows) |
| macOS             | `smb://onlyme-srv.science.ru.nl/onlyme` | [schijf koppelen](../mount-a-network-share#macos)   |
| Linux             | `smb://onlyme-srv.science.ru.nl/onlyme` | [schijf koppelen](../mount-a-network-share#linux)   |


## Timestamps
Files in the `temp` share are automatically cleaned up based on their timestamps. Some copy programs (such as rsync) retain the original timestamps and then the older files may be deleted by the temp disk. Therefore, after copying, update the timestamps, for example with the following command:

```
find /vol/temp/$USERNAME -exec touch {} +
```

# Alternative
[SURFfilesender](https://www.surffilesender.nl/) is also suitable for sending files up to 1TB.
> With SURFfilesender you can safely send large files, such as research data. The files are stored in the Netherlands. Encryption provides additional security.
