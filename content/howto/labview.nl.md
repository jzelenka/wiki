---
author: petervc
date: '2022-03-18T13:43:09Z'
keywords: []
lang: nl
tags:
- software
title: LabVIEW
wiki_id: '1084'
---
C&CZ verzorgt de licenties voor gebruik van
[LabVIEW](https://nl.wikipedia.org/wiki/LabVIEW) binnen FNWI. LabVIEW
van [NI](https://www.ni.com/) is een grafische programmeeromgeving die
met name geschikt is voor besturingstechniek, data-acquisitie, en het
communiceren met meetinstrumenten.

Vanaf versie 2022Q3 werkt LabVIEW niet meer standalone en onbeperkt qua
tijd. C&CZ beheert een LabVIEW licentieserver met een klein aantal jaarlijks
te verlengen licenties. Gebruikers hiervan moeten op de licentieserver
geregistreerd zijn.
