---
author: bram
date: 2022-11-04
title: OpenVPN
keywords: [ openvpn, vpn ]
tags: [ vpn ]
cover:
  image: img/2022/openvpn.png
ShowToc: false
TocOpen: false
---
# OpenVPN
Mocht het op een of andere manier niet lukken om onze standaard [VPN-dienst](../vpn) op je systeem aan de praat te krijgen, gebruik dan onze OpenVPN dienst.

Algemene instellingen:
| label        | waarde                   |
| ------------ | ------------------------ |
| server adres | `openvpn.science.ru.nl ` |
| port         | `443`                    |

{{< notice tip >}}
Je kun je VPN-verbinding testen door bijvoorbeeld de volgende (geweldige) website te bezoeken: 

> <https://ip.science.ru.nl>

Deze pagina vermeldt je ip-adres. Als je de Science VPN aan hebt staan, zal het ip-adres beginnen met `131.174.`. En de hostnaam zal lijken op `vpnXXXXXX.science.ru.nl` (waarbij XXX overeenkomt met het ip-adres).
{{< /notice >}}

## Instructies voor Ubuntu 22.04
[Download](https://gitlab.science.ru.nl/cncz/openvpn/raw/master/openvpn-ca-science.ovpn) het Science openvpn configuratiebestand. Of op deze manier:

```
wget https://gitlab.science.ru.nl/cncz/openvpn/raw/master/openvpn-ca-science.ovpn
```

Ga in "Systeem settings" naar "Network".  Zie afbeelding:

 ![vpn toevoegen ubuntu](/img/2022/newvpnubuntu.png)

Daar op het plusje `+` klikken om een nieuwe VPN-verbinding toe te voegen.

In het volgende dialoog, kies "Import from file..." en selecteer het zojuist gedownloade `ovpn`-bestand.

Je krijgt daarna het volgende scherm te zien:

![openvpn configuratie ubuntu](/img/2022/openvpn-ubuntu.png)

Hier hoef je slechts je [science loginname](../login) in te voeren.

Na opslaan, kun je je OpenVPN verbinding aanzetten middels de knoppen in de toolbar rechts-boven:

![vpn aanzetten ubuntu](/img/2022/ubuntu-enable-vpn.png)