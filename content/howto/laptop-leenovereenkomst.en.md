---
author: petervc
date: '2012-10-15T16:07:52Z'
keywords: []
lang: en
tags: []
title: Laptop leenovereenkomst
wiki_id: '893'
---
### Laptop lending agreement

AGREEMENT FOR BORROWING A LAPTOP FROM THE FACULTY OF SCIENCE OF RADBOUD
UNIVERSITEIT NIJMEGEN

I hereby declare:

-   NAME:………………………………………………

Student/employee of the Faculty of Science:

-   S-number:…………………………………
-   U-number:…………………………………

-   Document type of valid ID: …………………………………
-   Number of valid ID:…………………………………

that I have received the laptop in correct order.

-   Name/number of the laptop: …………………………………
-   Date: ………………………………… Time: …………………………………

Planned date/time of return: …………………………………

I have read the [lending regulations of the Faculty of Science for
borrowing laptops](/en/howto/laptop-uitleenreglement/) to students and
employees and will follow it.

I am obliged to:

-   returning the laptop at de date/time mentioned above, during opening
    hours of the Library of Science (Mon-Thu before 20:00 hours; Fri
    before 17:30 hours).
-   reporting all hard- or software problems of the laptop mentioned
    above that were encountered.
-   reimbursing all damage caused to the laptop during the lending
    period.
-   reimbursing the book value of the laptop if the laptop is not
    returned.

I gave my student or employee pass in pledge. I will get this back when
I return the laptop.

Borrowers signature: …………………………………

-   Signature employee Library of Science lending: …………………………………
-   Date/time lending: …………………………………

-   Signature employee Library of Science after correct return of the
    laptop: …………………………………
-   Date/time return: …………………………………
