---
author: petervc
date: '2012-10-15T16:07:52Z'
keywords: []
lang: nl
tags: []
title: Laptop leenovereenkomst
wiki_id: '893'
---
### Laptop leenovereenkomst

OVEREENKOMST VOOR HET LENEN VAN EEN LAPTOP VAN DE FACULTEIT
NATUURWETENSCHAPPEN, WISKUNDE EN INFORMATICA VAN DE RADBOUD UNIVERSITEIT
NIJMEGEN

Hierbij verklaar ik:

-   NAAM:………………………………………………

Student/medewerker van de Faculteit Natuurwetenschappen, Wiskunde en
Informatica:

-   S-nummer:…………………………………
-   U-nummer:…………………………………

-   Documenttype van het geldige legitimatiebewijs: …………………………………
-   Nummer van het geldige legitimatiebewijs:…………………………………

dat ik de laptop in goede staat ontvangen heb.

-   Naam/nummer laptop: …………………………………
-   Inclusief/exclusief stroom-adapter: …………………………………
-   Datum: ………………………………… Tijdstip: …………………………………

-   Geplande datum/tijd retour: …………………………………

Ik heb het [uitleenreglement van FNWI voor uitlenen van
laptops](/nl/howto/laptop-uitleenreglement/) aan studenten en medewerkers
gelezen en zal mij aan het hierin gestelde houden.

Ik verplicht mij tot het volgende:

-   Inleveren van de laptop op de hierboven vermelde datum voor
    sluitingstijd van de Library of Science (Ma-Do voor 20:00 uur; Vrij
    voor 17:30 uur).
-   Melding maken van alle geconstateerde hard- of software problemen
    m.b.t. bovenvermelde laptop.
-   Vergoeden van alle schade die tijdens de leenperiode aan de laptop
    ontstaan is.
-   Vergoeden van de boekwaarde van de laptop indien de laptop niet
    geretourneerd wordt.

De studenten- of medewerkerspas heb ik achtergelaten als onderpand. Deze
ontvang ik terug bij inlevering van de laptop.

-   Handtekening lener: …………………………………

-   Handtekening bibliotheekmedewerker bij uitleen: …………………………………
-   Datum/tijd uitleen: …………………………………

-   Handtekening bibliotheekmedewerker na constateren correct
    terugbrengen: …………………………………
-   Datum/tijd retour: …………………………………
