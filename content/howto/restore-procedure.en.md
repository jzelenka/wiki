---
author: wim
date: '2022-08-11T10:53:32Z'
keywords: []
lang: en
tags:
- medewerkers
- studenten
title: Restore procedure
wiki_id: '19'
---
With the information below in an email to postmaster\@science.ru.nl you
can request C&CZ to restore one or more files.

One problem is that C&CZ cannot be sure that the email-address of the
sender of such an email-request is not faked. Therefore C&CZ will first
check with the supposed sender. The fastest way is therefore to phone or
stop by C&CZ after sending an email with the information below to
postmaster\@science.ru.nl. If you do that on the day that you lost the
file(s), probably C&CZ can very easily copy the file(s) back from the
snapshot of that morning.

Fill in the fields as completely and accurately as possible.

------------------------------------------------------------------------

~~~ txt
name:
loginname:
machine:
creation date:
creation time:
date of last change:
time of last change:
date of loss:
time of loss:
reason of loss:
priority:
~~~

Please give here the complete filename(s), one per line.
