---
author: petervc
date: '2022-06-22T13:10:45Z'
keywords: []
lang: en
tags: []
title: Overcncz wieiswie secretariaat
wiki_id: '530'
---
Wie is zij?

Astrid Linssen Tel: 3652577

Wat doet zij?

Het secretariaat zorgt o.a. voor:

-   Afdelingsadministratie.
-   Functioneel beheer van het tijdregistratiesysteem.
-   Licenties Matlab en toolboxen.
-   Licenties Maple, Labview, Mathematica.
