---
author: polman
date: '2018-08-03T11:01:37Z'
keywords: []
lang: en
tags: []
title: Portfolio Handleiding Mentor
wiki_id: '628'
---
**Handleiding Portfolio 2008 - mentor**

**Handleiding: wiki.cncz.science.ru.nl/portfolio**

**Studiegidsen: studiegids.science.ru.nl/2008/science**

**Portfolio-applicatie: people.science.ru.nl**

# Algemene informatie

Voor het portfolio wordt gebruik gemaakt van ELGG, een open-source
web-applicatie die is voor ons is aangepast door Remco Aalbers van C&CZ.

# Inloggen

Om in te loggen klikt u op ‘inloggen’ links boven in en gebruikt u uw
normale Science gebruikersnaam en bijbehorend wachtwoord. U komt dan op
de hoofdpagina.

Hier kunt u account-instellingen wijzigen (naam, emailadres,
toegangsbeperkingen).

# Profielpagina

Via ‘eigen profiel’ in de bovenbalk, en vervolgens ‘bewerk dit profiel’
kunt u uw gegevens verder invullen. Plaats bij voorkeur een foto (onder
‘basis details’) en vul emailadres (onder ‘contact’) in. Klik op ‘bewaar
dit profiel’ onderin o m uw profiel op te slaan. Meer invullen mag, maar
hoeft niet, let wel op de toegangsrechten:

-   ‘Openbaar’: zichtbaar voor de hele wereld
-   ‘Ingelogde gebruikers’: alleen zichtbaar voor FNWI studenten en
    medewerkers
-   ‘Groep: Portfolio toegang’: zichtbaar voor de personen in een
    portfolio groep (niet van toepassing voor mentoren)
-   ‘Privé’: alleen voor uzelf zichtbaar

De studenten vullen hun opleiding in bij hun profielinformatie en
daarmee wordt de structuur van hun portfolio aangemaakt. Wanneer zij van
opleiding veranderen verandert deze structuur automatisch mee.

**Toegang tot het portfolio van studenten**

Dit jaar hebben wij alle studenten al aan hun mentor gekoppeld. Zodra u
ingelogd bent, ziet u in de rechterbalk de studenten staan waarvan u
mentor bent.

**Berichten sturen**

Bovenin uw profielpagina vindt u de mogelijkheid via ‘netwerk’ om een
bericht te sturen naar een van uw studenten. Deze krijgt een email-alert
dat er in zijn portfolio een bericht van u is. Omgekeerd kan een student
met u contact opnemen. (Gewoon email contact is natuurlijk ook
mogelijk).

# Op de studentpagina

Door te klikken op de naam van uw student komt u op diens profielpagina.

In de rechterbalk van de profielpagina van uw student is de toegang tot
het portfolio.

Via ‘eigen profiel’ in de bovenbalk gaat u terug eigen pagina.

**Portfolio**

Het portfolio heeft een doorklikbare map-structuur en is ingedeeld in
jaren – kwartalen – vakken.

De student maakt voor keuze- en differentiatievakken zelf mappen aan.
Gevulde mappen zijn herkenbaar. Zodra het beoordelingscijfer bekend is
voegt de student dat ook toe.

**Lock-unlock.**

Een student heeft 48 uur de tijd om wijzigingen aan te brengen in een
map, daarna is deze niet meer te wijzigen. Lege mappen kunnen worden
verwijderd door de student, volle niet.

Mocht u samen met de student tot de conclusie komen dat iets echt fout
is of niet thuishoort in het portfolio, dan kunt u het item ‘unlocken’.
De student heeft dan opnieuw 48 uur de tijd om correcties te maken.

Het bekijken van portfolioproducten spreekt voor zich.

Via **wiki.science.ru.nl/cncz/Portfolio** is de lijst beschikbaar met
zaken die de student in het portfolio moet opnemen.

**Mentoraat**

De mentor heeft als minimale taak:

-   tenminste enkele malen per jaar het portfolio te bekijken
-   tenminste twee maal per jaar contact hebben met de student over
    voortgang in vaardigheden en toekomstplanning. Aan het eind van het
    jaar komt het reflectie-verslag ter sprake.
-   Bespreekt met de student de specifieke portfolio-opdrachten.

De mentor moet in ieder geval op de hoogte zijn van de opleiding van
zijn student en een inschatting kunnen maken van de voortgang en kan
natuurlijk te allen tijde door de producten bladeren

Idealiter komt de student in het gesprek met de mentor aan het begin van
de opleiding al in aanraking met de academische context en kan terecht
met vragen rond keuzes binnen het Scheikundig/MLW/Natuurwetenschappelijk
veld.

Wanneer een student tegen inhoudelijke problemen aanloopt of
vaardigheids-problemen tegenkomt die de individuele vakken overstijgen
kan de mentor proberen hem in de goede richting aanwijzingen te geven.

De mentor hoeft uiteraard niet de individuele vak-docent te vervangen.

Ook is de mentor geen formeel studie-adviseur, al raken de terreinen
elkaar wel.

**Studieadviseur**

De mentor hoeft niet in de rol te treden van de studieadviseur en kan
altijd naar deze verwijzen:

-   wanneer de student problemen heeft op studievoortgangsgebied
-   wanneer de student inschrijvingsproblemen heeft, medische, sociale
    problemen, etc.
-   wanneer de student formele keuzes moet maken binnen de opleiding,
    minoren, keuzevakken, wat mag en wat niet, etc. etc.

Hierin ligt altijd een primaire rol van de studieadviseur. De mentor kan
wel een mening geven. In veel gevallen zal er contact zijn tussen mentor
en studieadviseur.

Richtlijn:

’‘’De studieadviseur’’’is deskundig op het gebied van de opleiding.

**De mentor** is deskundig op het vakgebied en beroepsgebied en is
docent.
