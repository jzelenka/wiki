---
author: bram
date: '2022-10-08T12:38:24Z'
keywords: []
lang: en
tags: []
title: LimeSurvey
wiki_id: '905'
cover:
  image: img/2022/limesurvey.png
---
# Introduction to LimeSurvey

Online surveys can be used for any type of data collection, not just for
marketing purposes. Possible uses of surveys are sign-up forms for
conferences, questionnaires for research projects, order forms, etc. For
students and employees of the beta-faculty, C&CZ offers a survey service
based on [LimeSurvey](https://community.limesurvey.org/).

LimeSurvey is an open source web application. It currently offers:

-   Sending of invitations, reminders and tokens by email
-   Multilingual surveys
-   Conditions for questions depending on earlier answers (Skip Logic /
    Branching)
-   Integration of pictures and movies into a survey

## Logging in

The admin login page is located at:

<https://u1.survey.science.ru.nl/admin/>

It is not necessary to request access: any Science login can login.

## Use policy

Take care of the following items before activating a survey:

-   Specify a survey email address (General settings \> Admin email).
    You might want to ask us for a dedicated mail alias.
-   Set an expiry date (General settings \> Publication & access
    control \> Expiry date/time). This allows us to see if there are
    active surveys.

## Preparing a survey

After logging in to the admin interface of LimeSurvey, you can create
new surveys, edit existing surveys and view / download responses. The
following hints will help you to get familiar with LimeSurveys user
interface:

### Nested interface

The structure of a survey in LimeSurvey is nested: survey \> question
groups \> questions. At the top right of the admin interface, you’ll
find a selection list to choose a survey, question group or question.

### Question groups

Generally, you’ll start creating a survey, add one or more question
groups, followed by adding questions. For your first basic survey,
you’ll probably be looking for a way to add questions to a survey. You
can’t find that option on the survey level. Question groups are
mandatory! So. first add a single question group, then start adding the
questions to it.

### Online documentation

There’s excellent online documentation. You’ll probably want to start
reading here at
[docs.limesurvey.org](http://docs.limesurvey.org/Creating+surveys+-+Introduction&structure=English+Instructions+for+LimeSurvey).
This particular link points to the section explaining how to get your
questions into a survey.

### Youtube crash course in ten minutes

[This is a quite clear
video](https://www.youtube.com/watch?feature=player_detailpage&v=KWE0wAoyvak)
that shows how to get started creating a first survey.

[Watch on
youtube](https://www.youtube.com/watch?feature=player_detailpage&v=KWE0wAoyvak)
