---
author: bram
date: '2014-12-25T21:07:28Z'
keywords: []
lang: en
tags: []
title: Loginschermen
wiki_id: '920'
---
These are the advertisements displayed on the login screens of the
[terminal rooms](/en/howto/terminalkamers):

![image01](https://cncz.pages.science.ru.nl/wallpapers/img/banners/image_win/image01.png)
![image02](https://cncz.pages.science.ru.nl/wallpapers/img/banners/image_win/image02.png)
![image03](https://cncz.pages.science.ru.nl/wallpapers/img/banners/image_win/image03.png)
![image04](https://cncz.pages.science.ru.nl/wallpapers/img/banners/image_win/image04.png)
![image05](https://cncz.pages.science.ru.nl/wallpapers/img/banners/image_win/image05.png)
![image06](https://cncz.pages.science.ru.nl/wallpapers/img/banners/image_win/image06.png)
![image07](https://cncz.pages.science.ru.nl/wallpapers/img/banners/image_win/image07.png)
![image08](https://cncz.pages.science.ru.nl/wallpapers/img/banners/image_win/image08.png)
![image09](https://cncz.pages.science.ru.nl/wallpapers/img/banners/image_win/image09.png)
![image10](https://cncz.pages.science.ru.nl/wallpapers/img/banners/image_win/image10.png)
