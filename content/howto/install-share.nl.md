---
author: petervc
date: '2018-01-10T15:14:49Z'
keywords: []
noage: true
lang: nl
tags:
- software
title: Install share
wiki_id: '811'
---
Veel campuslicentie software die door studenten en medewerkers van FNWI gebruikt mag worden, is te vinden in de `install` network share.

{{< notice info >}}
Zie [hier](../storage/#paden-voor-netwerkschijven) hoe je een netwerkschijf, zoals de install share kunt koppelen.
{{< /notice >}}

Meestal kan men deze software direct vanaf de install share installeren. Vaak is een licentiecode
nodig, die via mail naar [postmaster](../contact) verkregen kan worden. Elk
softwarepakket heeft eigen licentievoorwaarden. Soms is gebruik alleen
op RU-computers toegestaan, soms ook thuisgebruik door studenten.
