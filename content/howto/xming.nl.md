---
author: petervc
date: '2022-10-27T15:25:24Z'
keywords: []
lang: nl
tags: []
title: X onder Windows
wiki_id: '906'
---
Een krachtig en eenvoudig bruikbaar pakket om X applicaties onder windows te kunnen tonen is
[MobaXterm](http://mobaxterm.mobatek.net/), dat beschikbaar is op de
[S-schijf](/nl/howto/s-schijf/). Uit de MobaXterm website: “MobaXterm is
een verbeterde terminal voor Windows met een X11 server, een SSH client
met tabbladen en bevat een aantal andere netwerktools voor remote
computing (VNC, RDP, telnet, rlogin). MobaXterm brengt alle essentiële
Unix-commando’s naar het Windows bureaublad, in een enkele exe-bestand
die werkt uit de doos.” Ook de OpenGL-ondersteuning kan een reden zijn
om MobaXterm te gaan gebruiken. Bij professioneel gebruik dient men een
[licentie voor de Professional
Edition](http://mobaxterm.mobatek.net/download.html) te overwegen.
