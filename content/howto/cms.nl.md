---
author: petervc
date: '2021-06-03T09:18:55Z'
keywords: []
lang: nl
tags:
- studenten
- medewerkers
- contactpersonen
title: CMS
wiki_id: '911'
---
De Radboud Universiteit beschikt over een Content Management Systeem
(CMS) voor websites. Alle sites gebaseerd op dit CMS worden gehost door
het ISC. De eenheid Communicatie (onderdeel van Radboud Services) is de
eigenaar van de dienst en verantwoordelijk voor de correcte werking van
het systeem. C&CZ biedt ook [WWW\_Services](/nl/howto/www-service/) aan.
Welke vorm het meest geschikt is, hangt af van de wensen.
[C&CZ](/nl/howto/contact/) adviseert graag bij de keuze.

## Functioneel beheer en ondersteuning

Alle faculteiten en clusters hebben een of meer lokale CMS functioneel
beheerders. De belangrijkste taken van de functioneel beheerders zijn:

-   Beheer van logins en rechten van de gebruikers van het CMS, d.w.z.
    van de sitebeheerders.
-   Het creëren van nieuwe sites voor (nieuwe) CMS gebruikers, het
    verwijderen van sites die niet meer gebruikt worden en het uitvoeren
    van grote wijzigingen in de algehele structuur van sites op verzoek
    van de betreffende eigenaar.
-   Technische ondersteuning en advies voor CMS gebruikers en het
    coordineren van nieuwe bugmeldingen tussen CMS gebruikers en de
    eenheid Communicatie.
-   Beperkt en ad hoc inhoudelijk paginabeheer van algemene facultaire
    paginas (zelden hele sites).

Contactgegevens van de functioneel beheerders is te vinden op onze
[Contactpagina](/nl/howto/contact/).

## Website beheerders

Er zijn vele websites en dus ook vele website beheerders bij FNWI die
gebruik maken van het RU CMS. Er is een
[mailinglijst](http://mailman.science.ru.nl/mailman/listinfo/nwiweb)
waar men zich op kan abonneren om actuele informatie te ontvangen over
het CMS, onderhoudsberichten, bugmeldingen, enz.
