---
author: bram
date: '2022-09-19T16:05:54Z'
keywords: []
lang: nl
tags: []
title: Thunderbird Tags
wiki_id: '664'
---
## Thunderbird labels

In Thunderbird kun je berichten een of meerdere *labels* geven door met
de rechtermuisknop op het bericht te klikken en dan **Tag** te kiezen.
Er zijn vijf standaardlabels met omschrijvingen **Belangrijk**,
**Werk**, etc. Als de achterliggende mailserver een imapserver is zoals
**post.science.ru.nl**, dan worden de labels op de server opgeslagen.
Elke Thunderbird zal dan ook de labels laten zien waarmee een bericht is
gelabeld.

Iets ingewikkelder is het voor *zelf gemaakte labels*. Deze worden ook
op de imapserver opgeslagen, maar zijn niet zonder meer zichtbaar in
andere Thunderbirds. Een nieuw label kun je maken door rechtermuisknop
op een bericht en dan **New Tag** te klikken, of via het menu **Tools
-\> Options… -\> Display -\> Tags -\> Add**. De naam die je dan kiest
wordt zowel als naam voor het interne label gebruikt als voor de
omschrijving. Als je later de naam aanpast, verandert de bijbehorende
interne labelnaam niet meer. De interne labelnamen (dus de *eerst
gekozen* labelnaam) wordt als label op de server gebruikt. Zo’n nieuw
gemaakt label is alleen zichtbaar in een andere Thunderbird als daar ook
zo’n label met dezelfde interne labelnaam wordt gemaakt. Het is dus van
belang om de eerste labelnaam goed te kiezen. I.h.b. moet je dat
afspreken met degenen met wie de mailbox wordt gedeeld. Daarna kan
iedereen de kleur en labelnaam nog naar believen aanpassen, zonder dat
de zichtbaarheid van gelabelde berichten verdwijnt.

Het verschil tussen *interne labelnaam* en de *zichtbare labelnaam* is
goed te zien in de **Config Editor** (**Tools -\> Options… -\> Advanced
-\> General -\> Config Editor…** en dan filteren op het woord **tag**).
Zo zie je onder meer dat het interne labelnaam van **Belangrijk** tag
**\$label1** is.
