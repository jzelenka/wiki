---
author: polman
date: '2019-02-13T15:38:54Z'
keywords: []
lang: nl
tags:
- studenten
- medewerkers
- internet
title: Informatiebeveiliging
wiki_id: '938'
---
## Informatiebeveiliging

Europese richtlijnen en de Nederlandse wetgeving verbieden de opslag van
persoonsgegevens buiten de [Europese Economische
Ruimte](http://nl.wikipedia.org/wiki/Europese_Economische_Ruimte) (EER).
De meeste bekende cloud-diensten zijn gevestigd in de VS: Dropbox, alle
Google-diensten zoals Gmail en Google+, Hotmail, iCloud. Dit impliceert
dat (gegevensverzamelingen bevattende) persoonsgegevens niet opgeslagen
mogen worden in deze cloud services. In aanvulling op de wet hanteert de
Radboud Universiteit een intern veiligheidsbeleid dat de opslag van
gegevens die als kritisch worden geclassificeerd (dit omvat in elk geval
persoonsgegevens) verbiedt op alle (publieke) cloud services, zelfs
degene die in de EER gevestigd zijn. Echter, de RU beschouwt SURFnet’s
[SURFdrive](https://www.surfdrive.nl/),
[SURFfilesender](https://www.surffilesender.nl/) en
[EDUgroepen](https://www.edugroepen.nl/) als veilige community cloud
diensten voor gegevensuitwisseling tussen RU medewerkers, studenten en
externe partijen.

Daarnaast speelt altijd de vraag of de data aan de betreffende cloud
provider toevertrouwd kunnen worden. Afwegingen daarbij zijn de
mogelijke problemen die ontstaan als de dienst omvalt of als de provider
door een andere partij wordt overgenomen, als de dienst wordt gekraakt
of door een (lokale) overheid wordt gedwongen data af te staan.
Bovendien is nooit met zekerheid te zeggen wat de provider zelf met de
data doet.

Het [RU
informatiebeveiligingsbeleid](http://www.ru.nl/privacy/ru/beleid) en de
[data
classificatie](http://www.radboudnet.nl/informatiebeveiliging/classificaties_en/inleiding/)
methodiek zijn online beschikbaar.
