---
author: polman
date: '2021-10-15T12:33:06Z'
keywords: []
lang: nl
tags:
- email
title: Email authsmtp
wiki_id: '11'
---
## Voordelen van Authenticated SMTP

Het is wenselijk om met loginnaam en wachtwoord aan te melden bij de
mailserver bij het versturen van mail. De mailserver zal in dat geval
nooit de mail weigeren met de melding ‘Relaying denied’. Ook van buiten
FNWI kan op deze manier van de FNWI mailservers gebruik worden gemaakt
om mail af te leveren. Als je een laptop gebruikt, kun je dan altijd
gebruik maken van de SMTP-servers van FNWI, zowel thuis als op FNWI,
mits je je bij de SMTP-server authenticeert.

Als iedereen een vertrouwde manier gebruikt om mail te sturen, zoals
authenticated smtp of via onze webmailers, zouden we voor FNWI kunnen
kiezen om “het Internet” te vertellen dat mail met een `@science.ru.nl`
afzenderadres alleen verstuurd mag zijn via smtp servers van de RU. Dit
kan met
[SPF/DKIM/DMARC](https://en.wikipedia.org/wiki/DomainKeys_Identified_Mail).
Mail verstuurd via een externe smtp server zal dan geweigerd worden
indien de ontvangende mailserver deze controle gebruikt, dit is in
toenemende mate het geval. Dit alles is een belangrijke stap om phishing
mails te blokkeren.

**NB** voor `science.ru.nl` kun je ook andere door ons beheerde domeinen
invullen, zoals bijvoorbeeld `cs.ru.nl`.

## Authenticated SMTP

SMTP (Simple Mail Transfer Protocol) is het protocol waarmee op het
internet mail uitgewisseld wordt. Uw email programma gebruikt dit ook om
mail aan uw uitgaande SMTP-server te overhandigen. Wanneer u email wilt
versturen met uw favoriete email programma, dan bent u normaal gesproken
gebonden aan het gebruik van de SMTP-server van de partij die u toegang
tot het internet geeft. Als u veel onderweg bent en op verschillende
locaties email wilt kunnen versturen, of een laptop heeft die u zowel op
de campus als thuis gebruikt, dan zou dit betekenen dat u regelmatig een
andere SMTP-server zou moeten instellen.

Waarom steeds een andere SMTP server? Als u uw ‘oude’ SMTP-server
gebruikt terwijl u verbonden bent via een andere internet-provider, dan
ziet deze SMTP-server een verbinding vanaf een ‘vreemd’ netwerk’.
Normaal gesproken accepteert de SMTP-server vanaf een ‘vreemd’ netwerk
alleen mail voor zijn eigen gebruikers, maar niet mail die bedoeld is
voor gebruikers in de rest van het internet. Als de SMTP-server wel alle
mail accepteert en doorstuurt naar de rest van het internet, dan kan de
server misbruikt worden om spam te versturen, waarna de server op zwart
lijsten komt en niemand meer mail accepteert van deze server.

Het alternatief. U authenticeert uzelf met een persoonlijke
gebruikersnaam en wachtwoord bij een SMTP server waarbij u bekend bent.
De beheerder ervan kan dan mail die bestemd is voor het internet toch
accepteren. Bij misbruik weet de beheerder welke klant aangesproken moet
worden.

Beveiliging. Omdat u via ‘vreemde netwerken’ email gaat versturen en
daarbij ook uw wachtwoord doorgeeft aan onze SMTP server, wordt de
verbinding met SSL versleuteld. Alle gangbare moderne email clients
kunnen hiermee omgaan.

# Instellen van authenticated SMTP

## Algemene instellingen

-   Kies als SMTP-server **smtp.science.ru.nl**.
-   De **standaard tcp poort 25** kan worden gebruikt, maar ook de
    **submission poort 587** wordt ondersteund. Smtps (poort 465) wordt
    ondersteund.\
    **Sommige providers blokkeren uitgaand verkeer naar poort 25.**
    Norton firewall en antivirus weigeren ook versleuteld uitgaand
    verkeer naar poort 25 en het is niet altijd duidelijk hoe dat kan
    worden aangepast zonder de hele Norton firewall uit te zetten. In
    deze gevallen biedt de submission poort 587 uitkomst.\
    Andere firewalls waaronder de firewall van McAfee lijken standaard
    juist uitgaand verkeer naar poort 587 te blokkeren, maar niet naar
    de smtp poort 25.
-   Bij gebruik van poort 587 is authenticatie *verplicht*.
-   Bij gebruik van poort 587 wordt uitgaande e-mail niet op spaminhoud
    of virussen gecontroleerd. Dit geeft een mogelijkheid om
    bijvoorbeeld exe-bestanden te versturen, wat anders niet is
    toegestaan.
-   **Authenticatie** is mogelijk met **PLAIN** en **LOGIN**. I.h.b.
    zijn NTLM (een Microsoft protocol) en CRAM-MD5 niet mogelijk.\
    Als accountnaam moet altijd de loginnaam (dus zonder “.” en “@”)
    worden gebruikt.
-   Authenticatie is alleen toegestaan indien **TLS** wordt gebruikt.
    Dit wordt soms ook STARTTLS (Thunderbird) of SSL (Outlook, Pine)
    genoemd.

## Alternatieve RU-server

-   server: smtp-auth.ru.nl

-   gebruikersnaam: U/S-nummer

-   wachtwoord: RU-wachtwoord

## Instellen van Thunderbird

-   Kies **Extra → Account Settings…**. Bij sommige versies staat
    **Account Settings…** onder het menu **Bewerken**.

-   Klik op **Uitgaande server (SMTP)** in de linkerkolom en vul in of
    vink aan:

    \* Servernaam: **smtp.science.ru.nl**

    -   Poort: **25** of **587** (Zie [\|Algemene instellingen] voor
        meer informatie.)
    -   **Aanvinken**: Gebruikersnaam en wachtwoord
    -   Gebruikersnaam: *loginnaam* invullen.
    -   Beveiligde verbinding gebruiken: **TLS, als beschikbaar**\
        {{< figure src="/img/old/thunderbird-nl-smtp.jpg" title="Uitgaande server (SMTP) instellingen" >}}

## Instellen van Outlook

-   Kies **Extra → E-mailaccounts…**

-   Kies **Bestaande e-mailaccounts weergeven of wijzigen** en klik op
    **Volgende \>**.

-   Selecteer het account en klik op **Wijzigen…**:

    {{< figure src="/img/old/outlook-nl-accounts.jpg" title="E-mailaccounts" >}}

-   Vul in het volgende venster in:

    \* Het E-mailadres.

    -   Server voor uitgaande e-mail (SMTP): **smtp.science.ru.nl**

    -   Gebruikersnaam: *loginnaam* invullen.

    -   **Niet aanvinken**: Aanmelden met
        beveiligd-wachtwoordverificatie.

        {{< figure src="/img/old/outlook-nl-server.jpg" title="Instellingen" >}}

        Klik dan op **Meer instellingen…**

-   Ga naar het tabblad **Server voor uitgaande e-mail** en:

    \* **Vink aan:** Voor de server voor uitgaande e-mail (SMTP) is
    verificatie vereist.

    -   **Niet aanvinken**: Aanmelden met
        beveiligd-wachtwoordverificatie.

    -   Het lijkt erop dat Outlook XP geen authenticatie gebruikt,
        tenzij je ook nog aanvinkt **Aanmelden met** en **Wachtwoord
        onthouden**. Outlook 2003 heeft deze fout niet.

        {{< figure src="/img/old/outlook-nl-auth.jpg" title="Tabblad Server voor uitgaande e-mail" >}}

-   Ga na naar het tabblad **Geavanceerd** en pas aan:

    \* Uitgaande e-mail (SMTP): **25** of **587** (Zie [\|Algemene
    instellingen] voor meer informatie.)

    -   Kies TLS als encryptiemethode voor de uitgaande mail (let op:
        sommige versies van Outlook noemen die SSL!).

        {{< figure src="/img/old/outlook-nl-tls.jpg" title="Tabblad Geavanceerd" >}}

        In een modernere versie van Outlook, ziet dat er zo uit:

{{< figure src="/img/old/outlook2010-nl-tls.png" title="Tabblad Geavanceerd" >}}

## Instellen van Outlook Express

-   Kies **Extra → Accounts…**

-   Ga naar het tabblad **E-mail**, selekteer het account en klik op
    **Eigenschappen**:

    {{< figure src="/img/old/outlookexpress-nl-accounts.jpg" title="Internet-accounts" >}}

-   Ga in het volgende venster naar het tabblad **Servers** en pas aan:

    \* Uitgaande e-mail (SMTP): **smtp.science.ru.nl**

    -   Accountnaam: *loginnaam* invullen.

    -   **Niet aanvinken**: Aanmelden met
        beveiligd-wachtwoordverificatie.

    -   **Wel aanvinken**: Voor deze server is verificatie vereist.

    -   Klik eventueel nog op **Instellingen…** om de
        aanmeldingsgegevens aan te passen.

        {{< figure src="/img/old/outlookexpress-nl-servers.jpg" title="Tabblad Servers" >}}
        {{< figure src="/img/old/outlookexpress-nl-auth.jpg" title="Aanmeldingsgegevens" >}}

-   Ga nu naar het tabblad **Geavanceerd** en pas aan:

    \* Uitgaande e-mail (SMTP): **25** of **587** (Zie [\|Algemene
    instellingen] voor meer informatie.)

    -   **Vink aan:** Voor deze server is een gecodeerde verbinding
        vereist (SSL).

        {{< figure src="/img/old/outlookexpress-nl-tls.jpg" title="Tabblad Geavanceerd" >}}

## Instellen van Eudora

`Gebruik een recente versie van Eudora, minimaal versie 6 of hoger. Hoe ouder de versie van Eudora, hoe meer ellende met certificaten.`

-   Kies **Tools → Options…** en klik op **Sending Mail** in de
    linkerkolom.

-   Vul in of vink aan:

    \* Het e-mailadres.

    -   SMTP server: **smtp.science.ru.nl**

    -   **Aanvinken**: Allow authentication

    -   Indien aanwezig zou aangevinkt kunnen worden: Use submission
        port (587)\
        Zie [\|Algemene instellingen] voor meer informatie over de
        submission poort.

    -   Kies **If Available, STARTTLS** uit het menu onderin het
        venster.

        {{< figure src="/img/old/eudora-smtp.jpg" title="Sending Mail Options" >}}

-   Klik nu op het **New Message** pictogram of gebruik ctrl-N en
    probeer een e-mail naar jezelf te sturen. Eudora zal waarschijnlijk
    gaan klagen over certificaten:

    {{< figure src="/img/old/eudora-cert-reject.jpg" title="Server SSL Certificate Rejected" >}}

    Klik op **Yes**. Vanaf nu zou Eudora niet meer over certificaten
    moeten klagen bij het versturen van e-mail.

-   Oudere versies van Eudora zullen andere foutmeldingen over
    certificaten blijven geven en zullen weigeren om berichten te
    versturen. Als dit het geval is, dan moet het volgende worden
    gedaan:

    \* Ga naar het meest rechtse tabblad in de linkerkolom van Eudora.
    Dit tabblad heet **Personalities**. Klik met de rechter muisknop op
    de **Dominant** identiteit en kies **Properties**:

    {{< figure src="/img/old/eudora-last-ssl-info.jpg" title="Last SSL Info" >}}

    \* Klik op de knop **Last SSL Info**.

    -   In het volgende venster, klik op **Certificate Information
        Manager**.

    -   In het volgende venster, selekteer het certificaat dat Eudora
        nog niet vertrouwt en klik op **Add To Trusted**:

        {{< figure src="/img/old/eudora-cert-manager.jpg" title="Certificate Information Manager" >}}

-   Bij nog oudere versies van Eudora kan het ook nog nodig zijn om [de
    Microsoft (DER) versie van het C&CZ root
    certificaat](http://certificate.science.ru.nl/cacert.der) te
    importeren en toe te voegen aan de vertrouwde Eudora certificaten.
    Beter is om naar een meer recente versie van Eudora over te gaan.

## Instellen van Pine

`Gebruik `**`S`**` voor SETUP en daarna `**`C`**` voor Config en vul in:`

**``` smtp-server``   ``=``   ``smtp.science.ru.nl/user= ```***`loginnaam`*\
` Als pine wordt gebruikt op een niet-facultaire Sun, dan zal mogelijk het CA certificaat niet worden gevonden. Het makkelijkste is dan om bij de smtp-server de optie `**`/novalidate-cert`**` op te nemen, dus:`

**``` smtp-server``   ``=``   ``smtp.science.ru.nl/novalidate-cert/user= ```***`loginnaam`*

## Instellen van KMail

-   Kies in KMail **Instellingen → KMail instellen…**, selekteer
    **Accounts** en ga dan naar het tabblad **Verzenden** en klik dan op
    **Wijzigen…**.

-   Op het tabblad **Algemeen** invullen of aanvinken:

    {{< figure src="/img/old/kmail-nl-smtp.jpg" title="SMTP Algemeen" >}}

-   Op het tabblad **Beveiliging** aanvinken:

    {{< figure src="/img/old/kmail-nl-tls.jpg" title="SMTP Beveiliging" >}}

## Instellen van OS X Mail

-   Trek het menu **Mail** open en kies **Voorkeuren…**

-   Klik op **Accounts** (niet bij mail v1.0).

-   Selekteer het bestaande e-mail account in het linkerdeel van het
    venster en klik daarna op **Serverinstellingen…**:

    {{< figure src="/img/old/macosx-accounts.jpg" title="Mac OS X Accounts" >}}

-   Pas onderstaande instellingen aan:

    \* Server uitgaande post: **smtp.science.ru.nl**

    -   Serverpoort: **25** of **587** (Zie [\|Algemene instellingen]
        voor meer informatie.)

    -   **Aanvinken**: Gebruik Secure Sockets Layer (SSL)

    -   **Identiteitscontrole**: Wachtwoord

    -   Vul *loginnaam* en wachtwoord in.

        {{< figure src="/img/old/macosx-smtp.jpg" title="Mac OS X SMTP Opties" >}}
