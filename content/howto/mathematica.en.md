---
author: petervc
date: '2021-12-05T23:34:06Z'
keywords: []
lang: en
tags:
- software
title: Mathematica
wiki_id: '150'
---
[Mathematica](http://www.wolfram.com/products/mathematica/) is a mathematical software package made
by [Wolfram Research](http://www.wolfram.com/).

C&CZ manages the network license for the concurrent use
of Mathematica within Radboud University.

Until the end of 2023, there temporarily is an unlimited license for all RU staff and students.

One can get the
installation and license information from [C&CZ system
administration](/en/howto/systeemontwikkeling/).

The use of Mathematica is logged by the license server.

On the Linux PCs that are managed by C&CZ, Mathematica can be found in
/vol/mathematica. On MS-Windows PC’s Mathematica can be found on the
S-disk (software).
