---
author: bram
date: '2022-10-08T12:38:24Z'
keywords: []
lang: nl
tags: []
title: LimeSurvey
wiki_id: '905'
cover:
  image: img/2022/limesurvey.png
---
# Over LimeSurvey

Hoewel online enquêtes vooral bekend zijn als marketing tool, kun je ze
veel breder inzetten. Denk bijvoorbeeld aan inschrijf-formulieren voor
conferenties, verzamelen van research data, bestelformulieren etc. C&CZ
biedt studenten en medewerkers de mogelijkheid online enquêtes te
houden. We maken hiervoor gebruik van
[LimeSurvey](https://community.limesurvey.org/).

LimeSurvey is een open source web-applicatie. Een greep uit de feature
list:

-   Verzenden van uitnodigingen, herinneringen per email
-   Meertalige enquêtes
-   Logica in de vragenlijst (vragen wel of niet afbeelden als…)
-   Film- of beeldmateriaal in de enquête gebruiken

## Inloggen

Dit is de login-pagina van het beheer-interface van LimeSurvey:

<https://u1.survey.science.ru.nl/admin/>

Het is niet nodig om toegang te vragen: alle Science accounts kunnen
inloggen.

## Gebruiksregels

Regel de volgende zaken voor het activeren een enquête:

-   Geef een enquête mail-adres op (Algemene instellingen \> E-mail
    Beheerder). Vraag desgewenst een specifiek mail alias bij ons aan.
-   Stel een vervaldatum in (Algemene instellingen \> Publicatie &
    Toegangscontrole \> Vervaldatum/tijd). Dit helpt ons om te zien of
    er nog enquêtes actief zijn.

## Een enquête voorbereiden

Na het inloggen op het beheer-interface van LimeSurvey, heb je de
mogelijkheid om nieuwe enquêtes te maken, bestaande te bewerken en
resultaten te bekijken / downloaden. Hier volgen een aantal tips om snel
wegwijs te worden in LimeSurvey:

### Getrapte weergave

De structuur van een enquête in LimeSurvey is getrapt: enquête \>
vraaggroepen \> vragen. Rechtsboven in de beheer-interface vind je een
keuzelijst van enquêtes, vraaggroepen of vragen.

### Vraaggroepen

De algemene werkwijze na het aanmaken van een enquête is: een of meer
vraaggroepen toevoegen en dan vragen toevoegen. Het is niet mogelijk om
vragen direct aan een enquête toe te voegen zonder tussenkomst van
vraaggroepen. Vraaggroepen zijn verplichte kost! Dus eerst een
vraaggroep toevoegen en daarbinnen de vragen.

### Online documentatie

Er is prima online documentatie beschikbaar. Begin met lezen hier op
[docs.limesurvey.org](http://docs.limesurvey.org/Creating+surveys+-+Introduction&structure=English+Instructions+for+LimeSurvey).
Deze link wijst specifiek naar het gedeelte waar wordt uitgelegd hoe je
enquêtes aanmaakt.

### Spoedcursus van 10 minuten op Youtube

[Deze heldere
video-instructie](https://www.youtube.com/watch?feature=player_detailpage&v=KWE0wAoyvak)
laat zien hoe je je eerste enquête aanmaakt.

[Bekijk op
youtube](https://www.youtube.com/watch?feature=player_detailpage&v=KWE0wAoyvak)
