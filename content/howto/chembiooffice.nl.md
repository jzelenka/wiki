---
author: polman
date: '2022-04-05T11:52:47Z'
keywords: []
lang: nl
tags:
- software
title: ChemDraw / ChemBioOffice
wiki_id: '690'
---
### ChemOffice+ Cloud Standard/Signals Notebook Bundle License will expire on April 5 2023.

Met ingang van December 2022 is de ChemOffice + licentie vervangen voor een ChemDraw Prime campus licentie

ChemDraw Prime is compleet “entry-level” tekenprogramma voor moleculaire structuren, experimentele opstellingen, en TLC plaat teken tools . 

ChemDraw Prime Bevat onder andere de volgende functies:

- Analyze and check structures
- Structure clean-up
- Expand and contract labels
- Create and use nicknames
- Tetrahedral and geometric stereochemistry, including absolute and relative
- Multicenter attachment points for haptic and other pi bonds
- Chemical polymer tools
- Calculate properties including pKa, LogP, LogS and tPSA and hotlink to structure
- Read and write all common chemical and graphics files
- Read JCamp and Galactic spectra files
- Fragmentation tools
- Special “copy/paste as” command for CDX, CDXML, molfile, SMILES, InChI and InChIKey (copy only)
- In-place OLE editing of ChemDraw objects


    
    




Dit softwarepakket wordt veelvuldig gebruikt binnen verschillende chemische onderzoeksgroepen
van de Faculteit NWI en in het onderwijs van de moleculaire opleidingen. Door de
gebruiksvriendelijkheid van de software is het ook zeer geschikt voor
studenten. 

Iedereen met een e-mail adres eindigend op @ru.nl, @science.ru.nl of
@student.ru.nl is gerechtigd de software te downloaden en 3 keer te
installeren.

De installatie bestanden voor ChemDraw Prime (Windows en Mac-OS) zijn te downloaden op de
website van PerkinElmer:

**Nieuwe gebruikers**

Als nieuwe gebruiker moet je je eerst registeren bij PerkinElmer via

<https://informatics.perkinelmer.com/sitesubscription>

- Zoek Radboud University
- **Belangrijk** Kies voor de **bovenste** van de twee register opties. 
- Vul je naam, het adres van de faculteit en radboud emailadres in. en klik op “Submit”
- Na het versturen van de data ontvang je een verificatie email op het opgegeven email adres.
- Klik op “Create Account” 
- Maak op de “Sign Up” tab een nieuw account aan. Gebruik hiervoor je radboud emailadres en een zelfgekozen wachtwoord. Klik “Sign Up”
- Na korte tijd wordt je doorgelinked naar “List Entitlements”, deze toont de actieve licenties.
- Noteer of kopieer de 'Activation code' (deze heb je nodig tijdens de installlatie) en klik op de “Download Now” link in de laatste column van de ChemDraw Prime entry.
- Download de installatie bestanden voor het gewenste operating system.

**Bestaande gebruikers**

De Chemdraw license is overgegaan op een nieuwe prime license. Ook bestaande gebruikers moeten zich opnieuw registreren. 

Volg eerst de volgende stappen:

1. Ga naar https://informatics.perkinelmer.com/sitesubscription.
2. Zoek naar  Radboud University.
3. Registreer via de **bovenste** link (je moet echt opnieuw registreren, gebruik hetzelfde mailadres als eerder)
   Registreer om de meest recente versie van ChemDraw op te halen:
    Mnova ChemDraw Edition (available with ChemOffice+ Cloud)
4. Vul het formulier in en klik op submit.
5. Wacht op de verificatie email (komt misschien niet) of log in op https://perkinelmerinformatics.flexnetoperations.com
6. Download het pakket.
7. Installeer.

Vervolgens moet je de oude licentie deactiveren en de nieuwe activeren (dit is misschien niet meer nodig na 5 april):

8. Start de nieuwe versie van ChemDraw.
9. voor Mac gebruikers: Klik op  “ChemDraw" -> "Activate ChemDraw". Er verschijnt een  window met de oude licentie. Klik op “Deactivate”.
9. voor Windows gebruikers: Klik op “Help" -> "Activate ChemDraw". Er verschijnt een window met de oude  licentie. Klik op “Deactivate”.
10. Type de nieuwe code (die je in stap 5 hebt gekregen) in. Klik op “Activate” en dan zou de niewue versie van ChemDraw moeten werken tot 5 April 2026.
