---
author: remcoa
date: '2011-08-30T08:56:34Z'
keywords: []
lang: nl
tags:
- wiki
title: NieuweWiki
wiki_id: '682'
---
## Configuratie

Voor iedere wiki is er een apart LocalSettingsExtra.php
bestand waarmee je de standaard instellingen van je wiki kunt
veranderen. Dit bestand staat in /vol/wikis/JOUWIKI/ op UNIX logins
servers (solo, porthos) en is ook beschikbaar op Windows pc’s via
\\\\wikiconf\\wikisettings\\JOUWIKI Het kan een minuut duren voordat
eranderingen in dit bestand zichtbaar zijn in je wiki. Het is verstandig
dit bestand een keer door te lezen om een idee te krijgen voor wat er
mogelijk is. Merk op dat (syntax) fouten in dit bestand er toe kunnen
leiden dat het helemaal genegeerd wordt.

### Logo

Om je wiki een eigen logo te geven moet je de volgende stappen uitvoeren:

-   Converteer je logo naar een formaat van maximaal 135x135 pixels
-   Upload je logo bestand naar je wiki (Upload bestand in Hulpmiddelen)

Bepaal de lokatie van het bestand door op de link te klikken net onder
*Geen hogere resolutie beschikbaar* en net boven *Bestandsgeschiedenis*.
De link zal er typisch als volgt uitzien:
https://wiki.science.ru.nl/JOUWIKI/images/8/82/youlogo.jpg

-   gebruik deze link in je LocalSettingsExtra.php bestand:

``` php
 $wgLogo = "/JOUWIKI/images/8/82/youlogo.jpg";
```

Merk op dat je alles moet verwijderen voor de /JOUWIKI in de verkregen
link.

### Extensies

-   [Overzicht van lokaal geïnstalleerde extensies](/nl/howto/geïnstalleerde-mediawiki-extensies/) die je zo
    kunt gebruiken.
-   [Overzicht van beschikbare mediawiki extensies](http://www.mediawiki.org/wiki/Extension_Matrix), deze
    moeten mogelijk nog lokaal geïstalleerd worden. Een verzoek hiertoe
    kun je richten aan postmaster@@science.ru.nl .

### Default Skin

Om de lijst van beschikbare vormgevingen te zien log je in
op je wiki, gaat naar *mijn voorkeuren* en selecteer de Vormgeving Tab.
Je kunt een voorvertoning van de beschikbare vormgevingen krijgen door
op de corresponderende link te klikken.

Om een van de beschikbare vormgevingen als standaard voor *nieuwe* en
anonieme (niet ingelogde) gebruikers vast te leggen verander je de
waarde van de variabele *\$wgDefaultSkin* in je LocalSettingsExtra.php
bestand

``` php
 $wgDefaultSkin = 'fnwi';
```

vervang de waarde door de naam van de vormgeving (in kleine letters).

### User management

## Help
