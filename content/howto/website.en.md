---
author: petervc
date: '2022-06-24T12:55:42Z'
keywords: []
lang: en
tags:
- studenten
- medewerkers
- contactpersonen
title: Website
wiki_id: '910'
---
There are three major types of web sites in use at the Faculty of
Science:

-   LAMP (Linux, Apache, MySQL, PHP/Perl/Python) and MediaWiki based
    general purpose web servers (possibly offering some high level
    content management system). These sites are hosted, managed and
    supported by C&CZ. All content management tasks are performed by the
    respective web site owners: institutes and scientific departments,
    student organisations, service departments, etc. Please contact C&CZ
    for more information about [WWW\_Services](/en/howto/www-service/).
-   Externally hosted web sites (including sites hosted by ILS) for
    which custom management and support agreements may be in place.
-   Web sites based on the Radboud University’s Content Management
    System ([CMS](/en/howto/cms/)) (also known as IPROX, which is the
    name of the management software suite).
