---
author: wim,miek
date: 2022-12-01T11:07:29Z
noage: true
keywords: []
lang: nl
tags:
- internet
title: Mysql php security
wiki_id: '65'
---
## Mysql en php, password protectie

Een veel gebruikte combinatie bij het maken van een website is php met
mysql als database backend. Daarbij doet zich het probleem voor dat de
website een connectie met de database moet kunnen maken. Daarvoor is een
mysql account en wachtwoord nodig waar de webserver over moet kunnen
beschikken. Een veel gebruikte constructie daarvoor is het opslaan van
account naam en wachtwoord in een bestand ergens in de web-docs
directory boom dat door het php script ingelezen wordt dat de connectie
met de mysql server maakt. Dit is een erg onveilige oplossing omdat
noodzakelijkerwijs dit bestand leesbaar moet zijn voor de webserver. Dat
betekent op een webserver die meerdere websites host dat het bestand
leesbaar moet zijn voor de user www, in de praktijk wordt er meestal
voor gekozen het bestand leesbaar te maken voor de wereld. Dat betekent
dat iedereen die een Science login heeft dit bestand kan lezen als
hij/zij weet waar te kijken. Daarnaast is er het risico dat het bestand
via de webserver opvraagbaar is.

Er is een oplossing voor dit probleem, i.p.v. een wachtwoord in een
bestand te zetten kun je je wachtwoord versleutelen met behulp van een
voor de website specifieke geheime sleutel. In de php code waarmee je de
connectie met de mysql server maakt vervang je de regel waarin je het
wachtwoord zet:\
`$admin-password = "plaintextwachtwoord";`\
door\
`## EXTRA constructie om wachtwoord te beveiligen`\
`require_once( "passwd-crypt.inc.php" );`\
`$admin_password = decrypt("VERSLEUTELD WACHTWOORD");`\
waar “VERSLEUTELD WACHTWOORD” natuurlijk vervangen moet worden door de
versie die je van C&CZ hebt ontvangen. Denk eraan dat als deze
toewijzing in een class gebeurt je de toewijzing in de constructor van
de class moet plaatsen. Kopieer de onderstaande tekst in een bestand
passwd-crypt.inc.php

``` php
<?php

function hex2bin($hexdata) {
$bindata = '';
for ($i=0;$i<strlen($hexdata);$i+=2) {
$bindata.=chr(hexdec(substr($hexdata,$i,2)));
}
return $bindata;
}

function decrypt($crypted_admin_password) {
// secret krijg je van Apache uit website specifiek vhost file
$secretkey = $_SERVER["SECRET"];

$td = mcrypt_module_open('rijndael-256', '', 'ecb', '');

mcrypt_generic_init($td, $secretkey, NULL);
$admin_password = rtrim( mdecrypt_generic($td, hex2bin($crypted_admin_password)), '\0');
mcrypt_generic_deinit($td);
mcrypt_module_close($td);

return $admin_password;
}

?>
```

Indien je van deze constructie gebruik wilt maken stuur dan een mail
naar Postmaster
[[mailto:postmaster\@science.ru.nl?subject=SecureMysql&body=Database:%0DWebsite](mailto:postmaster@science.ru.nl?subject=SecureMysql&body=Database:%0DWebsite):]
met vermelding van de database en website waarvoor je dit wilt.
