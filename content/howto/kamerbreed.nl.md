---
author: stefan
date: '2019-07-11T06:55:08Z'
keywords: []
lang: nl
tags: []
title: Kamerbreed
wiki_id: '917'
---
{{< notice info >}}
De posterprinter is tijdelijk buiten gebruik. Zie [Posterprinter kamerbreed defect](../../cpk/1321)
{{< /notice >}}

Bij C&CZ kunnen grootformaat afdrukken worden gemaakt op een ‘HP
DesignJet Z3200ps 44" Photo’ poster printer, op 3 soorten materiaal:

-   hoge kwaliteit fotopapier (HP Everyday Pigment Ink Satin Photo),
    maximale breedte 110 cm, prijs € 24 voor een A0 afdruk (1
    m{{< sub "2" >}}).
-   canvasdoek (HP Artist Matte Canvas), maximale breedte 91 cm, prijs €
    36 voor een A0 afdruk (1 m{{< sup "2" >}}). Een
    canvasprint laat minder snel vouwen zien dan een papieren print, kan
    zelfs voorzichtig gevouwen worden en in de handbagage meegenomen
    worden.
-   stickers (HP Everyday Adhesive Matte Polypropylene), maximale
    breedte 105 cm, prijs € 30 voor een A0 afdruk (1
    m{{< sup "2" >}}).

De printer wordt bediend door [C&CZ](/nl/howto/contact/), prints kunnen
dus niet via het netwerk naar de printer worden gestuurd.

De kosten voor een afdruk worden bepaald door de afmetingen van de
bedrukte oppervlakte: er wordt afgerekend in
mm{{< sup "2" >}}. Voor vervoer/bescherming zijn ook
kokers te koop voor 1.50 euro/stuk. De afrekening vindt plaats van uw
huidige [printbudget](/nl/howto/printbudget/) (persoonlijk of van een
afdeling).
