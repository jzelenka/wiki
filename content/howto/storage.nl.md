---
author: bram
date: '2023-01-12T12:36:19Z'
keywords: []
lang: nl
tags:
- storage
- ceph
title: Data opslag
wiki_id: '48'
aliases:
- diskruimte
cover:
  image: img/2023/storage.jpg
ShowToc: true
TocOpen: true
---
# Introductie

In onze faculteit worden verschillende soorten dataopslag aangeboden:

| Storage klasse                       | Beschrijving                                                                                                                                                           | Risico op dataverlies |
| ------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------- |
| [Home directories](#homedirectories) | Klein (enkele GB), betrouwbaar, gebackedupte opslag voor individuen                                                                                                    | laag                  |
| [Netwerk schijven](#netwerkschijven) | Groter (< 1TB), betrouwbaar,gebackedupte opslag voor individuen en groepen                                                                                             | laag                  |
| [Ceph storage](#ceph-storage)        | Grootschalig, betrouwbaar, ge-snapshotte, maar niet gebackupte opslag voor groepen                                                                                     | middel                |
| [Lokale storage](#local-storage)     | Opslag zonder back-up op desktopcomputers en clusternodes, die waarschijnlijk verloren gaat bij een hardwareprobleem of wanneer de machine opnieuw wordt geïnstalleerd | hoog                  |


# Homedirectories
Bij je [Science account](../login) hoort standaard een homedirectory van 5GB. Hiervoor worden geen kosten in rekening gebracht. Dit is een veilige plek om je werkgerelateerde documenten op te slaan. Homedirectories staan op degelijke hardware,
en worden automatisch [gebackupped](../backup). Als je meer opslagruimte nodig hebt, kunnen we je homedirectory op [verzoek](../contact) vergroten.

## Homedirectory paden
Op door C&CZ beheerde systemen is je homedirectory als volgt te vinden:

| C&CZ systemen     | Pad                                |
| :---------------- | :--------------------------------- |
| Microsoft Windows | `U:` ook wel de `U-schijf` genoemd |
| Linux             | `/home/jescienceloginnaam` of `~`  |

Vanaf andere systemen is je homedirectory middels de volgende paden te vinden:
| Eigen systeem     | Pad                                            | Instructies                                         |
| ----------------- | ---------------------------------------------- | --------------------------------------------------- |
| Microsoft Windows | `\\home1.science.ru.nl\jescienceloginnaam`     | [schijf koppelen](../mount-a-network-share#windows) |
| macOS             | `smb://home1.science.ru.nl/jescienceloginnaam` | [schijf koppelen](../mount-a-network-share#macos)   |
| Linux             | `smb://home1.science.ru.nl/jescienceloginnaam` | [schijf koppelen](../mount-a-network-share#linux)   |

{{< notice info >}}
Voor toegang tot deze paden van buiten het RU-netwerk is een [VPN](../vpn) verbinding vereist.
{{< /notice >}}

{{< notice info >}}
De locatie van je homedirectory, bijvoorbeeld `home1` of `home2`, kan bekeken worden op [DHZ](../dhz).
{{< /notice >}}

## Toegangsrechten
Homedirectories zijn standaard alleen toegankelijk voor de eigenaren zelf. Het is echter mogelijk
om de toegangsrechten te wijzigen (bijvoorbeeld `chmod o+rx ~`).

# Netwerkschijven
Voor grotere opslagbehoeften en de mogelijkheid om bestanden in je groep te delen, zijn er netwerkschijven.

Netwerkschijven zijn beschikbaar als _Samba[^samba]_- en _NFS[^nfs]-share_. Zoals vermeld in het overzicht, worden
netwerkschijven geback-upt en gehost op betrouwbare servers. De kosten van netwerkschijven zijn afhankelijk
van de schijfgrootte en de [backup-opties](../backup).

[^nfs]: Network file system, https://nl.wikipedia.org/wiki/Network_File_System
[^samba]: Windows file sharing implementatie door het [Samba project](https://www.samba.org/)

## Kosten van een netwerkschijf
Netwerkschijven tot 1TB zijn beschikbaar met een standaard backups.
Network shares up to 1TB are available with our standard backup plan. Voor de kosten van een aangepast backup-schema, raadpleeg de [backup-pagina](../backup). Dit zijn de jaarlijkse kosten voor netwerkschijven:

| Grootte             |                 Met backup | Zonder backup |
| :------------------ | -------------------------: | ------------: |
| 200 GB              |                       € 40 |          € 10 |
| 400 GB              |                       € 80 |          € 20 |
| 1 TB[^what_is_a_tb] | € 200 [^small_file_notice] |          € 50 |
| > 1 TB              |           niet beschikbaar |     € 50 / TB |

[^what_is_a_tb]: 1TB is `1.000.000.000.000` bytes
[^small_file_notice]: Dagelijkse backup is niet mogelijk bij veel veranderende data of zeer veel kleine bestanden.

## Paden voor netwerkschijven
Op door C&CZ beheerde systemen zijn netwerkschijven als volgt te vinden. Met `sharename` als voorbeeld:
 | C&CZ systemen     | Pad                                       | Instructies                                         |
 | :---------------- | :---------------------------------------- | --------------------------------------------------- |
 | Microsoft Windows | `\\sharename-srv.science.ru.nl\sharename` | [schijf koppelen](../mount-a-network-share#windows) |
 | Linux             | `/vol/sharename`                          |                                                     |

Voor andere systemen zijn netwerkschijven als volgt te vinden:
 | Eigen systeem     | Pad                                           | Instructies                                         |
 | :---------------- | :-------------------------------------------- | --------------------------------------------------- |
 | Microsoft Windows | `\\sharename-srv.science.ru.nl\sharename`     | [schijf koppelen](../mount-a-network-share#windows) |
 | macOS             | `smb://sharename-srv.science.ru.nl/sharename` | [schijf koppelen](../mount-a-network-share#macos)   |
 | Linux             | `smb://sharename-srv.science.ru.nl/sharename` | [schijf koppelen](../mount-a-network-share#linux)   |

{{< notice info >}}
Voor toegang tot deze paden van buiten het RU-netwerk is een [VPN](../vpn) verbinding vereist.
{{< /notice >}}

## Toegangsrechten
Toegangsrechten voor netwerkschijven is op basis van groepen.
Groepen kunnen worden beheerd door de groepseigenaren op [DHZ](/en/howto/dhz/).

## Speciale netwerkschijven
Een aantal algemene diensten maakt gebruik van netwerkschijven:
| Schijf     | Doel                              | Documentatie                      |
| ---------- | --------------------------------- | --------------------------------- |
| `temp`     | Tijdelijke opslag van bestanden   | [Temp share](../temp-share)       |
| `cursus`   | Software voor cursussen           | [T-schijf](../t-schijf)           |
| `software` | Algemene (Windows) software       | [S-schijf](../s-schijf)           |
| `install`  | Installatiebestanden van software | [Install share](../install-share) |

## Bestellen
Bij het bestellen van een netwerkschijf, graag de volgende details aan [Postmaster](../contact) doorgeven:

| Detail                 | Beschrijving                                                                                                                             |
| ---------------------- | ---------------------------------------------------------------------------------------------------------------------------------------- |
| loginnaam              | Je [Science account](../login)                                                                                                           |
| schijfnaam             | Waarmee de schijf over het netwerk gevonden kan worden, zie [paths](#paden-voor-networkschijven)                                         |
| grootte                | grootte van de partitie, zie [kosten](#kosten-van-een-netwerkschijf)                                                                     |
| kostenplaats           | _kostenplaats_ of project code                                                                                                           |
| unix groep (optioneel) | De groep die toegang tot de schijf moet hebben. Indien niet opgegeven, wordt er een nieuwe groep gemaakt met dezelfde naam als de schijf |


# Ceph storage
Met ons [Ceph](https://en.wikipedia.org/wiki/Ceph_(software)) storage cluster kunnen we veel grotere opslag bieden voor onze faculteit.
Het storage cluster bestaat uit meerdere diskservers en is verspreid over drie locaties op de campus.

{{< notice warning >}}
**Ceph volumes worden niet gebackupped!** Het is daarvoor te groot.
{{< /notice >}}

## Keuzes in redundantie
Ceph biedt verschillende redundantie-opties voor het opslaan van data.
Standaard slaat Ceph gegevens in drievoud op: de `3copy` pool.
Dus wanneer één kopie verloren gaat, hebben de overige exemplaren nog steeds redundantie.
Een andere redundantiemogelijkheid is _erasure coding_ (EC). EC vereist veel minder opslagoverhead in vergelijking met 3copy.

## Kosten van Ceph
De kosten zijn afhankelijk van de gekozen storagepool.
Onderstaande prijzen zijn per TB[^what_is_a_tb] per jaar:
| Pool    | Beschrijving                                                                                                                                                                             | zonder backup |
| :------ | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------: |
| `EC8+3` | goedkoopste, als één datacenter wordt vernietigd, gaan alle gegevens verloren. Wanneer één datacenter tijdelijk niet beschikbaar is, zijn de gegevens nog steeds veilig, echter offline. |          € 50 |
| `EC5+4` | blijft beschikbaar wanneer een heel datacenter offline is of wegvalt. Data blijft veilig zolang twee datacenters goed werken.                                                            |          € 60 |
| `3copy` | sneller lezen + schrijven, blijft beschikbaar wanneer één datacenter niet meer beschikbaar is.                                                                                           |         € 100 |

{{< notice info >}}
De prestatie-eigenschappen van Ceph verschillen van traditionele serveropslag. De schrijfsnelheid is meestal hoger dan de leessnelheid. Het opslaan van veel kleine
bestanden is rampzalig voor de snelheid (zelfs erger dan bij traditionele opslag).
{{< /notice >}}

Ceph-bestandssystemen kunnen worden gebruikt als een gewone netwerkschijf of zijn toegankelijk voor sommige servers of clusterknooppunten die directe Ceph-toegang hebben: _Ceph-clients_. Zie de volgende alinea over [Ceph paden](#ceph-paden).

Objectopslag (compatibel met [S3](https://en.wikipedia.org/wiki/Amazon_S3)) is ook beschikbaar. Objectopslag verschilt fundamenteel van een traditioneel bestandssysteem, gegevens die zijn opgeslagen in een Ceph-bestandssysteem zijn niet toegankelijk via S3.

## Ceph paden

 | C&CZ systemen               | Pad                                       | Instructies                                         |
 | :-------------------------- | :---------------------------------------- | --------------------------------------------------- |
 | Microsoft Windows           | `\\sharename-srv.science.ru.nl\sharename` | [schijf koppelen](../mount-a-network-share#windows) |
 | Linux                       | `/vol/sharename`                          |                                                     |
 | Ceph clients[^ceph-clients] | `/ceph/sharename`                         |                                                     |

[^ceph-clients]: Ceph clients zijn door C&CZ beheerde systemen met directe toegang tot het Ceph-cluster

{{< notice info >}}
De paden voor het koppelen van Ceph storage op eigen systemen, zijn hetzelfde opgebouwd als die van traditionele [netwerkschijven](#paden-voor-netwerkschijven).
{{< /notice >}}

## Bestellen
Bij het bestellen van een Ceph storage, graag de volgende details aan [Postmaster](../contact) doorgeven:

| Detail      | Description                                                                                                                              |
| ----------- | ---------------------------------------------------------------------------------------------------------------------------------------- |
| loginnaam   | Je [Science account](../login)                                                                                                           |
| schijfnaam  | Waarmee de schijf over het netwerk gevonden kan worden, zie [paden](#ceph-paden)                                                         |
| pool        | Zie [keuzes in redundantie](#keuzes-in-redundantie) en [kosten](#kosten-van-ceph)                                                        |
| grootte     | disk quota op het Ceph-volume, zie [kosten](#kosten-van-ceph)                                                                            |
| kostenplaat | _kostenplaats_ of project code                                                                                                           |
| unix groep  | De groep die toegang tot de schijf moet hebben. Indien niet opgegeven, wordt er een nieuwe groep gemaakt met dezelfde naam als de schijf |


# Lokale opslag
Lokale opslag verwijst naar schijfruimte in of gekoppeld aan systemen zoals desktop-pc's en clusternodes. Meestal bestaat lokale opslag
uit enkel uitgevoerde schijven. Bij een defect aan een schijf is de kans groot dat er gegevens verloren gaan. Lokale schijven worden gewoonlijk `/scratch` genoemd om je te herinneren aan de tijdelijke aard van de opslag. Lokale wordt ***niet gebackupped***. Voordelen van lokale opslag zijn lage kosten en snelle schijftoegang.

{{< notice info >}}
Op gedeelde systemen, zoals loginservers en clusternodes, is het aan te raden eerst een persoonlijke map te maken in `/scratch`. D.w.z. `mkdir /scratch/$USER`. En vergeet niet om je software daadwerkelijk naar die map te laten schrijven in plaats van naar de standaard en veel kleinere `/tmp`.
{{< /notice >}}
