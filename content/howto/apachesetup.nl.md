---
author: bram
date: '2019-09-23T09:01:32Z'
keywords: []
lang: nl
tags: []
title: ApacheSetup
wiki_id: '687'
---
### Apache Setup

Om de beveiliging van websites te verbeteren zijn we over gegaan op een
nieuwe opzet voor onze webservers. Tot voor kort was het gebruikelijk om
op een webserver één apache httpd te draaien die middels apache virtual
hosts meerdere websites afhandelt. Dat heeft een groot voordeel, alle
websites delen de beschikbare pool van httpd processen, maar ook
meerdere (grote) nadelen met name wat betreft de onderlinge beveiliging
tussen de websites en gemeenschappelijke log files.

De nieuwe opzet waarvoor gekozen is, heeft deze nadelen niet. In plaats
van virtual hosts files die door één apache server worden afgehandeld
gebruiken we nu per website een eigen httpd met een aparte user per
website. Dat heeft tot gevolg dat de bestanden in de htdocs directory
niet langer leesbaar hoeven te zijn voor iedere gebruiker op de
webserver. Bovendien gaat iedere website nu op een eigen ip-adres
draaien wat tot gevolg heeft dat iedere website die https nodig heeft
zijn eigen SSL-certificaat kan krijgen.

De directory structuur ziet er als volgt uit (we gebruiken hier
*website* als voorbeeld naam):

-   **/var/www1/website**

top directory waarbinnen alle bestanden m.b.t. deze website zich
bevinden

-   **/var/www1/website/live/htdocs**

De document root voor de website. De standaard situatie voor de htdocs
directory wordt lees- en schrijfbaar voor de unix group met daarin
alleen de users die de website beheren. Daarnaast heeft de user
waaronder de website-specifieke httpd draait alleen leesrechten.

-   **/var/www1/website/test/htdocs**

De document root voor een test versie van de website, om bijvoorbeeld
een nieuwe versie van de website uit te proberen voordat deze *live*
gaat. Deze test-versie is bereikbaar via poort 82, d.w.z. gebruik als
URL: <http://website.science.ru.nl:82/> en poort 4432 voor https, d.w.z.
gebruik als URL: <http://website.science.ru.nl:4432/> (merk op dat de
echte site gebruik maakt van poort 80 voor http en poort 443 voor
https). Lees- en schrijfrechten gelijk aan de live/htdocs directory.

-   **/var/www1/website/live/logs**

De directory waarin de log-bestanden van de httpd voor deze website
terecht komen met als belangrijkste access\_log en error\_log. Merk op
dat wij er sterk op aandringen dat de eigenaren van de website zelf
controleren wat er in met name error\_log terecht komt en daarop actie
ondernemen om de error\_log zoveel mogelijk leeg te houden.

-   **/var/www1/website/test/logs**

Log-directory voor de test-versie van de website

-   **/var/www1/website/live/writable**

Bij voorkeur de enige directory waar httpd in mag schrijven, standaard
maken wij daar ook een *sessions* subdirectory in aan die door php als
session\_save\_path directory gebruikt wordt. Als er een schrijfbare
directory nodig is in de website, maak die dan bij voorkeur in deze
directory aan.

-   **/var/www1/website/live/cgi-bin**

De cgi-bin directory voor de website.

-   **/var/www1/website/test/cgi-bin**

En natuurlijk een voor de test-versie.

Vermijd het gebruik van absolute paden in webpagina’s, een website zou
altijd verplaatst moeten kunnen worden zonder dat daarvoor de pagina’s
aangepast hoeven te worden!

Aankoppelen gaat (onder Windows) via
\\\\website.science.ru.nl\\wwwwebsite, van buiten de campus kan dat
alleen met gebruik van [vpn](/nl/howto/vpn/) of nog sneller en
eenvoudiger met WinSCP naar lilo.science.ru.nl in de directory
/www/website.
