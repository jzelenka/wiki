---
author: polman
date: '2018-08-06T11:30:18Z'
keywords: []
lang: en
tags:
- storage
aliases:
- quota-bekijken
title: Disk quota
wiki_id: '26'
---
Your personal quota only determine the amount of disk space your own
data may occupy and the number of files and folders/directories you may
have on your U:-disk / home directory. Quotas may be active on other
disks as well, but not all disks have quotas. The mail servers also use
quotas to limit the disk space of each user’s mailbox.

## Windows

On Windows desktops where the home directory is available as U: or H:
disk right click on the disk, then choose `Properties` to view the
amounts of used and available personal disk space in kbytes.

For a graphical view of your disk usage you can use `WinDirStat`.
On managed systems this utility is installed at `S:\\windirstat` .

## Unix

On Linux systems use the following command to view the personal quota
and current usage:

` quota`

The output will show lines like:

`Disk quotas for mylogin (uid 207):`\
`Filesystem      usage   quota   limit    timeleft  files  quota  limit    timeleft`\
`/home/mylogin  491585  500000  600000              14901  50000  60000`

-   usage : The amount of disk space in kKB (kilobytes) currently in
    use.\
    quota : The maximum amount of disk space available for you.\
    On Unix systems you may exceed your quota temporarily (up to 7
    days). For Windows however this is an absolute maximum.
-   limit : The absolute maximum amount of disk space you can occupy
    (see above).\
    timeleft : The time left to get below your quota.\
    If you wait too long (i.e. if timeleft shows EXPIRED), you cannot
    write files (new or old) until you’ve cleaned up to get under quota
    again. There is a risk of losing data if you try to write when the
    quota grace period has been exceeded.
-   files : The number of files currently owned by you.\
    quota : The maximum number of files and folders you can have.\
    On Unix systems you may exceed your file quota temporarily (upto 7
    days). Again for Windows this is an absolute maximum.
-   limit : The absolute maximum number of files and folders you can
    have.\
    timeleft : The time left to get below your file quota.\
    If you wait too long (i.e. if timeleft shows EXPIRED) you cannot
    create files anymore until you’ve cleaned up to get under quota
    again.

## How to determine what your disk space is used for

On a Unix login server go to your home directory

` cd`\
` pwd`

The `pwd` command will show something like `/home/mylogin`.

Now type the following command:

` du -sh * .??* | sort -h`

This will produce a list of the files and folders in your home
directory, each preceeded by its size in MB (megabytes), sorted by size.

Another command to sum up disk usage is

` ncdu ~`

## Mail

The [Roundcube webmail service](http://roundcube.science.ru.nl) offers a
convenient way to view your mail quota and current use. The lower left
corner shows the percentage of quota in us. Next to that, clicking
Folder options, lists all folders. Clicking on a folder gives the option
to Click to get the folder size.
