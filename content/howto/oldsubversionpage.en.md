---
author: bram
date: '2016-04-26T10:32:53Z'
keywords: []
lang: en
tags: []
title: OldSubversionPage
wiki_id: '1017'
---
[GitLab](/en/howto/gitlab/) is the successor of our Subversion service!

------------------------------------------------------------------------

## Introduction

[thumb\|alt=Subversion
logo\|Subversion](/en/howto/bestand:subversion.png/)

Subversion (SVN) is an open source revision control system that allows
one or more users to easily share and maintain collections of files.

Although SVN focusses on the software development community it is very
well possible to use it as a general purpose centralized system for
sharing and maintaining information contained in all kinds of data
files.

We have set up a [subversion server for
FNWI](https://svn.science.ru.nl/) including an administrative interface
to manage repositories and repository users.

## Getting started

To be able to use our SVN server you need the following:

-   The URL of the repository;
-   an SVN username and password;
-   an SVN client installed on your computer.

[Contact C&CZ](mailto:postmaster@science.ru.nl) to create a repository
and appoint an owner.

### The repository

For remote access to our SVN repository the HTTPS protocol is used. The
web address (or URL) of our repositories is typically of the form:

  ---------------------------------------------------
  https://svn.science.ru.nl/repos/{repository name}
  ---------------------------------------------------

### WebSVN

[WebSVN](http://www.websvn.info/) offers a view onto your subversion
repositories that’s been designed to reflect the Subversion methodology.
You can view the log of any file or directory and see a list of all the
files changed, added or deleted in any given revision. You can also view
compare two versions of a file so as to see exactly what was changed in
a particular revision. The URL of our WebSVN installation is:

  --------------------------------------------------
  https://svn.science.ru.nl/wsvn/{repository name}
  --------------------------------------------------

### Authentication and Authorization

A subversion user can be owner of one or more repositories. Repository
owners can grant user read or read-write access to the repository. New
svn user accounts can be created if necessary. Account details such as
your full name, email address and password can be changed through the
web interface:

<https://svn.science.ru.nl>

#### Repository owners

We urge repository owners to use science account names for users who
already have a science account and to use account names starting with an
underscore (\_) for external users:

  for science users    external users
  -------------------- ----------------
  {science username}   \_{username}

svn usernames

#### Repository Users

If you have a science login name, the repository owner should have
created an account for you with the same account name. At the moment
your SVN and science accounts are not connected, i.e. you can change the
passwords of both independently. In the future this will probably
change.

When you first connect to the repository your SVN client will prompt you
for your account name and password. Most SVN clients will store/remember
your credentials for subsequent repository access.

### SVN client software

For all major platforms, the svn command line client is either available
as part of the distribution or can [downloaded
seperately](http://subversion.apache.org/packages.html). To determine if
you have subversion installed on your system, type:

`$ svn --version`

There are many graphical clients available, which are perhaps easier or
more intuitive to use.

#### Graphical SVN clients

  --------------------------------------------------------------------------------------------------------------------------------------------------
  Platform   Application                              at FNWI                                      Description
  ---------- ---------------------------------------- -------------------------------------------- -------------------------------------------------
  Windows    [TortoiseSVN](http://tortoisesvn.net/)   Optional for [Managed                        TortoiseSVN is a Windows shell extension, which
                                                      PC’s](/en/howto/windows-beheerde-werkplek/)   gives feedback on the state of versioned items by
                                                                                                   adding overlays to the icons in the Windows
                                                                                                   Explorer. Repository commands can be executed
                                                                                                   from the enhanced context menu provided by
                                                                                                   Tortoise; GPL license.

  Linux      [RapidSVN](http://www.rapidsvn.org/)     Ubuntu systems                               It’s a while since the latest release. Still one
                                                                                                   of the easiest to use open source SVN gui’s for
                                                                                                   Linux and OSX; GPL license.

  Mac OSX    \-                                                                                    
  --------------------------------------------------------------------------------------------------------------------------------------------------

Suggested Graphical SVN clients

## Using SVN

O’Reilly has published “Version control with Subversion” under the
Creative Commons Attribution License. It’s available in different
formats from <http://svnbook.red-bean.com/>.

## F.A.Q.

#### How do I login to the Administrative Interface?

Visit <https://svn.science.ru.nl/>; Click ‘Authenticate’.

#### I forgot my password, how can I reset it?

If you forgot your password you can reset it with the *“Forgot your
password?”* option. You have to specify your login name and the exact
e-mail address as registered in the SVN database. You’ll recieve an
email with a new password. If this fails, please [contact
C&CZ](/en/howto/contact/).
