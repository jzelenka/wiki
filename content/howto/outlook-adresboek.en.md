---
author: remcoa
date: '2011-06-10T14:40:39Z'
keywords: []
lang: en
tags:
- email
title: Outlook adresboek
wiki_id: '493'
---
Lees het volgende document over hoe het LDAP adresboek in te stellen en
te gebruiken is:
[Media:Outlook-LDAP-Adresboek.pdf](/download/old/outlook-ldap-adresboek.pdf)

------------------------------------------------------------------------

```{=html}
<table>
```
```{=html}
<tbody>
```
```{=html}
<tr class="odd">
```
```{=html}
<td>
```
```{=html}
<p>
```
Een ‘outlook adresboek’ moet bestaan. In principe kan een ‘Personal
Address Book’ ook, maar wordt afgeraden omdat deze een extra bestand
aanmaakt die niet bij een herconfiguratie van outlook automatisch
meegenomen wordt, niet alle functionaliteit beschikbaar en minder
intuitief werkt.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<p>
```
`<img src="contacts_new-email_clickon-to.jpg" title="fig:New email -&gt; To..." alt="New email -&gt; To..." />`{=html}`<img src="contacts_new-email_address-book-empty.jpg" title="fig:Addressbook empty..." alt="Addressbook empty..." />`{=html}
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="even">
```
```{=html}
<td>
```
```{=html}
<p>
```
Bij de eigenschappen van de contactpersonenmap, moet bij de tab “Outlook
Address Book” het vinkje “Show this folder as an e-mail Address Book”
aan staan.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="odd">
```
```{=html}
<td>
```
```{=html}
<p>
```
Ga in de menubalk naar “Tools -\> E-mail Accounts…”`<br />`{=html}
Selecteer onder “Directory” het keuzebolletje “View or change existing
directories or addressbooks”. Klik op [next].
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="outlook-address-book-view.jpg" title="Outlook addressbook, view existing." alt="" />`{=html}
```{=html}
<figcaption>
```
Outlook addressbook, view existing.
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="even">
```
```{=html}
<td>
```
```{=html}
<p>
```
Als je geen “Outlook Address Book ziet staan, klik dan op”Add…“.
Selecteer”Additional Address Books" en klik op “Next”.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="contacts_add-outlook_address_book.jpg" title="View or change existing directories or address books" alt="" />`{=html}
```{=html}
<figcaption>
```
View or change existing directories or address books
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="odd">
```
```{=html}
<td>
```
```{=html}
<p>
```
Selecteer bij “Additional Address Book Types” “Outlook Address Book” en
klik op “Next”.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="outlook-address-book-new.jpg" title="New Outlook addressbook" alt="" />`{=html}
```{=html}
<figcaption>
```
New Outlook addressbook
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="even">
```
```{=html}
<td>
```
```{=html}
<p>
```
Je krijgt de melding dat je eerst Outlook af moet sluiten voordat het
Outlook Adresboek werkt. Klik op “OK” en vervolgends “Finish”. Herstart
Outlook en ga dan verder bij het onderstaande punt.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="outlook-address-book-new-restart.jpg" title="Restart Outlook" alt="" />`{=html}
```{=html}
<figcaption>
```
Restart Outlook
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="odd">
```
```{=html}
<td>
```
```{=html}
<p>
```
Klik rechts op de contacts-folder die je zichtbaar wilt hebben in het
adresboek. Selecteer “Properties” en ga naar het tabblad “Outlook Address
Book”. Vink “Show this folder as an e-mail Address Book” aan, en vul bij
“name of the address book:” een naam in - deze zie je terug als
adresboek-naam. Klik op “OK”
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="outlook-address-book-contacts_properties.jpg" title="Outlook Address Book tabblad bij contacts" alt="" />`{=html}
```{=html}
<figcaption>
```
Outlook Address Book tabblad bij contacts
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
</tbody>
```
```{=html}
</table>
```
