---
author: polman
date: '2007-10-16T20:09:57Z'
keywords: []
lang: nl
noage: true
tags:
- internet
title: Url
wiki_id: '70'
---
## Universal Resource Locators

Een Universal Resource Locator (URL) geeft precies weer waar
je een World Wide Web pagina kunt
vinden. Elke URL is als volgt opgebouwd:

> `protocol`://`host`.`domain`:`port`/`path`

## Protocol

Het protocol geeft aan op wat voor manier
de informatie moet worden opgehaald. Geldige protocollen zijn o.a.

-   http: Hyper Text Transfer Protocol. Met dit protocol worden Web
    pagina’s opgehaald.\
    file of ftp: Geef je dit op als protocol, dan wordt het document als
    een gewoon bestand opgehaald.\
    telnet: Start een telnet sessie op naar
    host`.`domain.
    Het `/`path gedeelte ontbreekt.\
    news: Een link naar een Usenet nieuwsgroep. Het
    host plus
    domain gedeelte geeft de naam van een
    news-server aan (bijvoorbeeld `news.surfnet.nl`) en het
    path gedeelte is de nieuwsgroep.\
    mailto: In dit geval wordt de rest van de URL gevormd door een
    e-mail adres waar naartoe een mailtje gestuurd moet worden.

## Host en Domain

De volledige naam van de computer waarop het document te vinden is.
Gebruikelijk is om voor computers die een service als WWW leveren een
*alias* te definiëren. Dit heeft twee voordelen. Ten eerste kun je een
gemakkelijk te onthouden alias nemen, bijvoorbeeld `www` en ten tweede
is de service gemakkelijk te verplaatsen naar een andere computer door
simpelweg de alias om te hangen.

## Port

WWW verkeer over het Internet verloopt via een set van afspraken die
(ook) een protocol wordt genoemd: Internet Protocol of kortweg IP. In
deze afspraken is ook vastgelegd hoe de verschillende soorten
verbindingen (ftp, telnet, http, smtp, nntp, etc.) te herkennen zijn.
Dit gebeurt d.m.v. een getal, het zogenaamde poortnummer. De standaard
poort voor HTTP-verkeer is 80. Het `:`port
gedeelte van de URL mag worden weggelaten als de WWW service op dit
poortnummer loopt.

## Path

Afhankelijk van het
protocol is dit de padnaam naar een
document of de naam van een nieuwsgroep.

## Voorbeelden

-   <http://www.science.ru.nl/cncz/>: De homepage
    van C&CZ.
-   <ftp://ftp.science.ru.nl/> De ftp-service van de
    Faculteit der Natuurwetenschappen.
-   <http://opc.ubn.ru.nl> De Online Publieks Catalogus van de
    Universitaire Bibliotheek.
-   [mailto:Postmaster\@science.ru.nl](mailto:Postmaster@science.ru.nl)
    Een mailtje naar degene die op dit moment als Postmaster bij C&CZ
    de probleem-meldingen ontvangt.
