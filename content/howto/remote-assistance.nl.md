---
title: Ondersteuning op afstand
author: sioo
date: 2023-01-27
keywords: []
tags: []
cover:
  image: img/2023/remote-assistance.png
ShowToc: false
TocOpen: false
---
C&CZ helpdesk kan je helpen door je scherm over te nemen met behulp van *TeamViewer*.
Als je wordt gevraagd om TeamViewer te downloaden, gebruik dan de volgende link:

> https://get.teamviewer.com/ischelpdesk