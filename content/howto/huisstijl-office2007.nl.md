---
author: petervc
date: '2011-07-05T10:31:40Z'
keywords: []
lang: nl
tags: []
title: Huisstijl Office2007
wiki_id: '775'
---
### Huisstijl in Office 2007

Om de RU huisstijl in Office 2007 te gebruiken moet eenmalig de
koppeling tot stand gebracht worden met het ru\_huissstijl sjabloon.

-   Open Word 2007
-   Klik op de Microsoft Officeknop en klik (linksboven) op Opties voor
    Word.
-   Klik op Geavanceerd.
-   Onder Algemeen (helemaal naar beneden scrollen) klikt u op
    Bestandslocaties.
-   Klik op werkgroepsjablonen en dan op wijzig, browse naar de
    sjablonen/huisstijl folder van uw afdeling en klik op OK.
-   Klik op de Microsoft Office-knop (linksboven) en klik op Opties voor
    Word (onder in het window).
-   Klik op invoegtoepassingen.
-   Kies onderaan bij beheren: Sjablonen en klik op start.
-   Klik op toevoegen en ga naar de sjablonen/huisstijl folder en de
    folder waar de ru\_huisstijl.dot staat en klik ru\_huisstijl.dot
    aan.
-   Klik op OK en nogmaals op OK.

Er is een vervelend datum probleem als een brief meer dan 1 pagina
bevat, de datum op de tweede en volgende pagina’s wordt niet automatisch
aangepast en bovendien kun je deze niet eenvoudig zelf aanpassen omdat
het veld afgeschermd is. Dit kan worden opgelost door een keer
Afdrukvoorbeeld te bekijken. Dan wordt de datum wel aangepast.
