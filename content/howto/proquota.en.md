---
author: petervc
date: '2018-11-27T09:41:23Z'
keywords: []
lang: en
tags:
- studenten
- medewerkers
- software
title: Proquota
wiki_id: '601'
---
## ProQuota, what does it do ?

When you log in on a managed PC, your private settings (*Profile*) of
Windows and the applications are copied from the network to your current
PC. During the session, these copied settings on the PC are used and
changed if necessary. At logout, the (altered) settings are copied back
to the network. When you log in, at a later point in time, on a
different managed PC, these settings are copied to the new PC and your
own settings are available again. You may locate these settings under
`C:\Documents and Settings\<loginname>`.

ProQuota monitors the space that your *Profile* takes on the disk.
Bacause copying the *Profile* takes time during login and logout, and
because the physical disk space is limited, the amount of disk space for
*Profile* is limited.

Be aware that backup software discriminates between Home directories
(important) and other data. Store, therefore, your important data in
your Home directory.

What can you do in case you receive a Proquota message:

-   For Firefox and Thunderbird it would be smart to save your user
    profile somewhere under ‘My Documents’ (= home directory = U:\\
    drive).
-   Store your documents in your Home directory and not on your desktop.
