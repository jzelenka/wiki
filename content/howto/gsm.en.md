---
author: petervc
date: '2022-06-24T13:04:49Z'
keywords: []
lang: en
tags:
- telefonie
title: GSM
wiki_id: '166'
---
-   **GSM**

The Huygens building originally had poor mobile coverage, it has
features of a [Faraday cage](http://en.wikipedia.org/wiki/Faraday_cage).
Therefore, in 2011 the Faculty of Science installed a [DAS
installation](http://en.wikipedia.org/wiki/Distributed_antenna_system)
(distributed antenna system), providing good coverage for mobile
telephony on the Vodafone network. At the end of 2014, almost complete
coverage in the Huygens building for mobile phones has been realised by
the Faculty of Science for the Vodafone network that is used for
[Vodafone Wireless Office](/en/tags/telefonie/). This became possible,
because Ericsson and Vodafone were interested in doing a pilot in the
basements and transport corridors of the Huygens building with [the
first live enterprise deployment of the Radio Dot system
worldwide](http://www.ericsson.com/news/1878703). This reminds some
veterans of June 1987, when a pilot was carried out at the Faculty of
Science by PTT (now KPN), Philips, SURF and the university with the
first
[PABX](http://en.wikipedia.org/wiki/Private_branch_exchange#History) in
The Netherlands with a large amount of data traffic. This ISDN-prepared
PABX was phased out in January 2018.

Only Vodafone is connected to this in-house DAS installation. This is
used for the Vodafone Wireless Office (VWO) mobile business telephony.
All calls from VWO mobiles and RU wired phones to VWO mobiles are free
of charge as long as they are located within zone 1 (roughly Europe and
a few extra countries) as defined by Vodafone. Applications for VWO
subscriptions and their associated devices go through ILS, where
specific up-to-date information can be found.

Because of high costs, it is not likely that other providers will
connect to the DAS installation. Better internal coverage for other
providers is expected to come from [4G-bellen
(VoLTE)](https://en.wikipedia.org/wiki/Voice_over_LTE) and
[VoWLAN/Vowifi](https://en.wikipedia.org/wiki/Voice_over_WLAN). Also
since a couple of years there is a project that will result in campus
wide indoor cell coverage for all providers. Because of the scope of the
project a delivery date cannot be given at this time.

Cellphones, subscriptions are managed and rates are determined by ILS.
