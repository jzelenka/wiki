---
author: petervc
date: '2022-06-28T14:52:51Z'
keywords: []
lang: en
tags:
- software
title: Unix
wiki_id: '156'
---
## Linux software for employees and students of the Faculty of Science

### Available software on C&CZ managed Linux-computers

`<include src="/var/www1/wiki/live/htdocs/cncz/include/software-cfengine.wiki" nopre wikitext />`{=html}

-   All kinds of software in /usr/local, /opt/ [and]
    /vol/

-   Ubuntu packages
    -   A complete
        list of the thousands of installed Ubuntu packages can be seen
        with the command:

           aptitude search "[a-z]" | grep ^i

An even larger list of
all Ubuntu packages that can be installed, can be seen with the
command:

           aptitude search "[a-z]"

Send an email to [postmaster](/en/howto/contact/) if you want an extra
package to be installed.

### To install on your own Linux PC

The [Install share](/en/howto/install-share/) contains the following
Linux software:

-   Mathematics software:
    -   [Maple](/en/howto/maple/)
    -   [Matlab](/en/howto/matlab/)
    -   [Mathematica](/en/howto/mathematica/)
    -   IBM - [SPSS](http://www.ibm.com/spss)
        (Statistics)
-   Office software:
    -   Konica Minolta [printerdrivers](/en/howto/printers-en-printen/)
    -   TeX: [TeX Live](http://www.tug.org/texlive)
-   Data acquisition
    programming environment:
    -   National Instruments [LabVIEW](/en/news/)

### To be borrowed from C&CZ

At this moment the following Linux software can be borrowed from C&CZ in
room HG00.051 by employees and students of the Faculty of Science:

-   Operating system:
    -   [Ubuntu](http://www.ubuntu.com) Linux
