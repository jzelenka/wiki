---
author: wim
date: '2021-09-24T10:55:58Z'
keywords: [ vpn, vpnsec, ipsec, ikev2, handleiding ]
lang: nl
tags: [ vpn, macos, ubuntu, windows ]
title: VPN
wiki_id: '71'
ShowToc: true
TocOpen: true
aliases:
- vpnsec-linux-install
- vpnsec-macos-x-strongswan-app
- vpn-mac
cover:
  image: img/2022/vpn.png
---
# VPN
C&CZ beschikt over een VPN (Virtual Private Network) server, waarmee alle
gebruikers met hun [Science-account](../login) een beveiligde toegang tot
het netwerk kunnen krijgen.

De werkplek thuis (of ergens anders op het Internet) wordt daarmee
gezien als onderdeel van het campusnetwerk. Op deze manier kan men
toegang krijgen tot faciliteiten die alleen vanaf het campusnetwerk
toegankelijk zijn. Men kan hierbij denken aan [het aankoppelen van
netwerkschijven](/nl/howto/mount-a-network-share) of toegang tot speciale servers.

## Alternatieven

### EduVPN
Er is een [RU-centrale VPN-service
EduVPN](https://www.ru.nl/ict/medewerkers/off-campus-werken/vpn-virtual-private-network/),
waar je met je [RU-account](http://www.ru.nl/wachtwoord)
gebruik van kan maken.

### OpenVPN
We hebben ook een [OpenVPN](../openvpn) dienst. Deze kan gebruikt worden op apparaten waarop het
niet lukt om de standaard VPN-dienst aan de praat te krijgen.

### Universiteitsbibliotheek
Voor het gebruik van de [bibliotheek](http://www.ru.nl/ub) is VPN niet nodig,
want de UB gebruikt een proxy website *UB Off-Campus*, die na inloggen
met [RU-account en RU-wachtwoord](http://www.ru.nl/wachtwoord) toegang
vanuit het hele Internet mogelijk maakt.


# Science VPN dienst
Het [IPsec](https://wikipedia.org/wiki/IPsec) VPN server adres is `vpnsec.science.ru.nl`
en gebruikt je [Science account](../login) voor de authenticatie.

## Instructies voor Windows 10
> De meest beknopte uitleg: Maak een *nieuwe* VPN aan, met server `vpnsec.science.ru.nl`, dat is alles. 

Meer gedetaileerde instructies:

Vanuit Windows "Instellingen", ga naar "Netwerk en internet" > "VPN" > "+ Een VPN-verbinding toevoegen"

![Windows 10 vpnsec configuration screenshot](/img/2022/vpnsec-windows10-config_nl.png)

Vul in de gegevens uit onderstaande tabel:
| veld                       | instelling                   | opmerking             |
| -------------------------- | ---------------------------- | --------------------- |
| VPN-provider               | Windows (built-in)           | staat al goed         |
| Naam van verbinding        | Science VPN                  | kies iets herkenbaars |
| Servernaam of -adres       | `vpnsec.science.ru.nl`       |                       |
| VPN-type                   | Automatisch                  | staat al goed         |
| Type aanmeldingsgegevens   | Gebruikersnaam en wachtwoord | staat al goed         |
| Gebruikersnaam (optioneel) | [Science account](../login)  | optioneel             |
| Wachtwoord (optioneel)     | je wachtwoord                | optioneel             |

Sla de instellingen op.

### VPN starten
De VPN-verbinding kan gestart worden bij het netwerk of wifi icoon bij het klokje. Klik daar op "Science VPN" en dan op "Verbinding maken":

![vpn verbinding starten op windows 10](/img/2022/vpnsec-windows10-start_nl.png)

Als laatst wordt je gevraagd naar je [Science loginnaam](../login) en bijbehorende wachtwoord:

![vpn login op windows 10](/img/2022/vpnsec-windows10-login_nl.png)

> Als het goed is ben je nu verbonden. Zie [VPN testen](#vpn-testen). Zo niet? neem [contact](../contact) op!

### VPN stoppen
Stop de VPN-verbinding op de zelfde plek. Daar staat nu een knop "Verbinding verbreken":

![vpn verbinding verbreken op windows 10](/img/2022/vpnsec-windows10-stop_nl.png)

## Instructies voor macOS 13
Gebruik de volgende stappen om een VPNsec-verbinding op uw macOS-systeem te configureren:
1. Download dit bestand [vpnsec-ios.mobileconfig](/downloads/2022/vpnsec-ios.mobileconfig) (Link opslaan als...)

1. Dubbel-klik het `vpnsec-ios` bestand in Finder en importeer de settings.

1. Open "Systeemvoorkeuren"

1. Open de "Netwerk"-voorkeuren, waarna u het dialoogvenster krijgt dat wordt weergegeven in
   de afbeelding hieronder:

   ![macOS Network settings](/img/2022/macos-network-settings.png)


1. Klik op het knopje met de 3 puntjes `...`, en ga naar "VPN configuratie toevoegen" -> "IKEv2...":

   ![Add IKEv2 VPN macOS](/img/2022/macos-add-vpn-ikev2.png)

1. Vul daar de volgende gegevens in:
| veld                | waarde                           |
| ------------------- | -------------------------------- |
| Server address      | `vpnsec.science.ru.nl`           |
| Remote ID           | `vpnsec.science.ru.nl`           |
| Authentication Type | Gebruikersnaam                   |
| Username            | [je-science-loginname](../login) |
| Password            | xxxxx                            |

1. Klik "Toepassen"

> Als het goed is kun je nu verbinden. Zie [VPN testen](#vpn-testen). Zo niet? neem [contact](../contact) op!

## Instructies voor Ubuntu 20.04 & 22.04
Begin met het installeren van de benodigde pakketten:

``` console
$ sudo apt install strongswan-nm network-manager-strongswan \
libstrongswan-standard-plugins  strongswan-libcharon \
libstrongswan-extra-plugins strongswan-charon libcharon-extra-plugins
```

{{< notice warning >}}
Herstart je systeem na het installeren van deze pakketten!
{{< /notice >}}

Ga in "Systeem settings" naar "Netwerk". Daar op het plusje `+` klikken
 om een nieuwe VPN-verbinding toe te voegen. Zie afbeelding:

 ![vpn toevoegen ubuntu](/img/2022/newvpnubuntu.png)

Vul de velden in zoals hieronder afgebeeld:

![vpnsec instellingen ubuntu](/img/2022/vpnsec-ubuntu.png)

Wat overeenkomt met:
| field          | value                           |
| -------------- | ------------------------------- |
| Name           | Science VPN                     |
| Server Address | `vpnsec.science.ru.nl`          |
| Username       | [your-science-login](../login)  |
| Options        | [v] Request an inner IP address |
|                | [v] Enforce UDP encapsulation   |

Na het bewaren van de instellingen, kun je de VPN-verbinding aanzetten
middels de knoppen in de toolbar rechts-boven:

![vpn aanzetten ubuntu](/img/2022/ubuntu-enable-vpn.png)

> Als het goed is ben je nu verbonden. Zie hieronder. Zo niet? neem [contact](../contact) op!

# VPN testen
{{< notice tip >}}
Je kun je VPN-verbinding testen door bijvoorbeeld de volgende website te bezoeken: 

> <https://ip.science.ru.nl>

Deze pagina vermeldt je ip-adres en geeft aan of je met een van de VPN diensten verbonden bent.
{{< /notice >}}

# Overige systemen
## Instructies voor iPhone & iPad
Voor iOS kun je de instructie hierboven voor macOS zo goed mogelijk volgen, blijkt voor veel mensen te
werken.


## VPNsec setup Android

-   **Android**: Installeer de
    [strongSwan](https://play.google.com/store/apps/details?id=org.strongswan.android)
    app met “IKEv2 EAP (Username/Password)”. **NB: sommige
    tekens in een wachtwoord moeten worden ge-escaped met een
    “\\”**.

## Instructies KDE/Plasma
Voor Plasma desktop werkt het vrijwel hetzelfde als de algemene [instructies voor Ubuntu](#instructies-voor-ubuntu-2004--2204). In plaats van "Server", wordt de kreet "Gateway" gebruikt in het configuratiescherm.

