---
author: petervc
date: '2022-06-28T14:51:55Z'
keywords: []
lang: nl
tags:
- overcncz
title: Werkplekondersteuning
wiki_id: '128'
---
Operations/helpdesk is gelocaliseerd in kamer HG00.051.

## Wie zijn zij?

-   {{< author "dominic" >}}
-   {{< author "bjorn" >}}
-   {{< author "john" >}}
-   {{< author "stefan" >}}

## Wat doen zij?

-   Gebruikersondersteuning voor studenten en medewerkers
-   [Installatie en onderhoud van de studentenvoorzieningen, met name
    de [terminalkamers en openbare
    werkplekken](/nl/howto/terminalkamers/)][Installation and
    maintenance of hardware available for students, especially the
    [terminal rooms and public workstations](/nl/howto/terminalkamers/)]
-   Algemene ondersteuning van computerapparatuur op
    onderzoeksafdelingen
-   Beheer van de serverruimtes
-   Hulp of bemiddeling bij
    -   [het [installeren van PC’s](/nl/howto/installeren-werkplek/) en
        netwerksoftware][ [installing
        PC’s](/nl/howto/installeren-werkplek/) and network software]
    -   [ [reparaties van PC’s en printers](/nl/howto/reparaties/)][
        [repairing PC’s and printers](/nl/howto/reparaties/)]
