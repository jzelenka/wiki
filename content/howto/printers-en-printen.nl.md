---
author: wim
date: '2021-05-10T19:13:27Z'
keywords: []
lang: nl
tags:
- hardware
- software
title: Printers en printen
wiki_id: '47'
---
# Printers

## Peage en Konica Minolta multifunctionals

Over de hele campus verspreid staan multifunctionals die het
[Peage](/nl/howto/peage/) systeem gebruiken, beheerd door het
[ISC](http://www.ru.nl/isc). Het betreft Konica Minolta Bizhub
multifunctionals die ook als kopieerapparaat en kleurenscanner te
gebruiken zijn.

## Inbinden, snijden, nieten, boekjes, …

In de [Bibliotheek
FNWI](http://www.ru.nl/ubn/bibliotheek/locaties/library-of-science/) kan
men smeltmapjes van diverse diktes kopen om werkstukken e.d. in te
binden in het lamineerapparaat. Ook is daar een snijmachine, een
23-gaatjes perforator en diverse nietmachines. Alle Konica Minolta
printers kunnen automatisch nieten.

In de MFP bij HG00.002 zitten een gaatjesmaker en een rug-nieter die kan
vouwen. Hiermee is het mogelijk om complete boekjes te produceren. Er
kunnen 2 of 4 gaatjes worden geponst. Maximaal 20 vel kan in het midden
(op de rug) geniet worden en gevouwen, waardoor van A3-vellen een
A4-boekje gemaakt kan worden, of van A4-vellen een A5-boekje.

Bij HG03.089 is ook een snijmachine, een 23-gaatjes perforator en een
grote nietmachine aanwezig.

## C&CZ poster printer

C&CZ heeft een [A0 posterprinter kamerbreed](/nl/howto/kamerbreed/) voor
het maken van afdrukken van posters e.d. op hoge kwaliteit fotopapier,
canvas of plakpapier en de 3D-printers.

## C&CZ 3D-printers

C&CZ heeft een 3D-printservice voor FNWI-medewerkers en -studenten,
speciaal bedoeld voor het produceren van objecten in meer kleuren voor
gebruik in het onderwijs.

De eerste 3D-printer is een [BCN3D
Sigma](https://www.bcn3dtechnologies.com/en/catalog/bcn3d-sigma/), een
professionele open source FFF (Fused Filament Fabrication) 3D-printer
met twee onafhankelijke extruders, waardoor verschillende materialen,
kleuren of ondersteuning van het geprinte object gebruikt kunnen worden,
zodat men minder geometrische beperkingen heeft.

Per september 2017 is er ook een [Prusa i3
MK2](https://www.3dhubs.com/3d-printers/original-prusa-i3-mk2) 3D
Printer, die met de [multi-materiaal
upgrade](https://all3dp.com/prusa-i3-multi-material-upgrade/) 4 kleuren
kan printen uit 1 spuitmondje.

Wie wil beginnen met ontwerpen, wordt aangeraden om enkele [videolessen
FreeCAD
Tutorial](https://www.google.nl/search?q=freecad+tutorial#q=freecad+tutorial+site:youtube.com)
te volgen.

De prijs van een geprint object: 10 cent per gram.

## Afdelingsprinters (eigendom van de afdeling)

Enkele afdelingen beschikken nog over een eigen printertje. Deze
printers mogen in het algemeen alleen worden gebruikt door personen die
tot de betreffende afdeling behoren. De afdeling heeft namelijk de
aanschaf betaald en is ook verantwoordelijk voor het papier, de toner en
het onderhoudscontract. Bij vervanging gaat men i.h.a. over op een
Peage-printer.

### Afdelingsprinters (eigendom van de afdeling): Afdrukken onder Windows

#### ISC beheerde PC (SRW)

Eindgebruikers op een door het [ISC](http://www.ru.nl/isc) beheerde PC
kunnen het beste de procedure voor een zelf beheerde Windows 7 PC
hieronder volgen. Lukt dit niet, neem dan contact op met het ISC.

#### Windows 8, 8.1, 10 of hoger

Je zult een locale printer driver moeten installeren. Hiervoor dien je
de printerdriver eerst te downloaden en klaar te zetten om straks in
stap 2 te kunnen gebruiken.

Koppel eerst een netwerkschijf aan van de printer-server, dan zie je
namelijk foutmeldingen als er iets mis gaat.

-   Ga hiervoor eerst naar je **Bureaublad**.
-   De netwerkschijf aankoppelen gaat door in een **Windows Verkenner**
    scherm op de tab **Computer** en vervolgens **Map network drive** te
    klikken.
-   Vul bij **Station:** b.v. **P:** en bij **Map:**
    **`\\print.science.ru.nl\printtest`** in. Vink tevens **Opnieuw
    verbinding maken bij aanmelden** en **Verbinding maken met andere
    referenties** aan. Klik **Voltooien**.
-   Voer je science account en wachtwoord in in het Login dialoog dat
    verschijnt. Vink ook **Mijn referenties onthouden** aan en klik op
    **OK**.
-   Er hoort een window te verschijnen met enkele bestanden.

Installeer vervolgens de printer op een locale poort.

-   Dat kan via een initiële rechtsklik op **Start** -\>
    **Configuratiescherm** -\> **Apparaten en printers**,
-   Kies **Een printer toevoegen** en kies **De printer die ik wil
    gebruiken, staat niet in de lijst.**,
-   Kies **Een locale printer of netwerkprinter toevoegen methandmatige
    instellingen**. Klik **Volgende**,
-   Kies **Bestaande poort gebruiken:** **LPT1: (Printerpoort)**. Klik
    **Volgende**,
-   Installeer de juiste printerdriver,
-   Kies een **Printernaam:**, bv **`onzeeigenprinter`**. Klik
    **Volgende**,
-   Kies **Deze printer niet delen**. Klik **Volgende** en vervolgens
    **Voltooien**.

De printer staat nu in het lijstje van jouw printers. Nu moeten we de
nieuwe printer aanpassen zodat je naar de netwerkprinter gaat printen.

-   Rechtsklik op de nieuwe printer en kies **Eigenschappen van
    printer**.
-   Ga naar het tabblad **Poort** en klik op **Poort toevoegen…**.
-   Kies **Local Port** en klik op **Nieuwe Poort…**.
-   Vul bij **Geef een poortnaam op**
    **`\\printsmb.science.ru.nl\<printernaam>`** in en klik **OK**,
    **Sluiten** en nogmaals **Sluiten** aan.

Verifieer alvorens te printen of je de bestanden op die
**`\\printsmb.science.ru.nl\printtest`** kunt lezen. De printerkoppeling
**`\\print.science.ru.nl\`** in zowel **Poort toevoegen…** als de
netwerkdisk moeten exact hetzelfde zijn. Dit is helaas nodig om een
betrouwbare connectie naar de printer-server te maken.

#### Windows 7: Snel printers koppelen op een beheerde PC

-   Open **Verkenner** (windows toets + e).
-   Typ in de adresbalk: **`\\print.science.ru.nl\`** en druk op ENTER.
-   Als er om authenticatie wordt gevraagd, gebruik dan je
    science-account en wachtwoord.
-   Dubbelklik een van de printers die onder verschijnen.

#### Windows 7: Voor een zelf beheerde PC

Koppel eerst een netwerkschijf aan van de printerserver, dan zie je
namelijk foutmeldingen als er iets mis gaat. Die netwerkschijf
aankoppelen gaat door in een **Windows Verkenner** scherm als **Mijn
Documenten** te klikken op **Extra** -\> **Netwerkverbinding maken…** en
als **Map:** op te geven **`\\print.science.ru.nl\printtest`**.

-   Kies **Start**, dan **Apparaten en Printers**, dan **Een printer
    toevoegen** en kies dan voor **Netwerk printer, draadloze printer of
    Bluetooth-printer toevoegen**.
-   Klik **Een gedeelde printer op naam selecteren.** en vul in bij
    **Een gedeelde printer op naam selecteren:**
    **`\\print.science.ru.nl\``<printernaam>`**.
-   Klik **Volgende**, wacht even en klik daarna weer **Volgende**.
-   Controleer voor de zekerheid of de Voorkeuren en/of Eigenschappen op
    A4 papier staan in plaats van het US letter formaat papier.

#### Windows2000/XP

Koppel eerst een netwerkschijf aan van de printer-server, dan zie je
namelijk foutmeldingen als er iets mis gaat. Die netwerkschijf
aankoppelen gaat door in een **Windows Verkenner** scherm als **Mijn
Documenten** te klikken op **Extra** -\> **Netwerkverbinding maken…** en
als **Map:** op te geven **`\\print.science.ru.nl\printtest`**.

-   Vervolgens kan dan via **Start** -\> **Instellingen** -\> **Printers
    en faxapparaten** de printertaak **Een printer toevoegen** een
    nieuwe printer toegevoegd worden.
-   Kies voor **Netwerkprinter …**,
-   kies voor **Verbinding maken met deze printer …**,
-   en vul achter **Naam:** in:
    **`\\print.science.ru.nl\``<printernaam>`**, dus bv.
    **`\\printto.science.ru.nl\onzeeigenprinter`**.
-   Vervolgens kun je kiezen of dit de standaardprinter moet worden.
-   Klik dan nog op **Voltooien** om af te sluiten.

Bij een zelf beheerde PC of laptop is het verstandig de loginnaam op de
PC hetzelfde te kiezen als de loginnaam die men nodig heeft om de
printer aan te koppelen (de Science loginnaam), dan geeft Windows die
namelijk vanzelf mee aan de printer-server. In andere gevallen valt het
vaak wel aan de praat te krijgen door **Verbinding maken via een andere
gebruikersnaam** aan te klikken).

### Afdelingsprinters: Afdrukken op beheerde Linux werkplekken

Met **system-config-printer** kan de standaard printer per gebruiker
gekozen worden (“Set as Default”).

Voor het printen naar een gebudgetteerde printer is een Kerberos ticket
nodig. Als de homedirectory van een server komt die NFS4 met Kerberos
vereist (zoals *home1*, *home2*, *pile* en *bundle*), dan zul je al een
Kerberos ticket hebben. Met **klist** kun je zien of je een Kerberos
ticket hebt, met **kinit** kun je een Kerberos ticket krijgen of
vernieuwen en tenslotte **kdestroy** ruimt alle tickets op.

Om te kijken wat er in een printerqueue staat, moet **lpq
-P`<printernaam>`** worden gebruikt. Alle andere manieren,
waaronder de grafische applicatie, laten altijd een lege printerqueue
zien.

### Afdelingsprinters: Afdrukken op andere Linux machines

In Linux kan men kiezen voor printen op de Windows-manier, via SMB naar
een Samba-server: print.science.ru.nl.

Kies als device URI
**<smb://print.science.ru.nl/>`<printernaam>`**.

-   Als “device URI” kies
    **<smb://print.science.ru.nl/>`<printernaam>`**
    of
    **<smb://loginnaam:wachtwoord@print.science.ru.nl/>`<printernaam>`**,
    waar je zelf je eigen loginnaam en wachtwoord in moet vullen en de
    correcte naam van de printer als printernaam. Als je wachtwoord
    speciale tekens bevat dan kan dit problemen opleveren bij het
    invoeren van de URI. Vervang in dat geval het speciale teken door
    zijn hexadecimale code voorafgegaan door een ‘%’ (procent-teken).
    Voorbeeld: heb je een ‘!’ (uitroepteken) in je wachtwoord, vervang
    deze dan door ‘%21’.
-   Een alternatief voor het creeren van een cups printer is het
    commando **lpadmin -p PrinterName -v
    <smb://Username:Password@Server/Printer> -P
    /path/to/yourprintermodel.ppd**.

#### Gedetailleerde instructies voor OpenSuSE 11

Alleen in het Engels beschikbaar:

-   Ask C&CZ to give your PC a permanent IP addres and name.
-   Ask the owner of the printbudget to add your “login” name to the
    list of users that is allowed to use the printer. This can be done
    by the printbudget owner using <http://dhz.science.ru.nl>, choose
    “printing”. However: only C&CZ can give permission to print from a
    computer that is not in the list of “C&CZ trusted” machines. For
    this purpose C&CZ has to give the command: “printbudget -g
    budgetname -m +user\@host”.
-   Start the SuSE setup tool YAST, give root password.
-   Choose: “Hardware”, “Printer”, “Print via network”.
-   Click “connection wizard”, “Line printer daemon (LPD) protocol”.
-   In “Connection settings, IP Address or Host name” enter
    “printsmb.science.ru.nl”.
-   Enter queue name: “hertz” (or any other network printer).
-   Select manufacturer: “Konica Minolta” (check brand of printer).
-   Click on “Test connection”, the reply should be “Test OK” after a
    few seconds.
-   Click “OK”.
-   At “Find and assign driver; search for:” enter “Generic”.
-   Choose default paper size “A4”.
-   At “Set arbitrary name” enter name of queue, e.g. “pr-hg-03-038”.
-   To select a driver, scroll to bottom of the list for "Generic
    PostScript level 2 Printer Foomatic/PostScript.
-   Click “OK”.
-   Wait for “Creating New Printer Setup” to finish.
-   You will go back to the printer configuration menu. Click on “Edit”
    to modify the queue that was just created.
-   Click on “options”, select “Duplex/double sided printing” and choose
    “DuplexNoTumble” and “OK”.
-   Enter “printer location”, make sure “Accept Print Jobs” and “Enable
    Printing” are selected.
-   Optionally, select “Use as Default”.
-   Back in the “Printer Configurations” menu click “Print Test Page”
    (select two pages to test double sided printing). Warning: if you
    are still “root” the printqueue may not accept these test pages.

#### Gedetailleerde instructies voor Ubuntu

Alleen in het Engels beschikbaar:

-   Make sure you install
    -   system-config-printer-gnome : graphical setup tool for printers
    -   python-smbc python3-smbc smbclient : needed for
        system-config-printer to let it connect to samba printers; when
        not installed the samba printer option is not available in
        system-config-printer
    -   gnome-keyring : needed for storing password
    -   seahorse : optional needed for editing credentials stored in
        gnome-keyring
    -   **Install all with command:** sudo apt-get install
        system-config-printer-gnome python-smbc python3-smbc smbclient
        gnome-keyring seahorse
-   Note: credentials are stored in gnome-keyring and not in KDE’s
    kwallet.
-   From a terminal window execute as normal user the command:
    `sudo     system-config-printer`\
    On Ubuntu linux you are allowed to continu if you are an admin user,
    otherwise you have to supply in a popup window the credentials for a
    valid admin user.
-   In the started gui choose add new printer.
-   In the upcoming dialog window choose:
    -   Device: Network Printer - Windows printer via Samba
    -   URI Printer:
        <smb://printsmb.science.ru.nl/>`<printername>`
    -   Authenthication: you have choice from two options, where latter
        is preferred :
        -   user (**ru\\u-number** + password =\> password saved
            plain-text in /etc/cups/printers.conf
        -   ‘Prompt user if authentication required’ : user must supply
            password when printing. When asked for credentials you can
            choose the “save” option which causes the credentials to be
            stored in the gnome-keyring. Credentials in the
            gnome-keyring can always be edited/deleted using the
            seahorse program.
        -   **TIP**: in case of problems with authentication look at the
            terminal from which you started `system-config-printer` if
            there are any errors printed. Sometimes this can give an
            indication why the authentication fails.
-   Use as driver a specific driver for the printer which you can
    download online at printer manufacturer website. (select the ppd
    file)\
    However sometimes “generic postscript” also works.
-   Printing the first time can be best done by printing a webpage from
    the firefox browser program. Because this gives you an
    authentication window with the option to store the credentials which
    makes sure your credentials are stored safely in the gnome keyring.
    By storing it won’t ask you to specify them the next time. The
    reason to use firefox is that other linux programs may have slighty
    different authentication dialog window which don’t give you the
    storage option forcing you to supply credentials on each print!

### Afdelingsprinters: Afdrukken op een OS X machine

#### OS X 10.6 (Snow Leopard) of hoger

-   Ga naar Systeemvoorkeuren, selecteer **Afdrukken en faxen**.
-   Klik onder de lijst met printers op het plusje.
-   Klik in de knoppenbalk op **Geavanceerd** (staat deze er niet: klik
    rechts (of control-klik) op het menu en kies **pas knoppenbalk aan**
    en sleep de knop **Geavanceerd** naar de knoppenbalk).
-   Het kan gebeuren dat je niets kunt invullen in deze dialogbox, in
    dat geval staat mogelijk printer sharing aan.

Open Systeemvoorkeuren -\> Delen -\> Printer Delen en zet printer delen
uit. Reboot en daarna kun je wel iets toevoegen via deze weg.

-   Vul de volgende gegevens in:
    -   Type: Windows
    -   URI: **<smb://printsmb/>`<printernaam>`**\
        Indien eduroam gebruikt wordt voeg aan de **`/etc/hosts`** file
        de onderstaande regel toe:\
        **131.174.30.41 printsmb.science.ru.nl printsmb**\
        Vaak werkt het ook om gewoon om
        **<smb://printsmb.science.ru.nl/>`<printernaam>`** te
        gebruiken.
    -   **IMPORTANT**: when using eduroam make sure it uses this
        university credentials, otherwise you are seen as a guest on the
        network and disallowed to access the printers over the smb
        protocol!!
    -   Naam en locatie zijn eigen keuze. Besturingsbestand/printermodel
        kiezen.
-   Klik **Voeg toe**.
-   Bij de eerste print poging zal de printer een foutmelding tonen.
    Klik op **Resume**. Er verschijnt een dialoog welke naar account
    gegevens vraagt. Vul hier je science account en wachtwoord in.
    Eventueel kun je de credentials toevoegen aan je **Key Ring**.
-   en klaar.

N.b. Het is mogelijk dat in de URI de loginnaam en het wachtwoord
verwerkt moeten worden zoals aangegeven hieronder voor OS X 10.5. Als er
een ; (puntkomma) in je wachtwoord zit, leidt dit tot problemen.

#### OS X 10.5 (Leopard)

-   Ga naar Systeemvoorkeuren, selecteer “Afdrukken en faxen”
-   Klik onder de lijst met printers op het plusje
-   Klik in de knoppenbalk op “Geavanceerd” (staat deze er niet: klik
    rechts op het menu en kies “pas knoppenbalk aan” en sleep de knop
    “Geavanceerd” naar de knoppenbalk)
-   Vul de volgende gegevens in:
    -   Type: Windows
    -   URI:
        **<smb://loginnaam:wachtwoord@printsmb/>`<printernaam>`**\
        Indien eduroam gebruikt wordt voeg aan de **`/etc/hosts`** file
        de onderstaande regel toe:\
        **131.174.30.41 printsmb**
    -   Naam en locatie zijn eigen keuze. Eventueel
        besturingsbestand/printermodel kiezen.
-   Klik “Voeg toe”

#### OS X 10.4 (Tiger)

-   Open “System Preferences”, selecteer “Print & Fax”
-   Klik op de ”+” knop, waarna de “Printer Browser” opent
-   Houd de ALT-toets ingedrukt en klik dan op “More Printers”, dan
    verschijnt er een dialoogvenster
-   De bovenste selectieknop moet op “Advanced” staan en de tweede op
    “Windows Printer via SAMBA”
-   Als “device name” mag je de naam nemen waarmee je zelf naar de
    printer wil refereren
-   Als “device URI” moet je nemen
    <smb://loginnaam:wachtwoord@printsmb/printernaam>, waar je zelf je
    eigen loginnaam en wachtwoord in moet vullen en de officiele naam
    van de printer als printernaam.
-   Kies als je wil het precieze printermodel (bijvoorbeeld HP Laserjet
    4200 series printer met 500-sheet input tray en duplex unit en 64 MB
    memory)
-   Klik “Add”. Klaar!
