---
title: About Phishing via e-mail
author: sioo
date: 2022-12-16
keywords: []
tags: [email, security]
cover:
  image: img/2022/phishing.png
ShowToc: false
TocOpen: false
---
# Phishing

Trying to acquire useful data, ie login credentials, for abusing someone else's ICT infrastructure.

## Methods of phishing

In general, phishing works by send a clickable link surrounded by an important message about Salary, payment or a prize, a punishment or fee. This distracts your rational mind and ensures your emotional response controls your mouse-clicking finger.

{{< notice tip >}}
Always check the link (URL) on which you click (or not) if it is a known address. For instance, one ending with `.ru.nl`.

| url | safe/unsafe |
| ----| :----------------: |
| `https://mail.ru.nl/`  |    safe  |
| `https://mail-ru-nl.blatherbeastoftraal.in/`  |    unsafe  |
| `https://webmail.science.ru.nl/`  |    safe  |
| `https://scienceroundcube.lukewarmmail.com/`  |    unsafe  |
| `https://roundcube.pages.science.ru.nl/`  |   ??? |

Better safe than sorry, the last one could be an inside attacker!
{{< / notice >}}

The page you end up on looks like something you are used to login with your credentials and when you enter them, the site records the credentials and they can later be used to login to IT systems owned by us, and abuse them.

The leaked credentials are formally speaking a dataleak and this must be reported immediately to the ICT department!

## Consequenses of succesful phishing

Using a valid login gives the attacker access to all the rightful owner of the credentials is allowed to do on the IT systems.
Considering that the attacker is usually not inside the RU, attacking the infrastructure for external use is the first thing that happens.

## Consequenses for the Radboud University

- IT departments must contain the damage, so blocking accounts, reducing functionality, temporarily of permanently.
- Red tape must be produced in large quantities for administering the data leak
- Damage of reputation caused by abuse of e-mail infrastructure, causing problems for all Radboud e-mail users.
- In case of ransomware, loss of data and work, because older backups need to be restored (time!)
- And worse things...

## Consequenses for the owner of leaked credentials

- The account (or multiple accounts!) will be blocked
- new password(s) need to be set
- possible loss of data (because the attacker may have had access to the owners data)
- lots of `undeliverable` messages, also for others whose e-mail addresses were abused as sender, but not leaked.

## Special case; *spearphishing*

***Spearphishing*** is a more precise attack to trap a specific, named, person. Usually this is done to people who are higher up the hierarchy in an organisation, making success more valuable to the attacker.

A frequently used scam is one where a supervisor seems to ask one of his/her phd's or employees: "I'm in a meeting, I need a few vouchers ASAP, can you purchase and send them to me please!" (Or a variation on this theme)

{{< notice tip >}}
Check the sending address very carefully, check the e-mail headers if you have to. Usually it should be quite obvious that the e-mail address is not the one used usually by the person you thought it was from.
{{< / notice >}}
