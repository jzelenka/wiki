---
author: petervc
date: '2022-12-21T13:20:00Z'
keywords: []
lang: en
tags:
- software
title: SSH
wiki_id: '155'
---
## SSH Secure Shell

SSH is used to get a secure terminal-connection to a login server. All
[Linux loginservers](/en/howto/hardware-servers/) can be reached with SSH
client software, not only on the standard port 22, but also on ports 80
and 443.

**NB: as of December 2022, not all science logins are allowed to login on the
login servers anymore**. Should you have trouble logging in on our
loginservers, [please contact postmaster](/en/howto/contact/)

### Recommended SSH client software

-   Windows:
    -   [MobaXterm](http://mobaxterm.mobatek.net/). From the MobaXterm
        website: “MobaXterm is an enhanced terminal for Windows with an
        X11 server, a tabbed SSH client and several other network tools
        for remote computing (VNC, RDP, telnet, rlogin). MobaXterm
        brings all the essential Unix commands to Windows desktop, in a
        single portable exe file which works out of the box.” The
        support of OpenGL could also be a reason to start using
        MobaXterm. If you use it professionally, you should consider
        subscribing to [MobaXterm Professional
        Edition](http://mobaxterm.mobatek.net/download.html). MobaXterm
        is available on the [S-disc](/en/howto/s-schijf/).
    -   [Mosh](https://mosh.org/) (mobile Shell) when roaming and
        intermittent connections.
    -   [PuTTY](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html).
    -   The OpenSSH client provided by
        [Cygwin](https://www.cygwin.com/).
-   Linux: Your computer should have ssh installed by default.
    Otherwise, install the openssh-client package. Install
    [Mosh](https://mosh.org/) (mobile Shell) when roaming or having
    intermittent connections.
-   OS X: the ssh client should be available on your Mac. For
    graphical/X11 functionality one can install
    [XQuartz](http://xquartz.macosforge.org). Install
    [Mosh](https://mosh.org/) (mobile Shell) when roaming or having
    intermittent connections.
-   Android: [JuiceSSH](https://juicessh.com/) or
    [ConnectBot](https://connectbot.org/). Install
    [Mosh](https://mosh.org/) (mobile Shell) when roaming or having
    intermittent connections.

### Recommended software for filetransfer

-   Windows: [MobaXterm](http://mobaxterm.mobatek.net/) or
    [WinSCP](https://winscp.net)
-   Linux: scp
-   OS X and Windows: [Cyberduck](https://cyberduck.io/)

### SSH settings and tips

To avoid warnings about possibly changed ssh keys and prevent messages
with ‘unkown host’ the first time you connect to a host in the
science.ru.nl domain, we have signed the public keys of all our servers.
If you add the following lines to the file ‘config’ in the .ssh
directory in your (local) home directory (maybe this file has to be
created)

~~~
CanonicalDomains science.ru.nl
CanonicalizeFallbackLocal no
CanonicalizeHostname yes
~~~

and the following line to .ssh/known\_hosts

~~~
@cert-authority *.science.ru.nl ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBAHpJveyOrLKFRDsbiW/29OadbCbkmUaIXnWbhVwtytbpftAc7Stj2RYa8yBmgfdm82T/UBVu1tLbeeCYQI8UlCvbAALMx+I60ux+iEGVdDBgIOjeu6LuY12pksVlXy/nKc59+m3AdMXfGHA8cI/O8eFosQLJ+dck7SBcvTT4lPhEcSQxg==
~~~

then C&CZ-signed ssh keys of science.ru.nl hosts will be automatically
accepted. The change to the ‘config’ file ensures that

`ssh lilo7`

will match with lilo7.science.ru.nl and the line in known\_hosts ensures
that only for hostnames matching with \*.science.ru.nl ssh will check
whether the public key of the host is signed by C&CZ and if that is
indeed the case will accept the host key. If you do not want to change
the config file then you will always have to use the fully qualified
hostname lilo5.science.ru.nl.

Ssh can be used for:

-   port forwarding on another host
-   proxying for example web traffic
-   almost complete vpn functionality

Please consult [this
article](http://blogs.perl.org/users/smylers/2011/08/ssh-productivity-tips.html)
for some excellent tips on how to use and configure your ssh client.

### Preventing disconnects

In case you experience connectivity issues using ssh, use the following
settings for your ssh-client. This can be done by adding the following
lines to the config file .ssh/config (or /etc/ssh/ssh\_config):

~~~
TCPKeepAlive no
ServerAliveInterval 60
ServerAliveCountMax 10
~~~
