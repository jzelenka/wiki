---
title: Email
author: bram
date: 2022-10-14
keywords:
- email
tags:
- medewerkers
- studenten
- email
cover:
  image: img/2022/email.png
ShowToc: true
TocOpen: true
---
{{< notice tip >}}

# Webmail

For quick access to your email, use one of these webmailers:

## https://roundcube.science.ru.nl/

## https://rainloop.science.ru.nl/

{{< /notice >}}


# Email settings
When setting up your e-mail programs, it is sufficient to only enter your e-mail address. This is what your Science email address looks like:

> `I.Lastname@science.ru.nl`

All technical settings are then automatically filled in for you. If that does not work, or if it is necessary to fill in the details yourself, use the settings in the table below:


## Incoming email

| item         | setting              | description                                                                            |
| ------------ | -------------------- | -------------------------------------------------------------------------------------- |
| mailprotocol | `IMAP`               | With this protocol, your mail is stored on the mail server.                            |
| server name  | `post.science.ru.nl` | The server for reading your email. This is where your mail folders live.               |
| port         | `993`                |                                                                                        |
| encryption   | `TLS`                | This encryption is used in the communication of your email program and the mail server.|
| user name    | `scienceloginname`   | Use your [Science account](/en/howto/login).                                            |


## Outgoing email

| item       | setting              | description                                |
| ---------- | -------------------- | ------------------------------------------ |
| servername | `smtp.science.ru.nl` | This server is used for sending email.     |
| port       | `587`                |                                            |
| encryption | `TLS`                |                                            |
| user name  | `scienceloginnaam`   | Use your [Science account](/en/howto/login).|


## Address book

| item               | setting
| ----------------   | --------------------
| addressbook server | `ldap.science.ru.nl`
| port               | 389
| basedn             | `DN: o=addressbook`


# Science mail features

> Consult the [DHZ page](/en/howto/dhz) for settings regarding your email.

- C&CZ uses open protocols for email. This means that you can check mail with a program of your own preference.
- A simple [antivirus-](/en/howto/email-antivirus/) and [antispam-filter](/en/howto/email-spam) is applied to every mail message upon arrival.
- Mailboxes can be [shared](/en/howto/sharing-mailboxes) between users.
- Also [labels](/en/howto/thunderbird-tags/) for mail can be shared with other users.
- Using [Sieve](/en/howto/email-sieve/) mail messages can be filtered on the server.
- There are two options for setting up mailing lists: [address-lists](#mailing-lists) and [mailman](/en/howto/mailman-lists).
- The storage space for your mail is `5 GB` by default. This can be expanded on [request](/en/howto/contact).

{{< notice tip >}}

The "sent" mail folder is often overlooked when cleaning up email. Search this folder for the mails with the largest attachments.

{{< /notice >}}


## Mailing lists

Mail sent to mailing lists is forwarded to its members. Lists can be managed through [DIY](/en/howto/dhz). Need a new list? [Contact Postmaster](/en/howto/contact).


## Alternative mail settings

- Besides the `IMAP` protocol, the `POP3` protocol is supported. If you don't know what `POP3` is, do not use it ;-). If you insist on using POP, the service listens on port `995`.
- The recommended way of sending mail is using authenticated SMTP. However, from within the Huygens building network, it is possible to deliver unauthenticated mail. In that case, mail should be delivered on port `25`.


# `@ru.nl` mail

{{< notice tip >}}

The webmailer for your @ru.nl mail address is: **https://mail.ru.nl**

{{< /notice >}}

Every student and employee of the Radboud University should have a [@ru.nl email address](http://www.ru.nl/ict/medewerkers/mail-agenda/) in the format:

> Firstname.Lastname@ru.nl

## Forwarding @ru.nl mail

It is possible to forward `@ru.nl` mail to another mail address.
This can be arranged in the webmailer. Steps:
- Log in to https://mail.ru.nl
- Go to "Settings" (gear icon), then "Options" > "Organize email".
- By clicking the `+`, a new rule can be created for incoming messages.
- Choose the option to *redirect all incoming* email to another email address.
