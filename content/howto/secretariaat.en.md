---
author: petervc
date: '2022-06-28T11:28:42Z'
keywords: []
lang: en
tags:
- overcncz
title: Secretariaat
wiki_id: '534'
---
## Who are they?

-   {{< author "alinssen" >}}

## What does she do?

The secretary takes care of:

-   Department administration.
-   Functional administration of the time registration system.
-   Licences [Matlab and toolboxes](/en/howto/matlab/).
-   Licences [Maple](/en/howto/maple/), Labview,
    [Mathematica](/en/howto/mathematica/).
