---
author: petervc
date: '2018-08-15T16:13:41Z'
keywords: []
lang: nl
tags: []
title: Laptop uitleenreglement
wiki_id: '894'
---
### Laptop uitleenreglement

## Algemeen

Studenten en medewerkers van de faculteit kunnen bij de balie van de
Library of Science tijdens de openingstijden een laptop lenen, als er
een uitleenbaar is voor de gewenste periode. Gebruik in het facultaire
onderwijs heeft voorrang.

-   Veiligheid:De lener is zelf verantwoordelijk voor de
    laptop; laat deze niet onbeheerd achter, ook niet als je even naar
    het toilet gaat of koffie gaat halen. Bij de balie van de Library of
    Science kun je een laptopslot lenen als dat even nodig is.

-   Uitleenperiode:Een laptop wordt voor gebruik
    binnen de faculteit voor een afgesproken periode (meestal minder dan
    1 dag) uitgeleend. Het lenen van een laptop voor meer dagen en/of
    gebruik buiten het Huygensgebouw is voor facultaire medewerkers en
    studenten incidenteel mogelijk. Een laptop kan op werkdagen vanaf
    08.30 uur worden opgehaald en moet van maandag tot en met donderdag
    vóór 20.00 uur worden terugbezorgd of op vrijdag voor 17.30 uur.

-   Laptop hardware:Bij de laptop kan optioneel
    ook geleend worden: stroom-adapter en/of USB-muis.

-   Laptop software: De laptop is voorzien van
    o.a. de volgende software: Windows 10, Adobe Reader, MS Office,
    F-Secure Anti virus.

Er is ook een vrij complete [lijst van alle
software](/nl/howto/windows-beheerde-werkplek/).

## Uitleenreglement laptops FNWI

Artikel 1 De laptop is eigendom van de Faculteit
Natuurwetenschappen, Wiskunde en Informatica (FNWI): de uitlener. De
laptop wordt uitgeleend aan een lid van de in Artikel 2 beschreven
doelgroep: de lener.

Artikel 2 De laptop wordt alleen uitgeleend aan
medewerkers en studenten van de FNWI. Middels de medewerkers- of
studentenpas moet men aantonen tot de facultaire gemeenschap te behoren.
Daarnaast overlegt de lener een geldig legitimatiebewijs (rijbewijs,
paspoort of identiteitskaart).

Artikel 3 De laptop wordt voor een vooraf afgesproken
duur (normaal maximaal 1 dag) uitgeleend. Het lenen en inleveren van de
laptop is alleen mogelijk tijdens de openingstijden van de balie van de
Library of Science.

Artikel 4 De laptop mag uitsluitend gebruikt worden
t.b.v. onderwijs/onderzoeksdoeleinden.

Artikel 5 Het is de lener niet toegestaan de laptop aan
derden uit te lenen of anderszins afstand van de laptop te doen. Het is
de lener alleen na voorafgaande afspraak toegestaan de laptop buiten het
gebouw mee te nemen.

Artikel 6 Wanneer de lener de laptop niet tijdig en/of
niet in de oorspronkelijke staat bij de balie van de Library of Science
inlevert, behoudt FNWI zich het recht voor om de betreffende persoon in
de toekomst geen laptop meer te lenen.

Artikel 7 Schade en/of verlies van de laptop – met
uitzondering van schade die het gevolg is van normaal gebruik – komt
volledig voor rekening van de lener. Van diefstal wordt door de lener
altijd aangifte bij de politie gedaan. In geval van diefstal van de
laptop dient de lener de Faculteit hiervan onmiddellijk op de hoogte te
stellen waarbij het proces-verbaal overlegd wordt.

Artikel 8 Het is de lener niet toegestaan wijzigingen in
de configuratie van de laptop aan te brengen. Ook het zelf installeren
van software is niet toegestaan.

Artikel 9 De uitlener (FNWI) is bevoegd tussentijds en
zonder enige opzegtermijn de onmiddellijke teruggave van de laptop te
verlangen indien de lener de laptop verwaarloost, misbruikt, voor een
ander doel gebruikt dan waarvoor deze bestemd is of de lener op
enigerlei wijze in strijd handelt met de bepalingen van dit reglement.

Artikel 10 Het gebruik van de laptop is voor eigen
risico. De uitlener is niet aansprakelijk voor schade van welke aard dan
ook.
