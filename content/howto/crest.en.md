---
author: jzelenka
date: '2022-06-24T12:18:55Z'
keywords: []
lang: en
tags: []
title: Crest
wiki_id: '1090'
---
## Prerequisities

-   Same as for [gaussian](/en/howto/gaussian/)
-   set-up environment (check [gaussian](/en/howto/gaussian/))

## General topics

General topics like queue handling, queues and similar are covered in
the [gaussian](/en/howto/gaussian/) wiki entry.

## Submitting jobs

Basic syntax of crest submission script is following:

`cnorris@lilo7:~$ Crest structure.xyz`

where *structure.xyz* is the structure which you want to submit.

More advanced syntax looks like this:

`cnorris@lilo7:~$ Crest structure.xyz -chrg 1 -uhf 0 -quick`

Here I’ve added quick keyword which makes the screening less in-depth
but it goes much quicker. I also added chrg keyword to define charge and
uhf keyword to define spin state. For more detailed description and full
list of keywords, please refer to [crest command line arguments
documentation](https://xtb-docs.readthedocs.io/en/latest/crestcmd.html).

## Processing calculations

Once calculation is finished, conformers list and best structure from
crest structure pool is received. There is also a folder where rotamers
file and other files are stored.

As Crest uses very cheap calculations, the energies are not fully
reliable. After running crest you should go through the conformers list,
pick the conformations which differ substantially (preferably \~10
structures) and run them with a more precise calculation (for example
\#B97/SDD opt in Gaussian). Based on the outcome of this second
calculation batch you should decide which structures you want to submit
for expensive calculations, frequencies jobs and similar.

## Disclaimer

Crest is a metadynamics tool which is very useful for generating
low-energy structure candidates. The crest-best does not have to be the
best structure, but the set of structures (crest-conformers) usually has
the best available conformers within. Crest is a powerful and very
useful tool, but it should not be used uncritically. Keep attention to
the correct definition of charge and spin. If you think there is a way
to improve this guide, please contact me (Jan).

Happy computing! ;)
