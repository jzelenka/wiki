---
author: petervc
date: '2019-09-06T16:16:30Z'
keywords: []
lang: en
tags:
- internet
title: Internet studenten
wiki_id: '69'
---
## students

## WWW by students

All student associations of the Science Faculty have their
own WWW service. These are

-   [Thalia](http://www.thalia.nu/) (Computer
    Science), *[1](mailto:www-thalia@science.ru.nl)*
-   [Desda](https://www.desda.org/) (Mathematics),
    *[2](mailto:www-desda@science.ru.nl)*
-   [Sigma](https://www.vcmw-sigma.nl/) (Chemistry),
    *[3](mailto:www-sigma@science.ru.nl)*
-   [Marie Curie](http://www.marie.science.ru.nl/)
    (Physics), *[4](mailto:www-marie@science.ru.nl)*
-   [BeeVee](http://www.beevee.science.ru.nl/)
    (Biology), *[5](mailto:www-beevee@science.ru.nl)*
-   [Leonardo](http://www.leonardo.science.ru.nl/)
    (Sciences),
    *[6](mailto:www-leonardo@science.ru.nl)*

Students who would like to start their own www service are advised to
contact their association using the association’s website or by sending
a mail to one of the mail addresses.

If that doesn’t work out you can always visit C&CZ and talk to one of
the [system administrators](/en/howto/systeemontwikkeling/)
to see if it is possible to start a new WWW service.
