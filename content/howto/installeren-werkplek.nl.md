---
author: bram
date: '2022-09-19T16:27:01Z'
keywords: []
lang: nl
tags:
- hardware
title: Installeren werkplek
wiki_id: '40'
---
## PC installatie

Bij de installatie van een nieuwe PC zijn er verschillende
mogelijkheden.

-   Als de PC niet aan het netwerk gekoppeld wordt, zal C&CZ weinig of
    geen hulp kunnen bieden bij de installatie. U kunt DVD/CD’s met een
    Windows Operating System en applicatie software zoals MS-Office, etc
    bij C&CZ [lenen](/nl/tags/software).
-   Als de PC wel aan het netwerk gekoppeld is (middels een PXE enabled
    netwerkkaart), kan men een volledig automatische installatie van
    Windows met standaard applicaties laten uitvoeren. In dat geval
    wordt de PC een [beheerde werkplek PC](/nl/howto/windows-beheerde-werkplek/).\
    Het ook mogelijk om een volledig automatisch installatie van Ubuntu
    Linux met standaard applicaties uit te voeren.\
    U dient in beide gevallen contact op te nemen met C&CZ en de PC aan
    te melden.
