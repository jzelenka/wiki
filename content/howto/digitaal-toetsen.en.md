---
author: petervc
date: '2019-08-16T15:56:14Z'
keywords: []
lang: en
tags:
- studenten
- docenten
- onderwijs
title: Digitaal toetsen
wiki_id: '1004'
---
## RU digital assessment and Chromebooks

In order to facilitate large scale digital assessment, the university
purchased ca. 1000
[Chromebooks](https://en.wikipedia.org/wiki/Chromebook). For these,
[Cirrus Assessment](http://www.cirrusassessment.com/) was chosen as user
friendly assessment software. In the future, the faculty digital
assessment software (TAO, see below) will only be necessary for
assessments that Cirrus can’t handle. Also the wireless network in the
Gymnasion/RSC was upgraded to give this large number of Chromebooks a
good wireless network connection. More information on this cam be found
on the [RU page on digital
assessments](https://www.ru.nl/lecturers/education/assessment-appraisal/digital-assessment/).

Chromebooks only run webbased applications, in a browser. In June 2018,
more than 300 students Computing Science were successfully graded in a
large scale digital assessment with these Chromebooks using a Windows
application (a modern computer program development environment) that ran
in a tab of the Safe Exam Browser and was delivered through [Amazon
Appstream 2.0](https://aws.amazon.com/appstream2/).

## Faculty digital testing environment: TAO

We offer a digital testing/assessment environment, based on the open
source product [TAO Testing](http://www.taotesting.com/), that can be
used for both summative and formative testing. Although TAO supports
many question types, only a limited selection is supported in the
operational setup at the Faculty of Science:

-   [extendedTextInteraction](https://userguide.taotesting.com/3.2/interactions/extended-text-interaction.html)
-   [choiceInteraction](https://userguide.taotesting.com/3.2/interactions/choice-interaction.html)
-   [hotspotInteraction](https://userguide.taotesting.com/3.2/interactions/hotspot-interaction.html)
-   [associateInteraction](https://userguide.taotesting.com/3.2/interactions/associate-interaction.html)
-   [matchInteraction](https://userguide.taotesting.com/3.2/interactions/match-interaction.html)
-   [orderInteraction](https://userguide.taotesting.com/3.2/interactions/order-interaction.html)

In TAO itself, there is no support for entering mathematical or chemical
formulae in answers, although formulae can be used in questions.
Entering computer source code is possible in a free text field, but not
in a manner which resembles the computer program development
environments people are used to nowadays. In these cases, a programming
environment can used, next to the browser with TAO. Digital tests have
been done with Windows Paint, Microsoft Word,
[Clean](https://clean.cs.ru.nl/Clean) and [Virtual
NMR](http://www.virtualnmr.org)) next to the test software TAO.

## Parts

The environment consists of five parts, all are web-based:

1.  A “database” environment in which lecturers can create and manage
    their own collection of exam questions and answers.
2.  An “authoring” environment in which lecturers can create exams from
    questions from the database.
3.  A “testing” (exam) environment in which tests can be performed. The
    tests take place on the [computer lab](/en/howto/terminalkamers/) PCs
    managed by C&CZ with [Safe Exam
    Browser](https://www.safeexambrowser.org/).
4.  A “grading” environment in which the answers that were handed in can
    be reviewed and graded.
5.  A “review” environment in which participants can view their graded
    test.

At this moment, all parts of the environment exist and are operational.

## Advantages and disadvantages of digital assessments

The advantages of digital testing are many.

1.  Correcting/grading, especially for large numbers of participants,
    can be done much faster.
    1.  Answers to open questions are typed in, solving the poor
        handwriting problem.
    2.  Automatic grading of multiple-choice questions is possible.
2.  As the database of exam questions grows, it becomes easier for
    lecturers to create tests.
3.  For most students typing and/or clicking is much easier than
    writing.
4.  For courses in Computing Science, especially computer programming,
    the possibility to type and modify code instead of writing code on
    paper is a major improvement.
5.  Functionality as offered by Versatest (digital microscopy) is only
    possible in a digital environment (but it has not yet been
    integrated in TAO Testing).
6.  We have integrated the course survey (Alice) at the end of the
    digital test which has led to a vast increase of the survey response
    percentage.

There are also disadvantages:

1.  Digital testing introduces new cheating risks. It may be difficult
    to eliminate all of them.
2.  Digital testing places a burden on the available PCs for students.
    This problem increases as the use of digital testing grows. See
    [Chromebooks](/en/howto/digitaal-toetsen/).
3.  The PCs in the computer labs are not shielded from a major power
    outage. If this happens the exam can only be cancelled.
4.  The current solution can not easily be used for exam questions where
    the answer requires the use of mathematical or chemical formulae,
    reaction equations, diagrams, in general any substantial amount of
    free format “drawing” by the student.

