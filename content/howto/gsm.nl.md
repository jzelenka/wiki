---
author: petervc
date: '2022-06-24T13:04:49Z'
keywords: []
lang: nl
tags:
- telefonie
title: GSM
wiki_id: '166'
---
-   **GSM**

Het Huygensgebouw had origineel slechte mobiele dekking, het heeft
eigenschappen van een [kooi van
Faraday](http://nl.wikipedia.org/wiki/Kooi_van_Faraday). Daarom is in
2011 door de Faculteit NWI een
[DAS-installatie](http://en.wikipedia.org/wiki/Distributed_antenna_system)
(gedistribueerd antennesysteem) geïnstalleerd, waardoor mobiele
telefonie via de netwerken van de erop aangesloten provider Vodafone
mogelijk is. Eind 2014 is door FNWI in het Huygensgebouw vrijwel
complete dekking gerealiseerd voor mobiele telefoons op het
Vodafone-netwerk, dat gebruikt wordt voor [Vodafone Wireless
Office](/nl/tags/telefonie/). Dit werd mede mogelijk doordat Ericsson en
Vodafone in de kelders en transportgangen van het Huygensgebouw een
pilot wilden doen met [het eerste operationele Radio Dot-systeem
wereldwijd in de zakelijke markt](http://www.ericsson.com/news/1878703).
Sommige oudgedienden denken hierbij terug aan juni 1987, toen bij FNWI
door PTT, Philips, SURF en de universiteit een pilot is uitgevoerd met
de eerste [PABX](http://nl.wikipedia.org/wiki/PABX) in Nederland met
veel dataverkeer. Deze ISDN-voorbereide telefooncentrale is in januari
2018 uitgefaseerd.

Zoals gezegd is alleen Vodafone op de FNWI DAS-installatie aangesloten.
Dit wordt gebruikt voor de [Vodafone Wireless
Office](/nl/tags/telefonie/) (VWO) mobiele bedrijfstelefonie.

Bellen van VWO-GSM’s en van alle vaste toestellen van de SKU naar alle
VWO-GSM’s is gratis zolang dat is binnen zone 1 (ruwweg Europa en een
paar extra landen) zoals gedefinieerd door Vodafone. Aanvragen van
VWO-abonnementen en de bijbehorende toestellen loopt [via
ILS](http://www.ru.nl/ict/medewerkers/telefonie/vast-mobiel/), waar dus
ook de specifieke en up-to-date informatie te vinden is.

Vanwege de hoge kosten is het niet waarschijnlijk dat andere providers
op de DAS-installatie aan zullen sluiten. De verwachting is dat betere
interne bereikbaarheid voor andere providers dan Vodafone zal komen van
[4G-bellen (VoLTE)](https://en.wikipedia.org/wiki/Voice_over_LTE) en
[VoWLAN/Vowifi](https://en.wikipedia.org/wiki/Voice_over_WLAN). Sinds
een paar jaar is er een project dat zal resulteren in campusbrede
indoor-dekking voor alle providers. Gezien de omvang van het project kan
op dit moment geen opleverdatum gegeven worden.

Mobiele telefoons, beheer, abonnementen en tarieven vallen onder
verantwoordelijkheid van ILS.
