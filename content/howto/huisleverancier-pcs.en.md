---
author: petervc
date: '2022-06-28T14:57:16Z'
keywords: []
lang: en
tags:
- hardware
- hardware
title: Huisleverancier PC's
wiki_id: '42'
---
### Standard supplier: Dustin (formerly CentralPoint/Scholten Awater)

The standard supplier of pc’s for Radboud University is [Dustin
(formerly CentralPoint/Scholten Awater](http://www.dustin.nl/) since
March, 2006, after several European tenders.

### Limited selection

The [ISC](http://www.ru.nl/isc) picks a limited selection of tested
products with often a favourable price. These can be seen via BASS at
CentralPoint in “Mijn assortiment” as RU-laptop, RU-desktop etc.

### Website

From within [the purchasing system BASS](https://www.bass.ru.nl) you can
reach the [website of CentralPoint](http://www.centralpoint.nl/) with RU
pricing.

### Ordering

Just like other orders, through [the financial system](/en/howto/bass/)
delivered to [the purchasing department](http://www.ru.nl/inkoop).
Quotes can be attached.

### Basic configuration

There will always be some basic configurations for desktop, monitor and
laptop. These basic pc’s have been tested: they can be installed over
the network with a standard procedure. Take note: Radboud University has
a site license for an UPGRADE of MS-Windows for desktops. You can buy
the cheapest version of MS-Windows (home-edition, included in the basic
pc). When installing over the network, the professional version will be
installed. If you don’t use the network installation, you can borrow
[Windows CD/DVD’s](/en/howto/microsoft-windows/) from C&CZ.

### Warranty

See Dutch version.

### Packaging

See Dutch version.

### Disposal

See Dutch version.
