---
author: arranc
date: '2022-09-12T06:31:56Z'
keywords: []
lang: nl
tags: []
title: FoxIt PDF Reader
wiki_id: '674'
---
Het programma [FoxIt PDF
Reader](http://www.foxitsoftware.com/pdf/rd_intro.php) kan gebruikt
worden als alternatief voor Adobe Reader, het is een gratis programma om
PDF-bestanden te bekijken en te printen.

Een reden om soms Adobe Reader niet te gebruiken is dat Adobe Reader 8
PostScript produceert waar de PostScript engine in HP 4250/4350
printers niet mee overweg kan, zelfs niet na een firmware upgrade.
Daarom heeft C&CZ voor de meeste HP4250/HP4350 printers de (langzamere)
PCL-driver als standaard driver ingezet in plaats van de PS-driver.
