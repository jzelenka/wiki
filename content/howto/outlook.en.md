---
author: visser
date: '2013-05-31T08:01:20Z'
keywords: []
lang: en
tags:
- email
title: Outlook
wiki_id: '21'
---
## Bij 1e keer opstarten van Outlook op een **beheerde werkplek** PC een E-mail account instellen

::: {style="min-height: 48px; padding-left: 48px; background: url('/pics/outlook/messagebox_warning.gif') no-repeat"}
Heeft U al eerder outlook opgestart, zonder bij “Do you want to start
outlook in safe mode?” “No” gekozen te hebben, lees dan eerst:
[Outlook\#Bij 1e keer opstarten van Outlook naar safemode
gegaan](/en/howto/outlook#bij-1e-keer-opstarten-van-outlook-naar-safemode-gegaan/).
:::

```{=html}
<table>
```
```{=html}
<tbody>
```
```{=html}
<tr class="odd">
```
```{=html}
<td>
```
```{=html}
<p>
```
Start outlook door op het goede icoontje te klikken.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="01-first-click.jpg" title="Icon from Outlook" alt="" />`{=html}
```{=html}
<figcaption>
```
Icon from Outlook
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="even">
```
```{=html}
<td>
```
```{=html}
<p>
```
Bij de vraag “Do you want to start Outlook in safe mode?”, “No”
antwoorden.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="02-first-run_safe-mode.jpg" title="Start in safe mode?" alt="" />`{=html}
```{=html}
<figcaption>
```
Start in safe mode?
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="odd">
```
```{=html}
<td>
```
```{=html}
<p>
```
Hierna start outlook automatisch de “Outlook 2002 Startup” wizard, klik
op “Next”.
```{=html}
</p>
```
```{=html}
<ol>
```
```{=html}
<li>
```
N.B. de “Outlook 2007 Startup” wizard verschilt op details.
```{=html}
</li>
```
```{=html}
</ol>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="03-first-run_startup-wizard.jpg" title="Outlook 2002 startup wizard" alt="" />`{=html}
```{=html}
<figcaption>
```
Outlook 2002 startup wizard
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="even">
```
```{=html}
<td>
```
```{=html}
<p>
```
Het keuzebolletje staat op “Yes” standaard, wat we willen, klik op
“Next”.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="04-first-run_startup-add_account.jpg" title="Would you like to configure an E-mail account?" alt="" />`{=html}
```{=html}
<figcaption>
```
Would you like to configure an E-mail account?
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="odd">
```
```{=html}
<td>
```
```{=html}
<p>
```
Selecteer als server type bij voorkeur “IMAP”.
```{=html}
</p>
```
```{=html}
<ol>
```
```{=html}
<li>
```
Als U nog met de ‘oude’ IMAP server werkt (imap.science.ru.nl -of-
imap-srv.science.ru.nl) heeft het de voorkeur om naar de nieuwe IMAP
server over te stappen. Neem hierover contact op met de helpdesk of
systeembeheer.
```{=html}
</li>
```
```{=html}
<li>
```
Bij de nieuwe IMAP server kun je ook nog met POPS (POP-over-SSL)
verbinding maken mocht dat gewenst zijn. Bij de volgende stappen wordt
ervan uitgegaan dat U uw E-mail account met de nieuwe IMAP server opzet.
```{=html}
</li>
```
```{=html}
</ol>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="05-first-run_startup-server.jpg" title="Select Server Type..." alt="" />`{=html}
```{=html}
<figcaption>
```
Select Server Type…
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="even">
```
```{=html}
<td>
```
```{=html}
<p>
```
Vul uw naam in, en de overige gegevens zoals deze vermeld zijn op de
`<a href="http://dhz.science.ru.nl">`{=html}dhz website`</a>`{=html}.
Vul bij: “E-mail Address” in wat bij DHZ staat achter: “Email address:”
“Incoming mail server (IMAP)” in wat bij DHZ staat achter: “Imap/pop
mailserver:”. “Outgoing mail server (SMTP)” in: smtp.science.ru.nl “User
Name” in wat bij DHZ staat achter: “Username:”. “Password” niets in, en
zet het vinkje bij “Remember password” uit. - dit t.b.v. de veiligheid.
De “Secure Password Authentication (SPA)” optie niet aanvinken.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="06-first-run_imap-settings.jpg" title="Internet E-mail Settings (IMAP)" alt="" />`{=html}
```{=html}
<figcaption>
```
Internet E-mail Settings (IMAP)
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="odd">
```
```{=html}
<td>
```
```{=html}
<p>
```
Invullen van “More Settings…”. Het belangrijkste tabblad “Advanced” moet
als volgt ingesteld worden:
```{=html}
</p>
```
```{=html}
<ol>
```
```{=html}
<li>
```
Het tabblad “Advanced” “Incoming server (IMAP)” en “Outgoing server
(SMTP)” het vinkje “This server requires an SSL-secured connection
(SSL)” aan. Verander eventueel het poortnummer, bij SMTP - Port in 587,
zie de `<a href="email_authsmtp" title="wikilink">`{=html}Instellen van
Authenticated SMTP handleiding`</a>`{=html} voor uitleg hierover. Klik
op “OK”. De overige tabbladen behoren de onderstaande instellingen te
hebben:
```{=html}
</li>
```
```{=html}
<li>
```
Onder “Outlook 2007” ziet het “Advanced” tabblad er anders uit en dient
te worden gekozen voor “Use the following type of encrypted connection:
SSL” onder Incoming server. Terwijl onder Outgoing server het type op
“TLS” ingesteld moet worden.
```{=html}
</li>
```
```{=html}
</ol>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="outlook-advanced-imap_and_smtp-ssl.jpg" title="Internet E-Mail Settings -&gt; Advanced" alt="" />`{=html}
```{=html}
<figcaption>
```
Internet E-Mail Settings -\> Advanced
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="even">
```
```{=html}
<td>
```
```{=html}
<p>
```
Het tabblad “General”
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="07.1-first-run_imap-adv_general.jpg" title="Internet E-Mail Settings -&gt; General" alt="" />`{=html}
```{=html}
<figcaption>
```
Internet E-Mail Settings -\> General
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="odd">
```
```{=html}
<td>
```
```{=html}
<p>
```
Het tabblad “Outgoing Server”
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="outlook-authsmtp-vink.jpg" title="Internet E-Mail Settings -&gt; Outgoing Server" alt="" />`{=html}
```{=html}
<figcaption>
```
Internet E-Mail Settings -\> Outgoing Server
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="even">
```
```{=html}
<td>
```
```{=html}
<p>
```
Het tabblad “Connection”
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="07.3-first-run_imap-adv_connection.jpg" title="Internet E-Mail Settings -&gt; Connection" alt="" />`{=html}
```{=html}
<figcaption>
```
Internet E-Mail Settings -\> Connection
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="odd">
```
```{=html}
<td>
```
```{=html}
<p>
```
Klik op “Finish”.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="08-first-run_imap-finish.jpg" title="All required information has been entered." alt="" />`{=html}
```{=html}
<figcaption>
```
All required information has been entered.
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="even">
```
```{=html}
<td>
```
```{=html}
<p>
```
Deze foutmelding kun je krijgen bij het opstarten van Outlook, klik op
“OK”, het vinkje “Do not show this message” werkt helaas niet.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="09-launch-error.jpg" title="Activity Log Access Error" alt="" />`{=html}
```{=html}
<figcaption>
```
Activity Log Access Error
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="odd">
```
```{=html}
<td>
```
```{=html}
<p>
```
Vul de volledige naam en initialen in. Als dit niet de eerste keer is
dat een office applicatie geopend wordt, dan krijg je dit venster niet.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="10-first-run_user-name.jpg" title="Enter full name and initials." alt="" />`{=html}
```{=html}
<figcaption>
```
Enter full name and initials.
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="even">
```
```{=html}
<td>
```
```{=html}
<p>
```
Klik in de menubalk op “Tools -\> Customize…” Vink “Always show full
menus” aan. En klik op “Close”.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<p>
```
`<img src="11.1-first-run_customize.jpg" title="fig:Tools - Customize..." alt="Tools - Customize..." />`{=html}
`<img src="11.2-first-run_customize_show-full-menus.jpg" title="fig:Customize dialog box" alt="Customize dialog box" />`{=html}
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="odd">
```
```{=html}
<td>
```
```{=html}
<p>
```
Klik in de menubalk op “View -\> Outlook Bar” om het ‘vinkje’ weg te
halen…
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="12.1-first-run_view-folder-list.jpg" title="View -&gt; Outlook Bar" alt="" />`{=html}
```{=html}
<figcaption>
```
View -\> Outlook Bar
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="even">
```
```{=html}
<td>
```
```{=html}
<p>
```
ga daarna nogmaals naar de menubalk ok “View -\> Folder List” klikken om
de optie aan te zetten.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="12.2-first-run_view-folder-list.jpg" title="View -&gt; Folder List" alt="" />`{=html}
```{=html}
<figcaption>
```
View -\> Folder List
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="odd">
```
```{=html}
<td>
```
```{=html}
<p>
```
Uiteindelijk zou je aan de linkerkant van het ‘Outlook scherm’ deze
boomstructuur moeten zien. De “Outlook bar” zou zowiezo niet gebruikt
moeten worden omdat deze zowel incorrecte als onvolledige informatie kan
bevatten, de “Folder List” kan wat lastiger navigeren zijn maar klopt
(bijna) altijd.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="12.3-first-run_view-folder-list.jpg" title="Correct tree structure." alt="" />`{=html}
```{=html}
<figcaption>
```
Correct tree structure.
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="even">
```
```{=html}
<td>
```
```{=html}
<p>
```
Dubbelklik op “post.science.ru.nl” in de “Folder List” of klik op het
plusteken links ervan. Outlook vraagt nu om het wachtwoord van de
loginnaam die ingevuld is.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="13.1-first-run_credentials.jpg" title="Please enter username and password..." alt="" />`{=html}
```{=html}
<figcaption>
```
Please enter username and password…
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="odd">
```
```{=html}
<td>
```
```{=html}
<p>
```
Als je de volgende security warning krijgt, klik dan op [No], sluit
outlook af en importeer dan eerst het
`<a href="http://certificate.science.ru.nl/cacert.der">`{=html}root
certificaat`</a>`{=html}. Daarna krijg je deze foutmelding niet meer.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<figure>
```
`<img src="13.2-first-run_Internet_Security_Warning.jpg" title="Internet security warning" alt="" />`{=html}
```{=html}
<figcaption>
```
Internet security warning
```{=html}
</figcaption>
```
```{=html}
</figure>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="even">
```
```{=html}
<td>
```
```{=html}
<p>
```
Als het wachtwoord geaccepteerd is zie je onder “post.science.ru.nl” het
item “Inbox” met een plus ervoor. Rechts zie je een melding dat alleen
subfolders van “post.science.ru.nl” berichten kunnen bevatten. Dit zie
je altijd en geeft alleen aan dat het “post.science.ru.nl” item een
logische structuur is om verschillende (IMAP) accounts uit elkaar te
kunnen houden.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<p>
```
[[Image:13.3-first-run\_unable-to-display-root-of-IMAP-store.jpg\|Unable
to display the folder.
```{=html}
</p>
```
```{=html}
<p>
```
`          This is the root of an IMAP store. The root does not contain messages but may have subfolders that do.``<br />`{=html}
`          To see messages in the subfolders, display the folder first, and then click the subfolder.]]  `
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="odd">
```
```{=html}
<td>
```
```{=html}
<p>
```
Om te zorgen dat je altijd alle mail-folders kunt zien, klik rechts op
“post.science.ru.nl” en selecteer “IMAP Folders…” Haal het vinkje bij
“When displaying hierarchy in Outlook, show only subscribed folders.”
weg. En klik op “OK”.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<p>
```
`<img src="14.1-first-run_IMAP-folders-no_show-only-subscribed.jpg" title="fig:IMAP Folders..." alt="IMAP Folders..." />`{=html}
`<img src="14.2-first-run_IMAP-folders-no_show-only-subscribed.jpg" title="fig:IMAP Folders subscription dialog box" alt="IMAP Folders subscription dialog box" />`{=html}
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
<tr class="even">
```
```{=html}
<td>
```
```{=html}
<p>
```
AutoArchive uitzetten, niet noodzakelijk maar voorkomt dat email
“verdwijnt” zonder dat het expliciet geconfigureerd is. Tevens komt dan
alsnog email in een Outlook ‘.pst’ wat niet wenselijk is aangezien dit
een ‘Black box’ bestandsformaat is.`<br />`{=html} Ga via de menubalk
naar “Tools -\> Options” ga naar het tabblad “Other”. Zet het vinkje bij
“Enable Instant Messaging in Microsoft Outlook” uit. Klik op de
“AutoAchive…” knop. Zet het vinkje bij “Run autoarchive every … days”
uit. En klik beide vensters met “OK” dicht.
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
<td>
```
```{=html}
<p>
```
`<img src="16.1-first-run_AutoArchive-off.jpg" title="fig:Tools -&gt; Options -&gt; [Other] -&gt; [AutoAchive..." alt="Tools -&gt; Options -&gt; [Other] -&gt; [AutoAchive..." />`{=html}]
`<img src="16.2-first-run_AutoArchive-off-checkbox.jpg" title="fig:AutoArchive dialog box" alt="AutoArchive dialog box" />`{=html}
```{=html}
</p>
```
```{=html}
</td>
```
```{=html}
</tr>
```
```{=html}
</tbody>
```
```{=html}
</table>
```
## Bij 1e keer opstarten van Outlook bij een **zelf geïnstalleerde** PC een E-mail account instellen

1.  Na de installatie van Outlook de configuratie doorlopen zoals bij de
    [Outlook\#Bij 1e keer opstarten van Outlook bij een beheerde
    werkplek PC een E-mail account
    instellen](/en/howto/outlook#bij-1e-keer-opstarten-van-outlook-bij-een-beheerde-werkplek-pc-een-e-mail-account-instellen/)
    procedure.
2.  De vraag “Do you want to start outlook in safe mode?” zal niet
    verschijnen. Het Outlook databestand zal standaard op de “verkeerde”
    plek terechtkomen. Volg hierom na de configuratie nog [Outlook\#Bij
    1e keer opstarten van Outlook naar safemode gegaan. Wat
    nu?](/en/howto/outlook#bij-1e-keer-opstarten-van-outlook-naar-safemode-gegaan.-wat-nu?/).

## Bij 1e keer opstarten van Outlook naar safemode gegaan. Wat nu?

  ------------------------------------------------------- ----------------------------------------------------------------------------------------------------------
  Sluit Outlook af, als deze open staat. En ga naar       {{< figure src="/img/old/control-panel-category.jpg" title="Control Panel -\> category view" >}}
  “Start -\> Settings -\> Control Panel”. Klik op de      
  “Data Files…” knop.                                     

  Het “Outlook Data Files” venster opent. Let hierbij op  {{< figure src="/img/old/outlook-data-files-location.jpg" title="Outlook Data Files locatie" >}}
  de bestandsnaam “Filename” waarbij aangegeven staat in  
  de kolom “Comment” dat het de **Mail delivery           
  location** is. Begint de bestandsnaam met “C:\\” - of   
  preciezer met een driveletter die niet een gebackupde   
  netwerkschijf is ga dan verder met de onderstaande      
  stappen.                                                

  Zorg dat de **Filename** kolom breed genoeg is op de    {{< figure src="/img/old/outlook-data-files-location-cut.jpg" title="Knippen bestand." >}}
  bestandsnaam helemaal te lezen. Onthoud de              
  bestandsnaam. Klik in het “Outlook Data Files” venster  
  op de regel met **Mail delivery location** en klik op   
  **Open Folder…**.                                       

  Selecteer de zojuist onthouden bestandsnaam, en         {{< figure src="/img/old/personal-folders-location-error.jpg" title="Kan 'm niet vinden." >}}
  verplaats het bestand naar een (nieuwe) map op een      {{< figure src="/img/old/personal-folders-location-found.jpg" title="Selecteer het juiste bestand" >}}
  gebackupde netwerkschijf b.v. **H:\\My Outlook\\**. Ga  
  terug naar het “Outlook Data Files” venster. Dubbelklik 
  op de regel met in de kolom “Comment” dat het de **Mail 
  delivery location** - het bestand dat net verplaatst    
  is. Je ziet een foutmelding dat het bestand op de       
  originele lokatie niet gevonden kan worden. Klik op     
  **Ok**. Ga via het **Create/Open Personal Folders       
  file** dialoogvenster naar de lokatie waar het bestand  
  heen verplaatst is en klik op **Open**. Klik op “OK”,   
  “Close en nogmaals **Close** om de”Outlook - Mail       
  Setup" af te sluiten.                                   
  ------------------------------------------------------- ----------------------------------------------------------------------------------------------------------
