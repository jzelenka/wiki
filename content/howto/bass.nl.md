---
author: petervc
date: '2022-06-30T10:41:54Z'
keywords: []
lang: nl
tags:
- software
- medewerkers
title: BASS
wiki_id: '656'
---
De Radboud Universiteit gebruikt een concern-applicatie BASS, met voor
financiën
[BASS-FinLog](http://www.radboudnet.nl/bass/bass-finlog/handleidingen/)
en voor personeelszaken
[BASS-HRM](http://www.radboudnet.nl/bass/bass-hrm/handleidingen/). Dit
document legt uit hoe u als FNWI medewerker gebruik kunt maken van deze
applicatie.

### Logingegevens en authorisatie

Om BASS te kunnen gebruiken voor electronische declaraties (i-Expense)
heeft u nodig:

-   uw U-nummer (personeelsnummer).\
    Dit is te verkrijgen via
    [P&O](http://www.ru.nl/fnwi/po/afdeling_personeel/wie_wat_waar/).
    Voor medewerkers van FNWI kan C&CZ het RU-wachtwoord resetten.
-   uw budgetnummer/kostenplaats.\
    Dit is bekend op uw afdeling of te verkrijgen via [Planning en
    Control van
    FEZ](http://www.radboudnet.nl/fnwi/fez/planning_en_control/).

Om te kunnen bestellen (i-Procurement) en bestelling goed te keuren
heeft u verder nodig:

-   autorisatie in BASS/FinLog/Oracle, te verkrijgen via [een
    aanvraagformulier](http://www.radboudnet.nl/bass/bass-finlog/aanvragen/).

### Toegang tot BASS

-   De toegang tot BASS wordt formeel alleen ondersteund vanaf bepaalde
    platformen (combinaties van diverse Windows besturingssystemen,
    Internet Explorer versies, Java versies, enz.) maar in de praktijk
    blijken veel alternatieve combinaties (Mac, Linux, andere browsers)
    ook te werken.
-   BASS werkt gegarandeerd vanaf een standaard RU werkplek en een door
    C&CZ beheerde Windows werkplek.
-   BASS is alleen *rechtstreeks* te benaderen wanneer er vanaf
    campusnetwerk of via een VPN verbinding (vanaf een willekeurige
    Internet-locatie) wordt gewerkt. *Nota bene*: vanaf een klein deel
    van de werkplekken bij FNWI is directe toegang tot BASS niet
    mogelijk. Dat zijn werkplekken die op server-subnetten staan, met
    veel vanaf het Internet toegankelijk systemen. Gebruik in dat geval
    [vpn](/nl/howto/vpn/).
-   BASS is niet benaderbaar vanaf Server subnetten, dus ook niet vanaf
    onze **lilo** servers.
-   BASS functies zoals Tijdschrijven, Declareren en Bestellen maken
    uitsluitend gebruik van de webbrowser en zijn vrijwel altijd vanaf
    elke browser op elk platform te gebruiken (Chrome, IE, Firefox,
    Safari, op PC, Mac, Linux of tablet). Dus thuis declareren b.v.
    vanaf een iPad via VPN gaat prima.
-   Overige BASS functies (“forms”) maken gebruik van Java. Java moet
    dus geïnstalleerd zijn op de PC (Mac, Linux, etc.) om met forms te
    werken. Hier wordt het complexer.
    -   Op een iPad staat geen Java, dus daarop kan in principe niet met
        forms worden gewerkt.
    -   De Java versie op de client (PC, Mac, Linux) mag niet “teveel”
        afwijken van de versie op de BASS server. Een lagere versie op
        de client is geen probleem (voor BASS, wel voor de veiligheid
        van de werkplek!), een hogere versie is soms een probleem.
    -   Andere kenmerken van de client (bijv. 32-bit of 64-bit versie
        van het besturingssysteem) kunnen ook een rol spelen.
-   De functies in BASS die Java nodig hebben worden vooral op
    werkplekken van medewerkers van CIF, CFA, DPO, budgethouders en
    medewerkers die bestellingen doen gebruikt. Als een gebruiker vanaf
    thuis toch gegarandeerd met BASS wil werken, dan ligt het meer voor
    de hand om de werk-PC over te nemen (“bureaublad overnemen”).

### Voorbereiding PC

In deze handleiding wordt uitgegaan van Windows 7, maar voor Linux en de
Mac werkt e.e.a. vrijwel hetzelfde.

Het gebruik van deze webtoepassing vereist mogelijk aanpassingen van de
systeemconfiguratie van uw computer. Deze aanpassing is afhankelijk van
hoe uw computer met het netwerk verbonden is, zie volgende stap.

N.B. BASS is alleen bereikbaar vanaf de campus of via een
[VPN](/nl/howto/vpn/) verbinding met het campus-netwerk. Deze beperkingen
zijn vanuit veiligheidsoverwegingen ingesteld.

Voor sommige onderdelen van BASS dient Java 6.x of 7.x op de PC
geïnstalleerd te zijn. Alle door C&CZ beheerde PC’s hebben standaard de
Java Runtime Environment (JRE) geïnstalleerd.

### Login op BASS

Onderstaande URL brengt u direkt naar de loginpagina van BASS.

<http://bass.ru.nl>

{{< figure src="/img/old/bass\_ru\_app.png" title="bass\_ru\_app.png" >}}

Indien dit niet lukt, heeft u geen directe toegang tot bass.ru.nl. U
zult dan gebruik moeten maken van de [vpn](/nl/howto/vpn/).

## Bij wie kan ik terecht met inhoudelijke vragen over BASS-finlog?

Op de Radboudnet website staan de contactgegevens voor de

[BASS helpdesk](http://www.radboudnet.nl/bass).

## Waar moet ik zijn voor het aanvragen van autorisaties voor BASS?

Voor het aanvragen van autorisaties in BASS zijn ondertekende
autorisatieformulieren nodig. Gebruikers kunnen niet rechtstreeks om
autorisaties vragen bij de BASS-finlog helpdesk.

De informatie hierover is te vinden [op de Radboudnet
website](http://www.radboudnet.nl/bass/bass-finlog/aanvragen/). De
autorisatie aanvragen verlopen via FEZ van FNWI.

## Hoe kan ik een initieel BASS wachtwoord zetten of het later wijzigen?

Vanaf 1 juni 2013 is er geen apart BASS wachtwoord meer: men logt dan in
met het U-nummer en RU wachtwoord.

## Welke RU-werkplek IP-adressen hebben nog geen directe toegang tot BASS?

Alleen de volgende FNWI-werkplek IP-adressen hebben geen directe toegang
tot BASS, men kan wel bv via [VPN](/nl/howto/vpn/) naar BASS gaan:

    131.174.12.0 - 131.174.12.127
    131.174.15.33
    131.174.15.35
    131.174.15.36
    131.174.142.0 - 131.174.142.31
    131.174.192.0 - 131.174.192.255

## Ik krijg BASS niet aan de praat op m’n PC, zijn er nog alternatieven?

BASS is te bereiken en gebruiken met elke browser op elk apparaat.
