---
author: bram
date: '2013-01-18T09:40:37Z'
keywords: []
lang: nl
tags:
- internet
title: Internet setup
wiki_id: '67'
---
## De standaard WWW setup

Dit document beschrijft de standaard WWW-omgeving voor websites die op
één van de door C&CZ beheerde webservers lopen. Hierin staat
ORG voor de naam van de WWW-service
(organisatie), *SRV* voor de naam van de server (machine) die de
pagina’s onder zijn hoede heeft en DOM voor
het domein (science of cs).

### Een directory /vol/www/ORG

Dit is een stukje van de disk van de server. In het algemeen is deze
directory alleen schrijfbaar op de server zelf, tenzij het geen
login-server is. Dan zijn de bestanden beschikbaar via de Windows share
\\\\www\\www en op unix login servers. Er staan standaard drie
directory’s in, maar dat kan men (zelf) uitbreiden als dat nodig mocht
zijn. Hou wel vast aan de namen *web-docs* en *cgi-bin*, want de daemon
is van die naamgeving afhankelijk.

-   In **web-docs** komen de documenten te staan. Als je daar een
    bestand met de naam `index.html` maakt, dan zal dat als de Home Page
    worden gezien voor iemand die de [URL](/nl/howto/url/)
    `http://www.<var>DOM</var>.ru.nl/<var>ORG</var>/` kiest. Deze URL is
    de rechtstreekse weg naar de pagina’s van de WWW service
    ORG. Als er iemand ergens ter wereld
    naar deze pagina’s wil verwijzen, dan is dit de URL die daarvoor
    gebruikt moet worden.
-   In **cgi-bin** kun je je eigen [Common Gateway Interface](http://hoohoo.ncsa.uiuc.edu/cgi) scripts en binary’s
    (programma’s) zetten. Hoe je met cgi-scripts werkt, wordt in diverse
    via WWW bereikbare documenten uitgelegd. Zie hieronder voor enkele
    [referenties](#referenties). Je roept een script in je eigen cgi-bin
    directory aan als: \>
    /cgi-bin/naam\_van\_het\_script .
-   In **cgi-src** kun je sources van cgi-scripts kwijt.

### Een Unix-groep wwwORG

Er wordt ook een Unix-groep met de naam
wwwORG gemaakt, met daarin de logins die
bij de [WWW aanvraag](/nl/howto/internet-aanvragen/) zijn opgegeven als
de mensen die de pagina’s gaan onderhouden. Het is nu duidelijk waarom
een korte naam voor de service een klein organisatorisch voordeel heeft:
groepnamen mogen maar 8 tekens lang zijn, dus als
ORG langer is dan 5 tekens, moet voor de
Unix-groep een andere afkorting verzonnen worden.

De web-docs, cgi-bin en cgi-src directory’s zijn van deze groep en
bestanden en directory’s die je er in aanmaakt worden ook weer
automatisch van deze groep. Deze opzet is van belang voor www groepen
die uit meer dan 1 persoon bestaan: door de bestanden en directory’s
schrijfbaar te maken voor de groep, kunnen alle leden er aan werken,
terwijl buitenstaanders er niet (schrijfbaar) bij kunnen komen.

Zorg ervoor dat alle documenten die je aanbiedt leesbaar zijn “voor de
wereld”, anders kan de *http daemon* er niet bij.

=== Een mail-alias

    www-<var>ORG</var>@<var>DOM</var>.ru.nl

===

Er wordt ook een mail-alias
www-ORG@DOM.ru.nl
gemaakt. Alle mail die naar dit adres wordt gestuurd komt bij de
perso(o)n(en) terecht die bij de [WWW
aanvraag](/nl/howto/internet-aanvragen/) zijn opgegeven als de
contactperso(o)n(en). Dit adres kan in de documenten genoemd worden als
het contact-adres voor vragen over de pagina’s.

### Een standaard /images directory

De server heeft een **/images** directory waarin algemeen bruikbare
plaatjes staan (met veel dank aan de WWW-mensen van
[Thalia](http://www.science.ru.nl/thalia/) voor het beschikbaar stellen
hiervan). Een overzicht van de beschikbare plaatjes (als het goed is
zijn ze natuurlijk voor alle servers gelijk), is te krijgen via de URL:

     [http://www.science.ru.nl/images/ http://www.<var>DOM</var>.ru.nl/images/]

Je krijgt dan een lijst te zien en je kunt de plaatjes bekijken door ze
aan te klikken. In je documenten kun je plaatjes uit deze directory
opnemen met het statement

> \<IMG SRC=“/images/*naampje*.gif”\>

### Een plaats in het World Wide Web

Om opgenomen te worden in de [RU Home Page](http://www.ru.nl/), moet je
contact opnemen met de beheerder: *\<redactie-kuncis\@buro.ru.nl\>*. Als
je in je eigen pagina’s wilt verwijzen naar de RU Home Page, gebruik dan
de URL

> <http://www.ru.nl/>

In de **/images** directory staat een “ikoontje” met het RU-logo,
namelijk
“[/images/ru-icon.gif](http://www.science.ru.nl/images/ru-icon.gif)”,
dat in de pagina’s gebruikt kan worden ter identificatie. Een typische
verwijzing naar de RU Home Page wordt gemaakt met:

\<A HREF=“<http://www.ru.nl/%22%3E%3CIMG> SRC=”/images/ru-nl-logo.gif"
ALT=“\*”\>\</A\>\
Naar de \<A HREF="<http://www.ru.nl/%22%3ERU> Home Page\</A\>.

en ziet er uit als:

[{{< figure src="/img/old/ru-nl-logo.gif" title="\*" >}}](http://www.ru.nl/)
Naar de [RU Home Page](http://www.ru.nl/).

Merk op dat er twee hyperlinks in dit voorbeeld staan: het ikoontje en
de tekst “RU Home Page” wijzen beide naar dezelfde URL. (De `ALT="*"`
optie geeft aan wat niet-grafische browsers moeten laten zien in plaats
van het plaatje.)

### Referenties

Hier zijn enkele URL’s voor handleidingen. Er komen er dagelijks bij en
het niveau wisselt sterk. Zelf zoeken levert ongetwijfeld nog meer
handige URL’s op.

-   De opvolger van HTML heet
    [HTML+](http://www.w3.org/hypertext/WWW/MarkUp/HTMLPlus/htmlplus_1.html)
    maar dit wordt nog niet overal ondersteund.
-   Een [hypertext style
    guide](http://www.w3.org/hypertext/WWW/Provider/Style/Overview.html).
-   Een andere [beginnershandleiding voor
    HTML](http://www.vuw.ac.nz/who/Nathan.Torkington/ideas/www-html.html).
-   Een [introductie tot het World Wide
    Web](http://www.vuw.ac.nz/who/Nathan.Torkington/ideas/www-primer.html).
-   [Veel gestelde vragen (en antwoorden) over WWW
    zaken](http://sunsite.unc.edu/boutell/faq/www_faq.html).

Ook interessant kunnen zijn:

-   Een overzicht van [hulpprogramma’s voor WWW
    aanbieders](http://www.w3.org/hypertext/WWW/Tools/Filters.html).
-   Een uitleg van het NCSA
    [imagemap](http://hoohoo.ncsa.uiuc.edu/docs/setup/admin/Imagemap.html)
    cgi-programma.
-   Een beschrijving van de [NCSA http
    daemon](http://hoohoo.ncsa.uiuc.edu/docs/Overview.html).
-   De [NCSA Mosaic Home
    Page](http://www.ncsa.uiuc.edu/SDG/Software/Mosaic/NCSAMosaicHome.html).
-   De [Netscape Home Page](http://home.netscape.com/).
