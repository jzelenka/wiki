---
cpk_affected: Computersystemen en switches zonder UPS-noodstroomvoorziening.
cpk_begin: &id001 2015-06-11 20:00:00
cpk_end: 2015-06-11 20:30:00
cpk_number: 1134
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Services niet beschikbaar door stroomstoring
url: cpk/1134
---
Door een korte onderbreking in de netspanning, vielen op de hele campus
desktop-computers en netwerkapparatuur uit. Netwerkswitches die niet
achter UPS-noodstroom zijn geschakeld, hebben er ongeveer 20 minuten uit
gelegen. Een module van een netwerkswitch in het Transitorium bleef in
foutmodus staan totdat deze handmatig uit- en ingeschakeld werd op
vrijdagmorgen rond 8:30, waardoor voor ca. 46 aansluitingen de storing
tot ca. 8:40 duurde. Oorzaak was volgens het [UVB](http://www.ru.nl/uvb)
een kabelbreuk aan de zuidzijde van de Kapittelweg van ca 21:00 tot
00:30. Liander meldt daar een [stroomstoring een uur
later](https://www.liander.nl/storingen/overzicht/storing?storingsnummer=3963192).
