---
cpk_affected: Gebruikers van een van de licenties van deze server
cpk_begin: &id001 2021-10-11 04:40:00
cpk_end: 2021-10-11 08:26:00
cpk_number: 1286
date: *id001
tags:
- medewerkers
- docenten
title: Licentieserver probleem
url: cpk/1286
---
Een fout in de beheersoftware zorgde ervoor dat bij de herstart van de
licentieserver geen enkel licentieproces goed opstartte. Pas na
reparatie waren de licenties weer beschikbaar.
