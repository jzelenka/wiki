---
cpk_affected: GitLab gebruikers
cpk_begin: &id001 2017-06-30 09:00:00
cpk_end: 2017-06-30 11:00:00
cpk_number: 1211
date: *id001
tags:
- medewerkers
- studenten
title: GitLab tijdelijk niet beschikbaar tijdens upgrade
url: cpk/1211
---
Vanwege een geplande upgrade zullen gitlab.science.ru.nl en
gitlab.pep.cs.ru.nl niet beschikbaar zijn.
