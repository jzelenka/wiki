---
cpk_affected: Gebruikers van de printto en printsmb printerservers
cpk_begin: &id001 2013-09-24 11:41:00
cpk_end: 2013-09-24 12:22:00
cpk_number: 1044
date: *id001
tags:
- medewerkers
- studenten
title: Printto/printsmb server in de problemen
url: cpk/1044
---
De printserver was gecrasht. Na een herstart was het probleem verdwenen.
