---
cpk_affected: network
cpk_begin: &id001 2017-03-06 22:00:00
cpk_end: 2017-03-07 02:00:00
cpk_number: 1199
date: *id001
tags:
- medewerkers
- studenten
title: Science and RU Internet network maintenance
url: cpk/1199
---
Monday evening March 6, there will be maintenance on RU network
equipment. On March 7 between ca. 01:00-01:30 no network traffic will be
possible at the Faculty of Science on the wired and wireless networks.
From 23:00 until March 7 02:00 there will be a period where no traffic
between RU and Internet is possible. Please think which processes depend
on network connectivity and prepare for this outage if necessary.
