---
cpk_affected: Gebruikers van mx3, smtp3, crestron, gitlab (PEP), goudsmit, msql01
  and labservanttestvm
cpk_begin: &id001 2019-11-05 05:30:00
cpk_end: 2019-11-05 09:20:00
cpk_number: 1256
date: *id001
tags:
- medewerkers
- studenten
title: Probleem met host van virtuele machines
url: cpk/1256
---
De virtuele host ‘oscar’ draait sinds 5:30 niet meer. Er wordt op dit
moment gewerkt aan een oplossing. Update: Opgelost. Wederom waren
kapotte LVM snapshots het probleem.
