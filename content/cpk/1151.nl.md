---
cpk_affected: Gebruikers die printers aan wilden koppelen via \printto.science.ru.nl
cpk_begin: &id001 2015-11-19 00:00:00
cpk_end: 2015-11-19 13:00:00
cpk_number: 1151
date: *id001
tags:
- medewerkers
title: Printserver printto in de problemen
url: cpk/1151
---
Na een reboot van de printserver (en ca 10 minuten wachten) werkte alles
weer.
