---
cpk_affected: Gebruikers van deze data/file/volletjes server
cpk_begin: &id001 2014-05-26 06:45:00
cpk_end: 2014-05-26 13:45:00
cpk_number: 1094
date: *id001
tags:
- medewerkers
- studenten
title: Data/file/volletjes server (heap) in de problemen
url: cpk/1094
---
De server had een probleem, dat begonnen was bij de
maandagochtend-reboot. Na een herstart waren de problemen verdwenen.
Binnenkort zullen alle data van deze server verhuisd worden naar een
nieuwe server. Omdat iedereen de aliases van de vorm
iets-srv.science.ru.nl gebruikt, zal de overlast daarvan gering zijn.
