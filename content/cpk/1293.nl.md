---
cpk_affected: 'Gebruikers die met of vanaf astro.ru.nl wilden communiceren  '
cpk_begin: &id001 2022-04-28 12:02:00
cpk_end: 2022-05-13 14:00:00
cpk_number: 1293
date: *id001
title: Astro.ru.nl DNS(SEC) service down
url: cpk/1293
---
Bij de gebruikelijke vervanging van de
[DNSSEC](https://nl.wikipedia.org/wiki/DNSSEC)-sleutels, die het
DNS-verkeer beveiligen, is in de externe DNS van ru.nl een foute sleutel
voor astro.ru.nl ingevoerd. Daardoor verdween astro.ru.nl vanaf extern
gezien van het internet. Dat is op 2022-05-02 om ca. 14:00 met een
automatische procedure hersteld, maar het automatische proces gebruikte
een verkeerde versleuteling. Het duurde tot 2022-05-12 voordat ILS dit
met de hand gecorrigeerd had nadat we dat laat geconstateerd hadden.
Omdat men het DNS-antwoord dat astro.ru.nl niet bestaat, 24 uur mocht
gebruiken, is de overlast pas 2022-05-13 13:00 compleet verdwenen. De
voornaamste overlast was waarschijnlijk dat mail vanaf een @astro.ru.nl
adres geweigerd werd.
