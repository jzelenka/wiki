---
cpk_affected: Gebruikers van o.a. de Linux loginservers (lilo2, lilo3, lilo4)
cpk_begin: &id001 2014-06-05 16:00:00
cpk_end: 2014-06-05 17:45:00
cpk_number: 1098
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Reboot van loginservers na verhuizing netwerkschijven
url: cpk/1098
---
De geplande verhuizing van netwerkschijven van een oude naar een nieuwe
server veroorzaakte veel “stale NFS file handles”, waardoor we
genoodzaakt waren enkele loginservers te rebooten.
