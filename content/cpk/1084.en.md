---
cpk_affected: Users of Peage in the Huygens building
cpk_begin: &id001 2014-04-28 12:00:00
cpk_end: 2014-04-30 12:00:00
cpk_number: 1084
date: *id001
tags:
- studenten
- medewerkers
title: Peage MFP Huygens building in maintenance April 28/29/30
url: cpk/1084
---
April 28/29/30, the [Peage](/en/howto/peage/) MFP and the Peage POS near
the restaurant in the Huygens building cannot be used. The
[ISC](http://www.ru.nl/isc) will then investigate whether Peage can also
be made available for employees instead of only for students. April 28,
the MFP will be moved to the test location, April 30 it will be moved
back.
