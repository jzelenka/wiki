---
cpk_affected: Netwerk Huygensgebouw en omliggende gebouwen
cpk_begin: &id001 2015-09-12 09:00:00
cpk_end: 2015-09-12 13:00:00
cpk_number: 1144
date: *id001
tags:
- medewerkers
title: Korte netwerkonderbrekingen FNWI
url: cpk/1144
---
Op zaterdag 12 sept a.s. tussen 09:00 - 13:00 uur worden
onderhoudswerkzaamheden uitgevoerd op de netwerkrouter bij FNWI en een
aantal serverswitches in computerruimten van C&CZ. Dat betekent dat
tussen genoemde tijdstippen enkele korte netwerkonderbrekingen van circa
1 minuut zullen optreden en ook vele FNWI-servers op die momenten even
niet bereikbaar zijn. Ook de Mitel IP-telefoons zullen enkele malen
kortstondig niet werken. Alle overige telefoons blijven echter normaal
functioneren. De werkzaamheden zijn nodig om de verdere uitrol van
IP-telefonie bij FNWI mogelijk te maken. Voor nadere info kunt u terecht
bij [netmaster\@science.ru.nl](/nl/howto/contact/).
