---
cpk_affected: network in server room Huygens. Connection to many servers may be impossible.
cpk_begin: &id001 2014-11-11 06:00:00
cpk_end: 2014-11-11 06:15:00
cpk_number: 1113
date: *id001
tags:
- medewerkers
- studenten
title: Network maintenance in server room Huygens
url: cpk/1113
---
Because of a software upgrade of the network switches in the
buildings/floors/locations mentioned above, these network switches will
be rebooted at 06:00h (AM) exactly. This means that both wired en
wireless networks at these locations will be interrupted for 10 to 15
minutes and network traffic will not be possible. This includes the
network for access control, climate control etc.
