---
cpk_affected: Users of gitlab.science.ru.nl, mattermost.science.ru.nl, gitlab.pep.cs.ru.nl,
  mattermost.pep.cs.ru.nl
cpk_begin: &id001 2019-12-11 06:00:00
cpk_end: 2019-12-11 09:00:00
cpk_number: 1257
date: *id001
tags:
- medewerkers
- studenten
title: Unplanned GitLab security update
url: cpk/1257
---
A critical security update for GitLab has been released and is being
installed this moment. During the update, GitLab is offline.
