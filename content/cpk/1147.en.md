---
cpk_affected: Users of KM MFPs
cpk_begin: &id001 2015-10-08 08:00:00
cpk_end: 2015-10-08 12:00:00
cpk_number: 1147
date: *id001
tags:
- medewerkers
- studenten
title: Konica Minolta MFPs switched
url: cpk/1147
---
In the morning of October 8, some KM MFPs will be switched, in order to
have the faster/bigger machines at the locations where the print volume
is te largest. The replacement of each machine will not take much time.

Affected printers:

`pr-hg-00-011 pr-hg-00-089 pr-hg-00-627 pr-hg-00-825`
`pr-hg-02-038 pr-hg-02-556 pr-hg-02-832`
