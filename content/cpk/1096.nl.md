---
cpk_affected: alle gebruikers die Science-mail wilden lezen
cpk_begin: &id001 2014-06-05 14:05:00
cpk_end: 2014-06-05 14:15:00
cpk_number: 1096
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: IMAP mailserver probleem
url: cpk/1096
---
De IMAP server was weer overbelast. Het was nodig de IMAP service te
herstarten. We denken dat het met snapshots te maken heeft en laten nu
ongeldige snapshots regelmatig verwijderen.
