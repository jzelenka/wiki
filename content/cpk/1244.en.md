---
cpk_affected: Users of the poster printer "kamerbreed"
cpk_begin: &id001 2019-03-26 00:00:00
cpk_end: 2019-03-27 16:00:00
cpk_number: 1244
date: *id001
tags:
- medewerkers
- studenten
title: Poster printer "kamerbreed" broken
url: cpk/1244
---
The [poster printer](/en/howto/kamerbreed/) has to be repaired, but we
can’t easily fix it. We hope to have the printer operational again in
the near future. In the meantime you might use the [RU
copyshop](http://www.ru.nl/facilitairbedrijf/winkels/copyshop/).
