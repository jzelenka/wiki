---
cpk_affected: Gebruikers van horde webmail en gebruikers die naar o.a. hotmail.com
  wilden mailen
cpk_begin: &id001 2012-11-16 04:45:00
cpk_end: 2012-11-17 12:00:00
cpk_number: 1004
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Mailproblemen na weggeven wachtwoord aan phishers
url: cpk/1004
---
Horde webmail bleek wederom misbruikt te worden voor spam. Een naïeve
gebruiker had het Science-wachtwoord aan phishers/spammers gegeven,
waardoor dit mogelijk werd. Nadat eerst horde stopgezet is, is
vrijdagochtend vroeg de login van deze gebruiker afgezet en horde weer
herstart. Zaterdagochtend bleek deze kortdurende spam-outbreak toch
reden geweest te zijn voor de beheerders van hotmail.com om onze
uitgaande mailserver op hun zwarte lijst te zetten. Daarom hebben we
zaterdagochtend deze server een ander IP-adres gegeven.
