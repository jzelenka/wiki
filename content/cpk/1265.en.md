---
cpk_affected: Users of various websites.
cpk_begin: &id001 2020-06-18 15:45:00
cpk_end: 2020-06-18 16:25:00
cpk_number: 1265
date: *id001
title: Webserver 'havik' offline
url: cpk/1265
---
Several parts have been replaced. We assume the problem, that occurred
twice, is now resolved. For dual-boot pcs, the boot menu was served by
an alternative method during the repair.
