---
cpk_affected: GitLab gebruikers
cpk_begin: &id001 2018-01-16 23:00:00
cpk_end: 2018-01-17 11:00:00
cpk_number: 1224
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: GitLab tijdelijk niet beschikbaar tijdens upgrade
url: cpk/1224
---
Vanwege een geplande upgrade zullen gitlab.science.ru.nl en
gitlab.pep.cs.ru.nl niet beschikbaar zijn. Zie ook:
[1](https://about.gitlab.com/2018/01/12/gitlab-critical-release-preannouncement/)
