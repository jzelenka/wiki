---
cpk_affected: Users of phpmyadmin, printto and printsmb printer servers
cpk_begin: &id001 2013-10-01 15:18:00
cpk_end: 2013-10-01 15:31:00
cpk_number: 1049
date: *id001
tags:
- medewerkers
- studenten
title: Printto/printsmb/phpmyadmin server problems again
url: cpk/1049
---
The printserver crashed again, for yet unknown reasons. A reboot of the
machine solved the problem.
