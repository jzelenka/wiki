---
cpk_affected: Gebruikers van Science VPN diensten
cpk_begin: &id001 2021-02-03 13:00:00
cpk_end: 2021-02-03 14:02:00
cpk_number: 1273
date: *id001
title: Science VPNsec storing
url: cpk/1273
---
Het certificaat van onze [VPNsec](/nl/howto/vpn)-service werd niet
regelmatig gecontroleerd en kon daarom verlopen. We hebben binnen een
uur een nieuw certificaat geïnstalleerd. Natuurlijk zullen we ook van
dit certificaat vanaf nu de looptijd controleren. Voor Apple/Mac moet
een nieuwe mobileconfig gemaakt worden, dat kost even tijd, omdat in de
tussentijd de RU naar een andere Certicate Authority verhuisd was. Voor
Apple macOS was dat eind 4 februari gelukt, met een nieuwe
installatieprocedure. Voor Apple iOS (iPhone/iPad) moet men wel het oude
profiel verwijderen en de nieuwe mobileconfig installeren.
