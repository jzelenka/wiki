---
cpk_affected: Users with a Fedora based desktop PC
cpk_begin: &id001 2012-12-14 00:00:00
cpk_end: 2012-12-14 00:00:00
cpk_number: 1005
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: LDAP server vernieuwd
url: cpk/1005
---
Older Fedora desktop PC’s may experience startup problems after an
upgrade of one of our LDAP servers. A fix is available and has been
applied. If you still encounter this problem, please contact C&CZ.
