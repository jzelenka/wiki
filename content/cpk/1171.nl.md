---
cpk_affected: gebruikers van gitlab
cpk_begin: &id001 2016-03-16 21:40:00
cpk_end: 2016-03-16 22:05:00
cpk_number: 1171
tags:
- medewerkers
- studenten
date: *id001
title: Gitlab upgrade
url: cpk/1171
---

Vanwege een upgrade van de [GitLab](/nl/howto/gitlab/) server naar versie
8.5.7 was deze tijdelijk niet beschikbaar.
