---
cpk_affected: vrijwel alles aan het netwerk in FNWI (Heyendaal-Oost)
cpk_begin: &id001 2014-10-15 03:44:00
cpk_end: 2014-10-15 11:36:00
cpk_number: 1108
date: *id001
tags:
- medewerkers
- studenten
title: FNWI netwerk probleem
url: cpk/1108
---
De router die in het Huygensgebouw en omringende gebouwen alle
IP-verkeer tussen subnetten regelt, kreeg het [veel te
druk](http://cricket.science.ru.nl/grapher.cgi?target=%2Fswitches%2Fdr-huyg.heyendaal.net%2Fperformance;view=cpu).
Hierdoor was netwerkverkeer, vooral
[UDP](http://nl.wikipedia.org/wiki/User_Datagram_Protocol),
problematisch. ISC netwerkbeheer heeft door het versimpelen van
[ACLs](http://en.wikipedia.org/wiki/Access_control_list#Networking_ACLs)
en verminderen van logging geprobeerd de drukte te verminderen. Om ca.
12:00 was het probleem voorlopig opgelost. C&CZ zal alle ACLs nog verder
nalopen, met het doel om de router te ontlasten zonder de beveiliging
voor aangesloten systemen te verminderen.
