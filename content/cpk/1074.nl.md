---
cpk_affected: Gebruikers van deze U:/ home server
cpk_begin: &id001 2014-03-24 17:25:00
cpk_end: 2014-03-24 17:35:00
cpk_number: 1074
date: *id001
tags:
- medewerkers
- studenten
title: U:/ home server pile in de probleem
url: cpk/1074
---
De server had een probleem met een bepaalde partitie. Na een herstart
was het probleem verdwenen.
