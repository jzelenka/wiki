---
cpk_affected: Users of shared network disks
cpk_begin: &id001 2017-06-13 10:45:00
cpk_end: 2017-06-13 10:51:00
cpk_number: 1209
date: *id001
tags:
- medewerkers
- studenten
title: Fileserver needed reboot after fatal typo
url: cpk/1209
---
Apologies.

Affected disks:

`acfiles beevee bioniccell bio-orgchem celbio1 celbio2`
`chemprac csgi-archief csg-projecten csg-staf desda ds ehef1`
`ehef2 ehef3 femtospin geminstr2 giphouse gissig gmi hfml-backup hfml-backup2 hfml-backup-inst highres impuls`
`introcie isis janvanhest kaartenbak kangoeroe mathsolcie microbiology microbiology2`
`milkun2 milkun2RenD milkun3 milkun5 milkun6 milkun7 milkun7rw`
`mwstudiereis neuroinf neuroinf2 olympus puc randomwalkmodel secres2 sigma sigmacies spectra spectra-rw`
`spm spmdata3 spmdata4 spmdata5 spmdata6 spmdata7 spmstaff ssi staff-hfml stroom thalia ucm vsc`
`vsc1 vsc10 vsc11 vsc12 vsc13 vsc14 vsc15 vsc2 vsc3 vsc4 vsc5 vsc6 vsc7 vsc8 vsc9 vscadmin wiskalg wiskunde wkru`
