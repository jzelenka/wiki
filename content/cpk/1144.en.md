---
cpk_affected: Network Huygens building and peripheral buildings
cpk_begin: &id001 2015-09-12 09:00:00
cpk_end: 2015-09-12 13:00:00
cpk_number: 1144
date: *id001
tags:
- medewerkers
title: Brief network interruptions at FNWI
url: cpk/1144
---
Saturday September 12, between 9:00 a.m. and 1:00 p.m., maintenance work
will be conducted on the network router at the Faculty of Science and a
number of server switches in C&CZ data centers. This means that between
these points in time, a few brief network interruptions with a duration
of approximately 1 minute will occur. Many FNWI servers will be
unreachable at those moments. The Mitel IP phones will also go out of
service momentarily several times. All other phones, however, will
continue to operate normally. The maintenance work is necessary to
enable resumption of the IP telephony rollout at the Faculty of Science.
For further information, please contact
[netmaster\@science.ru.nl](/en/howto/contact/).
