---
cpk_affected: Gebruikers van jitsi.science.ru.nl
cpk_begin: &id001 2020-04-19 15:00:00
cpk_end: 2020-04-20 11:40:00
cpk_number: 1259
date: *id001
title: Jitsi.science.ru.nl niet bruikbaar
url: cpk/1259
tags: [jitsi]
---
Vanwege mislukte performance tuning is de installatie van
jitsi.science.ru.nl onbruikbaar geworden bij meer dan een enkele persoon
in de conference. Opgelost na reinstall van de server.
