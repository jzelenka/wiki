---
cpk_affected: Users of network disks
cpk_begin: &id001 2015-06-26 08:45:00
cpk_end: 2015-06-26 09:45:00
cpk_number: 1136
date: *id001
title: Network shares temporary not available during migration
url: cpk/1136
---
Starting from 8:45h, the network disks will not be available during a
migration to a new server.

Affected disks:

`acfiles, aerochem, amsbackup, amscommon,`
`beevee, bfac-backup, bio-orgchem, bioniccell, carisma, chemprac, csg-staf,`
`csgi-archief, desda, ds, ehef1, ehef2, ehef3, geminstr2, giphouse, gissig,`
`gmi, hfml-backup2, highres, impuls, introcie, isis, janvanhest, kaartenbak,`
`kangoeroe, mbaudit1, microbiology, microbiology2, milkun2, milkun3, milkun5,`
`milkun6, milkun7, milkun7rw, molbiol, mwstudiereis, neuroinf, neuroinf2, nfstest,`
`nwibackup, olympus, puc, ruversatest, secres2, secrmolbiol, sigma, sigmacies,`
`spectra, spm, spmdata3, spmdata4, spmdata5, spmdata6, spmdata7, spmmagstm,`
`spmnanolab, spmstaff, ssi, staff-hfml, stroom, thalia, ucm, vsc, vsc1, vsc10,`
`vsc11, vsc12, vsc13, vsc14, vsc15, vsc2, vsc3, vsc4, vsc5, vsc6, vsc7, vsc8,`
`vsc9, vscadmin, wiskalg, wiskunde en wkru.`
