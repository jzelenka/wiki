---
cpk_affected: all smb users miii
cpk_begin: &id001 2016-02-23 00:00:00
cpk_end: 2016-02-23 08:50:00
tags:
- medewerkers
- studenten
cpk_number: 1166
date: *id001
title: miii server problem
url: cpk/1166
---

For unknown reasons, the miii mail server was unavailable. Rebooted.
