---
cpk_affected: Gebruikers van science mailvoorziening
cpk_begin: &id001 2014-08-21 16:00:00
cpk_end: 2014-08-21 16:30:00
cpk_number: 1102
date: *id001
tags:
- medewerkers
- studenten
title: SMTP server offline voor een half uur
url: cpk/1102
---
Beide processorventilatoren van de smtp-server zijn gesneuveld en moeten
worden vervangen.
