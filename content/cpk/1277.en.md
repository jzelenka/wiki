---
cpk_affected: users of the RU network or services
cpk_begin: &id001 2021-02-27 08:00:00
cpk_end: 2021-02-27 20:00:00
cpk_number: 1277
date: *id001
tags:
- medewerkers
- studenten
title: Major RU network maintenance Saturday Feb. 27 08:00-20:00
url: cpk/1277
---
The ISC [announced](https://www.ru.nl/systeem-meldingen/) that Saturday
February 27 08:00-20:00 major RU network maintenance work will be
carried out. This will mean that all RU services will be unavailable
several times for at most an hour. This concerns all RU services
including those of FNWI/C&CZ: e-mail, VPN, wifi, BASS, OSIRIS,
Brightspace, Syllabus+, Corsa, etc.
