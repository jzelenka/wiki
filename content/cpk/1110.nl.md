---
cpk_affected: netwerk in Huygens vleugel 7 en in de centrale straat tussen vleugels
  5 en 7, op alle verdiepingen.
cpk_begin: &id001 2014-10-29 19:00:00
cpk_end: 2014-10-29 23:00:00
cpk_number: 1110
date: *id001
tags:
- medewerkers
- studenten
title: Netwerkonderhoud in Huygens vleugel 7
url: cpk/1110
---
Voor een upgrade van de netwerkswitches in bovenstaande
gebouwen/verdiepingen/locaties zullen deze worden vervangen. Dat
betekent dat het vaste en draadloze datanetwerk onderbroken zal zijn en
er dus geen netwerkverkeer mogelijk is. Dat geldt ook voor het netwerk
van toegangscontrole, klimaatregeling etc.
