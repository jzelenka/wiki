---
title: FNWI network maintenance Wednesday April 5 19:00-23:00 Huygens in/near wing 7
author: petervc
cpk_number: 1323
cpk_begin: 2023-04-05 19:00:00
# cpk_end: 2023-04-05 23:00:00
cpk_affected: network users in Huygens in/near wing 7
date: 2023-03-30
tags: []
url: cpk/1323
---
RU ILS Connectivity announced that network maintenance will be
carried out next Wednesday evening that will cause short disturbances
of network connectivity for wired and wireless systems in and
around wing 7 Huygens on all floors except the ground floor.
For wired network outlets, one can check
whether they will be affected by looking at the outlet number
(starting with 110- for affected systems) or visiting [FNWI
ethergids](https://cncz.science.ru.nl/en/howto/netwerk-ethergids/).
