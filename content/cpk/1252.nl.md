---
cpk_affected: Gebruikers van mx3, smtp3, crestron, gitlab (PEP), goudsmit, msql01,
  labservanttestvm and gitlabrunner
cpk_begin: &id001 2019-09-03 04:30:00
cpk_end: 2019-09-03 10:47:00
cpk_number: 1252
date: *id001
tags:
- medewerkers
- studenten
title: Opstartproblemen van host van virtuele machines
url: cpk/1252
---
Na een reboot kwam de vmhost ‘oscar’ niet op. Er wordt op dit moment
gewerkt aan een oplossing. Update: het probleem werd waarschijnlijk
veroorzaakt door volgelopen snapshots. Na het verwijderen van deze
snapshots startte het systeem weer goed op.
