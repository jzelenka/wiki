---
cpk_affected: Gebruikers van de printto/printsmb/phpMyAdmin service
cpk_begin: &id001 2014-05-08 16:24:00
cpk_end: 2014-05-08 16:33:00
cpk_number: 1089
date: *id001
tags:
- medewerkers
- studenten
title: Weer Print/phpMyAdmin server problemen
url: cpk/1089
---
Net als gisteren was er een herstart nodig om de server tot leven te
brengen. Morgenvroeg herstart de server met een nieuwe kernel. Als dat
niet helpt, gaan we een nieuwe Samba-versie proberen.
