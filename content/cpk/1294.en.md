---
cpk_affected: Users of coma, coma01 en coma46
cpk_begin: &id001 2022-05-03 13:47:00
cpk_end: 2022-05-03 14:55:00
cpk_number: 1294
date: *id001
title: Coma, coma01 and coma46 network problem
url: cpk/1294
---
This afternoon three coma nodes lost their network because of an
incorrect network configuration. They must have shown intermittent
network problems earlier. It took us some time to find out what caused
this network problem, but when found, it was easy to fix.
