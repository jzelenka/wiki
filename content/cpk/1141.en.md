---
cpk_affected: Users of the KM MFP pr-hg-03-825
cpk_begin: &id001 2015-08-31 00:00:00
cpk_end: 2015-09-03 00:00:00
cpk_number: 1141
date: *id001
tags:
- medewerkers
title: Water damage for pr-hg-03-825
url: cpk/1141
---
A leak above the KM MFP hg PR-03-825 made this fail beyond repair. We
hope to resolve this by replacing it by a little-used KM MFP of the same
type.
