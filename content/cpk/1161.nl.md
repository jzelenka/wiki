---
cpk_affected: fnwi login server gebruikers
cpk_begin: &id001 2016-02-01 06:30:00
cpk_end: 2016-02-01 09:40:00
cpk_number: 1161
date: *id001
tags:
- medewerkers
- studenten
title: Server lilo4 maandagochtend-reboot vertraagd
url: cpk/1161
---
De server lilo4 hing bij down gaan tijdens de maandag ochtend reboot.
Oplossing: reboot volgde automatisch nadat de wielewaal server opgekomen
was.
