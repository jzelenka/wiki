---
cpk_affected: Gebruikers van horde webmail
cpk_begin: &id001 2012-09-25 23:05:00
cpk_end: 2012-09-26 10:20:00
cpk_number: 997
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Horde webmail server down door spam
url: cpk/997
---
Gisteravond bleek horde webmail misbruikt te worden voor spam. Een
naieve gebruiker had het Science-wachtwoord aan spammers gegeven,
waardoor dit mogelijk werd. Nadat eerst horde stopgezet is, is
vanochtend de login van deze gebruiker afgezet en horde weer herstart.
