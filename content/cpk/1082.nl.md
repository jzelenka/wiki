---
cpk_affected: Gebruikers van de printto/printsmb/phpMyAdmin service
cpk_begin: &id001 2014-04-17 08:30:00
cpk_end: 2014-04-17 09:20:00
cpk_number: 1082
date: *id001
tags:
- medewerkers
- studenten
title: Print/phpMyAdmin server vervanging donderdag 17 april 08:30-09:20
url: cpk/1082
---
Vanwege de recente storingen met deze server zal een nieuwe server
ingezet worden om verdere overlast voor gebruikers te voorkomen.
