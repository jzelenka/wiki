---
title: Jitsi certificate expired
author: sioo
cpk_number: 1304
cpk_begin: 2022-11-22 06:30:00
cpk_end: 2022-11-22 12:30:00
cpk_affected: 
date: 2022-11-22
tags: [jitsi]
url: cpk/1304
---
The certificate for jitsi.science.ru.nl expired last week and was replaced, but a sub-service was still using a copy of the older certificate. Users were able to open the frontend, but not able to start a meeting. The problem was resolved by updating the copy of the sub-service.
