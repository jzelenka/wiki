---
cpk_affected: Users of Science mail
cpk_begin: &id001 2013-03-19 11:45:00
cpk_end: 2013-03-19 12:14:00
cpk_number: 1014
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Mail problems after supplying password to phishers
url: cpk/1014
---
Again a Science user supplied his Science password to phishers. We
notice that because Internet criminals use these passwords to get into
the Science mail servers (horde webmail, smtp) in order to send spam.

PLEASE: do not naively click on a link in an e-mail!
