---
cpk_affected: alle gebruikers van het netwerk op de RU
cpk_begin: &id001 2014-11-26 09:45:00
cpk_end: 2014-11-26 10:00:00
cpk_number: 1120
date: *id001
tags:
- medewerkers
- studenten
title: Netwerkprobleem RU
url: cpk/1120
---
Zoals [gerapporteerd door het ISC](http://www.ru.nl/systeem-meldingen/)
is de RU slachtoffer geweest van een DDOS-aanval. Als gevolg hiervan is
de connectie met het internet onderbroken geweest. Er zijn maatregelen
genomen om te voorkomen dat een vergelijkbare aanval in de toekomst deze
problemen veroorzaakt.
