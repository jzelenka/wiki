---
cpk_affected: diverse gebruikers in Huygens vleugel 5
cpk_begin: &id001 2016-12-07 21:15:00
cpk_end: 2016-12-08 10:10:00
cpk_number: 1192
date: *id001
tags:
- medewerkers
- studenten
title: Netwerkstoring in deel Huygens vleugel 5
url: cpk/1192
---
In Huygens vleugel 5 is een netwerk switchmodule uitgevallen, waardoor
diverse gebruikers tijdelijk geen netwerk en telefoon hadden. De switch
module is inmiddels vervangen.
