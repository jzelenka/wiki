---
cpk_affected: everything connected to the network within RU until 9:45, after that
  Eduroam users.
cpk_begin: &id001 2014-10-29 09:38:00
cpk_end: 2014-10-29 15:30:00
cpk_number: 1109
date: *id001
tags:
- medewerkers
- studenten
title: RU network problem
url: cpk/1109
---
Due to a short
[DDOS-attack](http://nl.wikipedia.org/wiki/Distributed_denial-of-service)
the RU was virtually disconnected from the Internet during ca 7 minutes.
All network connected systems may have had problems during that time.
Until 15:30 some Eduroam users still had problems acquiring an IP
address.
