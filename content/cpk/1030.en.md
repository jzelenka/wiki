---
cpk_affected: Users of lilo (lilo3)
cpk_begin: &id001 2013-07-15 11:00:00
cpk_end: 2013-07-15 11:15:00
cpk_number: 1030
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Lilo3 reboot for different IP number
url: cpk/1030
---
Today lilo3 appeared to have an incorrectly chosen IP number. A reboot
fixed this.
