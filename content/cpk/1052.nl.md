---
cpk_affected: alle wireless @ru.nl gebruikers
cpk_begin: &id001 2013-10-23 14:30:00
cpk_end: 2013-10-23 16:15:00
cpk_number: 1052
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Draadloos @ru.nl athenticatie storing
url: cpk/1052
---
Gistermiddag rond 14:30 uur is bij het [ISC](http://www.ru.nl/isc) een
naar men dacht onschuldige onderhoudsactie gepleegd op de LDAP-server,
waarna plotseling geen auth-requests vanuit Radius meer werden
afgehandeld. Daardoor konden wireless gebruikers niet meer authenticeren
met hun u/s/e-nummer. Gebruikers met de realm @science.ru.nl hadden hier
geen last van.
