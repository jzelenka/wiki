---
cpk_affected: Users of mx3, smtp3, crestron, gitlab (PEP), goudsmit, msql01 and labservanttestvm.
cpk_begin: &id001 2019-11-05 05:30:00
cpk_end: 2019-11-05 09:20:00
cpk_number: 1256
date: *id001
tags:
- medewerkers
- studenten
title: Problems with a virtual host
url: cpk/1256
---
The virtual machine host ‘oscar’ stopped running at 5:30am. The listed
virtual machines are not running at the moment. We are working on it.
Update: Resolved. Again, a broken LVM snapshot caused the problem.
