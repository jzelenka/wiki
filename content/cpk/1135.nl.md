---
cpk_affected: Medewerkers van DCC
cpk_begin: &id001 2015-06-15 06:30:00
cpk_end: 2015-06-15 15:30:00
cpk_number: 1135
date: *id001
tags:
- medewerkers
title: Server homedcc had oude homes na reboot
url: cpk/1135
---
Na de maandagochtend-reboot van de server waren de veranderingen van de
laatste 2 weken aan de homedirectories verdwenen. Het is nog niet
duidelijk hoe dit heeft kunnen gebeuren. De backups van zaterdag 13 juni
22:55 is teruggezet.
