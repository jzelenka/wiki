---
cpk_affected: users of the network
cpk_begin: &id001 2013-03-13 13:00:00
cpk_end: 2013-03-13 13:40:00
cpk_number: 1011
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Network problems due to installation of Matlab R2013a
url: cpk/1011
---
Yesterday Matlab R2013a has been installed. Today at 13:00 hours many
servers started to automatically copy this 5.4 GB to their local disc.
Some parts of the network were overloaded by all these copying, which
made accessing the network slow for many users. The distribution of this
software will now be scheduled to happen over a longer period, primarily
outside of working hours.
