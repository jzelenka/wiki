---
cpk_begin: &id001 2018-07-02 22:00:00
cpk_end: 2018-07-03 01:00:00
cpk_number: 1234
date: *id001
tags:
- medewerkers
- studenten
title: 'Netwerkonderhoud RU: maandagavond/nacht 2 op 3 juli vanaf 22:00 uur'
url: cpk/1234
---
Op maandagavond 2 juli zal onderhoud plaatsvinden aan centrale
netwerkonderdelen van de RU. Vanaf 22:00 uur zullen centrale RU-diensten
en -netwerk meestal niet beschikbaar zijn. Telefonie ondervindt geen
storing. Voor de C&CZ-services kan verstoring optrden: enige tijd geen
connectie met Internet. Denk s.v.p. na of deze mogelijke verstoring voor
u gevolgen heeft en bereid u indien nodig voor. Toekomstige
onderhoudsvensters zijn op de [website van het
ISC](http://www.ru.nl/systeem-meldingen/) te zien.
