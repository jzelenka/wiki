---
title: FNWI network maintenance Monday April 3 19:00-23:00 Huygens in/near wing 3 except ground floor
author: petervc
cpk_number: 1322
cpk_begin: 2023-04-03 19:00:00
# cpk_end: 2023-04-03 23:00:00
cpk_affected: network users in Huygens in/near wing 3 except ground floor
date: 2023-03-30
tags: []
url: cpk/1322
---
RU ILS Connectivity announced that network maintenance will be
carried out next Monday evening that will cause short disturbances
of network connectivity for wired and wireless systems in and
around wing 3 Huygens on all floors except the ground floor.
For wired network outlets, one can check
whether they will be affected by looking at the outlet number
(starting with 107- for affected systems) or visiting [FNWI
ethergids](https://cncz.science.ru.nl/en/howto/netwerk-ethergids/).
