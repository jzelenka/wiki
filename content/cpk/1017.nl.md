---
cpk_affected: alle FNWI gebruikers met homedirectories op de bundle
cpk_begin: &id001 2013-04-22 06:30:00
cpk_end: 2013-04-22 09:50:00
cpk_number: 1017
date: *id001
tags:
- medewerkers
- studenten
title: Homeserver bundle herstartte niet
url: cpk/1017
---
De fileserver kwam niet goed op na de wekelijkse
maandagochtend-herstart. Pas na een reddings-boot en het handmatig
verwijderen van alle snapshots startte de machine goed op.
