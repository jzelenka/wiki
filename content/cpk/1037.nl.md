---
cpk_affected: DNS clients
cpk_begin: &id001 2013-09-02 04:30:00
cpk_end: 2013-09-02 08:30:00
cpk_number: 1037
date: *id001
tags:
- medewerkers
- studenten
title: DNS nameserver probleem
url: cpk/1037
---
De DNS-server op ns1.science.ru.nl startte na de reboot niet vanwege een
syntaxfout in een van de zones. Na correctie van de fout startte die wel
op.
