---
cpk_affected: 30 pc's at the Study Area Huygens
cpk_begin: &id001 2016-01-20 12:57:00
cpk_end: 2016-01-20 13:25:00
cpk_number: 1158
date: *id001
tags:
- medewerkers
- studenten
title: Network failure in part of study area Huygens
url: cpk/1158
---
In Huygens wing 2 a network switchmodule crashed twice. About 30 pc’s in
the Study Area lost their connection to the network for some time. The
switchmodule will be replaced on 2016-01-21 at 07:30 am.
