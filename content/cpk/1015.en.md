---
cpk_affected: Users of disk volumes on file server Pile (userhomes).
cpk_begin: &id001 2013-04-08 06:30:00
cpk_end: 2013-04-08 08:15:00
cpk_number: 1015
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Disk server pile offline
url: cpk/1015
---
Pile did not shutdown properly during weekly reboot due to a kernel
panic. Solution: Executed power-cycle of the system
