---
cpk_affected: Computer systems and network switches that don't have a backup power
  supply (UPS). Among others FELIX.
cpk_begin: &id001 2015-06-11 20:00:00
cpk_end: 2015-06-11 20:30:00
cpk_number: 1134
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Services unavailable due to power outage
url: cpk/1134
---
A short power dip interrupted desktop computers and some network
switches without UPS backup power on the RU campus. It took approx. 20
minutes before the affected switches became fully operational again. A
network switch at the Transitorium reported a failed module until it was
reset around 8:30 am on Friday morning. For ca. 46 outlets/users the
network didn’t function until 8:40. The [UVB](http://www.ru.nl/uvb)
reported as cause a cable cut at the Kapittelweg. Liander reports a
[power disturbance an hour
later](https://www.liander.nl/storingen/overzicht/storing?storingsnummer=3963192)
at that location.
