---
cpk_affected: Dozens of websites on this webserver
cpk_begin: &id001 2014-09-09 10:46:00
cpk_end: 2014-09-09 11:50:00
cpk_number: 1105
date: *id001
tags:
- medewerkers
- studenten
title: Webserver (klopjacht) disk problem
url: cpk/1105
---
A defective disk made the server stop serving web requests. A reboot
fixed the problem. The defective disk will be replaced soon.
