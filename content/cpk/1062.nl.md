---
cpk_affected: Gebruikers van het netwerk in Huygens vleugel 1, speciaal op de begane
  grond.
cpk_begin: &id001 2013-12-17 19:00:00
cpk_end: 2013-12-17 23:59:00
cpk_number: 1062
date: *id001
tags:
- medewerkers
- studenten
title: Storing netwerk Huygens vleugel 1
url: cpk/1062
---
Op dinsdagavond 17 december 19:00-24:00 uur vinden er werkzaamheden
plaats aan het datanetwerk in Huygens vleugel 1. Zowel het bedrade als
draadloze netwerk zullen daardoor tussen genoemde tijdstippen enige
malen niet beschikbaar zijn op diverse locaties in die vleugel. De
voornaamste overlast zal op de begane grond zijn.
