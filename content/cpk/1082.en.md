---
cpk_affected: Users of the printto/printsmb/phpMyAdmin service
cpk_begin: &id001 2014-04-17 08:30:00
cpk_end: 2014-04-17 09:20:00
cpk_number: 1082
date: *id001
tags:
- medewerkers
- studenten
title: Print/phpMyAdmin server replacement Thursday April 17 08:30-09:20
url: cpk/1082
---
Because of the recent problems with this server, we are going to replace
the server, in order to prevent further service interruptions.
