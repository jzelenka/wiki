---
cpk_affected: Gebruikers van het Coma rekencluster
cpk_begin: &id001 2022-02-22 13:10:00
cpk_end: 2022-02-22 15:47:00
cpk_number: 1291
date: *id001
title: Netwerkswitch voor Astro Coma cluster down
url: cpk/1291
---
De netwerkswitch voor het Coma cluster lijkt defect, de aangesloten
nodes zijn afgesloten van de rest van het netwerk. We zetten z.s.m. een
vervangende switch in en zullen het probleem verder (laten) analyseren.
