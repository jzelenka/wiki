---
cpk_affected: medewerkers, studenten, mail versturen
cpk_begin: &id001 2016-02-25 07:25:00
cpk_end: 2016-02-25 08:30:00
cpk_number: 1167
tags:
- medewerkers
- studenten
date: *id001
title: Virtual servers problem
url: cpk/1167
---

Because of automated updates, Several virtual host had become
unreachable. Server vos: gitlab3, bfacdc04, smtp2, redmine2 Server wolf:
smtp1, bfacdc03, est2

Rebooted vos and wolf.
