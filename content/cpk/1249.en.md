---
cpk_affected: Users of network services
cpk_begin: &id001 2019-05-22 18:55:00
cpk_end: 2019-05-22 19:37:00
cpk_number: 1249
date: *id001
tags:
- medewerkers
- studenten
title: Network interruption central RU network
url: cpk/1249
---
Due to a faulty RU core network device, several service couldn’t be
reached. This device is in a redundant setup. The
[ISC](https://www.ru.nl/ict-uk/) network department will start an
investigation what exactly happened and how to prevent nuisance in the
future.
