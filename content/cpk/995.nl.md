---
cpk_affected: Gebruikers van diskruimte op de Plenty. Onder andere de S en T disks
  die in de Terminalkamers worden gebruikt.
cpk_begin: &id001 2012-09-24 06:30:00
cpk_end: 2012-09-24 09:00:00
cpk_number: 995
date: *id001
title: Disk server "Plenty" offline
url: cpk/995
---
De machine bleef in het BIOS hangen bij de wekelijkse reboot op
maandagmorgen.
