---
cpk_affected: Gebruikers met een Fedora desktop PC
cpk_begin: &id001 2012-12-14 00:00:00
cpk_end: 2012-12-14 00:00:00
cpk_number: 1005
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: LDAP server renewed
url: cpk/1005
---
Na het upgraden van een van onze LDAP servers, is gebleken dat oudere
systemen met Fedora linux niet meer goed opstarten. Hier is inmiddels
een fix voor toegepast. Wie hier toch tegen aanloopt wordt verzocht
contact op te nemen met C&CZ.

Older Fedora desktop PC’s may experience startup problems after an
upgrade of one of our LDAP servers. A fix is available and has been
applied. If you still encounter this problem, please contact C&CZ.
