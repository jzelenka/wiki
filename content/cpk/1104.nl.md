---
cpk_affected: Alle in- en uitgaande netwerkverkeer van de RU.
cpk_begin: &id001 2014-09-08 22:00:00
cpk_end: 2014-09-09 00:03:00
cpk_number: 1104
date: *id001
tags:
- medewerkers
- studenten
title: Surfnet uplink offline
url: cpk/1104
---
Door een [fout tijdens gepland onderhoud door
Surfnet](http://www.ru.nl/ictservicecentrum/actueel/storingsberichten/storing/)
aan de RU netwerkinterface was de uplink van de RU naar Surfnet down.
