---
title: Eduroam wifi niet beschikbaar tijdens upgrade
author: petervc
cpk_number: 1301
cpk_begin: &id001 2022-10-20 23:30:00
cpk_end: 2022-10-21 01:30:00
onderhoud: true
cpk_affected: Eduroam wifi in veel gebouwen, o.a. Huygens en gebouwen eromheen
date: *id001
url: cpk/1301
---
ILS wifi beheer liet ons weten dat zij de software van wireless access points
zullen upgraden vanwege securitygaten die in de nieuwste release gedicht zijn.

Het onderhoud is afgerond op 2022-10-21 01:30

Bron: [meldingen.ru.nl](https://meldingen.ru.nl/detail.php?id=1330&lang=nl&tg=0&f=0&ii)
