---
cpk_affected: Users of RU mail sending mail out of the RU mailserver
cpk_begin: &id001 2016-04-19 17:37:00
cpk_end: 2016-04-20 14:13:00
cpk_number: 1175
date: *id001
tags:
- medewerkers
- studenten
title: RU mailserver blacklisted, for Science too
url: cpk/1175
---
Yesterday the RU outgoing mailserver smtpout3.ru.nl got listed on the
[SORBS blacklist](http://dnsbl.sorbs.net/) of confirmed senders of spam
and phishing mails. The reason probably is that recently many students
fell for phishing mails, which made their username and password known to
Internet criminals. These criminals use that to send spam and phishing
mails. Because also the Science mailservers use that blacklist, we did
not accept mail from the RU mailserver anymore. The senders received an
error message: “Your message wasn’t delivered due to a permission or
security issue.” We solved this for our Science users by putting the RU
mailserver on our own whitelist.
