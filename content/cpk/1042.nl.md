---
cpk_affected: Gebruikers van netwerkschijven
cpk_begin: &id001 2013-09-16 06:30:00
cpk_end: 2013-09-16 13:05:00
cpk_number: 1042
date: *id001
tags:
- medewerkers
- studenten
title: Fileserver chunk herstartte niet
url: cpk/1042
---
De fileserver kwam niet goed op na de wekelijkse
maandagochtend-herstart. Pas na een reddings-boot en het verwijderen van
alle snapshots startte de machine goed op.

Getroffen schijven:

`arb botgarden ccs4 excienwi exoarchief FIT gi1 gipsy isis-dhz`
`itt iwwr1 leonardo mercator micord molspec olcwis ons pvs`
`ratio tzacad vb vscxray WiskundeToernooi zeegras`
