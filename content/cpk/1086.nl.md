---
cpk_affected: Medewerkers die met Uxxxxxx@ru.nl Eduroam gebruikten, voornamelijk in
  FNWI
cpk_begin: &id001 2014-04-30 13:00:00
cpk_end: 2014-05-01 16:25:00
cpk_number: 1086
date: *id001
tags:
- medewerkers
title: U-nummer authenticatieprobleem Eduroam
url: cpk/1086
---
Het nieuwe [RU IdM-systeem](http://www.ru.nl/idm/) overschreef onterecht
de [RU-wachtwoord
hash](http://en.wikipedia.org/wiki/Cryptographic_hash_function#Password_verification)
bij veranderde gegevens van een persoon. Dit trof voornamelijk
FNWI-medewerkers, omdat voor deze groep het Science email-adres
ingelezen werd. Dat kan gebruikt gaan worden als extern mail-adres voor
het resetten van het RU-wachtwoord. Nadat het probleem herkend werd, is
dit in IdM snel verholpen door het terugzetten vanaf een backup van de
RU-wachtwoord hashes.
