---
cpk_affected: Gebruikers van de posterprinter "kamerbreed"
cpk_begin: &id001 2013-09-25 17:00:00
cpk_end: 2013-11-20 14:00:00
cpk_number: 1058
date: *id001
tags:
- medewerkers
- studenten
title: Posterprinter "kamerbreed" defect
url: cpk/1058
---
Het moederbord van de printer was defect. Omdat er geen
onderhoudscontract meer is op deze oude printer en de onderdelen slecht
leverbaar waren, heeft de reparatie lang geduurd.
