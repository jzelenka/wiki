---
cpk_affected: Gebruikers van Eduroam en Ru-guest
cpk_begin: &id001 2014-05-19 20:00:00
cpk_end: 2014-05-19 23:59:00
cpk_number: 1092
date: *id001
tags:
- medewerkers
- studenten
title: Onderhoud wireless infrastructuur op 19 mei a.s.
url: cpk/1092
---
Op maandagavond 19 mei a.s. vindt er door het ISC een major upgrade
plaats van de wireless infrastructuur en het bijbehorende beheersysteem
(Prime geheten). Vanaf 20:00 uur zal daardoor de wireless-service (lees:
Eduroam) enkele malen onderbroken worden. Bestaande wireless
verbindingen worden verbroken en nieuwe verbindingen opzetten is dan
tijdelijk niet mogelijk. Vanwege de complexe voorbereidingen is er een
kans dat deze werkzaamheden niet of niet volledig op 19 mei kunnen
worden afgerond. In dat geval zullen de werkzaamheden worden voortgezet
op maandag 26 mei a.s., eveneens met onderbrekingen vanaf 20 uur.
