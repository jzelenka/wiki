---
cpk_affected: netwerk in Mercator III zuidzijde en laagbouw.
cpk_begin: &id001 2014-11-19 18:30:00
cpk_end: 2014-11-19 23:00:00
cpk_number: 1118
date: *id001
tags:
- medewerkers
- studenten
title: Netwerkonderhoud in Mercator III zuid en laagbouw
url: cpk/1118
---
De netwerkswitches in bovenstaande gebouwen/verdiepingen/locaties zullen
vervangen worden. Dat betekent dat het vaste en draadloze datanetwerk
onderbroken zal zijn en er dus geen netwerkverkeer mogelijk is. Dat
geldt ook voor het netwerk van toegangscontrole, klimaatregeling etc.
