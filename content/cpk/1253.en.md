---
cpk_affected: Users of NFS network shares from server 'peck' (/vol/* on Linux systems).
  Only disks that have a DNS record ({volumename}-srv.science.ru.nl) pointing to file
  server 'peck' are affected.
cpk_begin: &id001 2019-09-23 06:30:00
cpk_end: 2019-09-23 09:00:00
cpk_number: 1253
date: *id001
title: NFS service interruption
url: cpk/1253
---
Due to a configuration error, the NFS service on file server ‘peck’
refused to start. After fixing the error, the NFS service started up
correctly.
