---
cpk_affected: Eduroam gebruikers
cpk_begin: &id001 2014-05-06 13:40:00
cpk_end: 2014-05-06 18:29:00
cpk_number: 1087
date: *id001
tags:
- medewerkers
- studenten
title: Eduroam probleem
url: cpk/1087
---
Sommige Eduroam-gebruikers kregen geen IP-nummer, omdat een van de
reeksen geen nummers meer vrij had. Dit was de reeks waar ook alle
@science.ru.nl een IP-nummer van kregen. De netwerkbeheerders van het
[ISC](http://www.ru.nl/isc) hebben eerst de leasetijd verlaagd. Nu gaat
men ook deze reeks vrij maken voor alleengebruik door @science.ru.nl
gebruikers. Dit voorkomt dat het probleem binnenkort nog eens voorkomt.
