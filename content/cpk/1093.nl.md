---
cpk_affected: Gebruikers van de posterprinter "kamerbreed"
cpk_begin: &id001 2014-04-21 00:00:00
cpk_end: 2014-05-16 00:00:00
cpk_number: 1093
date: *id001
tags:
- medewerkers
- studenten
title: Posterprinter "kamerbreed" defect
url: cpk/1093
---
De printer is defect. Er zijn onderdelen besteld. We hopen dat de
printer half mei weer beschikbaar zal zijn.
