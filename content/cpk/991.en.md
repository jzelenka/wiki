---
cpk_affected: 'Science mail users trying to send mail to MS-domains: hotmail.com,
  live.com, ...'
cpk_begin: &id001 2012-07-11 03:08:00
cpk_end: 2012-07-11 14:55:00
cpk_number: 991
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: SMTP server blacklisted by MS Live Hotmail
url: cpk/991
---
This morning, users reported that mail from smtp.science.ru.nl to
hotmail users was being bounced by hotmail. We have tried to let the
hotmail administrators change this fast, but when this took too long, we
changed the IP-number of our smtp-server.
