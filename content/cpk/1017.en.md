---
cpk_affected: all FNWI users with a homedirectory on fileserver bundle
cpk_begin: &id001 2013-04-22 06:30:00
cpk_end: 2013-04-22 09:50:00
cpk_number: 1017
date: *id001
tags:
- medewerkers
- studenten
title: Homeserver bundle failed reboot
url: cpk/1017
---
The fileserver failed the regular Monday morning reboot. Only after a
rescue boot and a manual removal of all snapshots rebooting the machine
worked.
