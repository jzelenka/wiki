---
cpk_affected: fnwi login server users
cpk_begin: &id001 2016-01-11 06:30:00
cpk_end: 2016-01-11 07:30:00
cpk_number: 1154
date: *id001
tags:
- medewerkers
- studenten
title: Server lilo3 and lilo4 Monday morning reboot failed
url: cpk/1154
---
The server lilo3/4 stalled at shutdown during the monday morning reboot.
Solution: power cycle. Cause currently unknown.
