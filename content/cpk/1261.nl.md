---
cpk_affected: slurm op ubuntu 16.04 (cn07)
cpk_begin: &id001 2020-05-18 09:50:00
cpk_end: 2020-05-19 12:15:00
cpk_number: 1261
date: *id001
tags:
- medewerkers
- studenten
title: CN00 Slurm master ubuntu 16.04 down
url: cpk/1261
---
Door een mislukte BIOS update is de hardware van de database server
blijven hangen en start niet meer op (brick). De functionaliteit van de
server is overgezet op de hardware van cn00, waardoor die nu uit de
lucht is. Als de hardware van sperwer weer in orde is, wordt de situatie
weer hersteld.
