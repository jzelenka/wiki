---
cpk_affected: DNS clients
cpk_begin: &id001 2013-09-02 04:30:00
cpk_end: 2013-09-02 08:30:00
cpk_number: 1037
date: *id001
tags:
- medewerkers
- studenten
title: DNS nameserver problem
url: cpk/1037
---
The DNS server on ns1.science.ru.nl didn’t start after the reboot, due
to a syntax error in one of the zone files. When this had been
corrected, it started without problems.
