---
cpk_affected: wifi gebruikers
cpk_begin: &id001 2013-03-04 18:00:00
cpk_end: 2013-03-04 18:15:00
cpk_number: 1010
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Meer IP-nummers voor ru-wlan en Science (draadloos)
url: cpk/1010
---
Op maandag 4 maart 2013 om 18.00 uur wordt het aantal IP-nummers dat in
de FNWI-gebouwen beschikbaar is voor ru-wlan en Science, verdubbeld.
Omdat ru-wlan naar een nieuwe nummerreeks verhuist, zullen gebruikers
van ru-wlan daardoor verlies van connectiviteit hebben gedurende
maximaal 15 minuten. Er was al een plan om ru-wlan en Science binnen de
FNWI-gebouwen te vervangen door het RU-brede Eduroam en ru-wlan. Maar
het gebruik van het draadloze netwerk is zo snel gegroeid, dat we niet
konden wachten tot dit plan uitgevoerd zou zijn. Vorige week konden
enkele gebruikers soms zelfs geen IP-nummer meer krijgen, terwijl de
lease-tijd al naar 30 minuten teruggebracht was. Daarom werd deze
tijdelijke maatregel toch noodzakelijk, met invoering op korte termijn.
