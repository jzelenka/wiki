---
cpk_affected: alle FNWI gebruikers tot 9h30 en daarna "bundle" homedirectories, draadloos,
  "plus" netwerkschijven en diverse websites
cpk_begin: &id001 2012-10-18 03:00:00
cpk_end: 2012-10-18 10:00:00
cpk_number: 1001
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Services niet beschikbaar door stroom- en netwerkstoring
url: cpk/1001
---
In de nacht van woensdag op donderdag heeft een stroomstoring gezorgd
voor het uitvallen van netwerksystemen in de (kelder) ICT zaal. Door om
de UPS (Uninterruptable Power Supply) heen te werken lukte het rond 9h15
om stroom naar netwerkapparatuur te leiden. Omdat de meeste servers niet
zonder stroom zijn geweest, bleken deze automatisch service te verlenen
na herstel van de netwerkverbindingen. Alleen op de homedirectories
server “bundle” was het noodzakelijk de netwerkdriver-software te
herstarten. Aangezien bleek dat PC’s niet correct wilden opstarten
hebben we verschillende websites herstart die tijdens het boot-proces
worden geraadpleegd. Ook draadloze apparatuur, lees smartphones, bleken
niet correct te kunnen werken vanwege een split-brain situatie binnen de
DHCP service. Hierdoor werden IP adressen niet meer uitgedeeld en was
draadloze toegang tot het netwerk niet mogelijk tot ongeveer 13h.
Hieraan ongerelateerd was een hardwarestoring met het RAID array van de
“plus” die later in de dag verholpen is. Toegang tot de volgende
netwerkschijven is hersteld na de lunch voor: sofie, ams\*, molchem,
mb\*, encapson, milkun4, snn, neuropi, digicd. carta, …
