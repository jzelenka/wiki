---
cpk_affected: 'Users of this U: / home server'
cpk_begin: &id001 2014-04-04 11:35:00
cpk_end: 2014-04-04 12:25:00
cpk_number: 1078
date: *id001
tags:
- medewerkers
- studenten
title: 'U: / home server bundle problems'
url: cpk/1078
---
From the moment of yesterday’s snapshots (13:00 hours), more and more
processes were hung at the server. The first complaints arrived at C&CZ
ca. 11:15 hours this morning. Therefore we decided ca. 11:35 to restart
the server. The reboot resolved the problem. The number of snapshots
will be reduced in order to try to prevent problems due to snapshots in
the future.
