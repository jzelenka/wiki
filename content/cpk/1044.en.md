---
cpk_affected: Users of the printto and printsmb printer servers
cpk_begin: &id001 2013-09-24 11:41:00
cpk_end: 2013-09-24 12:22:00
cpk_number: 1044
date: *id001
tags:
- medewerkers
- studenten
title: Printto/printsmb server problems
url: cpk/1044
---
The printserver crashed. A reboot of the machine solved the problem.
