---
cpk_affected: Gebruikers van Ubuntu 10 clusternodes
cpk_begin: &id001 2015-06-03 12:10:00
cpk_end: 2015-06-03 12:10:00
cpk_number: 1132
date: *id001
tags:
- medewerkers
title: Ubuntu 10.04 cluster / cn99 server probleem
url: cpk/1132
---
De SUN Grid Engine master voor Ubuntu 10.04 jobs is niet meer
beschikbaar omdat deze door de cn99 verzorgd werd. Deze machine is meer
dan vijf jaar oud en valt daardoor buiten onderhoudscontracten. Tijdens
het booten worden de benodigde filesystemen niet meer gevonden maar dat
zal dus niet worden opgelost. De clusternodes cn67, cn74, cn75 en cn81
die als enige nog het verouderde Ubuntu 10 operating systeem draaien
kunnen nog wel gebruikt worden door daarop rechtstreeks in te loggen.
