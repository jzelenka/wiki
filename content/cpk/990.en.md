---
cpk_affected: stack fileservices
cpk_begin: &id001 2012-06-22 17:03:00
cpk_end: 2012-06-24 19:30:00
cpk_number: 990
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: 'Planned service interruption: file server with problems'
url: cpk/990
---
A hardware failure of a boot disk of the fileserver stack was reported
Friday morning June 22. We decided to repair this after working hours.
Thus at approximately 17:00 the defective boot disk was removed from the
machine and replaced by a spare one. Enabling the disk, making it
bootable, restoring file systems and rebooting the machine (after
removing all snapshots) took a lot of time. When this was resolved
Friday evening, the NFS/SMB fileservice was not active on the mounted
filesystems. It took a reboot Sunday evening to resolve all problems.
