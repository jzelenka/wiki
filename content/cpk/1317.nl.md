---
title: Reboot cn106
author: wim
cpk_number: 1317
cpk_begin: 2023-02-23 13:11:00
cpk_end: 2023-02-23 13:11:00
cpk_affected: 
date: 2023-02-23
tags: []
url: cpk/1317
---
I.v.m problemen en bereikbaarheid machine herstart.

Oorzaak is helaas onbekend.
