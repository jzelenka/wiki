---
cpk_affected: Gebruikers van een van de ca. 100 netwerkschijven van deze server
cpk_begin: &id001 2021-09-17 14:30:00
cpk_end: 2021-09-17 15:30:00
cpk_number: 1285
date: *id001
tags:
- medewerkers
- docenten
title: Fileserver 'flock' overbelast
url: cpk/1285
---
Vooraf geteste cursussoftware veroorzaakte bij het gebruik door 100
studenten een te grote belasting op de fileserver. Alle gebruikers van
deze fileserver hadden hier last van.
