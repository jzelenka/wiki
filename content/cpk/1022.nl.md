---
cpk_affected: alle gebruikers op wireless netwerken ru-wlan en eduroam.
cpk_begin: &id001 2013-06-06 22:00:00
cpk_end: 2013-06-06 23:59:00
cpk_number: 1022
date: *id001
tags:
- medewerkers
- studenten
title: Werkzaamheden Wireless@RU
url: cpk/1022
---
Op donderdag 6 juni a.s. vanaf 22:00 uur vinden er werkzaamheden plaats
aan het wireless-netwerk. Dit als voorbereiding op de a.s. gedeeltelijke
vernieuwing van de wireless infrstructuur. De wireless netwerken ru-wlan
en eduroam zijn dan campusbreed minstens twee uur achter elkaar niet
beschikbaar en alle bestaande wireless verbindingen zullen worden
verbroken. Uiteraard wordt er van alles aan gedaan om de onderbreking zo
kort mogelijk te laten zijn. Gebruikers van het wireless netwerk Science
zullen echter geen overlast ondervinden, dit netwerk blijft beschikbaar.
