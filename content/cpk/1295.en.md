---
cpk_affected: all network outlets in Mercator, Proeftuin and Logistiek Centrum will
  be down for max. 5 minutes
cpk_begin: &id001 2022-05-14 09:00:00
cpk_end: 2022-05-14 10:00:00
cpk_number: 1295
date: *id001
title: Saturday May 14 adjacent buildings (Mercator, Proeftuin, Logistiek) 5 minutes
  without network
url: cpk/1295
---
RU/ILS network management will switch to new hardware. This will lead to
a network interruption of at most 5 minutes.
