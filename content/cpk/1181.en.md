---
cpk_affected: Users of licenses, see below
cpk_begin: &id001 2016-05-30 06:30:00
cpk_end: 2016-05-30 09:23:00
cpk_number: 1181
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: License server reboot problem
url: cpk/1181
---
Due to a problem with a new license the license server didn’t go down
for the weekly reboot. When this came to attention, the server was
rebooted and the problem was solved.

`Altera Ansys Accelrys Genesys Hdlworks Idapro`
`Xilinx IntelCompiler Ingr Maple Mathematica Matlab MentorGraphics Mestrelab`
`Topspin Orcad Originlab StateaseRlm`
