---
cpk_affected: 'Gebruikers van deze U: / home server of de Linux loginservers'
cpk_begin: &id001 2014-03-05 13:00:00
cpk_end: 2014-03-05 16:40:00
cpk_number: 1072
date: *id001
tags:
- medewerkers
- studenten
title: 'U: / home server pile en Linux loginservers lilo/stitch in de problemen'
url: cpk/1072
---
De server kreeg een hoge load en verleende geen diensten meer aan
gebruikers, waarschijnlijk vanwege een probleem met de snapshots die om
13:00 uur gemaakt worden. Na een herstart was het probleem verdwenen.
