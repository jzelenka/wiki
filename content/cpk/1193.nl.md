---
cpk_affected: diverse gebruikers in Huygens vleugel 8
cpk_begin: &id001 2017-01-24 10:50:00
cpk_end: 2017-01-24 11:55:00
cpk_number: 1193
date: *id001
tags:
- medewerkers
- studenten
title: Netwerkstoring in deel Huygens vleugel 8
url: cpk/1193
---
In Huygens vleugel 8 is een netwerk switchmodule uitgevallen, waardoor
diverse gebruikers tijdelijk geen netwerk en telefoon hadden. De switch
module is inmiddels vervangen.
