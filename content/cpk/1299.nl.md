---
cpk_affected: verbinding campus/internet (18:30-20:30) en centrale RU-servers (19:30-22:30)
cpk_begin: &id001 2022-09-23 18:30:00
cpk_end: 2022-09-23 22:30:00
cpk_number: 1299
date: *id001
title: Vrijdag 23 september 18:30-22:30 onderhoud RU-firewalls
url: cpk/1299
---
ILS netwerkbeheer deelde mee: Op vrijdag 23 september a.s. wordt er
tussen 18.00 – 23.00 uur netwerkonderhoud aan de DMZ- en SRV-firewalls
uitgevoerd. Er wordt enkele malen een korte onderbreking van 20 tot 40
seconden verwacht. Als het niet volgens verwachting verloopt, kunnen er
enkele onderbrekingen van 5 tot 10 minuten optreden.
