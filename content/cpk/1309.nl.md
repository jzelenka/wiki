---
title: Ongeplande reboot van een vmhost
author: bram
cpk_number: 1309
cpk_begin: 2022-12-16 16:34:00
cpk_end: 2022-12-16 16:36:00
cpk_affected: Gebruikers van labservant, slurm20 en gitlab runner1
date: 2022-12-16
tags: []
url: cpk/1309
---
Na het vervangen van een defecte disk was een reboot nodig om de vervangende disk te kunnen gebruiken. Helaas heeft dit niet geholpen. Bij een volgende poging met een andere disk is het wel gelukt, de machine is de RAID aan het herstellen.
