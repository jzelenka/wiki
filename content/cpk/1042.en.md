---
cpk_affected: Users of network disks
cpk_begin: &id001 2013-09-16 06:30:00
cpk_end: 2013-09-16 13:05:00
cpk_number: 1042
date: *id001
tags:
- medewerkers
- studenten
title: File server chunk failed reboot
url: cpk/1042
---
The fileserver failed the regular Monday morning reboot. Only after a
rescue boot and a removal of all snapshots rebooting the machine worked.

Affected disks:

`arb botgarden ccs4 excienwi exoarchief FIT gi1 gipsy isis-dhz`
`itt iwwr1 leonardo mercator micord molspec olcwis ons pvs`
`ratio tzacad vb vscxray WiskundeToernooi zeegras`
