---
cpk_affected: Users of horde webmail
cpk_begin: &id001 2012-09-25 23:05:00
cpk_end: 2012-09-26 10:20:00
cpk_number: 997
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Horde webmail server down because of spam
url: cpk/997
---
Yesterday evening, horde webmail appeared to be misused for sending
spam. This could happen because a naive user gave the Science password
to spammers. First we stopped horde. This morning we disabled the
account of the naive user and restarted horde.
