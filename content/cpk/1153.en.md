---
cpk_affected: Users FTP
cpk_begin: &id001 2016-01-05 06:30:00
cpk_end: 2016-01-05 14:30:00
cpk_number: 1153
date: *id001
title: Server comas1 Monday morning reboot failed
url: cpk/1153
---
The server comas1 did not start due to some possible timing problems
between the raid array and OS. Solution: skipping the mount of /vd0 and
/vd1 during boot (key S), mounts fixed manually later on followed by
exportfs -uaf; exportfs -raf.

Machine failed for unknown reason at boot. Solved by rebooting.
