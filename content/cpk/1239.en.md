---
cpk_affected: Users of crestron (AVD) gitlab7 (PEP) goudsmit (NMR) msql01
cpk_begin: &id001 2018-08-23 01:41:00
cpk_end: 2018-08-23 11:35:00
cpk_number: 1239
date: *id001
tags:
- medewerkers
- studenten
title: Host hardware of virtual machines broken
url: cpk/1239
---
Last night the host for a number of vm’s (oscar) broke down. By using a
replacement server (joule) this problem has been temporarily resolved.
Tomorrow the machine will be repaired ans we will determine when the
machines will be switched back.
