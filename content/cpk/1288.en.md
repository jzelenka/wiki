---
cpk_affected: users of Ceph filesystems and websites on webvm01
cpk_begin: &id001 2021-11-16 00:00:00
cpk_end: 2021-11-17 00:00:00
cpk_number: 1288
date: *id001
tags:
- medewerkers
title: Ceph storage expansion caused performance issues
url: cpk/1288
---
As a result of the expansion of the Ceph storage cluster, the cluster
had performance and availability issues. The problems were resolved this
morning.
