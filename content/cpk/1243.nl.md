---
cpk_affected: vele Eduroam-gebruikers met science accounts
cpk_begin: &id001 2019-03-10 18:00:00
cpk_end: 2019-03-13 08:30:00
cpk_number: 1243
date: *id001
tags:
- medewerkers
- studenten
title: Geen verbinding met Eduroam met science-accounts
url: cpk/1243
---
Door een fout in de configuratie van de Science radius server mislukten
vele connecties naar Eduroam voor gebruikers met Science accounts. Het
vergde twee dagen grondig onderzoek door zowel ISC als C&CZ om dit te
herstellen.
