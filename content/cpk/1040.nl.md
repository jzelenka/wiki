---
cpk_affected: FNWI-studenten
cpk_begin: &id001 2013-09-02 13:33:00
cpk_end: 2013-09-03 15:02:00
cpk_number: 1040
date: *id001
tags:
- studenten
title: Zwart-wit KM pr-hg-00-002 printte in kleur
url: cpk/1040
---
De zwart-wit geknepen Konica Minolta printer/MFP pr-hg-00-002 bleek
plotseling in kleur uit te kunnen printen. C&CZ heeft de student waarbij
dit gebeurde, het budget vergoed en de printer stilgezet totdat KM de
instellingen van de printer gecorrigeerd had.
