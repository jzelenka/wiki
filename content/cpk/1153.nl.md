---
cpk_affected: Gebruikers FTP
cpk_begin: &id001 2016-01-05 06:30:00
cpk_end: 2016-01-05 14:30:00
cpk_number: 1153
date: *id001
title: Server comas1 maandag ochtend reboot gefaald
url: cpk/1153
---
De server comas1 starte niet correct op vanwege mogelijk timing probleem
tussen het raid array en OS. Oplossing: overslaan mount /vd0 en vd1
tijdens boot (toets S), later handmatig mounts gefixed gevolgd door
exportfs -uaf; exportfs -raf.

Om onbekende reden was de machine niet goed opgekomen. Opgelost na reboot.
