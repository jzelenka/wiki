---
cpk_affected: Users of shared network disks
cpk_begin: &id001 2016-02-09 09:00:00
cpk_end: 2016-02-09 15:37:00
cpk_number: 1163
date: *id001
tags:
- medewerkers
- studenten
title: Fileserver problems
url: cpk/1163
---
The Ubuntu 14.04 fileservers “flock” and “muckle” had problems a few
times. After we reverted to the previous kernel version, the service
seems to be stable again.
