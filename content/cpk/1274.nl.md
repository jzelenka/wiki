---
cpk_affected: gebruikers van GitLab en Mattermost
cpk_begin: &id001 2021-02-07 04:00:00
cpk_end: 2021-02-07 12:50:00
cpk_number: 1274
date: *id001
title: Gitlab upgrade
url: cpk/1274
---

Vanwege een upgrade van GitLab en Mattermost naar
de laatste versie zijn deze tijdelijk niet beschikbaar.
