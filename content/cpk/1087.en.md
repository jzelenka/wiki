---
cpk_affected: Eduroam users
cpk_begin: &id001 2014-05-06 13:40:00
cpk_end: 2014-05-06 18:29:00
cpk_number: 1087
date: *id001
tags:
- medewerkers
- studenten
title: Eduroam problem
url: cpk/1087
---
Some Eduroam users didn’t get an IP number, because one of the IP ranges
had no free leases. This ranges contained all @science.ru.nl users. The
[ISC](http://www.ru.nl/isc) networking department first decreased the
lease time. Now they also make this range free for use by @science.ru.nl
users. This will prevent the problem from occuring again soon.
