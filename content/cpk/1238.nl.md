---
cpk_affected: Gebruikers die Science mail lezen
cpk_begin: &id001 2018-08-09 13:05:00
cpk_end: 2018-08-22 09:25:00
cpk_number: 1238
date: *id001
tags:
- medewerkers
- studenten
title: Bug in nieuwe IMAP-mailserver verholpen
url: cpk/1238
---
De nieuwe versie van de IMAP-mailserversoftware bevatte een serieuze bug
in de afhandeling van SSL-connecties, die er toe leidde dat connecties
vaak wegvielen en afhankelijk van de mailclient tot een bijna onwerkbare
situatie leidde. Vanochtend is een patch voor deze bug geïnstalleerd.
