---
cpk_affected: users of lilo
cpk_begin: &id001 2021-02-25 17:30:00
cpk_end: 2021-03-04 16:45:00
cpk_number: 1278
date: *id001
tags:
- medewerkers
- studenten
title: Lilo6 down
url: cpk/1278
---
As of Thursday afternoon, the lilo6 is down due to hardware issues.
Because lilo6 was the default linux login server (lilo referred to
lilo6), this affected many users of lilo. The impact is limited, because
we have alternative lilo's, namely lilo5 and lilo7. As of March 1st lilo
now refers to lilo7, ssh will warn about DNS SPOOFING, which is due to
the difference host keys for lilo7
`ECDSA SHA256:si3g2elo5m6TShx3PjX0+vF50pZ8NK/iXz/ESB+ZeP0`
