---
cpk_affected: 'Users of this U: / home server or thee Linux loginservers'
cpk_begin: &id001 2014-03-05 13:00:00
cpk_end: 2014-03-05 16:40:00
cpk_number: 1072
date: *id001
tags:
- medewerkers
- studenten
title: 'U: / home server pile and Linux login server lilo/stitch problems'
url: cpk/1072
---
The server had a high load and stopped servicing users, probably because
of problems with the creation of new snapshots at 13:00 hours. A reboot
solved the problem.
