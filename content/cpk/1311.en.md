---
title: Maintenance RU internet connection Friday Feb 10 18:00-23:00
author: petervc
cpk_number: 1311
cpk_begin: 2023-02-10 18:00:00
cpk_end: 2023-02-10 23:00:00
cpk_affected: RU network traffic to or from campus
date: 2023-01-13
tags: []
url: cpk/1311
---
ILS Connectivity announced that network maintenance will be carried out on the internet connection of Radboud University on February 10 between 18:00 and 23:00. Network connections may be disturbed several times, each lasting less than 15 minutes.
