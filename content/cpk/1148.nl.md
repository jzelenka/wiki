---
cpk_affected: Alle gebruikers van de RU Internetverbinding
cpk_begin: &id001 2015-10-20 00:00:00
cpk_end: 2015-10-20 03:00:00
cpk_number: 1148
date: *id001
tags:
- medewerkers
- studenten
title: RU Internetverbinding uit vanwege onderhoud
url: cpk/1148
---
``

Het [ISC](http://www.ru.nl/isc) heeft
[aangekondigd](http://www.ru.nl/systeem-meldingen/) dat op
dinsdagochtend 20 oktober 2015 tussen 00:00 en 03:00 uur SURFnet en het
ISC onderhoud uitvoeren aan de verbinding van de RU met
SURFnet/Internet. Van 00:00 tot 01:30 is er geen verbinding. Tussen
01:30 en 03:00 kunnen korte onderbrekingen optreden. Het advies is om
zelf na te gaan welke systemen hiervan hinder kunnen ondervinden en zo
mogelijk maatregelen te nemen door netwerkactiviteit op een andere tijd
te laten plaatsvinden. Denk evt. ook aan systemen met alarmering via
Internet.
