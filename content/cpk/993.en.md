---
cpk_affected: Users with homedirectory server "pile" (as can be seen on `<http://DIY.science.ru.nl>`)
cpk_begin: &id001 2012-07-24 07:00:00
cpk_end: 2012-07-24 09:00:00
cpk_number: 993
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: 'Postponed: Announced downtime: home server "pile" down for replacement'
url: cpk/993
---
The downtime below has been postponed, because we had a few questions on
the new server, that could not be answered in time. To be continued…

Next Tuesday morning, the home server “pile” will be replaced by a new,
more powerful server. Because the data have been synchronized with the
new server, there will not be much downtime. The new server should be
very dependable: hardware
[RAID-6](http://en.wikipedia.org/wiki/RAID#RAID_6_replacing_RAID_5_in_enterprise_environments),
double processors and power supplies and a 5-year support contract from
the supplier. The performance has improved, e.g. by using hardware RAID
with a 1 GB [write cache with battery
backup](http://serverfault.com/questions/65096/battery-backed-write-cache).
