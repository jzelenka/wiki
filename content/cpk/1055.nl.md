---
cpk_affected: 'Gebruikers van deze U: / home server'
cpk_begin: &id001 2013-10-29 16:00:00
cpk_end: 2013-10-29 17:17:00
cpk_number: 1055
date: *id001
tags:
- medewerkers
- studenten
title: 'U: / home server bundle in de problemen'
url: cpk/1055
---
De server kreeg om onduidelijke redenen een hoge load en verleende geen
diensten meer aan gebruikers. Na een herstart was het probleem
verdwenen.
