---
cpk_affected: almost all computers within FNWI
cpk_begin: &id001 2014-09-20 10:45:00
cpk_end: 2014-09-20 14:04:00
cpk_number: 1107
date: *id001
tags:
- medewerkers
- studenten
title: DNS resolver problem
url: cpk/1107
---
The server that acts within FNWI as first [DNS
resolver](http://en.wikipedia.org/wiki/Domain_Name_System#DNS_resolvers)
crashed. This made the network virtually unusable for a lot of computers
in FNWI. Only after a reboot of the server, the problem was solved.
Measures to reduce the inconvenience of such a crash and to make such a
crash less likely, have been partly taken and are partly in preparation.
