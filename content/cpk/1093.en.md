---
cpk_affected: Users of the poster printer "kamerbreed"
cpk_begin: &id001 2014-04-21 00:00:00
cpk_end: 2014-05-16 00:00:00
cpk_number: 1093
date: *id001
tags:
- medewerkers
- studenten
title: Poster printer "kamerbreed" broken
url: cpk/1093
---
The printer has to be repaired. Spare parts have been ordered. We hope
to have the printer operational again mid-May.
