---
cpk_affected: science gebruikers
cpk_begin: &id001 2017-11-22 07:30:00
cpk_end: 2017-11-22 09:20:00
cpk_number: 1221
date: *id001
tags:
- medewerkers
- studenten
title: Homedirectory niet beschikbaar
url: cpk/1221
---
Door een package update van samba is er een probleem ontstaan met de
smbd service, deze wilde niet meer opstarten. Het is opgelost door een
reboot (home1) of handmatig opstarten van smbd (home2).
