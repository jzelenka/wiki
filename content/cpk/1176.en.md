---
cpk_affected: Users of license services hosted on lepelaar.
cpk_begin: &id001 2016-05-25 06:30:00
cpk_end: 2016-05-25 09:00:00
cpk_number: 1176
date: *id001
tags:
- medewerkers
- studenten
title: License server (lepelaar) reboot problem
url: cpk/1176
---
Matlab (until 08:45), Origin (09:00), Mestrenova (9:20), Mathematica
(9:30) and Orcad (11:20) service not started. Cause yet unknown.
