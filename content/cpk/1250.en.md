---
cpk_affected: Science mail receivers
cpk_begin: &id001 2019-07-01 10:33:00
cpk_end: 2019-07-02 12:28:00
cpk_number: 1250
date: *id001
title: Mail problems with newly introduced incoming mail server
url: cpk/1250
---
A new receiving mailserver was introduced July 1. This server has the
most recent version of Ubuntu and other software and settings that are
more conforming to [the directives (in
Dutch)](https://www.forumstandaardisatie.nl/open-standaarden/lijst/verplicht?f%5B0%5D=field_keywords%3A68).
After the introduction, problems appeared with receiving mail from
Microsoft Office 365 Exchange Online (mail.protection.outlook.com). An
even bigger problem was that some accepted mails were not forwarded, but
bounced to the sender. Because that problem couldn’t be fixed
immediately, the newly introduced server has been shut down on July 2.
At the end of August, C&CZ will test the probable solution. If the test
succeeds, the new incoming server will be put into production.
