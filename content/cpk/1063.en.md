---
cpk_affected: Users with homedirectory server "bundle" (as can be seen on `<http://DIY.science.ru.nl>`)
cpk_begin: &id001 2013-12-23 07:30:00
cpk_end: 2013-12-23 09:00:00
cpk_number: 1063
date: *id001
tags:
- medewerkers
- studenten
title: Postponed replacement of home-server "bundle"
url: cpk/1063
---
The replacement of the old home server “bundle” has been postponed and
will now guaranteed take place on Monday morning December 23. Because
the data have been synchronized with the new server, there will not be
much downtime. The new server should be very dependable: hardware
[RAID-6](http://en.wikipedia.org/wiki/RAID#RAID_6_replacing_RAID_5_in_enterprise_environments),
double processors and power supplies and a 5-year support contract from
the supplier. The performance has improved, e.g. by using hardware RAID
with a 1 GB [write cache with battery
backup](http://serverfault.com/questions/65096/battery-backed-write-cache).
