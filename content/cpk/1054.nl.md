---
cpk_affected: alle gebruikers die Science-mail wilden lezen
cpk_begin: &id001 2013-10-28 13:35:00
cpk_end: 2013-10-28 14:08:00
cpk_number: 1054
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: Nogmaals IMAP mailserver probleem
url: cpk/1054
---
Net als 3 dagen geleden werd de IMAP server na de middag overbelast. Pas
na een reboot was de service weer beschikbaar. We verdenken nu het maken
van een backup-snapshot en hebben het snapshot onder werktijd uitgezet.
