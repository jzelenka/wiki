---
cpk_affected: 'all users wanting to read Science mail '
cpk_begin: &id001 2016-02-15 11:20:00
cpk_end: 2016-02-15 13:45:00
cpk_number: 1164
date: *id001
tags:
- studenten
- medewerkers
- docenten
title: IMAP mail server problem
url: cpk/1164
---
For unknown reasons, the IMAP mail server was unavailable. Mail services
were halted after some time because of logging activity. As a temporary
workaround logging is now performed using UDP instead of TCP.
