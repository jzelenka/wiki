---
cpk_affected: GitLab gebruikers
cpk_begin: &id001 2016-05-02 21:30:00
cpk_end: 2016-05-03 10:15:00
cpk_number: 1179
date: *id001
tags:
- medewerkers
- studenten
title: Gitlab tijdelijk niet beschikbaar
url: cpk/1179
---
[GitLab](/nl/howto/gitlab/) zal deze nacht niet beschikbaar zijn vanwege
een [major security
update](https://about.gitlab.com/2016/04/28/gitlab-major-security-update-for-cve-2016-4340/)
die rond middernacht door gitlab.com beschikbaar zal worden gesteld. We
zullen de update morgenochtend uitvoeren. Daarna zal
gitlab.science.ru.nl weer beschikbaar zijn.
