---
cpk_affected: Eduroam wifi users
cpk_begin: &id001 2017-05-22 22:00:00
cpk_end: 2017-05-23 02:00:00
cpk_number: 1207
date: *id001
tags:
- medewerkers
- studenten
title: Eduroam wifi behind firewall
url: cpk/1207
---
Due to maintenance the wifi network on campus (Eduroam) will not be
available for twenty minutes on Monday night between 10 pm and 8 pm.
During those twenty minutes your mobile device appears to be connected
with the wifi network but you will notice there is no internet
connection. After this interruption the connection will be established
automatically. During the maintenance period additional short
interruptions may occur.

Monday evening the wifinetwork will be placed behind the new firewalls
of the Radboud University. When this is done, users of Eduroam on our
campus are no longer directly reachable from the Internet. We are doing
this to protect wifi users against viruses like the Wannacry virus that
was used for the worldwide ransomware attack last week.
