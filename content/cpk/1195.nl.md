---
cpk_affected: medewerkers en studenten bij FNWI
cpk_begin: &id001 2017-01-30 07:00:00
cpk_end: 2017-01-30 07:30:00
cpk_number: 1195
date: *id001
tags:
- medewerkers
- studenten
title: Onderhoud netwerk Serverruimte FNWI in Huygensgebouw
url: cpk/1195
---
De centrale netwerkswitch in de serverruimte van FNWI wordt herstart ivm
een software upgrade. Vele servers en services zullen gedurende ca 20
minuten niet beschikbaar zijn.
