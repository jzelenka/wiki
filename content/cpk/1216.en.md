---
cpk_affected: Science users on windows clients
cpk_begin: &id001 2017-09-22 07:20:00
cpk_end: 2017-09-22 09:08:00
cpk_number: 1216
date: *id001
tags:
- medewerkers
- studenten
title: Homedirectories unavailable
url: cpk/1216
---
This morning, the homeservers were automatically upgraded with normal
updates, after which the windows file servicewas not restarted. The
problem was resolved by restarting the machines.
