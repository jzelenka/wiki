---
cpk_affected: Gebruikers van de printto/printsmb/phpMyAdmin service
cpk_begin: &id001 2014-04-10 12:54:00
cpk_end: 2014-04-10 13:15:00
cpk_number: 1079
date: *id001
tags:
- medewerkers
- studenten
title: Print/phpMyAdmin server weer in de problemen
url: cpk/1079
---
Net als twee weken geleden, reageerde de server reageerde niet meer,
oorzaak onbekend. Na een herstart was het probleem verdwenen. We gaan
met de leverancier nadenken wat er vervangen zou moeten worden om dit in
de toekomst te voorkomen.
