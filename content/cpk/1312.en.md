---
title: Wing 4 Huygens network maintenance Monday January 23 19:00-23:00
author: petervc
cpk_number: 1312
cpk_begin: 2023-01-23 19:00:00
cpk_end: 2023-01-23 21:30:00
cpk_affected: Wired/wireless users in and around wing 4 Huygens, all floors
date: 2023-01-22
tags: []
url: cpk/1312
---
ILS Connectivity announced that network maintenance will be carried out next Monday evening
that will cause short disturbances of network connectivity for wired and wireless systems in and near wing 4 of the Huygens building.
For wired network outlets, one can check whether they will be affected by looking at
the outlet number (starting with 108- for affected systems) or visiting [FNWI ethergids](https://cncz.science.ru.nl/en/howto/netwerk-ethergids/).
