---
cpk_affected: Science mail gebruikers die mail wilden versturen
cpk_begin: &id001 2021-01-22 10:00:00
cpk_end: 2021-01-22 10:30:00
cpk_number: 1271
date: *id001
title: Science smtp-service tijdelijk niet beschikbaar
url: cpk/1271
---
<itemTags>medewerkers, studenten</itemTags>


Een configuratiewijziging maakte onbedoeld de smtp-service onbruikbaar.
Toen we dat opmerkten is dat direct gecorrigeerd.
