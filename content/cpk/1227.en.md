---
cpk_affected: Users of imappsecr, imappdir and astropresentie shares
cpk_begin: &id001 2018-03-06 08:40:00
cpk_end: 2018-03-06 14:00:00
cpk_number: 1227
date: *id001
tags:
- medewerkers
title: Gijzelsoftwware op IMAPP mappen
url: cpk/1227
---
Due to an accidental opening of an e-mail attachment (called
Resume.doc), which contained a macro-based ransomware, all the files and
directories on the mentioned shares were either removed or encrypted and
a message was left on the infected computer to contact a certain URL to
get the files back. The files were restored from backup and the PC wiped
clean and re-installed. NB. The f-secure software did not recognise this
malware yet at the time.
