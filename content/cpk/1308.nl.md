---
title: Accounts op Linux loginservers beperkt tot recente gebruikers
author: petervc
cpk_number: 1308
cpk_begin: 2022-12-09 12:12:00
cpk_end: 2022-12-09 12:12:00
cpk_affected: 
date: 2022-12-09
tags: []
url: cpk/1308
---
Onlangs zijn enkele Science accounts gehackt nadat de gebruikers
in een phishing mail getrapt zijn. Deze accounts werden daarna
misbruikt door internetcriminelen om mail te sturen vanaf onze [Linux
loginservers](/nl/howto/hardware-servers/#linux-loginservers).
Daarom hebben we de mogelijkheid om in te loggen op de Linux loginservers
weggenomen van alle Science accounts die hier toch geen gebruik van
maakten. Neem [contact](/nl/howto/contact/) op met C&CZ als u op de
Linux loginservers in wilt kunnen loggen.
