---
cpk_affected: Gebruikers van de printto/printsmb/phpMyAdmin service
cpk_begin: &id001 2014-05-07 12:49:00
cpk_end: 2014-05-07 13:14:00
cpk_number: 1088
date: *id001
tags:
- medewerkers
- studenten
title: Nogmaals Print/phpMyAdmin server problemen
url: cpk/1088
---
Net als gisteren, was er een herstart nodig om de server tot leven te
brengen. We gaan onderzoeken of we met nieuwere versies software (Samba,
Ubuntu, …) dit probleem kunnen verhelpen.
