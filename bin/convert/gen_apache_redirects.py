import slug
import os

BASE_DIR = os.path.join(os.path.dirname(__file__), "../../")
os.chdir(BASE_DIR)

with open('bin/convert/wikilinks') as file:
    lines = file.readlines()
    links = [line.rstrip() for line in lines]

for link in links:
    filename = slug.slug(link.replace("_", "-"))
    # check of het news of cpk is geworden
    if link == "/Storingen_archief" or link == '/Recente_Storingen':
        newlink = 'en/cpk/'
    elif link == '/Nieuws' or link == "Nieuws_archief":
        newlink = 'en/news'
    else:
        # check of er een (nl)md file is in content/wiki
        if os.path.isfile(f'content/wiki/{ filename }.nl.md'):
            newlink=f"en/wiki/{ filename }"
        else:
            newlink="en/"

    print(f"Redirect 302 {link:<30}\thttps://cncz.science.ru.nl/{newlink}")
