// convert [nederlands][english] to just 'nederlands' or 'english'
// takes an argument to output the first (hopefully) Dutch  or English.
// I.e. go run blockfix.go 1 <my-file-to-be-processed.md>
// All output is written to standard output, except errors.

package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
)

const (
	sStart = iota
	sDutch
	sDutchEnd
	sEnglish
	sEnglishEnd
)

type (
	State = struct {
		state int
		start int // index of [
		end   int // index of last ]
		split int // index of ] in the middle
	}
)

func main() {
	flag.Parse()

	if flag.NArg() != 2 {
		log.Fatal("need two arguments")
	}
	which := flag.Arg(0)
	filename := flag.Arg(1)

	buf, err := os.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}

	s := State{sStart, 0, 0, 0}
	// save them for conversion
	nl := [][]byte{}
	en := [][]byte{}
	all := [][]byte{}
	for i, r := range buf {
		switch s.state {
		case sStart:
			if r == '[' {
				s = State{state: sDutch, start: i}
			}
			continue

		case sDutch:
			if r == ']' {
				s.state, s.split = sDutchEnd, i
			}
			continue

		case sDutchEnd:
			if r == '[' {
				s.state = sEnglish
				continue
			}
			// Any other character, means reset.
			s.state = sStart
			continue

		case sEnglish:
			if r == ']' {
				s.state, s.end = sEnglishEnd, i

				switch which {
				case "1":
					nl = append(nl, buf[s.start+1:s.split])
					all = append(all, buf[s.start:s.end+1])
				case "2":
					en = append(en, buf[s.split+2:s.end])
					all = append(all, buf[s.start:s.end+1])
				}

				s = State{sStart, 0, 0, 0}
				continue
			}
		}
	}
	switch which {
	case "1":
		for i := range nl {
			fmt.Fprintf(os.Stderr, "%s <- %s\n", nl[i], all[i])
			buf = bytes.Replace(buf, all[i], nl[i], 1)
		}
	case "2":
		for i := range en {
			fmt.Fprintf(os.Stderr, "%s <- %s\n", en[i], all[i])
			buf = bytes.Replace(buf, all[i], en[i], 1)
		}
	}
	fmt.Printf("%s", buf)
}
