import datetime
import glob
import os
import io
import re
from xml.etree import ElementTree as ET

import pypandoc
import slug
import yaml
from dateparser import parse

outputdir = "content"

# estimated number of cpk's
numtitles = 1260

BASE_DIR = os.path.join(os.path.dirname(__file__), "../../")
os.chdir(BASE_DIR)

# determine wiki export file
list_of_files = glob.glob(f"source/Cncz-*.xml")
latest_file = max(list_of_files, key=os.path.getmtime)

print(f"Opening {latest_file}")
# open xml file
it = ET.iterparse(latest_file)
for _, el in it:
    el.tag = el.tag.split("}", 1)[1]  # strip all namespaces
root = it.root
# pages that need to be split up
blog = {}
blog["storingen-archief"] = "cpk"
blog["recente-storingen"] = "cpk"
blog["nieuws"] = "news"
blog["nieuws-archief"] = "news"

# regexes
title_regex = (
    r"(?P<h>==+)\s?(\[(?P<title_nl>[\w -?\n]+)\])(\[(?P<title_en>[\w -?\n]+)\])\s?==+"
)

title_regex = r"(?P<h>=+)\s?\[(?P<nl>.*)\]\[(?P<en>.*)\]\s?(=+)"
title_subst = "\g<h>[nl]\g<nl>[/nl][en]\g<en>[/en]\g<h>"

nlblock_regex = r"\[nl\]([\S\s]+?)\[\/nl\]"
enblock_regex = r"\[en\]([\S\s]+?)\[\/en\]"

langtag_regex = r"\[\/?(nl|en)\]"
category_regex = r"\[\[Category:(?P<category>.*)\]\]"
tag_regex = r"<itemTags>(?P<tags>.*)<\/itemTags>"

# welke rommel achteraf opruimen
to_delete = []
for wikipage in blog:
    to_delete.append(f"wiki/{ wikipage }")
to_delete.append("news/0000-00-00_")
to_delete.append(f"cpk/1258")
to_delete.append(f"cpk/1259")
to_delete.append("wiki/wiki-aanpassen")
to_delete.append("wiki/geinstalleerde-mediawiki-extensies")
to_delete.append("wiki/acopyright")


def parsedatetime(string):
    try:
        return parse(
            string,
            settings={
                "TO_TIMEZONE": "Europe/Amsterdam",
                "RETURN_AS_TIMEZONE_AWARE": False,
            },
        )
    except:
        return datetime.datetime(2000, 1, 1)


def concat_codeblocks(text):
    codelineregex = "^`(.*)`"
    codelinesub = "\\1"
    prev_n = 0
    out = []
    for line in text.split("\n"):
        line = line.strip()
        line, n = re.subn(codelineregex, codelinesub, line)

        if n != prev_n:
            out.append("```")
        out.append(line.strip())

        prev_n = n
    return "\n".join(out)


# loop over pages
for wikipage in root:
    # er zit nog een <siteinfo> sectie tussen de pages. deze skippen
    if wikipage.tag != "page":
        continue

    # collect some data
    page = {}
    page["id"] = wikipage.find("id").text
    page["wiki_title"] = wikipage.find("title").text
    page["author"] = wikipage.find("revision/contributor/username").text.lower()
    page["timestamp"] = wikipage.find("revision/timestamp").text
    page["rawtext"] = wikipage.find("revision/text").text
    page["lines"] = []

    # print progress
    print(f"title: {page['wiki_title']}")

    # skip redirect-pages
    try:
        redirect = wikipage.find("redirect")
        a = redirect.attrib
    except:
        pass
    else:
        print("Skip: Page is a redirect")
        continue

    # skip empty pages
    if not page["rawtext"]:
        print("Skip: Page is empty")
        continue

    categories = re.findall(category_regex, page["rawtext"], re.MULTILINE)
    # er zitten |s strings bij, waarbij de | ergens van stond, filter alles achter | (en de pipe zelf eruit.
    categories = [re.sub("\|.*$", "", category) for category in categories]

    # insert lang-tags in titles
    page["rawtext"] = re.sub(title_regex, title_subst, page["rawtext"])

    # remove category links
    page["rawtext"] = re.sub(category_regex, "", page["rawtext"], 0, flags=re.MULTILINE)

    # remove (CET)/(CEST) text
    page["rawtext"] = re.sub(r"\(CEST\)", "", page["rawtext"], 0, flags=re.MULTILINE)
    page["rawtext"] = re.sub(r"\(CET\)", "", page["rawtext"], 0, flags=re.MULTILINE)

    # voordat wikimedia naar md wordt geconverteerd, moeten alle [nl][en] blokjes eruit
    # dus eerst spliten in EN en NL
    page["document_en"] = re.sub(
        nlblock_regex, r"", page["rawtext"], count=0, flags=re.MULTILINE
    )
    page["document_nl"] = re.sub(
        enblock_regex, r"", page["rawtext"], count=0, flags=re.MULTILINE
    )

    # nu de lang tags strippen
    page["document_en"] = re.sub(
        langtag_regex, "", page["document_en"], count=0, flags=re.MULTILINE
    )
    page["document_nl"] = re.sub(
        langtag_regex, "", page["document_nl"], count=0, flags=re.MULTILINE
    )

    # convert to markdown
    page["markdown_en"] = pypandoc.convert_text(
        source=page["document_en"], format="mediawiki", to="gfm"
    )
    page["markdown_nl"] = pypandoc.convert_text(
        source=page["document_nl"], format="mediawiki", to="gfm"
    )

    # determine output filename
    page["slug"] = slug.slug(page["wiki_title"])
    page["outputfile_en"] = f"{outputdir}/howto/{page['slug']}.en.md"
    page["outputfile_nl"] = f"{outputdir}/howto/{page['slug']}.nl.md"

    # front-matter, see https://gohugo.io/content-management/front-matter/
    # gooi categories in tags - zodat we 1 manier van 'taggen' hebben.
    frontmatter = {
        "title": page["wiki_title"],
        "date": page["timestamp"],
        "author": page["author"],
        "tags": [category.lower() for category in categories],
        "keywords": [],
        # "slug": page["slug"], # slug niet perse nodig, hugo maakt zelf al een slug op basis van de titel
        "lang": "en",
        "wiki_id": page["id"],
    }
    # \[ en \] vervangen door [ en  ]
    page["markdown_en"] = page["markdown_en"].replace("\[", "[").replace("\]", "]")
    page["markdown_nl"] = page["markdown_nl"].replace("\[", "[").replace("\]", "]")

    with io.open(page["outputfile_en"], "w", encoding="utf8") as outfile:
        outfile.write("---\n")
        yaml.dump(frontmatter, outfile, encoding="utf-8")
        outfile.write("---\n")
        outfile.write(page["markdown_en"].strip())

    # nu de nederlandstalige pagina
    frontmatter["lang"] = "nl"

    with io.open(page["outputfile_nl"], "w", encoding="utf8") as outfile:
        outfile.write("---\n")
        yaml.dump(frontmatter, outfile, encoding="utf-8")
        outfile.write("---\n")
        outfile.write(page["markdown_nl"].strip())
    print(page["slug"])
    # split some pages into blog-fragments

    if page["slug"] not in blog:
        continue

    ## split some pages into a blog-items
    blogtype = blog[page["slug"]]
    # process blog items
    # extract info from converted markdown-format
    #
    post_title_regex = r"^##+\s?(?P<title>.*)"
    post_tags_regex = r"<itemTags>(?P<tags>[\w,]*)<\/itemTags>"
    post_author_date_regex = (
        r"\[.*\]\(Gebruiker:(?P<gebruiker>\w*)..wikilink.\)\ (?P<datetime>.*)"
    )
    post_cpk_begin_regex = r"`\s*Begin\s*:\s(?P<begin>[\w\-\s:]*)"
    post_cpk_end_regex = r"`\s*Ei?nde?\s*:\s*(?P<end>[0-9\-\s:]+)"
    post_cpk_affected_regex = r"`\s*(Getroffen|Affected)\s*:\s?(?P<affected>.*)`"

    titleslugs = []
    for lang in ["en", "nl"]:
        post_title = ""
        post_lines = []
        post = {}
        titlecounter = 0
        for line in page[f"markdown_{lang}"].splitlines():
            line = line.replace("\xa0", " ").strip()
            post_title_match = re.match(post_title_regex, line)
            post_tags_match = re.match(post_tags_regex, line)
            post_author_date_match = re.match(post_author_date_regex, line)
            post_cpk_begin_match = re.match(post_cpk_begin_regex, line)
            post_cpk_end_match = re.match(post_cpk_end_regex, line)
            post_cpk_affected_match = re.match(post_cpk_affected_regex, line)

            # get title
            # if a title is found, process info of previous entry
            if post_title_match:
                if post_title_match["title"] == "":
                    continue

                if lang == "en":
                    # alleen Engelse slugs aanvullen aan array
                    titleslug = slug.slug(post_title)
                    titleslugs.append(titleslug)
                else:
                    # bij het processen van de nl-berichten ze er weer uit halen
                    titleslug = titleslugs[titlecounter]

                titlecounter += 1
                # print(f"{titlecounter} {lang}\t{titleslug}\t{post_title}")
                post["title"] = post_title
                post["content"] = "\n".join(post_lines)

                ## write file

                datestring = "0000-00-00"
                if "date" in post and isinstance(post["date"], datetime.datetime):
                    datestring = post["date"].strftime("%Y-%m-%d")
                elif "cpk_begin" in post and isinstance(
                    post["cpk_begin"], datetime.datetime
                ):
                    post["date"] = post["cpk_begin"]
                    datestring = post["cpk_begin"].strftime("%Y-%m-%d")

                frontmatter = {}
                for key in ["title", "author", "date", "cpk_begin", "cpk_end"]:
                    if key in post and post[key] is not None:
                        frontmatter[key] = post[key]

                # Behandel tags apart, tis een typle in een list.
                if "tags" in post:
                    frontmatter["tags"] = [t for t in post["tags"]][0]

                outputfile = (
                    f"{outputdir}/{blogtype}/{datestring}_{titleslug}.{lang}.md"
                )
                if blogtype == "cpk":
                    # gebruik nummertjes voor cpks
                    cpk_number = numtitles - titlecounter
                    outputfile = f"{outputdir}/{blogtype}/{ cpk_number }.{lang}.md"

                    post["content"] = post["content"].strip()
                    if "cpk_affected" in post:
                        frontmatter["cpk_affected"] = post["cpk_affected"]
                    # geef cpk's nog een eigen nummer
                    frontmatter["cpk_number"] = cpk_number
                    frontmatter["url"] = "cpk/" + str(cpk_number)
                else:
                    # news
                    outputfile = (
                        f"{outputdir}/{blogtype}/{datestring}_{titleslug}.{lang}.md"
                    )

                with io.open(outputfile, "w", encoding="utf8") as outfile:
                    outfile.write("---\n")
                    yaml.dump(frontmatter, outfile, encoding="utf-8")
                    outfile.write("---\n")
                    outfile.write(post["content"])
                    outfile.write("\n")

                post_lines = []
                post = {}

                post_title = post_title_match["title"]

            elif post_tags_match:
                post["tags"] = (
                    [tag.strip() for tag in post_tags_match["tags"].lower().split(",")],
                )
            elif post_author_date_match:
                post["author"] = post_author_date_match["gebruiker"].lower().strip()
                # fix date string
                post["date"] = parsedatetime(post_author_date_match["datetime"])

            elif post_cpk_begin_match:
                post["cpk_begin"] = parsedatetime(post_cpk_begin_match["begin"])
            elif post_cpk_end_match:
                post["cpk_end"] = parsedatetime(post_cpk_end_match["end"])
            elif post_cpk_affected_match:
                post["cpk_affected"] = post_cpk_affected_match["affected"]
            else:
                # regular content line
                post_lines.append(line)

for document in to_delete:
    print(document)
    list_of_files = glob.glob(f"{ outputdir }/{ document }.*.md")
    print(list_of_files)
    for file in list_of_files:
        print(f"removing { file }")
        os.remove(file)
