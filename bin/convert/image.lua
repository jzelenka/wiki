local text = pandoc.text

-- See https://pandoc.org/lua-filters.html#type-link .
function Image(el)
    -- [Uitgaande server (SMTP) instellingen](Thunderbird-NL-smtp.jpg%20%22Uitgaande%20server%20(SMTP)%20instellingen%22)
    -- naar:
    -- {{< figure src="/media/spf13.jpg" title="Steve Francia" caption="..." >}}

    i, j = string.find(el.src, "%%20%%22") -- " ..." - url encoded title seperator
    if i ~= nil then
        title = string.sub(el.src, i)
        title = string.gsub(title, "%%20%%22", "")
        title = string.gsub(title, "%%22", "")
        title = string.gsub(title, "%%20", " ")
        el.title = title
        el.src = string.sub(el.src, 1, i-1)
    end

    el.title = string.gsub(el.title, "fig:", "")
    el.title = string.gsub(el.title, '"', "'") -- " -> '

    src = "/img/old/" .. el.src
    src = text.lower(src)

    s = pandoc.Str()
    s.text = '{{< figure src="' .. src .. '"' .. ' title="' .. el.title .. '" >}}'
    return s
end
