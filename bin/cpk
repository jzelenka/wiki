#!/bin/bash

FMT=$(which fmt)
if [ -z "$FMT" ]; then
	echo "fmt command required" >&2
	exit 1
fi

cpkdir=$(dirname $0)/../content/cpk

catcpk() {
	echo
	cpkhead "$1"
	echo
	cpkbody "$1"
	echo
}

compose_stdout() {
	echo "$1"
	echo
	cat "$2"
}

compose_thunderbird() {
	subject="$1"
	message="$2"
	thunderbird -compose "to=cpk@science.ru.nl,subject=${subject},message=${message},format=text"
	# give thunderbird the chance to read the message
	sleep 2
}

compose_clipboard() {
	subject="$1"
	message="$2"

	if [ ! -x /usr/bin/xsel ]; then
		echo "ERROR: could not find xsel"
		exit 1
	fi

	(
		echo $subject
		cat $message
	) | xsel --clipboard

	# optionally use boxes
	[ -x /usr/bin/boxes ] && boxes="boxes -p a1 -d peek" || boxes=cat
	echo "CPK message is in your clipboard!" | $boxes >&2
}


cpktitle() {
	grep '^title:' $1 | sed 's/^title: //'
}

cpksubject() { # cpknumber $cpksub1 $cpksub2
	# TODO: output should not contain any double quotes
	echo "CPK#"$1 $(cpktitle $2) " | "$(cpktitle $3)
}

cpkhead() {
	# print and format cpk_begin, _end and _affected
	egrep '^cpk_begin|^cpk_end|^cpk_affected' "$1" |
		awk '
		{
    		split($0, f, ":");
    		sub(/^([^:]+:)/, "", $0);
    		printf " %-10s:%s\n", f[1], $0
  		}'
	echo
}

cpkbody() {
	# remove line 1 "---", and the metadata
	# replace the internal links with the text of the link
	sed -r -e '1d' -e '1,/---/d' "$1" \
		-e 's/\[(.*)\]/\1 /g' | $FMT
}

askauthor() {
	echo -n "Do you want to use \"$(whoami)\" as author-id? (Y/N)"
	read ANSWER
	case "$ANSWER" in
	Y | J | y | yes | ja | Yes | Ja)
		AUTHOR=$(whoami)
		echo "AUTHOR=$AUTHOR" >>$contentdir/../.env
		;;
	*)
		echo "Please create an \".env\" with the variable AUTHOR in it, see README.md" >&2
		exit 1
		;;
	esac
}

case $(basename $0) in
firstcpk)
	ans=$(ls $cpkdir | sort -n -r | tail -1 | cut -f1 -d.)
	;;

lastcpk)
	ans=$(ls $cpkdir | sort -n | tail -1 | cut -f1 -d.)
	;;

cpk)
	ans=$(ls $cpkdir | sort -n | tail -1 | cut -f1 -d.)
	ans=$((${ans} + 1))
	;;

mailcpk)
	cpknumber=$1
	if [ -z "$cpknumber" ]; then
		# if number is not specified, use the last cpk
		cpknumber=$($(dirname $0)/lastcpk)
	fi

	en=$cpkdir/$cpknumber.en.md
	nl=$cpkdir/$cpknumber.nl.md

	if [ ! -f $nl -o ! -f $en ]; then
		echo ERROR: could not find $en or $nl >&2
		exit 1
	fi
	tmpfile=$(mktemp)
	(
		echo English follows Dutch
		catcpk $nl |
			sed 's/cpk_begin/Begin   /' |
			sed 's/cpk_end/Einde /' |
			sed 's/cpk_affected/Getroffen/'
		echo
		echo bron: https://cncz.science.ru.nl/nl/cpk/$cpknumber
		echo "====================="
		catcpk $en |
			sed 's/cpk_begin/Begin  /' |
			sed 's/cpk_end/End  /' |
			sed 's/cpk_affected/Affected/'
		echo
		echo source: https://cncz.science.ru.nl/en/cpk/$cpknumber
	) > $tmpfile

	if [ -f $(dirname $0)/../.env ]; then
		source $(dirname $0)/../.env
	fi

	if [ -z "$MAILER" ]; then
		echo "MAILER not found in .env"

		if [ -f $(dirname $0)/../.mailer ]; then
			MAILER=$(<$(dirname $0)/../.mailer)
			echo "MAILER=$MAILER" >> $(dirname $0)/../.env
			rm $(dirname $0)/../.mailer
		fi
	fi

	if [ -z "$MAILER" ]; then
		MAILER=stdout
	fi

	compose_$MAILER "$(cpksubject $cpknumber $en $nl)" $tmpfile

	rm $tmpfile
	exit 0
	;;
esac

if [[ -z ${ans} ]]; then
	echo "Geen $(basename $0) gevonden" >&2
	exit 1
fi

echo $ans
